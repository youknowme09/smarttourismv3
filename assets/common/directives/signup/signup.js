/**
 * Created by ansonliu on 2015/5/7.
 */


angular.module("ng.directive")
  .controller('signupCtrl', function ($scope, $state, $mdDialog) {

    function signup ($scope, $mdDialog, $timeout) {
      $scope.step = 1;
      $scope.gender = -1;
      $scope.years = [];
      for (var i = 1900; i <= 2015; i++) {
        $scope.years.push(i);
      }
      $scope.cancel = function() {
        $mdDialog.cancel();
      };
      $scope.setStep = function(step) {
        $scope.step = step;
      };
      $scope.signup = function(answer) {
        $scope.onSignup = true;
        $timeout(function () {
          $scope.onSignup = false;
        }, 1000);
      };
      $scope.goToLogin = function () {
        goToLogin();
      };
    }

    $scope.openSignupModal=function(){
      $mdDialog.show({
        controller: signup,
        templateUrl: '../assets/common/partials/signupMadal.html'
      })
        .then(function(answer) {
          $scope.alert = 'You said the information was "' + answer + '".';
        }, function() {
          $scope.alert = 'You cancelled the dialog.';
        });
    }
  })
  .directive('signup', function(){
    return {
      restrict: 'EA',
      controller: 'signupCtrl',
      template:'',
      link: function ($scope, element, iAttrs, controller) {
        element.bind('click', function() {
          $scope.openSignupModal();
        });
      }
    };
  });

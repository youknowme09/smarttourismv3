angular.module("ng.directive")
  .controller('hotCollectionCtrl', function ($scope) {


    $scope.collectionThemeList = [
      {
        "title": "自然生態",
        "collection": 237,
        "description" : "華人的飲食文化淵遠流長，舉世皆知，雖然今天在國際上各大都市均可吃到中國菜，但是行家們仍一致認為唯有在臺灣才能品嚐到各式各樣道地的中國菜餚。",
        "images": [
          {"url": "../public/images/collections/collection-1.jpg"}
        ]
      },
      {
        "title": "宗教信仰",
        "collection": 237,
        "description" : "華人的飲食文化淵遠流長，舉世皆知，雖然今天在國際上各大都市均可吃到中國菜，但是行家們仍一致認為唯有在臺灣才能品嚐到各式各樣道地的中國菜餚。",
        "images": [
          {"url": "../public/images/collections/collection-2.jpg"}
        ]
      },
      {
        "title": "歷史古蹟",
        "collection": 237,
        "description" : "華人的飲食文化淵遠流長，舉世皆知，雖然今天在國際上各大都市均可吃到中國菜，但是行家們仍一致認為唯有在臺灣才能品嚐到各式各樣道地的中國菜餚。",
        "images": [
          {"url": "../public/images/collections/collection-3.jpg"}
        ]
      },
      {
        "title": "養生溫泉",
        "collection": 237,
        "description" : "華人的飲食文化淵遠流長，舉世皆知，雖然今天在國際上各大都市均可吃到中國菜，但是行家們仍一致認為唯有在臺灣才能品嚐到各式各樣道地的中國菜餚。",
        "images": [
          {"url": "../public/images/collections/collection-4.jpg"}
        ]
      },
      {
        "title": "農村體驗",
        "collection": 237,
        "description" : "華人的飲食文化淵遠流長，舉世皆知，雖然今天在國際上各大都市均可吃到中國菜，但是行家們仍一致認為唯有在臺灣才能品嚐到各式各樣道地的中國菜餚。",
        "images": [
          {"url": "../public/images/collections/collection-5.jpg"}
        ]
      },
      {
        "title": "親子同樂",
        "collection": 237,
        "description" : "華人的飲食文化淵遠流長，舉世皆知，雖然今天在國際上各大都市均可吃到中國菜，但是行家們仍一致認為唯有在臺灣才能品嚐到各式各樣道地的中國菜餚。",
        "images": [
          {"url": "../public/images/collections/collection-6.jpg"}
        ]
      },
      {
        "title": "夜生活活動",
        "collection": 237,
        "description" : "華人的飲食文化淵遠流長，舉世皆知，雖然今天在國際上各大都市均可吃到中國菜，但是行家們仍一致認為唯有在臺灣才能品嚐到各式各樣道地的中國菜餚。",
        "images": [
          {"url": "../public/images/collections/collection-7.jpg"}
        ]
      },
      {
        "title": "人文創意",
        "collection": 237,
        "description" : "華人的飲食文化淵遠流長，舉世皆知，雖然今天在國際上各大都市均可吃到中國菜，但是行家們仍一致認為唯有在臺灣才能品嚐到各式各樣道地的中國菜餚。",
        "images": [
          {"url": "../public/images/collections/collection-8.jpg"}
        ]
      },
      {
        "title": "城市夜景",
        "collection": 237,
        "description" : "華人的飲食文化淵遠流長，舉世皆知，雖然今天在國際上各大都市均可吃到中國菜，但是行家們仍一致認為唯有在臺灣才能品嚐到各式各樣道地的中國菜餚。",
        "images": [
          {"url": "../public/images/collections/collection-9.png"}
        ]
      },
      {
        "title": "離島冒險",
        "collection": 237,
        "description" : "華人的飲食文化淵遠流長，舉世皆知，雖然今天在國際上各大都市均可吃到中國菜，但是行家們仍一致認為唯有在臺灣才能品嚐到各式各樣道地的中國菜餚。",
        "images": [
          {"url": "../public/images/collections/collection-10.png"}
        ]
      }
    ]
  })
  .directive('hotCollection', function(){
    return {
      restrict: 'E',
      controller: 'hotCollectionCtrl',
      templateUrl: '../assets/common/directives/hotCollection/hotCollection.html',
      replace: true,
      link: function ($scope, iElm, iAttrs, controller) {

      }
    };
  });

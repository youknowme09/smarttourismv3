angular.module("ng.directive").directive('datepicker', function(){
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function(scope, element, attrs, modelCtrl) {
      $(function() {
        element.datepicker({
          dateFormat: 'yy-mm-dd',
          numberOfMonths: 2,
          showOtherMonths: true,
          selectOtherMonths: false,
          firstDay: 1,
          monthNames: [ "1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月" ],
          dayNamesMin: [ "日", "一", "二", "三", "四", "五", "六" ],
          prevText: '',
          nextText: '',
        });
      });
    }
  };
});
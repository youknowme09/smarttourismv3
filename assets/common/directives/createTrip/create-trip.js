/**
 * Created by ansonliu on 2015/5/7.
 */


angular.module("ng.directive")
  .controller('createTripCtrl', function ($scope, $state, $mdDialog) {

    function createTrip($scope, $mdDialog, $timeout){

      $scope.step = 1;
      $scope.setting = {
        tripDensity: 2,
        destinations: []
      };
      $scope.days = [];
      for (var i = 1; i <= 30; i++) {
        $scope.days.push(i);
      }
      $scope.cancel = function() {
        $mdDialog.cancel();
      };
      $scope.backStep = function() {
        $scope.step = $scope.step - 1;
        if ($scope.step < 1) {
          $scope.step = 1;
        }
      };
      $scope.setStep = function(step) {
        $scope.step = step;
      };
      $scope.createTrip = function(answer) {
        $scope.onCreateTrip = true;
        $timeout(function () {
          $scope.onCreateTrip = false;
          $scope.createTripSuccess = true;
        }, 1000);
      };
      $scope.goToLogin = function () {
        goToLogin();
      };
      $scope.initDestinations = function () {
        $scope.setting.destinations.length = 0;
        for (var i = 0; i < $scope.setting.tripDays; i++) {
          $scope.setting.destinations.push({id:-1});
        }
        $scope.setStep(2);
      };

      $scope.triplabels = [
        {
          name: '商圈',
          checked: false
        },
        {
          name: '體育健身',
          checked: false
        },
        {
          name: '遊憩公園',
          checked: false
        },
        {
          name: '文化藝術',
          checked: false
        },
        {
          name: '國家風景區',
          checked: false
        },
        {
          name: '古蹟廟宇',
          checked: false
        },
        {
          name: '溫泉',
          checked: false
        },
        {
          name: '自然生態',
          checked: false
        },
        {
          name: '休閒農業',
          checked: false
        }
      ];

      $scope.tripDestinations = [
        {
          id: 1,
          name: '台北市'
        },
        {
          id: 2,
          name: '台中市'
        },
        {
          id: 3,
          name: '高雄市'
        }
      ];

      $scope.currentTab = 'popular';
      $scope.switchTab = function(tab) {
        $scope.currentTab = tab;
      }

      $scope.types = [
        {
          id: '1',
          name: '景點'
        },
        {
          id: '2',
          name: '美食'
        },
        {
          id: '3',
          name: '住宿'
        },
        {
          id: '4',
          name: '商店'
        }
      ];
      $scope.setting.type = $scope.types[0].id;

      $scope.spots = [
        {
          title: "台北101大樓",
          descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
          rank: 4,
          price: 120,
          types: ["商圈"],
          author: "奧革士",
          isCollect : false,
          location: "台北",
          collectCount: 234
        },
        {
          title: "國立故宮博物院",
          descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，園",
          rank: 4,
          price: 0,
          types: ["藝術文化"],
          author: "奧革士",
          isCollect : true,
          location: "台北",
          collectCount: 234
        },
        {
          title: "西門町",
          descript: "西門町位於台北市萬華區，由武昌街二段、漢中街、峨嵋街的行人徒步區所形成的區域為西門町的黃金地帶。大台北地區來往公車的密集交界，流行元素走在前端，是許多年輕學子及各國觀光客朝聖之處。",
          rank: 4,
          price: 0,
          types: ["藝術文化", "自然生態"],
          author: "奧革士",
          isCollect : false,
          location: "台北",
          collectCount: 234
        },
        {
          title: "中正紀念堂",
          descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式，大廳中央放置蔣介石坐姿銅像，底座部份設有展覽室和放映室，陳列蔣介石的一些文物，供民眾參觀。",
          rank: 4,
          price: 120,
          types: ["藝術文化", "自然生態"],
          author: "奧革士",
          isCollect : false,
          location: "台北",
          collectCount: 234
        },
        {
          title: "九份老街",
          descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
          rank: 4,
          price: 0,
          types: ["藝術文化", "自然生態"],
          author: "奧革士",
          isCollect : false,
          location: "台北",
          collectCount: 234
        },
        {
          title: "鵝鑾鼻公園",
          descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，",
          rank: 4,
          price: 120,
          types: ["藝術文化", "自然生態"],
          author: "奧革士",
          isCollect : false,
          location: "台北",
          collectCount: 234
        },
        {
          title: "高雄捷運美麗島站",
          descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
          rank: 4,
          price: 0,
          types: ["藝術文化", "自然生態"],
          author: "奧革士",
          isCollect : false,
          location: "台北",
          collectCount: 234
        },
        {
          title: "野柳地質公園",
          descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
          rank: 4,
          price: 120,
          types: ["藝術文化", "自然生態"],
          author: "奧革士",
          isCollect : false,
          location: "台北",
          collectCount: 234
        },
        {
          title: "台北101大樓",
          descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
          rank: 4,
          price: 120,
          types: ["藝術文化", "自然生態"],
          author: "奧革士",
          isCollect : false,
          location: "台北",
          collectCount: 234
        },
        {
          title: "國立故宮博物院",
          descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，園",
          rank: 4,
          price: 120,
          types: ["藝術文化", "自然生態"],
          author: "奧革士",
          isCollect : true,
          location: "台北",
          collectCount: 234
        },
        {
          title: "西門町",
          descript: "西門町位於台北市萬華區，由武昌街二段、漢中街、峨嵋街的行人徒步區所形成的區域為西門町的黃金地帶。大台北地區來往公車的密集交界，流行元素走在前端，是許多年輕學子及各國觀光客朝聖之處。",
          rank: 4,
          price: 120,
          types: ["藝術文化", "自然生態"],
          author: "奧革士",
          isCollect : false,
          location: "台北",
          collectCount: 234
        },
        {
          title: "中正紀念堂",
          descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式，大廳中央放置蔣介石坐姿銅像，底座部份設有展覽室和放映室，陳列蔣介石的一些文物，供民眾參觀。",
          rank: 4,
          price: 120,
          types: ["藝術文化", "自然生態"],
          author: "奧革士",
          isCollect : false,
          location: "台北",
          collectCount: 234
        },
        {
          title: "九份老街",
          descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
          rank: 4,
          price: 120,
          types: ["藝術文化", "自然生態"],
          author: "奧革士",
          isCollect : false,
          location: "台北",
          collectCount: 234
        },
        {
          title: "鵝鑾鼻公園",
          descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，",
          rank: 4,
          price: 120,
          types: ["藝術文化", "自然生態"],
          author: "奧革士",
          isCollect : false,
          location: "台北",
          collectCount: 234
        },
        {
          title: "高雄捷運美麗島站",
          descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
          rank: 4,
          price: 120,
          types: ["藝術文化", "自然生態"],
          author: "奧革士",
          isCollect : false,
          location: "台北",
          collectCount: 234
        },
        {
          title: "野柳地質公園",
          descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
          rank: 4,
          price: 120,
          types: ["藝術文化", "自然生態"],
          author: "奧革士",
          isCollect : false,
          location: "台北",
          collectCount: 234
        },
      ];

      $scope.populars = angular.copy($scope.spots);

      $scope.mycollects = angular.copy($scope.spots);
      // $scope.mycollects.length = 0;
      $scope.hasLogin = false;

      $scope.searchTextChange = function(keyword) {
        // TODO
      };

      $scope.selectedItemChange = function(item) {
        // TODO
      };

      $scope.querySearch = function(keyword) {
        // TODO
        return [
          {
            display: '九份'
          },
          {
            display: '九份老街'
          },
          {
            display: '九份阿柑姨芋圓'
          }
        ];
      };

      $scope.goTrip = function(answer) {
        $mdDialog.hide();
      };
    }

    $scope.openCreateTripModal=function(ev){
      $mdDialog.show({
        controller: createTrip,
        templateUrl: '../assets/common/partials/createTripMadal.html',
        clickOutsideToClose: false,
        targetEvent: ev,
      }).then(function(answer) {
        $scope.alert = 'You said the information was "' + answer + '".';
      }, function() {
        $scope.alert = 'You cancelled the dialog.';
      });
    }

  })
  .directive('createTrip', function(){
    return {
      restrict: 'E',
      controller: 'createTripCtrl',
      template: '<div class="button small blue" ng-click="openCreateTripModal($event)" ><i class="icon-Plus"></i> 建立行程</div>',
      link: function ($scope, element, iAttrs, controller) {
         /* element.bind('click', function() {
            $scope.openModal();
          });*/
      }
    };
  });

/**
 * author:ansonliu
 * 當視窗寬度 > $mdMedia('gt-md') 則開啟mdDialog 載入連結, 否則新開分頁
 */

angular.module("ng.directive")
  .controller("innerFrameDialogCtrl", function ($window, $scope, $mdDialog, $mdMedia) {

    $scope.showDialog = function(url){
      var parentEl = angular.element(document.body);

      if( $mdMedia('gt-md') ){
        $mdDialog.show({
          clickOutsideToClose: true,
          parent : parentEl,
          scope: $scope,        // use parent scope in template
          preserveScope: true,  // do not forget this if use parent scope
          template: '<md-dialog >' +
          '<md-subheader class="md-sticky-no-effect">' + url + '</md-subheader>' +
          '  <md-dialog-content>' +
          '<iframe src="'+url+'" width="1024px" height="640px" ></iframe>' +
          '  </md-dialog-content>' +
          '</md-dialog>',
          controller: function DialogController($scope, $mdDialog) {
            $scope.closeDialog = function() {
              $mdDialog.hide();
            }
          }
        });
      }else{
        $window.open(url);
      }

    }

  })
  .directive("innerFrameDialog", function () {
    return {
      restrict: "A",
      link: function (scope, ele, attrs) {

        ele.on('click', function(e){

          e.preventDefault();
          scope.showDialog($(this).attr('href'));
        });
      },
      controller: "innerFrameDialogCtrl"
    };
  })

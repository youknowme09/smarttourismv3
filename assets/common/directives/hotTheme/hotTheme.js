/**
 * Created by ansonliu on 2015/5/7.
 */


angular.module("ng.directive")
  .controller('hotThemeCtrl', function ($scope) {
  $scope.ThemeSlide = {
      currentIdx: 0,
      prev: function () {
        $scope.ThemeSlide.currentIdx = $scope.ThemeSlide.currentIdx > 0 ? $scope.ThemeSlide.currentIdx - 1 : $scope.ThemeList.length - 1;

      },
      next: function () {
        $scope.ThemeSlide.currentIdx = $scope.ThemeSlide.currentIdx < ($scope.ThemeList.length - 1) ? $scope.ThemeSlide.currentIdx + 1 : 0;
      },
      set: function (idx) {
        $scope.ThemeSlide.currentIdx = idx;
      }
    };

    $scope.ThemeList = [[{
        "title": "8月 - 大自然美景",
        "images": [
        {"url": "../public/images/theme/themepic1.png"}
        ]
      },
      {
        "title": "9月 - 大自然美景 ",
        "images": [
          {"url": "../public/images/theme/themepic2.png"}
        ]
      },
      {
        "title": "10月 - 大自然美景",
         "images": [
          {"url": "../public/images/theme/themepic3.png"}
        ]
      }],[{
        "title": "8月 - 大自然美景",
        "images": [
        {"url": "../public/images/theme/themepic1.png"}
        ]
      },
      {
        "title": "9月 - 大自然美景 ",
        "images": [
          {"url": "../public/images/theme/themepic2.png"}
        ]
      },
      {
        "title": "10月 - 大自然美景",
         "images": [
          {"url": "../public/images/theme/themepic3.png"}
        ]
      }]];











  })
  .directive('hotTheme', function(){
    return {
      restrict: 'E',
      controller: 'hotThemeCtrl',
      templateUrl: '../assets/common/directives/hotTheme/hotTheme.html',
      link: function ($scope, iElm, iAttrs, controller) {

      }
    };
  });

angular.module("ng.directive").directive("autoFocus", function($rootScope, $timeout) {
    return {
        restrict: "A",
        link: function(scope, ele, attrs) {
            scope.$on(attrs.autoFocus, function() {
                $timeout(function() {
                    $(ele).focus();
                    $(ele).select();
                }, 100);
            })
        }
    }
})
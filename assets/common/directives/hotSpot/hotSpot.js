/**
 * Created by ansonliu on 2015/5/7.
 */


angular.module("ng.directive")
  .controller('hotSpotCtrl', function ($scope) {
    $scope.tagColors = [
      "purple", "light-green"
    ];
 $scope.attractionSlide = {
      currentIdx: 0,
      prev: function () {
        $scope.attractionSlide.currentIdx = $scope.attractionSlide.currentIdx > 0 ? $scope.attractionSlide.currentIdx - 1 : $scope.attractionList.length - 1;

      },
      next: function () {
        $scope.attractionSlide.currentIdx = $scope.attractionSlide.currentIdx < ($scope.attractionList.length - 1) ? $scope.attractionSlide.currentIdx + 1 : 0;
      },
      set: function (idx) {
        $scope.attractionSlide.currentIdx = idx;
      }
    };

    $scope.attractionList = [[
      {
        "title": "台北101大樓台北101大樓台北101大樓台北101大樓台北101大樓台北101大樓",
        "collection": 237,

        "description" : "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        "tags": [
          "台北"
        ],
        "images": [
          {"url": "../public/images/spots/spot-1.jpg"}
        ],
        "rank":2.74,
        "comment":100,
        "collect":100
      }
      ,
      {
        "title": "台北101大樓",
        "collection": 237,
        "description" : "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        "tags": [
          "台北"
        ],
        "images": [
          {"url": "../public/images/spots/spot-2.jpg"}
        ],
        "rank":2.2,
        "comment":10,
        "collect":10
      },
      {
        "title": "台北101大樓",
        "collection": 237,
        "description" : "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        "tags": [
          "台北"
        ],
        "images": [
          {"url": "../public/images/spots/spot-3.jpg"}
        ],
        "rank":3.5,
        "comment":10,
        "collect":10
      },
      {
        "title": "台北101大樓",
        "collection": 237,
        "description" : "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        "tags": [
          "台北"
        ],
        "images": [
          {"url": "../public/images/spots/spot-4.jpg"}
        ],
        "rank":3.5,
        "comment":10,
        "collect":10
      },
      {
        "title": "台北101大樓",
        "collection": 237,
        "description" : "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        "tags": [
          "台北"
        ],
        "images": [
          {"url": "../public/images/spots/spot-5.jpg"}
        ],
        "rank":1.5,
        "comment":10,
        "collect":10
      },
      
    ],[
      {
        "title": "台北101大樓",
        "collection": 237,

        "description" : "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        "tags": [
          "台北"
        ],
        "images": [
          {"url": "../public/images/spots/spot-1.jpg"}
        ],
        "rank":2.74,
        "comment":10000,
        "collect":10000
      }
      ,
      {
        "title": "台北101大樓",
        "collection": 237,
        "description" : "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        "tags": [
          "台北"
        ],
        "images": [
          {"url": "../public/images/spots/spot-2.jpg"}
        ],
        "rank":2.2,
        "comment":10,
        "collect":10
      },
      {
        "title": "台北101大樓",
        "collection": 237,
        "description" : "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        "tags": [
          "台北"
        ],
        "images": [
          {"url": "../public/images/spots/spot-3.jpg"}
        ],
        "rank":3.5,
        "comment":10,
        "collect":10
      },
      {
        "title": "台北101大樓",
        "collection": 237,
        "description" : "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        "tags": [
          "台北"
        ],
        "images": [
          {"url": "../public/images/spots/spot-4.jpg"}
        ],
        "rank":3.5,
        "comment":10,
        "collect":10
      },
      {
        "title": "台北101大樓",
        "collection": 237,
        "description" : "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        "tags": [
          "台北"
        ],
        "images": [
          {"url": "../public/images/spots/spot-5.jpg"}
        ],
        "rank":1.5,
        "comment":10,
        "collect":10
      },
      
    ]];
    //caculate return left
    for (var i = 0; i <$scope.attractionList.length; i ++) {
      angular.forEach($scope.attractionList[i], function(value, key) {
      value.rank = 0.5*Math.round(value.rank/0.5);
      value.rank = -160+value.rank*32;
    });
    }
    
  })
  .directive('hotSpot', function(){
    return {
      restrict: 'E',
      controller: 'hotSpotCtrl',
      templateUrl: '../assets/common/directives/hotSpot/hotSpot.html',
      link: function ($scope, iElm, iAttrs, controller) {

      }
    };
  });

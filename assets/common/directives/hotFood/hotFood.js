/**
 * Created by ansonliu on 2015/5/7.
 */


angular.module("ng.directive")
  .controller('hotFoodCtrl', function ($scope) {


    $scope.foodThemeList = [
      {
        "title": "火烤料理",
        "collection": 237,
        "description" : "華人的飲食文化淵遠流長，舉世皆知，雖然今天在國際上各大都市均可吃到中國菜，但是行家們仍一致認為唯有在臺灣才能品嚐到各式各樣道地的中國菜餚。",
        "images": [
          {"url": "../public/images/foods/food-1.jpg"}
        ]
      },
      {
        "title": "火烤料理",
        "collection": 237,
        "description" : "華人的飲食文化淵遠流長，舉世皆知，雖然今天在國際上各大都市均可吃到中國菜，但是行家們仍一致認為唯有在臺灣才能品嚐到各式各樣道地的中國菜餚。",
        "images": [
          {"url": "../public/images/foods/food-2.jpg"}
        ]
      },
      {
        "title": "火烤料理",
        "collection": 237,
        "description" : "華人的飲食文化淵遠流長，舉世皆知，雖然今天在國際上各大都市均可吃到中國菜，但是行家們仍一致認為唯有在臺灣才能品嚐到各式各樣道地的中國菜餚。",
        "images": [
          {"url": "../public/images/foods/food-3.jpg"}
        ]
      },
      {
        "title": "火烤料理",
        "collection": 237,
        "description" : "華人的飲食文化淵遠流長，舉世皆知，雖然今天在國際上各大都市均可吃到中國菜，但是行家們仍一致認為唯有在臺灣才能品嚐到各式各樣道地的中國菜餚。",
        "images": [
          {"url": "../public/images/foods/food-4.jpg"}
        ]
      },
      {
        "title": "火烤料理",
        "collection": 237,
        "description" : "華人的飲食文化淵遠流長，舉世皆知，雖然今天在國際上各大都市均可吃到中國菜，但是行家們仍一致認為唯有在臺灣才能品嚐到各式各樣道地的中國菜餚。",
        "images": [
          {"url": "../public/images/foods/food-5.jpg"}
        ]
      },
      {
        "title": "火烤料理",
        "collection": 237,
        "description" : "華人的飲食文化淵遠流長，舉世皆知，雖然今天在國際上各大都市均可吃到中國菜，但是行家們仍一致認為唯有在臺灣才能品嚐到各式各樣道地的中國菜餚。",
        "images": [
          {"url": "../public/images/foods/food-6.jpg"}
        ]
      },
      {
        "title": "火烤料理",
        "collection": 237,
        "description" : "華人的飲食文化淵遠流長，舉世皆知，雖然今天在國際上各大都市均可吃到中國菜，但是行家們仍一致認為唯有在臺灣才能品嚐到各式各樣道地的中國菜餚。",
        "images": [
          {"url": "../public/images/foods/food-7.jpg"}
        ]
      },
      {
        "title": "火烤料理",
        "collection": 237,
        "description" : "華人的飲食文化淵遠流長，舉世皆知，雖然今天在國際上各大都市均可吃到中國菜，但是行家們仍一致認為唯有在臺灣才能品嚐到各式各樣道地的中國菜餚。",
        "images": [
          {"url": "../public/images/foods/food-8.jpg"}
        ]
      },
      {
        "title": "火烤料理",
        "collection": 237,
        "description" : "華人的飲食文化淵遠流長，舉世皆知，雖然今天在國際上各大都市均可吃到中國菜，但是行家們仍一致認為唯有在臺灣才能品嚐到各式各樣道地的中國菜餚。",
        "images": [
          {"url": "../public/images/foods/food-8.jpg"}
        ]
      },
      {
        "title": "火烤料理",
        "collection": 237,
        "description" : "華人的飲食文化淵遠流長，舉世皆知，雖然今天在國際上各大都市均可吃到中國菜，但是行家們仍一致認為唯有在臺灣才能品嚐到各式各樣道地的中國菜餚。",
        "images": [
          {"url": "../public/images/foods/food-8.jpg"}
        ]
      },
    ]
  })
  .directive('hotFood', function(){
    return {
      restrict: 'E',
      controller: 'hotFoodCtrl',
      templateUrl: '../assets/common/directives/hotFood/hotFood.html',
      link: function ($scope, iElm, iAttrs, controller) {

      }
    };
  });

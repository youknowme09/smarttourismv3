angular.module("ng.directive")
  .controller('poiMapModeCtrl', function ($compile, $scope) {

    console.log($scope.filter);
    $scope.$on('markerClick',function(event, data){
      markerClick(data);
    });
    $scope.markerClick=markerClick;
    $scope.$watch('layout', function(newValue, oldValue){
      if(newValue == 'Map'){
        fectchPoiList();
        console.log(newValue);
      }
    });
    $scope.$watch('filter', function(newValue, oldValue){
      if($scope.layout == 'Map')
        fectchPoiList();


    },true);

    function markerClick(poiId){

      angular.forEach($scope.markerList,function(marker){

        if(marker.args.id == poiId){

          var html='<div class="poiname-click tri-mid-click"><a href="/'+$scope.poiType+'/'+marker.args.id+'">'+marker.args.title+'</a></div>';
          marker.div.innerHTML=html;
          setMapCenter(marker.map,marker.latlng);
          console.log(marker);
        }else{
          var html='<div class="poiname tri-mid"><a href="/'+$scope.poiType+'/'+marker.args.id+'">'+marker.args.title+'</a></div>';
          marker.div.innerHTML=html;
        }
      });
    }

    function fectchPoiList(){
      $scope.spots = [
        {
          id:1,
          title: "台北101大樓",
          descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
          rank: 4,
          price: 120,
          types: ["商圈"],
          author: "奧革士",
          isCollect : false,
          location: "台北",
          collectCount: 234,
          location2:{
            lat:25.0339031,
            lng:121.5645098
          }
        },
        {
          id:2,
          title: "國立故宮博物院",
          descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，園",
          rank: 4,
          price: 0,
          types: ["藝術文化"],
          author: "奧革士",
          isCollect : true,
          location: "台北",
          collectCount: 234,
          location2:{
            lat:25.1011729,
            lng:121.5487908
          }
        },
        {
          id:3,
          title: "西門町",
          descript: "西門町位於台北市萬華區，由武昌街二段、漢中街、峨嵋街的行人徒步區所形成的區域為西門町的黃金地帶。大台北地區來往公車的密集交界，流行元素走在前端，是許多年輕學子及各國觀光客朝聖之處。",
          rank: 4,
          price: 0,
          types: ["藝術文化", "自然生態"],
          author: "奧革士",
          isCollect : false,
          location: "台北",
          collectCount: 234,
          location2:{
            lat:25.0451181,
            lng:121.5076305
          }
        },
        {
          id:4,
          title: "中正紀念堂",
          descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式，大廳中央放置蔣介石坐姿銅像，底座部份設有展覽室和放映室，陳列蔣介石的一些文物，供民眾參觀。",
          rank: 4,
          price: 120,
          types: ["藝術文化", "自然生態"],
          author: "奧革士",
          isCollect : false,
          location: "台北",
          collectCount: 234,
          location2:{
            lat:25.034731,
            lng:121.521934
          }
        },
        {
          id:5,
          title: "九份老街",
          descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
          rank: 4,
          price: 0,
          types: ["藝術文化", "自然生態"],
          author: "奧革士",
          isCollect : false,
          location: "台北",
          collectCount: 234,
          location2:{
            lat:25.108158,
            lng:121.8437955
          }
        },
        {
          id:6,
          title: "鵝鑾鼻公園",
          descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，",
          rank: 4,
          price: 120,
          types: ["藝術文化", "自然生態"],
          author: "奧革士",
          isCollect : false,
          location: "台北",
          collectCount: 234,
          location2:{
            lat:21.9020441,
            lng:120.8529676
          }
        },
        {
          id:7,
          title: "高雄捷運美麗島站",
          descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
          rank: 4,
          price: 0,
          types: ["藝術文化", "自然生態"],
          author: "奧革士",
          isCollect : false,
          location: "台北",
          collectCount: 234,
          location2:{
            lat:22.631386,
            lng:120.301951
          }
        },
        {
          id:8,
          title: "野柳地質公園",
          descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
          rank: 4,
          price: 120,
          types: ["藝術文化", "自然生態"],
          author: "奧革士",
          isCollect : false,
          location: "台北",
          collectCount: 234,
          location2:{
            lat:25.2072029,
            lng:121.690895
          }
        },
        {
          id:9,
          title: "台北101大樓",
          descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
          rank: 4,
          price: 120,
          types: ["藝術文化", "自然生態"],
          author: "奧革士",
          isCollect : false,
          location: "台北",
          collectCount: 234,
          location2:{
            lat:25.0339031,
            lng:121.5645098
          }
        },
        {
          id:10,
          title: "國立故宮博物院",
          descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，園",
          rank: 4,
          price: 120,
          types: ["藝術文化", "自然生態"],
          author: "奧革士",
          isCollect : true,
          location: "台北",
          collectCount: 234,
          location2:{
            lat:25.1011729,
            lng:121.5487908
          }
        },
        {
          id:11,
          title: "西門町",
          descript: "西門町位於台北市萬華區，由武昌街二段、漢中街、峨嵋街的行人徒步區所形成的區域為西門町的黃金地帶。大台北地區來往公車的密集交界，流行元素走在前端，是許多年輕學子及各國觀光客朝聖之處。",
          rank: 4,
          price: 120,
          types: ["藝術文化", "自然生態"],
          author: "奧革士",
          isCollect : false,
          location: "台北",
          collectCount: 234,
          location2:{
            lat:25.0451181,
            lng:121.5076305
          }
        },
        {
          id:12,
          title: "中正紀念堂",
          descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式，大廳中央放置蔣介石坐姿銅像，底座部份設有展覽室和放映室，陳列蔣介石的一些文物，供民眾參觀。",
          rank: 4,
          price: 120,
          types: ["藝術文化", "自然生態"],
          author: "奧革士",
          isCollect : false,
          location: "台北",
          collectCount: 234,
          location2:{
            lat:25.034731,
            lng:121.521934
          }
        },
        {
          id:13,
          title: "九份老街",
          descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
          rank: 4,
          price: 120,
          types: ["藝術文化", "自然生態"],
          author: "奧革士",
          isCollect : false,
          location: "台北",
          collectCount: 234,
          location2:{
            lat:25.108158,
            lng:121.8437955
          }
        },
        {
          id:14,
          title: "鵝鑾鼻公園",
          descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，",
          rank: 4,
          price: 120,
          types: ["藝術文化", "自然生態"],
          author: "奧革士",
          isCollect : false,
          location: "台北",
          collectCount: 234,
          location2:{
            lat:21.9020441,
            lng:120.8529676
          }
        },
        {
          id:15,
          title: "高雄捷運美麗島站",
          descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
          rank: 4,
          price: 0,
          types: ["藝術文化", "自然生態"],
          author: "奧革士",
          isCollect : false,
          location: "台北",
          collectCount: 234,
          location2:{
            lat:22.631386,
            lng:120.301951
          }
        },
        {
          id:16,
          title: "野柳地質公園",
          descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
          rank: 4,
          price: 120,
          types: ["藝術文化", "自然生態"],
          author: "奧革士",
          isCollect : false,
          location: "台北",
          collectCount: 234,
          location2:{
            lat:25.2072029,
            lng:121.690895
          }
        }

      ];

      init();
    }

    function init() {
      // Basic options for a simple Google Map
      // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
      var mapOptions = {
        // How zoomed in you want the map to start at (always required)
        zoom: 13,
        // The latitude and longitude to center the map (always required)
        center: new google.maps.LatLng($scope.spots[0].location2.lat, $scope.spots[0].location2.lng), // TW
        scrollwheel: true,
        navigationControl: false,
        mapTypeControl: false,
        //scaleControl: false,
        draggable: true
        // How you would like to style the map.
        // This is where you would paste any style found on Snazzy Maps.
       };

      var mapElement = $('#check-in-map .map')[0];
      var map = new google.maps.Map(mapElement, mapOptions);

      //// Responsive google map
      google.maps.event.addDomListener(window, "resize", function() {
        var center = map.getCenter();
        google.maps.event.trigger(map, "resize");
        map.setCenter(center);
      });


      $scope.markerList = [];

      function CustomMarker(latlng, map, args) {
        this.latlng = latlng;
        this.args = args;
        this.setMap(map);
      }


      angular.forEach($scope.spots, function (item, index) {
        $scope.item = item;
        //console.log('item',item);
        var myLatlng=new google.maps.LatLng(item.location2.lat, item.location2.lng);

        CustomMarker.prototype = new google.maps.OverlayView();
        CustomMarker.prototype.draw = function() {
          var self = this;
          var div = this.div;
          var html='<div class="poiname tri-mid"><a href="/'+$scope.poiType+'/'+item.id+'">'+item.title+'</a></div>';
          if (!div) {
            div = this.div = document.createElement('div');
            div.className = 'marker';
            div.innerHTML=html;

            if (typeof(self.args.marker_id) !== 'undefined') {
              div.dataset.marker_id = self.args.marker_id;
            }

            google.maps.event.addDomListener(div, "click", function(event) {
              google.maps.event.trigger(self, "click");
            });

            var panes = this.getPanes();
            panes.overlayImage.appendChild(div);
          }

          var point = this.getProjection().fromLatLngToDivPixel(this.latlng);

          if (point) {
            div.style.left = point.x + 'px';
            div.style.top = point.y + 'px';
          }
        };
        CustomMarker.prototype.remove = function() {
          if (this.div) {
            this.div.parentNode.removeChild(this.div);
            this.div = null;
          }
        };
        CustomMarker.prototype.getPosition = function() {
          return this.latlng;
        };
        var marker = new CustomMarker(
          myLatlng,
          map,
          item
        );

        $scope.markerList.push(marker);
        if($scope.markerList.length == $scope.spots.length){
          var center = map.getCenter();
          google.maps.event.trigger(map, "resize");
          map.setCenter(center);
        }
        //$scope.markerList.forEach(function(m){
        //  console.log('marker:',m);
        //});


      });



    }

    function setMapCenter(map,center){
      google.maps.event.trigger(map, "resize");
      map.setCenter(new google.maps.LatLng(center.G, center.K));
    }
  })
  .directive('poiMapMode', ['$rootScope', function (rootScope) {
    // Runs during compile

    return {
      // name: '',
      // priority: 1,
      // terminal: true,
      scope: {
        layout: '=layout',
        filter: '=filter',
        poiType:'=poiType'
      }, // {} = isolate, true = child, false/undefined = no change
      controller: 'poiMapModeCtrl',
      // require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
      restrict: 'E', // E = Element, A = Attribute, C = Class, M = Comment
      //template: 'assets/common/directives/checkinMap/checkinMap.html',
      templateUrl: '../assets/common/directives/poiMapMode/poiMapMode.html',
      // replace: true,
      // transclude: true,
      // compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),
      link: function ($scope, iElm, iAttrs, controller) {
        //google.maps.event.addDomListener(window, 'load', $scope.init);
      }
    };
  }]);

/**
 * Created by ansonliu on 2015/4/23.
 */


 angular.module("ng.directive")
 .controller('hotGuideCtrl', function ($scope) {

  $scope.tagColors = [
  "purple", "light-green"
  ];

  $scope.hotTripList = [
  {
    "title": "台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊",
    "images": [
    {"url": "../public/images/trips/trip-1.jpg"}
    ],
    "date":"2016/12/17",
    "admin":"奧革士",
    "adminicon":"../public/images/trips/trip-1.jpg"
  },
   {
    "title": "台北都會一日遊",
    "images": [
    {"url": "../public/images/trips/trip-2.jpg"}
    ],
    "date":"2016/12/17",
    "admin":"奧革士",
    "adminicon":"../public/images/trips/trip-1.jpg"
  },
   {
    "title": "台北都會一日遊",
    "images": [
    {"url": "../public/images/trips/trip-3.jpg"}
    ],
    "date":"2016/12/17",
    "admin":"奧革士",
    "adminicon":"../public/images/trips/trip-1.jpg"
  },
   {
    "title": "台北都會一日遊",
    "images": [
    {"url": "../public/images/trips/trip-4.jpg"}
    ],
    "date":"2016/12/17",
    "admin":"奧革士",
    "adminicon":"../public/images/trips/trip-1.jpg"
  },
   {
    "title": "台北都會一日遊",
    "images": [
    {"url": "../public/images/trips/trip-5.jpg"}
    ],
    "date":"2016/12/17",
    "admin":"奧革士",
    "adminicon":"../public/images/trips/trip-1.jpg"
  }

  ]
})
.directive('hotGuide', function () {

  return {
    restrict: 'E',
    controller: 'hotGuideCtrl',
    templateUrl: '../assets/common/directives/hotGuide/hot-guide.html',
    link: function ($scope, iElm, iAttrs, controller) {

    }
  };
});

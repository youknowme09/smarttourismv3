/**
 * Created by ansonliu on 2015/5/7.
 */


angular.module("ng.directive")
  .controller('loginCtrl', function ($scope, $state, $mdDialog) {

    $scope.step = 0;
    $scope.hasSend = false;

    function login ($scope, $mdDialog, $timeout) {
      $scope.step = 1;
      $scope.setStep = function(step) {
        $scope.step = step;
      };
      $scope.cancel = function() {
        $mdDialog.cancel();
      };
      $scope.login = function(answer) {
        $scope.onLogin = true;
        $timeout(function () {
          $scope.onLogin = false;
          $scope.loginFail = true;
        }, 1000);
      };
      $scope.goToSignup = function () {
        goToSignup();
      };
      $scope.sendResetPasswordMail = function () {
        $scope.onSending = true;
        $timeout(function () {
          $scope.onSending = false;
          $scope.hasSend = true;
        }, 1000);
      };
    }

    $scope.openLoginModal=function(){
      $mdDialog.show({
        controller: login,
        templateUrl: '../assets/common/partials/loginMadal.html',
        //targetEvent: ev
      })
        .then(function(answer) {
          $scope.alert = 'You said the information was "' + answer + '".';
        }, function() {
          $scope.alert = 'You cancelled the dialog.';
        });
    }
  })
  .directive('login', function(){
    return {
      restrict: 'EA',
      controller: 'loginCtrl',
      template:'',
      link: function ($scope, element, iAttrs, controller) {
         element.bind('click', function() {
          $scope.openLoginModal();
         });
      }
    };
  });

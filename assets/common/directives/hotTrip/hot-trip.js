/**
 * Created by ansonliu on 2015/4/23.
 */


 angular.module("ng.directive")
 .controller('hotTripCtrl', function ($scope) {

  $scope.tagColors = [
  "purple", "light-green"
  ];

  $scope.hotTripList = [
  {
    "title": "台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊",
    "collection": 237,
    "days": 7,
    "admin": "奧革士",
    "images": [
    {"url": "../public/images/trips/trip-1.jpg"}
    ]
  },
  {
    "title": "台北都會一日遊",
    "collection": 237,
    "days": 7,
    "admin": "奧革士",

    "images": [
    {"url": "../public/images/trips/trip-2.jpg"}
    ]
  },
  {
    "title": "台北都會一日遊",
    "collection": 237,
    "days": 7,
    "admin": "奧革士",

    "images": [
    {"url": "../public/images/trips/trip-3.jpg"}
    ]
  },
  {
    "title": "台北都會一日遊",
    "collection": 237,
    "days": 7,
    "admin": "奧革士",

    "images": [
    {"url": "../public/images/trips/trip-4.jpg"}
    ]
  },
  {
    "title": "台北都會一日遊",
    "collection": 237,
    "days": 7,
    "admin": "奧革士",

    "images": [
    {"url": "../public/images/trips/trip-5.jpg"}
    ]
  }

  ]
})
.directive('hotTrip', function () {

  return {
    restrict: 'E',
    controller: 'hotTripCtrl',
    templateUrl: '../assets/common/directives/hotTrip/hot-trip.html',
    link: function ($scope, iElm, iAttrs, controller) {

    }
  };
});

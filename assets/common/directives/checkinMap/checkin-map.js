angular.module("ng.directive")
  .controller('checkInMapCtrl', function ($scope) {
 $scope.checkinSlide = {
      currentIdx: 0,
      prev: function () {
        $scope.checkinSlide.currentIdx = $scope.checkinSlide.currentIdx > 0 ? $scope.checkinSlide.currentIdx - 1 : $scope.checkinList.length - 1;

      },
      next: function () {
        $scope.checkinSlide.currentIdx = $scope.checkinSlide.currentIdx < ($scope.checkinList.length - 1) ? $scope.ThemeSlide.currentIdx + 1 : 0;
      },
      set: function (idx) {
        $scope.checkinSlide.currentIdx = idx;
      }
    };
    var checkinList = [[
      { "id":1,
        "location" : "高雄",
        "lat": 25.0338992,
        "lng": 121.5646514,
        "showHover":false,
        "title": "101 觀景台",
        "descript": "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        "checkin": 9999999,
        "images": [
          {"url": "../public/images/theme/pinpic-1.png"}
        ]
      },
      {
        "id":2,
        "location" : "台北",
        "lat": 24.1486841,
        "lng": 120.6673279,
        "showHover":false,
        "title": "九份老街",
        "descript": "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        "checkin": 8888888,
        "images": [
          {"url": "../public/images/theme/pinpic-2.png"}
        ]
      },
      {
        "id":3,
        "location" : "台北",
        "lat": 24.0779521,
        "lng": 120.601747,
        "showHover":false,
        "title": "台北101",
        "descript": "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        "checkin": 777777,
        "images": [
          {"url": "../public/images/theme/pinpic-3.png"}
        ]
      },
      {
        "id":4,
        "location" : "花蓮",
        "lat": 23.508902,
        "lng": 121.802147,
        "showHover":false,
        "title": "太魯閣國家公園",
        "descript": "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        "checkin": 666666,
        "images": [
          {"url": "../public/images/theme/pinpic-4.png"}
        ]
      },
      {
        "id":5,
        "location" : "屏東",
        "lat": 22.352192,
        "lng": 121.3770985,
        "showHover":false,
        "title": "白沙屯拱天宮",
        "descript": "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        "checkin": 555555,
        "images": [
          {"url": "../public/images/theme/pinpic-5.png"}
        ]
      },
      {
        "id":6,
        "location" : "台北",
        "lat": 23.652192,
        "lng": 121.3770985,
        "showHover":false,
        "title": "澄清湖棒球場",
        "descript": "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        "checkin": 555555,
        "images": [
          {"url": "../public/images/theme/pinpic-6.png"}
        ]
      },
      {
        "id":7,
        "location" : "台北",
        "lat": 23.652192,
        "lng": 120.1770985,
        "showHover":false,
        "title": "向天湖",
        "descript": "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        "checkin": 555555,
        "images": [
          {"url": "../public/images/theme/pinpic-7.png"}
        ]
      },
      {
        "id":8,
        "location" : "新竹",
        "lat": 22.252192,
        "lng": 121.3770985,
        "showHover":false,
        "title": "鐵花村",
        "descript": "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        "checkin": 555555,
        "images": [
          {"url": "../public/images/theme/pinpic-8.png"}
        ]
      }
    ],[
      { "id":1,
        "location" : "高雄",
        "lat": 25.0338992,
        "lng": 121.5646514,
        "showHover":false,
        "title": "101 觀景台",
        "descript": "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        "checkin": 9999999,
        "images": [
          {"url": "../public/images/theme/pinpic-1.png"}
        ]
      },
      {
        "id":2,
        "location" : "台北",
        "lat": 24.1486841,
        "lng": 120.6673279,
        "showHover":false,
        "title": "九份老街",
        "descript": "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        "checkin": 8888888,
        "images": [
          {"url": "../public/images/theme/pinpic-2.png"}
        ]
      },
      {
        "id":3,
        "location" : "台北",
        "lat": 24.0779521,
        "lng": 120.601747,
        "showHover":false,
        "title": "台北101",
        "descript": "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        "checkin": 777777,
        "images": [
          {"url": "../public/images/theme/pinpic-3.png"}
        ]
      },
      {
        "id":4,
        "location" : "花蓮",
        "lat": 23.508902,
        "lng": 121.802147,
        "showHover":false,
        "title": "太魯閣國家公園",
        "descript": "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        "checkin": 666666,
        "images": [
          {"url": "../public/images/theme/pinpic-4.png"}
        ]
      },
      {
        "id":5,
        "location" : "屏東",
        "lat": 22.352192,
        "lng": 121.3770985,
        "showHover":false,
        "title": "白沙屯拱天宮",
        "descript": "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        "checkin": 555555,
        "images": [
          {"url": "../public/images/theme/pinpic-5.png"}
        ]
      },
      {
        "id":6,
        "location" : "台北",
        "lat": 23.652192,
        "lng": 121.3770985,
        "showHover":false,
        "title": "澄清湖棒球場",
        "descript": "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        "checkin": 555555,
        "images": [
          {"url": "../public/images/theme/pinpic-6.png"}
        ]
      },
      {
        "id":7,
        "location" : "台北",
        "lat": 23.652192,
        "lng": 120.1770985,
        "title": "向天湖",
        "showHover":false,
        "descript": "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        "checkin": 555555,
        "images": [
          {"url": "../public/images/theme/pinpic-7.png"}
        ]
      },
      {
        "id":8,
        "location" : "新竹",
        "descript": "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        "lat": 22.252192,
        "lng": 121.3770985,
        "showHover":false,
        "title": "鐵花村",
        "checkin": 555555,
        "images": [
          {"url": "../public/images/theme/pinpic-8.png"}
        ]
      }
    ]];


    $scope.checkinList = checkinList;

    $scope.clickCheckin = function (checkin) {

    };
    //hover Pin
    $scope.hoverShow = function(item){
  item.showHover = true;
};

$scope.hoverhide = function(item){
  item.showHover = false;
};




  })



  .directive('checkInMap', ['$rootScope', '$http', function (rootScope, $http) {
    // Runs during compile

    

    return {
      // name: '',
      // priority: 1,
      // terminal: true,
      // scope: {}, // {} = isolate, true = child, false/undefined = no change
      controller: 'checkInMapCtrl',
      // require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
      restrict: 'E', // E = Element, A = Attribute, C = Class, M = Comment
      //template: 'assets/common/directives/checkinMap/checkinMap.html',
      templateUrl: '../assets/common/directives/checkinMap/checkinMap.html',
      // replace: true,
      // transclude: true,
      // compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),
      link: function ($scope, iElm, iAttrs, controller) {
        //google.maps.event.addDomListener(window, 'load', init);
        init();
        function init() {
          // Basic options for a simple Google Map
          // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
          var mapOptions = {
            // How zoomed in you want the map to start at (always required)
            zoom: 8,
            // zoom: 14,
            // center: {lat: 37.775, lng: -122.434},
            // The latitude and longitude to center the map (always required)
            center: new google.maps.LatLng(23.583234, 120.8825975), // TW
            scrollwheel: false,
            navigationControl: false,
            mapTypeControl: false,
            scaleControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            draggable: true,
            // How you would like to style the map.
            // This is where you would paste any style found on Snazzy Maps.
            styles: []
          };

          var mapElement = $('#check-in-map .map')[0];
          var map = new google.maps.Map(mapElement, mapOptions);
          var markerList = [];

          var points = [];
          $http.get('./assets/point.json').success(function(data) { 
            var points = [];
            data.forEach(function (l){
              var lat = l.location.lat,
                  lng = l.location.lng,
                  o = new google.maps.LatLng(lat, lng);
              points.push(o);
            });

          

          });

          

          

          angular.forEach($scope.checkinList[0], function (item, index) {
            var marker = new google.maps.Marker({
              position: new google.maps.LatLng(item.lat, item.lng),
              map: map,
              title: item.title,
              icon: '../public/images/svg/pin-actived.svg'
            });
            markerList.push(marker);

            google.maps.event.addListener(marker, 'click', (function (marker) {
              return function () {

                // close all
                angular.forEach(markerList, function(m){
                  m.setIcon('../public/images/svg/pin-actived.svg');
                  if( m.ib)
                    m.ib.close();
                });
                marker.setIcon('../public/images/svg/pin-actived.svg');

               
              }
            })(marker));
          });
        }
      }
    };
  }]);

"use strict";

angular.module('ng.controller').controller('headerCtrl' ,
  function($rootScope, $scope, $state, $mdDialog) {
  	$scope.$state = $state;
  	$scope.currentSearchType = 1;
  	$rootScope.isShowMenu = false;
  	$scope.isShowSearch = false;
    $scope.hasLogin = true;
  	$scope.showMenu = function () {
  		$rootScope.isShowMenu = true;
  		console.log("test");
  	};
  	$scope.closeMenu = function () {
  		$rootScope.isShowMenu = false;
  		console.log("test2");
  	};

    $scope.showSearch = function () {
      $scope.isShowSearch = true;
      $rootScope.$broadcast("SEACH_GLOBAL");
    };

    $scope.closeSearch = function () {
      $scope.isShowSearch = false;
    };
    $scope.logout = function () {
      $scope.hasLogin = false;
    };


    $scope.searchTextChange = function(keyword) {
        // TODO
    };

    $scope.selectedItemChange = function(item) {
        // TODO
    };

    $scope.querySearch = function(keyword) {
        // TODO
        return [
            {
                display: '九份'
            },
            {
                display: '九份老街'
            },
            {
                display: '九份阿柑姨芋圓'
            },
            {
                display: '更多“<span class="highlight">九</span>”的搜尋結果'
            }
        ];
    };
  

    $scope.problemReport = function (ev) {
      $mdDialog.show({
        controller: function ($scope, $mdDialog, $timeout) {
          $scope.report = {
            email: '',
            content: ''
          };
          $scope.cancel = function() {
            $mdDialog.cancel();
          };
          $scope.sendReport = function(answer) {
            $scope.reporting = true;
            $timeout(function () {
              $scope.reporting = false;
              $scope.reportSuccess = true;
            }, 1000);
          };
        },
        templateUrl: '../assets/common/partials/problemReportMadal.html',
        targetEvent: ev,
      })
      .then(function(answer) {
        $scope.alert = 'You said the information was "' + answer + '".';
      }, function() {
        $scope.alert = 'You cancelled the dialog.';
      });
    };

  }
);

"use strict";

angular.module('ng.controller').controller('userCommentsController' ,
  function( userService, $mdSidenav, $mdBottomSheet, $log, $q, $scope) {



    $scope.searchTextChange = function(keyword) {
        // TODO
    };

    $scope.selectedItemChange = function(item) {
        // TODO
    };

    $scope.querySearch = function(keyword) {
        // TODO
        return [
            {
                display: '九份'
            },
            {
                display: '九份老街'
            },
            {
                display: '九份阿柑姨芋圓'
            }
        ];
    };



    $scope.commentList = [
        {
            thumbnail: '../public/images/spots/spot-1.jpg',
            title: '日月潭旅人小屋',
            comments: [
                {
                    commentTime: '2015-06-01 09:20:32',
                    content: '台中近來的新人氣景點，就是在台中火車站前的 宮原眼科，如果你沒來過的話，就遜了啊～宮原眼科現在可不是眼科，可是早期宮原眼科的建築。'
                }
            ]
        },
        {
            thumbnail: '../public/images/spots/spot-2.jpg',
            title: '紅米國際青年旅館',
            comments: [
                {
                    commentTime: '2015-06-01 09:20:32',
                    content: '来到这里的第一天，感觉好像在茫茫的雨天里找到了一个很好的藏身之所。不一样的经历，舒服。。很有欧洲的 B&B Feel. Pantry 还供应了饮料和面包，对背包旅行者而言。。很体贴。还会再来，希望下次来的时候，房间都会供应拖鞋，和搽脚布...那就完美了。'
                },
                {
                    commentTime: '2015-06-01 09:20:32',
                    commentTo: '奧革士',
                    content: '来到这里的第一天，感觉好像在茫茫的雨天里找到了一个很好的藏身之所。不一样的经历，舒服。。很有欧洲的 B&B Feel. Pantry 还供应了饮料和面包，对背包旅行者而言。'
                },
                {
                    commentTime: '2015-06-01 09:20:32',
                    commentTo: '奧革士',
                    content: '来到这里的第一天，感觉好像在茫茫的雨天里找到了一个很好的藏身之所。不一样的经历，舒服。。很有欧洲的 B&B Feel. Pantry 还供应了饮料和面包，对背包旅行者而言。'
                }
            ]
        },
        {
            thumbnail: '../public/images/spots/spot-3.jpg',
            title: '花蓮國際石雕藝術季',
            comments: [
                {
                    commentTime: '2015-06-01 09:20:32',
                    content: '台中近來的新人氣景點，就是在台中火車站前的 宮原眼科，如果你沒來過的話，就遜了啊～宮原眼科現在可不是眼科，可是早期宮原眼科的建築。'
                }
            ]
        },
        {
            thumbnail: '../public/images/spots/spot-4.jpg',
            title: '台北 101 大樓',
            comments: [
                {
                    commentTime: '2015-06-01 09:20:32',
                    content: '台中近來的新人氣景點，就是在台中火車站前的 宮原眼科，如果你沒來過的話，就遜了啊～宮原眼科現在可不是眼科，可是早期宮原眼科的建築。'
                }
            ]
        }
    ];

  }
);

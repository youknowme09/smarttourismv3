"use strict";

angular.module('ng.controller').controller('userCalendarController',
    function(userService, $mdSidenav, $mdBottomSheet, $log, $q, $scope) {

        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            lang: "zh-tw",
            defaultDate: '2015-02-12',
            businessHours: true, // display business hours
            editable: true,
            eventClick: function(event) {
		        if (event.url) {
		            window.open(event.url);
		            return false;
		        }
		    },
            events: [
            	{
                    title: '提供 台北101 景點勘誤',
                    url: "/#/stays/poi",
                    start: '2015-02-10',
                    constraint: 'availableForMeeting', // defined below
                    color: '#07B5D9'
                },
                {
                    title: '獲得 海上的旅遊者 勳章',
                    url: "/#/stays/poi",
                    start: '2015-02-10',
                    constraint: 'availableForMeeting', // defined below
                    color: '#77BF56'
                },
            	{
                    title: '完成 花蓮美麗3日遊 遊記',
                    url: "/#/stays/poi",
                    start: '2015-02-13',
                    constraint: 'availableForMeeting', // defined below
                    color: '#07B5D9'
                },
                {
                    title: '完成 台灣環島7日遊 行程規劃',
                    url: "/#/stays/poi",
                    start: '2015-02-13',
                    constraint: 'availableForMeeting', // defined below
                    color: '#07B5D9'
                },
                {
                    title: '提供 台北101 景點勘誤',
                    url: "/#/stays/poi",
                    start: '2015-02-13',
                    constraint: 'availableForMeeting', // defined below
                    color: '#07B5D9'
                },
                {
                    title: '獲得 海上的旅遊者 勳章',
                    url: "/#/stays/poi",
                    start: '2015-02-13',
                    constraint: 'availableForMeeting', // defined below
                    color: '#77BF56'
                },
                {
                    title: '獲得 5點 遊記經驗值',
                    url: "/#/stays/poi",
                    start: '2015-02-13',
                    constraint: 'availableForMeeting', // defined below
                    color: '#77BF56'
                },
                {
                    title: '進行 台灣環島7日遊 旅程',
                    url: "/#/stays/poi",
                    start: '2015-02-13',
                    end: '2015-02-21',
                    constraint: 'availableForMeeting', // defined below
                    color: '#F88523'
                },
                {
                    title: '提供 台北101 景點勘誤',
                    url: "/#/stays/poi",
                    start: '2015-02-18',
                    constraint: 'availableForMeeting', // defined below
                    color: '#07B5D9'
                },
                {
                    title: '獲得 海上的旅遊者 勳章',
                    url: "/#/stays/poi",
                    start: '2015-02-18',
                    constraint: 'availableForMeeting', // defined below
                    color: '#77BF56'
                },
                {
                    title: '獲得 5點 遊記經驗值',
                    url: "/#/stays/poi",
                    start: '2015-02-18',
                    constraint: 'availableForMeeting', // defined below
                    color: '#77BF56'
                },
                {
                    title: '提供 台北101 景點勘誤',
                    url: "/#/stays/poi",
                    start: '2015-02-23',
                    constraint: 'availableForMeeting', // defined below
                    color: '#07B5D9'
                },
                {
                    title: '提供 台北101 景點勘誤',
                    url: "/#/stays/poi",
                    start: '2015-02-23',
                    constraint: 'availableForMeeting', // defined below
                    color: '#07B5D9'
                },
                {
                    title: '提供 台北101 景點勘誤',
                    url: "/#/stays/poi",
                    start: '2015-02-23',
                    constraint: 'availableForMeeting', // defined below
                    color: '#07B5D9'
                },
                {
                    title: '進行 台灣環島7日遊 旅程',
                    url: "/#/stays/poi",
                    start: '2015-02-25',
                    end: '2015-02-27',
                    constraint: 'availableForMeeting', // defined below
                    color: '#F88523'
                },

            ]
        });

    }
);

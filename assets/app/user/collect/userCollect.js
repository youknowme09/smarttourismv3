"use strict";

angular.module('ng.controller').controller('userCollectController',
    function(userService, $mdSidenav, $mdBottomSheet, $log, $q, $scope) {



        $scope.searchTextChange = function(keyword) {
            // TODO
        };

        $scope.selectedItemChange = function(item) {
            // TODO
        };

        $scope.querySearch = function(keyword) {
            // TODO
            return [{
                display: '九份'
            }, {
                display: '九份老街'
            }, {
                display: '九份阿柑姨芋圓'
            }];
        };



        $scope.filterTypes = [{
            id: 0,
            name: '全部類別'
        }, {
            id: 1,
            name: '景點'
        }, {
            id: 2,
            name: '美食'
        }, {
            id: 3,
            name: '活動'
        }, {
            id: 4,
            name: '住宿'
        }, {
            id: 5,
            name: '商店'
        }];
        $scope.currentFilterType = $scope.filterTypes[0].id;



        $scope.orderLists = [{
            id: 0,
            name: '評價最高'
        }, {
            id: 1,
            name: '價格低到高'
        }, {
            id: 2,
            name: '價格高到低'
        }, {
            id: 3,
            name: '最多收藏'
        }, {
            id: 4,
            name: '發表日期'
        }];
        $scope.currentOrder = $scope.orderLists[0].id;



        $scope.hasCollect = true;



        $scope.collectList = [{
            thumbnail: '../public/images/foods/food-1.jpg',
            title: "黃家牛肉麵",
            summary: '免門票 / 遊憩公園、自然生態',
            descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式，大廳中央放置蔣介石坐姿銅像，底座部份設有展覽室和放映室，陳列蔣介石的一些文物，供民眾參觀。",
            metas: '237 人收藏',
            mustgo: true,
            location: '台北',
            address: '台北市大同區長安西路33號',
            view: '5,169',
            comment: '271',
        }, {
            thumbnail: '../public/images/foods/food-2.jpg',
            title: "國立故宮博物院",
            summary: '免門票 / 遊憩公園、自然生態',
            descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式，大廳中央放置蔣介石坐姿銅像，底座部份設有展覽室和放映室，陳列蔣介石的一些文物，供民眾參觀。",
            metas: '237 人收藏',
            mustgo: true,
            location: '高雄',
            address: '台北市松山區敦化北路158號',
            view: '471',
            comment: '21',
        }, {
            thumbnail: '../public/images/foods/food-3.jpg',
            title: "九份老石屋 藍海房",
            summary: '民宿 / 每晚平均 875 元',
            descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式，大廳中央放置蔣介石坐姿銅像，底座部份設有展覽室和放映室，陳列蔣介石的一些文物，供民眾參觀。",
            metas: '237 人收藏',
            mustgo: false,
            location: '新竹',
            address: '新竹市東區博愛街111號',
            view: '8,715',
            comment: '654',
        }, {
            thumbnail: '../public/images/foods/food-4.jpg',
            title: "國立故宮博物院",
            summary: '青年旅館 / 每晚平均 875 元',
            descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式，大廳中央放置蔣介石坐姿銅像，底座部份設有展覽室和放映室，陳列蔣介石的一些文物，供民眾參觀。",
            metas: '237 人收藏',
            mustgo: false,
            location: '花蓮',
            address: '花蓮縣花蓮市中華路144號第9棟',
            view: '3,751',
            comment: '477',
        }, {
            thumbnail: '../public/images/foods/food-5.jpg',
            title: "日月潭旅人小屋",
            summary: '青年旅館 / 每晚平均 875 元',
            descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式，大廳中央放置蔣介石坐姿銅像，底座部份設有展覽室和放映室，陳列蔣介石的一些文物，供民眾參觀。",
            metas: '237 人收藏',
            mustgo: true,
            location: '台南',
            address: '台南市安南區長和路一段250號',
            view: '571',
            comment: '21',
        }, {
            thumbnail: '../public/images/foods/food-6.jpg',
            title: "紅米國際青年旅館",
            summary: '青年旅館 / 每晚平均 875 元',
            descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式，大廳中央放置蔣介石坐姿銅像，底座部份設有展覽室和放映室，陳列蔣介石的一些文物，供民眾參觀。",
            metas: '237 人收藏',
            mustgo: false,
            location: '高雄',
            address: '高雄市岡山區介壽西路西首1號',
            view: '571',
            comment: '27',
        }, {
            thumbnail: '../public/images/foods/food-7.jpg',
            title: "花蓮國際石雕藝術季",
            summary: '文藝活動 / 門票 120元',
            descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式，大廳中央放置蔣介石坐姿銅像，底座部份設有展覽室和放映室，陳列蔣介石的一些文物，供民眾參觀。",
            metas: '237 人收藏',
            mustgo: true,
            location: '屏東',
            address: '屏東縣車城鄉保力村竹社路5號',
            view: '4,791',
            comment: '328',
        }, {
            thumbnail: '../public/images/foods/food-8.jpg',
            title: "法式牛肉捲",
            summary: '免門票 / 遊憩公園、自然生態',
            descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式，大廳中央放置蔣介石坐姿銅像，底座部份設有展覽室和放映室，陳列蔣介石的一些文物，供民眾參觀。",
            metas: '237 人收藏',
            mustgo: false,
            location: '宜蘭',
            address: '宜蘭縣宜蘭市宜興路一段240號',
            view: '921',
            comment: '12',
        }, {
            thumbnail: '../public/images/foods/food-9.jpg',
            title: "台北 101 大樓",
            summary: '免門票 / 遊憩公園、自然生態',
            descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式，大廳中央放置蔣介石坐姿銅像，底座部份設有展覽室和放映室，陳列蔣介石的一些文物，供民眾參觀。",
            metas: '237 人收藏',
            mustgo: true,
            location: '台中',
            address: '台中市南屯區文心路一段289號',
            view: '257',
            comment: '36',
        }, {
            thumbnail: '../public/images/foods/food-10.jpg',
            title: "三義國際木雕藝術節",
            summary: '節慶活動 / 免門票',
            descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式，大廳中央放置蔣介石坐姿銅像，底座部份設有展覽室和放映室，陳列蔣介石的一些文物，供民眾參觀。",
            metas: '237 人收藏',
            mustgo: false,
            location: '南投',
            address: '南投縣埔里鎮桃米里桃米巷52-12號',
            view: '725',
            comment: '92',
        }, {
            thumbnail: '../public/images/foods/food-11.jpg',
            title: "台北 101 大樓",
            summary: '免門票 / 遊憩公園、自然生態',
            descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式，大廳中央放置蔣介石坐姿銅像，底座部份設有展覽室和放映室，陳列蔣介石的一些文物，供民眾參觀。",
            metas: '237 人收藏',
            mustgo: true,
            location: '苗栗',
            address: '苗栗縣苗栗市公園路',
            view: '2,171',
            comment: '41',
        }, {
            thumbnail: '../public/images/foods/food-12.jpg',
            title: "國立故宮博物院",
            summary: '免門票 / 遊憩公園、自然生態',
            descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式，大廳中央放置蔣介石坐姿銅像，底座部份設有展覽室和放映室，陳列蔣介石的一些文物，供民眾參觀。",
            metas: '237 人收藏',
            mustgo: false,
            location: '台東',
            address: '台東縣東河鄉',
            view: '271',
            comment: '99',
        }];

    }
);

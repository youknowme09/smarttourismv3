"use strict";

angular.module('ng.controller').controller('userSpotsErrataController' ,
  function( userService, $mdSidenav, $mdBottomSheet, $log, $q, $scope, $mdDialog) {



    $scope.searchTextChange = function(keyword) {
        // TODO
    };

    $scope.selectedItemChange = function(item) {
        // TODO
    };

    $scope.querySearch = function(keyword) {
        // TODO
        return [
            {
                display: '九份'
            },
            {
                display: '九份老街'
            },
            {
                display: '九份阿柑姨芋圓'
            }
        ];
    };



    $scope.spotsErrataList = [
        {
            title: '九份老街',
            state: 'wait',
            stateText: '等待審查',
            replyMsg: '',
            submitDate: '2015年8月24日 10:23'
        },
        {
            link: '#',
            title: '台北 101 大樓',
            state: 'success',
            stateText: '審核通過',
            replyMsg: '審查通過，感謝您提供資訊',
            submitDate: '2015年8月24日 10:23'
        },
        {
            title: '台北 101 大樓',
            state: 'success',
            stateText: '審核通過',
            replyMsg: '審查通過，感謝您提供資訊',
            submitDate: '2015年8月24日 10:23'
        },
        {
            title: '台北 101 大樓',
            state: 'success',
            stateText: '審核通過',
            replyMsg: '審查通過，感謝您提供資訊',
            submitDate: '2015年8月24日 10:23'
        },
        {
            title: '台北 101 大樓',
            state: 'fail',
            stateText: '審核通過',
            replyMsg: '審查失敗，部分資訊違反智慧財產權',
            submitDate: '2015年8月24日 10:23'
        },
        {
            title: '台北 101 大樓',
            state: 'success',
            stateText: '審核通過',
            replyMsg: '審查通過，感謝您提供資訊',
            submitDate: '2015年8月24日 10:23'
        },
        {
            title: '台北 101 大樓',
            state: 'success',
            stateText: '審核通過',
            replyMsg: '審查通過，感謝您提供資訊',
            submitDate: '2015年8月24日 10:23'
        },
        {
            title: '台北 101 大樓',
            state: 'success',
            stateText: '審核通過',
            replyMsg: '審查通過，感謝您提供資訊',
            submitDate: '2015年8月24日 10:23'
        },
        {
            title: '台北 101 大樓',
            state: 'success',
            stateText: '審核通過',
            replyMsg: '審查通過，感謝您提供資訊',
            submitDate: '2015年8月24日 10:23'
        },
        {
            title: '台北 101 大樓',
            state: 'success',
            stateText: '審核通過',
            replyMsg: '審查通過，感謝您提供資訊',
            submitDate: '2015年8月24日 10:23'
        }
    ];

    $scope.editSpotsInfo = function (ev) {
      $mdDialog.show({
        controller: function ($scope, $mdDialog, $timeout) {
          $scope.spotData = {
            stay: 'minute'
          };
          $scope.currentTab = 1;
          $scope.switchTab = function (tab) {
            $scope.currentTab = tab;
          };
          $scope.cancel = function() {
            $mdDialog.cancel();
          };
          $scope.save = function(answer) {
            $scope.saving = true;
            $timeout(function () {
              $scope.saving = false;
              $scope.editSuccess = true;
            }, 1000);
          };
          $scope.slideList = [
            {
              "imgUrl" : "../public/images/spots/spot-1.jpg"
            },
            {
              "imgUrl" : "../public/images/spots/spot-2.jpg"
            },
            {
              "imgUrl" : "../public/images/spots/spot-3.jpg"
            },
            {
              "imgUrl" : "../public/images/spots/spot-4.jpg"
            },
            {
              "imgUrl" : "../public/images/spots/spot-5.jpg"
            },
            {
              "imgUrl" : "../public/images/spots/spot-6.jpg"
            },
            {
              "imgUrl" : "../public/images/spots/spot-7.jpg"
            },
            {
              "imgUrl" : "../public/images/spots/spot-1.jpg"
            },
            {
              "imgUrl" : "../public/images/spots/spot-2.jpg"
            },
            {
              "imgUrl" : "../public/images/spots/spot-3.jpg"
            },
            {
              "imgUrl" : "../public/images/spots/spot-4.jpg"
            },
            {
              "imgUrl" : "../public/images/spots/spot-5.jpg"
            }
          ];

          $scope.currentSlideIdx = 0;

          $scope.prevSlide = function () {
            $scope.currentSlideIdx = $scope.currentSlideIdx > 0 ? $scope.currentSlideIdx - 1 : $scope.slideList.length - 1;
          };

          $scope.nextSlide = function () {
            $scope.currentSlideIdx = $scope.currentSlideIdx < ($scope.slideList.length - 1) ? $scope.currentSlideIdx + 1 : 0;
          };

          $scope.setSlide = function (idx) {
            $scope.currentSlideIdx = idx;
          };

          $scope.uploadPhoto = function () {};

          $scope.citys = [
            {
              id: 1,
              name: '基隆市'
            },
            {
              id: 2,
              name: '台北市'
            },
            {
              id: 3,
              name: '新北市'
            },
            {
              id: 4,
              name: '桃園市'
            },
            {
              id: 5,
              name: '新竹市'
            },
            {
              id: 6,
              name: '新竹縣'
            },
            {
              id: 7,
              name: '宜蘭縣'
            },
            {
              id: 8,
              name: '苗栗縣'
            },
            {
              id: 9,
              name: '台中市'
            },
            {
              id: 10,
              name: '彰化縣'
            },
            {
              id: 11,
              name: '南投縣'
            },
            {
              id: 12,
              name: '雲林縣'
            },
            {
              id: 13,
              name: '嘉義市'
            },
            {
              id: 14,
              name: '嘉義縣'
            },
            {
              id: 15,
              name: '台南市'
            },
            {
              id: 16,
              name: '高雄市'
            },
            {
              id: 17,
              name: '屏東縣'
            },
            {
              id: 18,
              name: '澎湖縣'
            },
            {
              id: 19,
              name: '花蓮縣'
            },
            {
              id: 20,
              name: '台東縣'
            },
            {
              id: 21,
              name: '金門縣'
            },
            {
              id: 22,
              name: '連江縣'
            }
          ];
          $scope.spotData.city = $scope.citys[1].id;

          $scope.spotData.labels = [
            {
              name: '觀光藝文',
              checked: true
            },
            {
              name: '商圈',
              checked: true
            },
            {
              name: '體育健身',
              checked: false
            },
            {
              name: '遊憩公園',
              checked: false
            },
            {
              name: '文化藝術',
              checked: false
            },
            {
              name: '國家風景區',
              checked: false
            },
            {
              name: '古蹟廟宇',
              checked: false
            },
            {
              name: '溫泉',
              checked: false
            },
            {
              name: '自然生態',
              checked: false
            },
            {
              name: '休閒農業',
              checked: false
            }
          ];

          $scope.spotData.otherLabels = [
            {
              text: '台灣小吃'
            }
          ];

          $scope.tempSpotData = {};

          $scope.editSpotsCategory = function () {
            $scope.showSpotCategory = true;
            $scope.tempSpotData = angular.copy($scope.spotData);
            $scope.tempSpotData.hasOtherLabels = true;
          };

          $scope.cancelEditSpotsCategory = function () {
            $scope.showSpotCategory = false;
          };

          $scope.saveCategory = function () {
            $scope.onSavingCategory = true;
            $timeout(function () {
              $scope.spotData = angular.copy($scope.tempSpotData);
              $scope.onSavingCategory = false;
              $scope.showSpotCategory = false;
              if (!$scope.spotData.hasOtherLabels) {
                $scope.spotData.otherLabels.length = 0;
              }
            }, 1000);
          };

          $scope.addCategory = function (category) {
            $scope.spotData.otherLabels.push(category);
          };

          $scope.removeCategory = function (index) {
            $scope.spotData.otherLabels.splice(index, 1);
          };

        },
        templateUrl: '../assets/common/partials/addSpotsInfoMadal.html',
        targetEvent: ev,
      })
      .then(function(answer) {
        $scope.alert = 'You said the information was "' + answer + '".';
      }, function() {
        $scope.alert = 'You cancelled the dialog.';
      });
    };

  }
);

"use strict";

angular.module('ng.controller').controller('userFunthingsController' ,
  function( userService, $mdSidenav, $mdBottomSheet, $log, $q, $scope, $mdDialog) {
    angular.module('angularWaterfallApp',["ngWaterfall","ui.router"])
    $scope.filters = { };
     $scope.clearFilter = function() {
      $scope.filters = {};
    };
    
    $scope.showNO = function(type){
        return type.type==4||type.type==1||type.type==2||type.type==3;
    };
    
    
//type 0:已發佈 1:審核中 2:未通過 3:尚未添加地點 4:無效的趣事位置
$scope.collectList = [
      {
        thumbnail: '../public/images/spots/spot-3.jpg',
        where: "九份",
        descript: "最後一場《小清晰》，窗口裡最後一位售票員。最後一場《小清晰》，窗口裡最",
        thumb:30,
        date:"2016年10月1日",
        type:0,
        isthumb:true,
        ismov:false
      },{
        thumbnail: '../public/images/spots/spot-1.jpg',
        where: "太魯閣國家公園",
        descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式，大廳中央放置蔣介石坐姿銅像，底座部份設有展覽室和放映室，陳列蔣介石的一些文物，供民眾參觀。",
        thumb:30,
        date:"2016年10月1日",
        type:1,
        isthumb:false,
        ismov:true
      },{
        thumbnail: '../public/images/spots/spot-2.jpg',
        where: "九份",
        descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式，大廳中央放置蔣介石坐姿銅像，底座部份設有展覽室和放映室，陳列蔣介石的一些文物，供民眾參觀。",
        thumb:30,
        date:"2016年10月1日",
        type:2,
        isthumb:false,
        ismov:false
      },{
        thumbnail: '../public/images/spots/spot-4.jpg',
        where: "九份",
        descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式，大廳中央放置蔣介石坐姿銅像，底座部份設有展覽室和放映室，陳列蔣介石的一些文物，供民眾參觀。",
        thumb:30,
        date:"2016年10月1日",
        type:3,
        isthumb:true,
        ismov:true
      },{
        thumbnail: '../public/images/spots/spot-5.jpg',
        where: "九份",
        descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式，大廳中央放置蔣介石坐姿銅像，底座部份設有展覽室和放映室，陳列蔣介石的一些文物，供民眾參觀。",
        thumb:30,
        date:"2016年10月1日",
        type:4,
        isthumb:false,
        ismov:false
      },{
        thumbnail: '../public/images/spots/spot-1.jpg',
        where: "九份",
        descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式，大廳中央放置蔣介石坐姿銅像，底座部份設有展覽室和放映室，陳列蔣介石的一些文物，供民眾參觀。",
        thumb:30,
        date:"2016年10月1日",
        type:0,
        isthumb:false,
        ismov:false
      },{
        thumbnail: '../public/images/spots/spot-2.jpg',
        where: "九份",
        descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式，大廳中央放置蔣介石坐姿銅像，底座部份設有展覽室和放映室，陳列蔣介石的一些文物，供民眾參觀。",
        thumb:30,
        date:"2016年10月1日",
        type:0,
        isthumb:false,
        ismov:false
      },{
        thumbnail: '../public/images/spots/spot-3.jpg',
        where: "九份",
        descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式，大廳中央放置蔣介石坐姿銅像，底座部份設有展覽室和放映室，陳列蔣介石的一些文物，供民眾參觀。",
        thumb:30,
        date:"2016年10月1日",
        type:0,
        isthumb:false,
        ismov:false
      },{
        thumbnail: '../public/images/spots/spot-1.jpg',
        where: "九份",
        descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式，大廳中央放置蔣介石坐姿銅像，底座部份設有展覽室和放映室，陳列蔣介石的一些文物，供民眾參觀。",
        thumb:30,
        date:"2016年10月1日",
        type:1,
        isthumb:false,
        ismov:false
      },{
        thumbnail: '../public/images/spots/spot-1.jpg',
        where: "九份",
        descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式，大廳中央放置蔣介石坐姿銅像，底座部份設有展覽室和放映室，陳列蔣介石的一些文物，供民眾參觀。",
        thumb:30,
        date:"2016年10月1日",
        type:3,
        isthumb:false,
        ismov:false
      },{
        thumbnail: '../public/images/foods/food-1.jpg',
        where: "九份",
        descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式，大廳中央放置蔣介石坐姿銅像，底座部份設有展覽室和放映室，陳列蔣介石的一些文物，供民眾參觀。",
        thumb:30,
        date:"2016年10月1日",
        type:2,
        isthumb:false,
        ismov:true
      },{
        thumbnail: '../public/images/foods/food-1.jpg',
        where: "九份",
        descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式，大廳中央放置蔣介石坐姿銅像，底座部份設有展覽室和放映室，陳列蔣介石的一些文物，供民眾參觀。",
        thumb:30,
        date:"2016年10月1日",
        type:0,
        isthumb:true,
        ismov:false
      }
      
    ];
$scope.showFunDetail = function (ev) {
  $mdDialog.show({
    controller: function ($scope, $mdDialog, $timeout,items,nfilter) {
      $scope.nowList = items;
      $scope.nIndex = ev;
      $scope.filters = nfilter;
      
      $scope.cancel = function() {
        $mdDialog.cancel();
      };
      $scope.toPrev = function() {
      if($scope.nIndex>0){
              $scope.nIndex --;
                            
        }
      };
      $scope.toNext = function() {
      if($scope.nIndex<$scope.nowList.length-1){
             $scope.nIndex ++;
            
            
        }
      };
    },
    templateUrl: '../assets/common/partials/funthingsDetailView.html',
    locals: {
          items: $scope.collectList,
          nfilter: $scope.filters
        },
    targetEvent: ev,
  })
  .then(function(answer) {
    $scope.alert = 'You said the information was "' + answer + '".';
  }, function() {
    $scope.alert = 'You cancelled the dialog.';
  });
};
  }
);

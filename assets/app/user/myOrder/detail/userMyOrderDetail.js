"use strict";

angular.module('ng.controller').controller('userMyOrderDetailController' ,
  function( userService, $mdSidenav, $mdBottomSheet, $log, $q, $scope, $mdDialog) {



    $scope.purchaseReturn = function (ev) {
      $mdDialog.show({
        controller: function ($scope, $mdDialog, $timeout) {
          $scope.step = 1;
          $scope.returnData = {
            reasons: {
              personal: false,
              quality: false,
              notMatch: false,
              failsConventions: false,
              notReceived: false,
              other: false
            }
          };
          $scope.haveReturnReason = function () {
            return $scope.returnData.reasons.personal || 
              $scope.returnData.reasons.quality || 
              $scope.returnData.reasons.notMatch || 
              $scope.returnData.reasons.failsConventions || 
              $scope.returnData.reasons.notReceived || 
              $scope.returnData.reasons.other;
          };
          $scope.cancel = function () {
            $mdDialog.cancel();
          };
          $scope.backStep = function() {
            $scope.step = $scope.step - 1;
            if ($scope.step < 1) {
              $scope.step = 1;
            }
          };
          $scope.setStep = function(step) {
            $scope.step = step;
          };
          $scope.submit = function () {
            $scope.submitting = true;
            $timeout(function () {
              $scope.submitting = false;
              $scope.hasSubmit = true;
            }, 1000);
          };
        },
        templateUrl: '../assets/common/partials/purchaseReturnMadal.html',
        targetEvent: ev,
      })
      .then(function(answer) {
        $scope.alert = 'You said the information was "' + answer + '".';
      }, function() {
        $scope.alert = 'You cancelled the dialog.';
      });
    };



    $scope.orderComment = function (ev) {
      $mdDialog.show({
        controller: function ($scope, $mdDialog, $timeout) {
          $scope.cancel = function () {
            $mdDialog.cancel();
          };
          $scope.submit = function () {
            $scope.submitting = true;
            $timeout(function () {
              $scope.submitting = false;
              $scope.hasSubmit = true;
              $mdDialog.cancel();
            }, 1000);
          };
        },
        templateUrl: '../assets/common/partials/orderCommentMadal.html',
        targetEvent: ev,
      })
      .then(function(answer) {
        $scope.alert = 'You said the information was "' + answer + '".';
      }, function() {
        $scope.alert = 'You cancelled the dialog.';
      });
    };

    

  }
);

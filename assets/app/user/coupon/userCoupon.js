"use strict";

angular.module('ng.controller').controller('userCouponController' ,
  function( userService, $mdSidenav, $mdBottomSheet, $log, $q, $scope, $mdDialog) {



    $scope.searchTextChange = function(keyword) {
        // TODO
    };

    $scope.selectedItemChange = function(item) {
        // TODO
    };
    //changemode  0:可兌換 1:已兌換 2:餘額不足
    $scope.couponlist = [
    {
        thumbnail: '../public/images/spots/spot-1.jpg',
        title: "台北 - 水舞饌(大直店)",
        summary: '2015年5月6日~2015年7月2日 遊歷 5 縣市 10 景點',
        descript: "格上汽車租賃(股)公司有效時間：2015-11-22 至 2016-02-29",
        coin:5,
        changemode:0,
    },
    {
        thumbnail: '../public/images/foods/food-2.jpg',
        title: "台北 - 水舞饌(大直店)",
        summary: '2015年5月6日~2015年7月2日 遊歷 5 縣市 10 景點',
        descript: "格上汽車租賃(股)公司有效時間：2015-11-22 至 2016-02-29",
        coin:50,
        changemode:1,
    },
    {
        thumbnail: '../public/images/foods/food-3.jpg',
        title: "台北 - 水舞饌(大直店)",
        summary: '2015年5月6日~2015年7月2日 遊歷 5 縣市 10 景點',
        descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。",
        coin:50,
        changemode:2,
    },
    {
        thumbnail: '../public/images/foods/food-4.jpg',
        title: "台北 - 水舞饌(大直店)",
        summary: '2015年5月6日~2015年7月2日 遊歷 5 縣市 10 景點',
        descript: "格上汽車租賃(股)公司有效時間：2015-11-22 至 2016-02-29",
        coin:5,
        changemode:0,

    },
    {
        thumbnail: '../public/images/foods/food-3.jpg',
        title: "台北 - 水舞饌(大直店)",
        summary: '2015年5月6日~2015年7月2日 遊歷 5 縣市 10 景點',
        descript: "格上汽車租賃(股)公司有效時間：2015-11-22 至 2016-02-29",
        coin:5,
        changemode:0,

    },
    {
        thumbnail: '../public/images/foods/food-4.jpg',
        title: "台北 - 水舞饌(大直店)",
        summary: '2015年5月6日~2015年7月2日 遊歷 5 縣市 10 景點',
        descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式",
        coin:5,
        changemode:0,

    }
    ];
    $scope.filters = { };
     $scope.clearFilter = function() {
      $scope.filters = {};
    };
    $scope.querySearch = function(keyword) {
        // TODO
        return [
        {
            display: '九份'
        },
        {
            display: '九份老街'
        },
        {
            display: '九份阿柑姨芋圓'
        }
        ];
    };
       $scope.toCoin = function (ev) {
      $mdDialog.show({
        controller: function ($scope, $mdDialog, $timeout) {
          $scope.report = {
            reasons: {
              stopSales: false,
              other: false
            }
          };
          $scope.haveReportReason = function () {
            return $scope.report.reasons.stopSales || 
              $scope.report.reasons.other;
          };
          $scope.cancel = function() {
            $mdDialog.cancel();
          };
          $scope.sendReport = function(answer) {
            $scope.reporting = true;
            $timeout(function () {
              $scope.reporting = false;
              $scope.reportSuccess = true;
            }, 1000);
          };
           $scope.Reportfailed = function(answer) {
              $scope.reporting = true;
            $timeout(function () {
              $scope.reporting = false;
            $scope.reportFailed = true;
            }, 1000);
          };
        },
        templateUrl: '../assets/common/partials/spendCoinMadal.html',
        targetEvent: ev,
      })
      .then(function(answer) {
        $scope.alert = 'You said the information was "' + answer + '".';
      }, function() {
        $scope.alert = 'You cancelled the dialog.';
      });
    };

}
);

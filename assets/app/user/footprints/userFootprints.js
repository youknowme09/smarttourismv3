"use strict";

angular.module('ng.controller').controller('userFootprintsController' ,
  function( userService, $mdSidenav, $mdBottomSheet, $log, $q, $scope) {

    $scope.citys = [
      {
        thumbnail: '../public/images/spots/spot-1.jpg',
        city: '台北市',
        spotsCount: 57
      },
      {
        thumbnail: '../public/images/spots/spot-2.jpg',
        city: '新北市',
        spotsCount: 72
      },
      {
        thumbnail: '../public/images/spots/spot-3.jpg',
        city: '桃園市',
        spotsCount: 17
      },
      {
        thumbnail: '../public/images/spots/spot-4.jpg',
        city: '高雄市',
        spotsCount: 20
      }
    ];

  }
);

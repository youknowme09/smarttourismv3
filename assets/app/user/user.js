"use strict";

angular.module('ng.controller').controller('userController' ,
  function( userService, $mdSidenav, $mdBottomSheet, $log, $q, $scope, $state, $mdDialog) {

   $scope.$state = $state;

   var vm = $scope;

   $scope.slideList = [
   {
    "imgUrl" : "../public/images/spots/spot-1.jpg"
  },
  {
    "imgUrl" : "../public/images/spots/spot-2.jpg"
  },
  {
    "imgUrl" : "../public/images/spots/spot-3.jpg"
  }
  ];

  $scope.currentSlideIdx = 0;

  $scope.prevSlide = function () {
    $scope.currentSlideIdx = $scope.currentSlideIdx > 0 ? $scope.currentSlideIdx - 1 : $scope.slideList.length - 1;
  };

  $scope.nextSlide = function () {
    $scope.currentSlideIdx = $scope.currentSlideIdx < ($scope.slideList.length - 1) ? $scope.currentSlideIdx + 1 : 0;
  };

  $scope.setSlide = function (idx) {
    $scope.currentSlideIdx = idx;
  };

  $scope.editPersonalInfo = function (ev) {
    $mdDialog.show({
      controller: function ($scope, $mdDialog, $timeout) {
       $scope.gender = -1;
       $scope.years = [];
       for (var i = 1900; i <= 2015; i++) {
         $scope.years.push(i);
       }
       $scope.cancel = function() {
        $mdDialog.cancel();
      };
      $scope.save = function(answer) {
        $scope.saving = true;
        $timeout(function () {
          $scope.saving = false;
          $scope.saveSuccess = true;
          $mdDialog.hide();
        }, 1000);
      };
      $scope.resetpassword = function() {
        $mdDialog.hide();
        vm.resetpassword();
      };
    },
    templateUrl: '../assets/common/partials/editPersonalInfoMadal.html',
    targetEvent: ev,
  })
    .then(function(answer) {
      $scope.alert = 'You said the information was "' + answer + '".';
    }, function() {
      $scope.alert = 'You cancelled the dialog.';
    });
  };


  $scope.resetpassword = function (ev) {
    $mdDialog.show({
      controller: function ($scope, $mdDialog, $timeout) {
        $scope.cancel = function () {
          $mdDialog.cancel();
        };
        $scope.reset = function () {
          $scope.reseting = true;
          $timeout(function () {
            $scope.reseting = false;
            $scope.hasReset = true;
          }, 1000);
        };
      },
      templateUrl: '../assets/common/partials/resetpasswordMadal.html',
      targetEvent: ev,
    })
    .then(function(answer) {
      $scope.alert = 'You said the information was "' + answer + '".';
    }, function() {
      $scope.alert = 'You cancelled the dialog.';
    });
  };


  $scope.cancelBindFacebook = function (social, ev) {
    $mdDialog.show({
      controller: function ($scope, $mdDialog, $timeout) {
        $scope.social = social;
        $scope.cancel = function () {
          $mdDialog.cancel();
        };
        $scope.confirm = function () {
          $scope.doing = true;
          $timeout(function () {
            $scope.doing = false;
            $mdDialog.hide();
          }, 1000);
        };
      },
      templateUrl: '../assets/common/partials/cancelBindSocialMadal.html',
      targetEvent: ev,
    })
    .then(function(answer) {
      $scope.alert = 'You said the information was "' + answer + '".';
    }, function() {
      $scope.alert = 'You cancelled the dialog.';
    });
  };
  $scope.scheduleList = [
  {
    thumbnail: '../public/images/spots/spot-1.jpg',
    title: "悠閒台南3日遊",
    summary: '2015年5月6日~2015年7月2日 遊歷 5 縣市 10 景點',
    descript: "台中近來的新人氣景點，就是在台中火車站前的 宮原眼科，如果你沒來過的話，就遜了啊～宮原眼科現在可不是眼科，可是早期宮原眼科的建築。",
    isprivate:true,
    view:20,
    comment:3,
    days:15,
    draft:false,
  },
  {
    thumbnail: '../public/images/foods/food-2.jpg',
    title: "文化的探索",
    summary: '2015年5月6日~2015年7月2日 遊歷 5 縣市 10 景點',
    descript: "台中近來的新人氣景點，就是在台中火車站前的 宮原眼科，如果你沒來過的話，就遜了啊～宮原眼科現在可不是眼科，可是早期宮原眼科的建築。",
    isprivate:true,
    view:330,
    comment:3,
    days:10,
    draft:true,
  },
  {
    thumbnail: '../public/images/foods/food-3.jpg',
    title: "九份老石屋 藍海房",
    summary: '2015年5月6日~2015年7月2日 遊歷 5 縣市 10 景點',
    descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式，大廳中央放置蔣介石坐姿銅像，底座部份設有展覽室和放映室，陳列蔣介石的一些文物，供民眾參觀。",
    isprivate:false,
    view:10020,
    comment:3,
    days:3,
    draft:false,

  },
  {
    thumbnail: '../public/images/foods/food-4.jpg',
    title: "國立故宮博物院",
    summary: '2015年5月6日~2015年7月2日 遊歷 5 縣市 10 景點',
    descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式，大廳中央放置蔣介石坐姿銅像，底座部份設有展覽室和放映室，陳列蔣介石的一些文物，供民眾參觀。",
    isprivate:false,
    view:5550,
    comment:3,
    days:7,
    draft:false,

  },
  {
    thumbnail: '../public/images/foods/food-3.jpg',
    title: "九份老石屋 藍海房",
    summary: '2015年5月6日~2015年7月2日 遊歷 5 縣市 10 景點',
    descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式，大廳中央放置蔣介石坐姿銅像，底座部份設有展覽室和放映室，陳列蔣介石的一些文物，供民眾參觀。",
    isprivate:false,
    view:10020,
    comment:3,
    days:3,
    draft:false,

  },
  {
    thumbnail: '../public/images/foods/food-4.jpg',
    title: "國立故宮博物院",
    summary: '2015年5月6日~2015年7月2日 遊歷 5 縣市 10 景點',
    descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式，大廳中央放置蔣介石坐姿銅像，底座部份設有展覽室和放映室，陳列蔣介石的一些文物，供民眾參觀。",
    isprivate:false,
    view:5550,
    comment:3,
    days:7,
    draft:false,

  }
  ];
   $scope.travelNotes = [
  {
    thumbnail: '../public/images/spots/spot-4.jpg',
    title: "[食記]台中市‧台中新地標‧宮原眼科掛號吃冰",
    summary: '遊歷 5 縣市 10 景點',
    descript: "台中近來的新人氣景點，就是在台中火車站前的 宮原眼科，如果你沒來過的話，就遜了啊～宮原眼科現在可不是眼科，可是早期宮原眼科的建築，經過台中知名糕餅業 日出 改裝包裝，將原本幾近頹傾的建築予以最大部份的保留，並且再加上現代的設計元素，從此變成 日出最特別的旗艦店，販售大部份日出的糕點，而且還有好吃的冰淇淋、珍珠奶茶。",
    isprivate:true,
    view:20,
    comment:3,
    time:"2015/05/16",
    draft:false,
  },
  {
    thumbnail: '../public/images/foods/food-2.jpg',
    title: "文化的探索",
    summary: '遊歷 5 縣市 10 景點',
    descript: "台中近來的新人氣景點，就是在台中火車站前的 宮原眼科，如果你沒來過的話，就遜了啊～宮原眼科現在可不是眼科，可是早期宮原眼科的建築。",
    isprivate:true,
    view:330,
    comment:3,
    time:"2015/05/16",
    draft:true,
  },
  {
    thumbnail: '../public/images/foods/food-3.jpg',
    title: "九份老石屋 藍海房",
    summary: '遊歷 5 縣市 10 景點',
    descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式，大廳中央放置蔣介石坐姿銅像，底座部份設有展覽室和放映室，陳列蔣介石的一些文物，供民眾參觀。",
    isprivate:false,
    view:10020,
    comment:3,
    time:"2015/05/16",
    draft:false,

  },
  {
    thumbnail: '../public/images/foods/food-4.jpg',
    title: "國立故宮博物院",
    summary: '遊歷 5 縣市 10 景點',
    descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式，大廳中央放置蔣介石坐姿銅像，底座部份設有展覽室和放映室，陳列蔣介石的一些文物，供民眾參觀。",
    isprivate:false,
    view:5550,
    comment:3,
    time:"2015/05/16",
    draft:false,

  },
  {
    thumbnail: '../public/images/foods/food-3.jpg',
    title: "九份老石屋 藍海房",
    summary: '遊歷 5 縣市 10 景點',
    descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式，大廳中央放置蔣介石坐姿銅像，底座部份設有展覽室和放映室，陳列蔣介石的一些文物，供民眾參觀。",
    isprivate:false,
    view:10020,
    comment:3,
    time:"2015/05/16",
    draft:false,

  },
  {
    thumbnail: '../public/images/foods/food-4.jpg',
    title: "國立故宮博物院",
    summary: '遊歷 5 縣市 10 景點',
    descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式，大廳中央放置蔣介石坐姿銅像，底座部份設有展覽室和放映室，陳列蔣介石的一些文物，供民眾參觀。",
    isprivate:false,
    view:5550,
    comment:3,
    time:"2015/05/16",
    draft:false,

  }
  ];
}
);

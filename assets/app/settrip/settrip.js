"use strict";

angular.module('ng.controller').controller('SetTripController' ,
  function( userService, $log, $q, $scope, $mdDialog,tripData) {
    $("#godate").dateRangePicker({stickyMonths: true,startOfWeek: 'monday',showTopbar: false,startDate: new Date(),getValue: function()
    {
      if ($('#godate').val() && $('#backdate').val() )
        return $('#godate').val() + ' to ' + $('#backdate').val();
      else
        return '';
    },
    setValue: function(s,s1,s2)
    {
      $('#godate').val(s1);
      $('#backdate').val(s2);
      checkVal();
    }});
    $("#backdate").dateRangePicker({stickyMonths: true,startOfWeek: 'monday',showTopbar: false,startDate: new Date(),getValue: function()
    {
      if ($('#godate').val() && $('#backdate').val() )
        return $('#godate').val() + ' to ' + $('#backdate').val();
      else
        return '';
    },
    setValue: function(s,s1,s2)
    {
      $('#godate').val(s1);
      $('#backdate').val(s2);
      checkVal();

    }});
    $scope.goplace = "";
    $scope.backplace  = "";
    $scope.zoneitems = ['不限', '基隆市', '臺北市', '新北市', '桃園市', '新竹縣', '新竹市', '苗栗縣', '臺中市', '彰化縣', '南投縣', '雲林縣', '嘉義縣', '嘉義市', '臺南市', '高雄市', '屏東縣', '宜蘭縣', '花蓮縣', '臺東縣', '澎湖縣', '金門縣', '馬祖', '離島'];
    $scope.themeitems = ['不限', '自然生態', '宗教信仰', '歷史古蹟', '文創藝術', '離島冒險', '夜市商圈', '休閒農業', '遊憩樂活', '養生溫泉', '體育健康', '其他'];
    $scope.trafficitems = ['公眾運輸', '自行開車'];

    $scope.gosearch = true;
    $scope.backsearch = true;
    $scope.querySearch = function(keyword) {

      var Narray = [
      {
        display: '九份'
      },
      {
        display: '九份老街'
      },
      {
        display: '九份阿柑姨芋圓'
      }
      ];
      $scope.gosearch = true;
      return Narray;
    };
    $scope.searchTextChange = function(keyword) {
      checkVal();
    };
    $scope.selectedItemChange = function(item) {
      $scope.goplace = item;


    };

    $scope.querySearch2 = function(keyword) {

      var Narray = [

      ];
      $scope.backsearch = false;
      return Narray;
    };
    $scope.searchTextChange2 = function(keyword) {
      checkVal();

    };
    $scope.selectedItemChange2 = function(item) {
      $scope.backplace = item;


    };
    $scope.setZone = function($event,menuItem) {
      var n = $scope.zoneitems.indexOf(menuItem);
      if(n==0){
        $("#zone .trip-tag").removeClass( "select" );
        $("#zone .trip-tag").eq( 0 ).addClass( "select" );
      }else{
        if($("#zone .trip-tag").eq( n ).hasClass("select")){
          $("#zone .trip-tag").eq( n ).removeClass( "select" );
          
        }else{
          $("#zone .trip-tag").eq( n ).addClass( "select" );
        }


        $("#zone .trip-tag").eq( 0 ).removeClass( "select" );
      }
        //check for all select
        if($("#zone .trip-tag.select").length==($scope.zoneitems.length-1)){
          $("#zone .trip-tag").removeClass( "select" );
          $("#zone .trip-tag").eq( 0 ).addClass( "select" );
        }
        if($("#zone .trip-tag.select").length==0){
          $("#zone .trip-tag").eq( 0 ).addClass( "select" );
        }
        

      }
      $scope.setTheme = function($event,menuItem) {
        var n = $scope.themeitems.indexOf(menuItem);
         if(n==0){
        $("#theme .trip-tag").removeClass( "select" );
        $("#theme .trip-tag").eq( 0 ).addClass( "select" );
      }else{
        if($("#theme .trip-tag").eq( n ).hasClass("select")){
          $("#theme .trip-tag").eq( n ).removeClass( "select" );
          
        }else{
          $("#theme .trip-tag").eq( n ).addClass( "select" );
        }


        $("#theme .trip-tag").eq( 0 ).removeClass( "select" );
      }
        //check for all select
        if($("#theme .trip-tag.select").length==($scope.themeitems.length-1)){
          $("#theme .trip-tag").removeClass( "select" );
          $("#theme .trip-tag").eq( 0 ).addClass( "select" );
        }
        if($("#theme .trip-tag.select").length==0){
          $("#theme .trip-tag").eq( 0 ).addClass( "select" );
        }
        

      }
      $scope.setTraffic= function($event,menuItem) {
        var n = $scope.trafficitems.indexOf(menuItem);
        $("#traffic .trip-tag").removeClass( "select" );
        $("#traffic .trip-tag").eq( n ).addClass( "select" );
      }

      $scope.nextStep = function($event) {
     //get godate,backdate,goplace,backplace
     var godate = $('#godate').val();
     var backdate = $('#backdate').val();
     var goplace = $("#goplace input").val();
     var backplace = $("#backplace input").val();
     // get zone array 
     var zoneArray = [];
     $("#zone .trip-tag.select").each(function() {
      zoneArray.push($(this).text());
    });
    // get theme array
    var themeArray = [];
    $("#theme .trip-tag.select").each(function() {
      themeArray.push($(this).text());
    });
    // get traffic 
    var trafficString = $("#traffic.trip-tag.select").text();
    location.href='/#/settripstep2';
    tripData.loadval()["godate"] = godate;
    tripData.loadval()["backdate"] = backdate;
    tripData.loadval()["goplace"] = goplace;
    tripData.loadval()["backplace"] = backplace;
    tripData.loadval()["zoneArray"] = zoneArray;
    tripData.loadval()["themeArray"] = themeArray;
    tripData.loadval()["traffic"] = trafficString;


  };
  function checkVal(){
    if($('#godate').val().length>0&&$('#backdate').val().length>0&&$("#goplace input").val().length>0&&$("#backplace input").val().length>0){

      $('.next-step button').addClass('done');
    }else{
      $('.next-step button').removeClass('done');
    }
  }


}
);
angular.module('ng.service').service('tripData',
  function ($q){
    var data = 
    {
      godate: '',
      backdate: '',
      goplace: '',
      backplace: '',
      traffic:'',
      zoneArray: [],
      themeArray: []
    }
    ;



  // Promise-based API
  return {
    loadval : function() {
      // Simulate async nature of real remote calls
      return data;
    }
  };
});

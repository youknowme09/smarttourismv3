"use strict";

angular.module('ng.controller').controller('FoodsController' ,
  function( userService, $mdSidenav, $mdBottomSheet, $log, $q, $scope) {

  	$scope.days = 1;
  	// $scope.layout = "List";
  	$scope.layout = "Grid";
  	$scope.minPrice;
  	$scope.maxPrice;
    $scope.isOpenFilter = false;

    $scope.getTypes = function (spot) {
      return spot.types.slice(0, 2).join("、");
    };

    $scope.closeFilter = function () {
      $scope.isOpenFilter = false;      
    };

    $scope.openFilter = function () {
      $scope.isOpenFilter = true;      
    };

    $scope.showAllOption = {};

    $scope.toggleMore = function (option) {
      $scope.showAllOption[option] = !$scope.showAllOption[option];
    };

    $scope.orderBy = 'topRated';

  	$scope.foods = [
  		{
  			title: "洋旗牛排餐廳",
  			descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
        istripadv:false,
        mustgo:true,
        rank: 4,
        view:999,
        comment:999,
        price:200,
        isCollect : false,
        label: '台灣名產',
        address: "台北市大同區長安西路33號",
        locationtag: "TW1",
        location: "台北",
        tags: ["優惠"],
        location2:{
          lat:25.0339031,
          lng:121.5645098
        }
  		},
  		{
        title: "洋旗牛排餐廳",
        descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
        istripadv:true,
        mustgo:true,
        rank: 3.2,
        view:999,
        comment:999,
        price:200,
        isCollect : false,
        label: '台灣名產',
        address: "台北市大同區長安西路33號",
        locationtag: "TW1",
        location: "台北",
        tags: ["優惠"],
        location2:{
          lat:25.0339031,
          lng:121.5645098
        }
      },
      {
        title: "洋旗牛排餐廳",
        descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
        istripadv:true,
        mustgo:true,
        rank: 4,
        view:999,
        comment:999,
        price:200,
        isCollect : false,
        label: '台灣名產',
        address: "台北市大同區長安西路33號",
        locationtag: "TW1",
        location: "台北",
        tags: ["優惠"],
        location2:{
          lat:25.0339031,
          lng:121.5645098
        }
      },
      {
        title: "洋旗牛排餐廳",
        descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
        istripadv:true,
        mustgo:true,
        rank: 4,
        view:999,
        comment:999,
        price:200,
        isCollect : false,
        label: '台灣名產',
        address: "台北市大同區長安西路33號",
        locationtag: "TW1",
        location: "台北",
        tags: ["優惠"],
        location2:{
          lat:25.0339031,
          lng:121.5645098
        }
      },
      {
        title: "洋旗牛排餐廳",
        descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
        istripadv:true,
        mustgo:true,
        rank: 4,
        view:999,
        comment:999,
        price:200,
        isCollect : false,
        label: '台灣名產',
        address: "台北市大同區長安西路33號",
        locationtag: "TW1",
        location: "台北",
        tags: ["優惠"],
        location2:{
          lat:25.0339031,
          lng:121.5645098
        }
      },
      {
        title: "洋旗牛排餐廳",
        descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
        istripadv:true,
        mustgo:true,
        rank: 4,
        view:999,
        comment:999,
        price:200,
        isCollect : false,
        label: '台灣名產',
        address: "台北市大同區長安西路33號",
        locationtag: "TW1",
        location: "台北",
        tags: ["優惠"],
        location2:{
          lat:25.0339031,
          lng:121.5645098
        }
      },
      {
        title: "洋旗牛排餐廳",
        descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
        istripadv:true,
        mustgo:true,
        rank: 4,
        view:999,
        comment:999,
        price:200,
        isCollect : false,
        label: '台灣名產',
        address: "台北市大同區長安西路33號",
        locationtag: "TW1",
        location: "台北",
        tags: ["優惠"],
        location2:{
          lat:25.0339031,
          lng:121.5645098
        }
      },
      {
        title: "洋旗牛排餐廳",
        descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
        istripadv:true,
        mustgo:true,
        rank: 4,
        view:999,
        comment:999,
        price:200,
        isCollect : false,
        label: '台灣名產',
        address: "台北市大同區長安西路33號",
        locationtag: "TW1",
        location: "台北",
        tags: ["優惠"],
        location2:{
          lat:25.0339031,
          lng:121.5645098
        }
      },
      {
        title: "洋旗牛排餐廳",
        descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
        istripadv:true,
        mustgo:true,
        rank: 4,
        view:999,
        comment:999,
        price:200,
        isCollect : false,
        label: '台灣名產',
        address: "台北市大同區長安西路33號",
        locationtag: "TW1",
        location: "台北",
        tags: ["優惠"],
        location2:{
          lat:25.0339031,
          lng:121.5645098
        }
      },
      {
        title: "洋旗牛排餐廳",
        descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
        istripadv:true,
        mustgo:true,
        rank: 4,
        view:999,
        comment:999,
        price:200,
        isCollect : false,
        label: '台灣名產',
        address: "台北市大同區長安西路33號",
        locationtag: "TW1",
        location: "台北",
        tags: ["優惠"],
        location2:{
          lat:25.0339031,
          lng:121.5645098
        }
      },
      {
        title: "洋旗牛排餐廳",
        descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
        istripadv:true,
        mustgo:true,
        rank: 4,
        view:999,
        comment:999,
        price:200,
        isCollect : false,
        label: '台灣名產',
        address: "台北市大同區長安西路33號",
        locationtag: "TW1",
        location: "台北",
        tags: ["優惠"],
        location2:{
          lat:25.0339031,
          lng:121.5645098
        }
      },
      {
        title: "洋旗牛排餐廳",
        descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
        istripadv:true,
        mustgo:true,
        rank: 4,
        view:999,
        comment:999,
        price:200,
        isCollect : false,
        label: '台灣名產',
        address: "台北市大同區長安西路33號",
        locationtag: "TW1",
        location: "台北",
        tags: ["優惠"],
        location2:{
          lat:25.0339031,
          lng:121.5645098
        }
      },

      

  	];
     angular.forEach($scope.foods, function(value, key) {
      value.rankstar = 0.5*Math.round(value.rank/0.5);
      value.rankstar = -160+value.rankstar*32;
    });


  	$scope.toggleCollect = function (trip) {
  		trip.isCollect = !trip.isCollect;
  	};

  	$scope.switchLayout = function (layout) {
  		$scope.layout = layout;
  	};


  }
);

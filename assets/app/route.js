

angular.module('ng.router').config(function($urlRouterProvider, $stateProvider) {
    $stateProvider.state('common', {
        abstract: true,
        url: '',
        templateUrl: '../assets/common/layout/layout.html'
    })

    .state('common.main', {
        url: "/",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/main/content.html',
                controller: 'UserController as ul'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('common.resetpassword', {
        url: "/resetpassword",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/resetpassword/resetpassword.html',
                controller: 'ResetPasswordController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('common.shoppingCart', {
        url: "/shoppingCart",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/shoppingCart/shoppingCart.html',
                controller: 'ShoppingCartController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('common.confirmOrder', {
        url: "/confirmOrder",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/confirmOrder/confirmOrder.html',
                controller: 'ConfirmOrderController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('common.search', {
        url: "/search",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/search/search.html',
                controller: 'searchController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })


    .state('common.theme', {
        url: "/theme",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/theme/theme.html',
                controller: 'SpotsController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('common.spots', {
        url: "/spots",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/spots/spots.html',
                controller: 'SpotsController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('common.spots.poi', {
        url: "/poi",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/spots/spotDetail/spotsDetail.html',
                controller: 'SpotsDetailController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('common.trips', {
        url: "/trips",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/trips/trips.html',
                controller: 'TripsController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('common.trips.poi', {
        url: "/poi",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/trips/tripDetail/tripsDetail.html',
                controller: 'TripsDetailController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('common.foods', {
        url: "/foods",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/foods/foods.html',
                controller: 'FoodsController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('common.foods.poi', {
        url: "/poi",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/foods/foodDetail/foodsDetail.html',
                controller: 'FoodsDetailController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('common.stays', {
        url: "/stays",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/stays/stays.html',
                controller: 'StaysController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('common.stays.poi', {
        url: "/poi",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/stays/stayDetail/staysDetail.html',
                controller: 'StaysDetailController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('common.events', {
        url: "/events",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/events/events.html',
                controller: 'EventsController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('common.events.poi', {
        url: "/poi",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/events/eventDetail/eventsDetail.html',
                controller: 'EventsDetailController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('common.stores', {
        url: "/stores",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/stores/stores.html',
                controller: 'StoresController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('common.stores.poi', {
        url: "/poi",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/stores/storeDetail/storesDetail.html',
                controller: 'StoresDetailController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('common.traffic', {
        url: "/traffic",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/traffic/traffic.html',
                controller: 'TrafficController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('common.traffic.poi', {
        url: "/poi",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/traffic/trafficDetail/trafficDetail.html',
                controller: 'TrafficDetailController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('common.taiwanInfo', {
        url: "/taiwanInfo",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/taiwanInfo/taiwanInfo.html',
                controller: 'TaiwanInfoController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('common.souvenir', {
        url: "/souvenir",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/souvenir/souvenir.html',
                controller: 'SouvenirController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('common.souvenir.poi', {
        url: "/poi",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/souvenir/souvenirDetail/souvenirDetail.html',
                controller: 'SouvenirDetailController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('common.purchases', {
        url: "/purchases",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/purchases/purchases.html',
                controller: 'PurchasesController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('common.purchases.spots', {
        url: "/spots",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/purchases/spots/purchasesSpots.html',
                controller: 'PurchasesSpotsController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('common.purchases.spots.poi', {
        url: "/poi",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/purchases/spots/spotDetail/purchasesSpotsDetail.html',
                controller: 'PurchasesSpotsDetailController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('common.purchases.events', {
        url: "/events",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/purchases/events/purchasesEvents.html',
                controller: 'PurchasesEventsController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('common.purchases.events.poi', {
        url: "/poi",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/purchases/events/eventDetail/purchasesEventsDetail.html',
                controller: 'PurchasesEventsDetailController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('common.purchases.foods', {
        url: "/foods",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/purchases/foods/purchasesFoods.html',
                controller: 'PurchasesFoodsController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('common.purchases.foods.poi', {
        url: "/poi",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/purchases/foods/foodDetail/purchasesFoodsDetail.html',
                controller: 'PurchasesFoodsDetailController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('common.purchases.stays', {
        url: "/stays",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/purchases/stays/purchasesStays.html',
                controller: 'PurchasesStaysController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('common.purchases.stays.poi', {
        url: "/poi",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/purchases/stays/stayDetail/purchasesStaysDetail.html',
                controller: 'PurchasesStaysDetailController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('common.purchases.trips', {
        url: "/trips",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/purchases/trips/purchasesTrips.html',
                controller: 'PurchasesTripsController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('common.purchases.trips.poi', {
        url: "/poi",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/purchases/trips/tripDetail/purchasesTripsDetail.html',
                controller: 'PurchasesTripsDetailController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('common.purchases.stores', {
        url: "/stores",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/purchases/stores/purchasesStores.html',
                controller: 'PurchasesStoresController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('common.purchases.stores.poi', {
        url: "/poi",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/purchases/stores/storeDetail/purchasesStoresDetail.html',
                controller: 'PurchasesStoresDetailController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('common.purchases.cars', {
        url: "/cars",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/purchases/cars/purchasesCars.html',
                controller: 'PurchasesCarsController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('common.purchases.cars.poi', {
        url: "/poi",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/purchases/cars/carDetail/purchasesCarsDetail.html',
                controller: 'PurchasesCarsDetailController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('common.travelNotes', {
        url: "/travelNotes",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/travelNotes/travelNotes.html',
                controller: 'TravelNotesController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('common.travelNotes.poi', {
        url: "/poi",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/travelNotes/travelNoteDetail/travelNotesDetail.html',
                controller: 'TravelNotesDetailController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('common.officialEvents', {
        url: "/officialEvents",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/officialEvents/officialEvents.html',
                controller: 'OfficialEventsController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('user', {
        abstract: true,
        url: '',
        templateUrl: '../assets/common/layout/userLayout.html',
        controller: 'userController'
    })

    .state('user.home', {
        url: "/user",
        views: {
            "header@user": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "userContainer@user": {
                templateUrl: '../assets/app/user/user.html',
                controller: 'userController'
            },
            "footer@user": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })
    .state('usersee', {
        abstract: true,
        url: '',
        templateUrl: '../assets/common/layout/userseeLayout.html',
        controller: 'userController'
    })

    .state('usersee.home', {
        url: "/usersee",
        views: {
            "header@usersee": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "userContainer@usersee": {
                templateUrl: '../assets/app/user/usersee.html',
                controller: 'userController'
            },
            "footer@usersee": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('user.notifications', {
        url: "/user/notifications",
        views: {
            "header@user": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "userContainer@user": {
                templateUrl: '../assets/app/user/notifications/userNotifications.html',
                controller: 'userNotificationsController'
            },
            "footer@user": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('user.calendar', {
        url: "/user/calendar",
        views: {
            "header@user": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "userContainer@user": {
                templateUrl: '../assets/app/user/calendar/calendar.html',
                controller: 'userCalendarController'
            },
            "footer@user": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('user.trips', {
        url: "/user/trips",
        views: {
            "header@user": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "userContainer@user": {
                templateUrl: '../assets/app/user/trips/userTrips.html',
                controller: 'userTripsController'
            },
            "footer@user": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })
    .state('user.funthings', {
        url: "/user/funthings",
        views: {
            "header@user": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "userContainer@user": {
                templateUrl: '../assets/app/user/funthings/userFunthings.html',
                controller: 'userFunthingsController'
            },
            "footer@user": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })
    .state('user.collect', {
        url: "/user/collect",
        views: {
            "header@user": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "userContainer@user": {
                templateUrl: '../assets/app/user/collect/userCollect.html',
                controller: 'userCollectController'
            },
            "footer@user": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('user.footprints', {
        url: "/user/footprints",
        views: {
            "header@user": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "userContainer@user": {
                templateUrl: '../assets/app/user/footprints/userFootprints.html',
                controller: 'userFootprintsController'
            },
            "footer@user": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('user.footprints.city', {
        url: "/city",
        views: {
            "header@user": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "userContainer@user": {
                templateUrl: '../assets/app/user/footprints/city/userFootprintsCity.html',
                controller: 'userFootprintsCityController'
            },
            "footer@user": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('user.travelNotes', {
        url: "/user/travelNotes",
        views: {
            "header@user": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "userContainer@user": {
                templateUrl: '../assets/app/user/travelNotes/userTravelNotes.html',
                controller: 'userTravelNotesController'
            },
            "footer@user": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('user.comments', {
        url: "/user/comments",
        views: {
            "header@user": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "userContainer@user": {
                templateUrl: '../assets/app/user/comments/userComments.html',
                controller: 'userCommentsController'
            },
            "footer@user": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('user.myOrder', {
        url: "/user/myOrder",
        views: {
            "header@user": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "userContainer@user": {
                templateUrl: '../assets/app/user/myOrder/userMyOrder.html',
                controller: 'userMyOrderController'
            },
            "footer@user": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('user.myOrder.detail', {
        url: "/detail",
        views: {
            "header@user": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "userContainer@user": {
                templateUrl: '../assets/app/user/myOrder/detail/userMyOrderDetail.html',
                controller: 'userMyOrderDetailController'
            },
            "footer@user": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('user.coupon', {
        url: "/user/coupon",
        views: {
            "header@user": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "userContainer@user": {
                templateUrl: '../assets/app/user/coupon/userCoupon.html',
                controller: 'userCouponController'
            },
            "footer@user": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('user.coupon.detail', {
        url: "/detail/:param1",
        views: {
            "header@user": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "userContainer@user": {
                templateUrl: '../assets/app/user/coupon/detail/userCouponDetail.html',
                controller: 'userCouponDetailController'
            },
            "footer@user": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('user.spotsErrata', {
        url: "/user/spotsErrata",
        views: {
            "header@user": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "userContainer@user": {
                templateUrl: '../assets/app/user/spotsErrata/userSpotsErrata.html',
                controller: 'userSpotsErrataController'
            },
            "footer@user": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('about', {
        abstract: true,
        url: '',
        templateUrl: '../assets/common/layout/aboutLayout.html',
        controller: 'aboutController'
    })

    .state('about.home', {
        url: "/about",
        views: {
            "header@about": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "aboutContainer@about": {
                templateUrl: '../assets/app/about/about.html',
                controller: 'aboutController'
            },
            "footer@about": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('about.privacy', {
        url: "/about/privacy",
        views: {
            "header@about": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "aboutContainer@about": {
                templateUrl: '../assets/app/about/privacy/privacy.html',
                controller: 'privacyController'
            },
            "footer@about": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('about.protection', {
        url: "/about/protection",
        views: {
            "header@about": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "aboutContainer@about": {
                templateUrl: '../assets/app/about/protection/protection.html',
                controller: 'protectionController'
            },
            "footer@about": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })
    .state('about.information', {
        url: "/about/information",
        views: {
            "header@about": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "aboutContainer@about": {
                templateUrl: '../assets/app/about/information/information.html',
                    //controller: 'protectionController'
                },
                "footer@about": {
                    templateUrl: '../assets/common/layout/footer.html'
                }
            },
            resolve: {

            }
        })

    .state('about.service', {
        url: "/about/service",
        views: {
            "header@about": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "aboutContainer@about": {
                templateUrl: '../assets/app/about/service/service.html',
                controller: 'serviceController'
            },
            "footer@about": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('about.announcement', {
        url: "/about/announcement",
        views: {
            "header@about": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "aboutContainer@about": {
                templateUrl: '../assets/app/about/announcement/announcement.html',
                controller: 'announcementController'
            },
            "footer@about": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('help', {
        abstract: true,
        url: '',
        templateUrl: '../assets/common/layout/helpLayout.html',
        controller: 'helpController'
    })

    .state('help.home', {
        url: "/help",
        views: {
            "header@help": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "helpContainer@help": {
                templateUrl: '../assets/app/help/help.html',
                controller: 'helpController'
            },
            "footer@help": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })

    .state('createTrip', {
        url: "/createTrip",
        templateUrl: '../assets/app/createTrip/createTrip.html',
        controller: 'CreateTripController'
    })

    .state('travelNotesEditor', {
        url: "/travelNotesEditor",
        templateUrl: '../assets/app/travelNotesEditor/travelNotesEditor.html',
        controller: 'TravelNotesEditorController'
    })

    .state('common.brand', {
        url: "/brand",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/brand/brand.html',
                controller: 'BrandController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })
    .state('common.brand.poi', {
        url: "/poi",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/brand/brandDetail/brandDetail.html',
                controller: 'BrandDetailController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })
    .state('common.themeEvents', {
        url: "/themeEvents",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/themeEvents/themeEvents.html',
                controller: 'themeEventsController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })
    .state('settrip', {
        url: "/settrip",
        templateUrl: '../assets/app/settrip/settrip.html',
        controller: 'SetTripController'
    })
    .state('settripstep2', {
        url: "/settripstep2",
        templateUrl: '../assets/app/settripstep2/settripstep2.html',
        controller: 'SetTripStep2Controller'
    })
.state('common.addpoi', {
        url: "/addpoi",
        views: {
            "header@common": {
                templateUrl: '../assets/common/layout/header.html',
                controller: 'headerCtrl'
            },
            "section@common": {
                templateUrl: '../assets/app/addpoi/addpoi.html',
                controller: 'AddPoiController'
            },
            "footer@common": {
                templateUrl: '../assets/common/layout/footer.html'
            }
        }
    })


});

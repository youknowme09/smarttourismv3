"use strict";

angular.module('ng.controller').controller('StaysController' ,
  function( userService, $mdSidenav, $mdBottomSheet, $log, $q, $scope) {

  	$scope.people = 1;
  	// $scope.layout = "List";
  	$scope.layout = "Grid";
  	$scope.minPrice;
  	$scope.maxPrice;
    $scope.isOpenFilter = false;

    $scope.closeFilter = function () {
      $scope.isOpenFilter = false;      
    };

    $scope.openFilter = function () {
      $scope.isOpenFilter = true;      
    };

    $scope.showAllOption = {};

    $scope.toggleMore = function (option) {
      $scope.showAllOption[option] = !$scope.showAllOption[option];
    };

    $scope.orderBy = 'topRated';

  	$scope.stays = [
  		{
  			title: "金瓜石 月河民宿 鄉村雙人雅房",
  			descript: "紅米青年旅館座落於台北火車站附近，位於高鐵、火車、巴士和捷運交會點。旅館的設計年輕有活力，大片的景觀窗戶搭配著繽紛色彩的交誼廳，讓您在高樓眺望台北的",
  			istripadv:true,
        mustgo:true,
        rank: 4,
        view:999,
        comment:999,
        price:200,
        isCollect : false,
        address: "台北市大同區長安西路33號",
        locationtag: "TW1",
        location: "台北",
        location2:{
          lat:21.9020441,
          lng:120.8529676
        }
  		},
  		{
  			title: "九份老石屋_藍海房",
  			descript: "紅米青年旅館座落於台北火車站附近，位於高鐵、火車、巴士和捷運交會點。旅館的設計年輕有活力，大片的景觀窗戶搭配著繽紛色彩的交誼廳，讓您在高樓眺望台北的",
  			istripadv:true,
        mustgo:false,
        rank: 4,
        view:999,
        comment:999,
        price:200,
        isCollect : false,
        address: "台北市大同區長安西路33號",
        locationtag: "TW1",
        location: "台北",
        location2:{
          lat:21.9020441,
          lng:120.8529676
        }
  		},
  		{
  			title: "療癒系公寓 乾淨又舒適的溫暖空間",
  			descript: "紅米青年旅館座落於台北火車站附近，位於高鐵、火車、巴士和捷運交會點。旅館的設計年輕有活力，大片的景觀窗戶搭配著繽紛色彩的交誼廳，讓您在高樓眺望台北的",
  			istripadv:true,
        mustgo:false,
        rank: 4,
        view:999,
        comment:999,
        price:200,
        isCollect : false,
        address: "台北市大同區長安西路33號",
        locationtag: "TW1",
        location: "台北",
        location2:{
          lat:21.9020441,
          lng:120.8529676
        }
  		},
  		{
  			title: "日月潭旅人小屋",
  			descript: "紅米青年旅館座落於台北火車站附近，位於高鐵、火車、巴士和捷運交會點。旅館的設計年輕有活力，大片的景觀窗戶搭配著繽紛色彩的交誼廳，讓您在高樓眺望台北的",
  			istripadv:true,
        mustgo:false,
        rank: 4,
        view:999,
        comment:999,
        price:200,
        isCollect : false,
        address: "台北市大同區長安西路33號",
        locationtag: "TW1",
        location: "台北",
        location2:{
          lat:21.9020441,
          lng:120.8529676
        }
  		},
  		{
  			title: "Woo House 典雅雙人房",
  			descript: "紅米青年旅館座落於台北火車站附近，位於高鐵、火車、巴士和捷運交會點。旅館的設計年輕有活力，大片的景觀窗戶搭配著繽紛色彩的交誼廳，讓您在高樓眺望台北的",
  			istripadv:true,
        mustgo:false,
        rank: 4,
        view:999,
        comment:999,
        price:200,
        isCollect : false,
        address: "台北市大同區長安西路33號",
        locationtag: "TW1",
        location: "台北",
        location2:{
          lat:21.9020441,
          lng:120.8529676
        }
  		},
  		{
  			title: "日月潭旅人小屋",
  			descript: "紅米青年旅館座落於台北火車站附近，位於高鐵、火車、巴士和捷運交會點。旅館的設計年輕有活力，大片的景觀窗戶搭配著繽紛色彩的交誼廳，讓您在高樓眺望台北的",
  			istripadv:true,
        mustgo:false,
        rank: 4,
        view:999,
        comment:999,
        price:200,
        isCollect : false,
        address: "台北市大同區長安西路33號",
        locationtag: "TW1",
        location: "台北",
        location2:{
          lat:21.9020441,
          lng:120.8529676
        }
  		},
  		{
  			title: "紅米國際青年旅館",
  			descript: "紅米青年旅館座落於台北火車站附近，位於高鐵、火車、巴士和捷運交會點。旅館的設計年輕有活力，大片的景觀窗戶搭配著繽紛色彩的交誼廳，讓您在高樓眺望台北的",
  			istripadv:true,
        mustgo:false,
        rank: 4,
        view:999,
        comment:999,
        price:200,
        isCollect : false,
        address: "台北市大同區長安西路33號",
        locationtag: "TW1",
        location: "台北",
        location2:{
          lat:21.9020441,
          lng:120.8529676
        }
  		},
  		{
  			title: "群悅青年旅館",
  			descript: "紅米青年旅館座落於台北火車站附近，位於高鐵、火車、巴士和捷運交會點。旅館的設計年輕有活力，大片的景觀窗戶搭配著繽紛色彩的交誼廳，讓您在高樓眺望台北的",
  			istripadv:true,
        mustgo:false,
        rank: 4,
        view:999,
        comment:999,
        price:200,
        isCollect : false,
        address: "台北市大同區長安西路33號",
        locationtag: "TW1",
        location: "台北",
        location2:{
          lat:21.9020441,
          lng:120.8529676
        }
  		},
  	];

  	$scope.toggleCollect = function (trip) {
  		trip.isCollect = !trip.isCollect;
  	};

  	$scope.switchLayout = function (layout) {
  		$scope.layout = layout;
  	};


  }
);

"use strict";

angular.module('ng.controller').controller('StaysDetailController',
    function(userService, $mdSidenav, $mdBottomSheet, $log, $q, $scope, $mdDialog) {



        $scope.bugReport = function(ev) {
            $mdDialog.show({
                    controller: function($scope, $mdDialog, $timeout) {
                        $scope.report = {
                            reasons: {
                                stopSales: false,
                                other: false
                            }
                        };
                        $scope.haveReportReason = function() {
                            return $scope.report.reasons.stopSales ||
                                $scope.report.reasons.other;
                        };
                        $scope.cancel = function() {
                            $mdDialog.cancel();
                        };
                        $scope.sendReport = function(answer) {
                            $scope.reporting = true;
                            $timeout(function() {
                                $scope.reporting = false;
                                $scope.reportSuccess = true;
                            }, 1000);
                        };
                    },
                    templateUrl: '../assets/common/partials/bugReportMadal.html',
                    targetEvent: ev,
                })
                .then(function(answer) {
                    $scope.alert = 'You said the information was "' + answer + '".';
                }, function() {
                    $scope.alert = 'You cancelled the dialog.';
                });
        };



        $scope.slideList = [{
            "imgUrl": "../public/images/stays/stay-1.jpg"
        }, {
            "imgUrl": "../public/images/stays/stay-2.jpg"
        }, {
            "imgUrl": "../public/images/stays/stay-3.jpg"
        }, {
            "imgUrl": "../public/images/stays/stay-4.jpg"
        }, {
            "imgUrl": "../public/images/stays/stay-5.jpg"
        }, {
            "imgUrl": "../public/images/stays/stay-6.jpg"
        }, {
            "imgUrl": "../public/images/stays/stay-7.jpg"
        }];

        $scope.currentSlideIdx = 0;

        $scope.prevSlide = function() {
            $scope.currentSlideIdx = $scope.currentSlideIdx > 0 ? $scope.currentSlideIdx - 1 : $scope.slideList.length - 1;
        };

        $scope.nextSlide = function() {
            $scope.currentSlideIdx = $scope.currentSlideIdx < ($scope.slideList.length - 1) ? $scope.currentSlideIdx + 1 : 0;
        };

        $scope.setSlide = function(idx) {
            $scope.currentSlideIdx = idx;
        };

        $scope.toggleBool = function(bool) {
            $scope[bool] = !$scope[bool];
        };



        $scope.tickets = [{
            title: '墾丁沙灘．小鹿斑比．仁武彩繪三日',
            description: '座落於東港渡船頭邊的黃昏市場，提供各式各樣現撈且物美價廉的海鮮魚貨而遠近馳名，攤位數達四百多個，目前已集中規劃為鮮魚區、生、熟食區、漁特產區及美食攤。',
            price: 120,
            specialPrice: 120
        }, {
            title: '鼎泰豐300元限量飲食現金折價券',
            description: '座落於東港渡船頭邊的黃昏市場，提供各式各樣現撈且物美價廉的海鮮魚貨而遠近馳名，攤位數達四百多個，目前已集中規劃為鮮魚區、生、熟食區、漁特產區及美食攤。',
            price: 120,
            specialPrice: 120
        }];



        $scope.staysSlide = {
            currentIdx: 0,
            prev: function() {
                $scope.staysSlide.currentIdx = $scope.staysSlide.currentIdx > 0 ? $scope.staysSlide.currentIdx - 1 : $scope.staysGroup.length - 1;
            },
            next: function() {
                $scope.staysSlide.currentIdx = $scope.staysSlide.currentIdx < ($scope.staysGroup.length - 1) ? $scope.staysSlide.currentIdx + 1 : 0;
            },
            set: function(idx) {
                $scope.staysSlide.currentIdx = idx;
            }
        };

        $scope.staysGroup = [
            [{
                title: '高級雙人或雙床間',
                price: 4235,
                maxPeople: 3,
                hasBreakfast: true,
                thumbnail: '../public/images/stays/stay-1.jpg'
            }, {
                title: 'Metropolis 雙床間',
                price: 4235,
                maxPeople: 3,
                hasBreakfast: true,
                thumbnail: '../public/images/stays/stay-2.jpg'
            }, {
                title: '高級雙人或雙床間',
                price: 4235,
                maxPeople: 3,
                hasBreakfast: true,
                thumbnail: '../public/images/stays/stay-3.jpg'
            }, {
                title: 'etropolis 雙床間',
                price: 4235,
                maxPeople: 3,
                hasBreakfast: true,
                thumbnail: '../public/images/stays/stay-4.jpg'
            }],
            [{
                title: '高級雙人或雙床間',
                price: 4235,
                maxPeople: 3,
                hasBreakfast: true,
                thumbnail: '../public/images/stays/stay-5.jpg'
            }, {
                title: 'Metropolis 雙床間',
                price: 4235,
                maxPeople: 3,
                hasBreakfast: true,
                thumbnail: '../public/images/stays/stay-6.jpg'
            }, {
                title: '高級雙人或雙床間',
                price: 4235,
                maxPeople: 3,
                hasBreakfast: true,
                thumbnail: '../public/images/stays/stay-7.jpg'
            }, {
                title: 'etropolis 雙床間',
                price: 4235,
                maxPeople: 3,
                hasBreakfast: true,
                thumbnail: '../public/images/stays/stay-8.jpg'
            }]
        ];

        $scope.videos = [{
            thumbnail: '../public/images/stays/stay-1.jpg',
            title: '紅米青年旅館台北導覽',
            author: '奧革士',
            viewcount: '33210',
            description: '從桃園機場搭乘國光客運機場巴士至終點站台北車站下車後，如何走到紅米青年旅館之路線­指引'
        }, {
            thumbnail: '../public/images/stays/stay-2.jpg',
            title: '國光客運台北車站下車處 to 紅米',
            author: '奧革士',
            viewcount: '33210',
            description: '在春秋時代，有一位愛國詩人叫屈原，因為無法順利幫助自己的君王避開敵國的陷害，太過傷心就跳入泪羅江自殺了，當地居民被他的愛國情操感動，就用竹葉包的糯米飯糰投進江中給魚吃，讓魚不要吃屈原的身體；而竹葉包的糯米飯糰就演變成現代的「粽子」，又相傳當時有人要尋找屈原的屍體，所以演變成今日的「划龍舟」。'
        }, {
            thumbnail: '../public/images/stays/stay-3.jpg',
            title: '紅米送愛心活動',
            author: '奧革士',
            viewcount: '33210',
            description: '江榮原先生阿原肥皂創辦人阿原工作室與阿原肥皂即是以創辦人江榮原先生為名。 創辦人江榮原先生，由於家庭因素而在高職畢業後就 ...'
        }, {
            thumbnail: '../public/images/stays/stay-4.jpg',
            title: '2014高空看煙火跨年夜雙人套票 限量300組 12月1日起全台開賣',
            author: '奧革士',
            viewcount: '33210',
            description: '台北101大樓，一轉眼已經啟用10年，或許不少國人跟外國遊客都曾經上過位於91樓­的戶外觀景台，但別以為最高就到此為止，其實台北101真的有這第101層，面積只有­62坪，還得多轉乘兩次電梯才能抵達，以往都是招待國外貴賓或者超級VIP，就連許多­101員工都不一定知道有這第101層，今天首度曝光，不只觀景視野更棒，同時也成為­業者週年慶噱頭，得消費101萬才有資格登上這難得開放的第101層。'
        }];

        $scope.tickets = [{
            title: '$100 鼎泰豐飲食現金券',
            price: 60
        }, {
            title: '鼎泰豐300元限量飲食現金折價券',
            price: 120
        }];

        $scope.activities = [{
            title: '食我鑰匙大放送',
            startDate: '2015/03/10',
            endDate: '2015/05/10',
            message: '下載 Smart Tourism Taiwan 台灣智慧觀光 APP 並完成註冊、登入，馬上獲得食我鑰匙10把序號乙組！'
        }, {
            title: 'Smart Tourism Taiwan 會員募集抽獎活動',
            startDate: '2015/03/10',
            endDate: '2015/05/10',
            message: '凡於活動期間下載 Smart Tourism Taiwan APP 並註冊成為會員，於行動裝置上的抽獎活動頁面登錄相關資訊，即可參加抽獎。'
        }];

        $scope.travelNotes = [
            [{
                title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
                descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
                rank: 4,
                types: ["文化", "樂活"],
                date: "2015/3/7",
                author: "奧革士",
                isCollect: false,
                location: "台北、桃園市、新竹市",
                collectCount: 234,
                recommend: true,
                comment: 233,
                view: 100
            }, {
                title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
                descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，園",
                rank: 4,
                types: ["文化", "樂活"],
                date: "2015/3/7",
                author: "奧革士",
                isCollect: true,
                location: "台北、桃園市、新竹市",
                collectCount: 234,
                recommend: false,
                comment: 233,
                view: 100
            }, {
                title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
                descript: "西門町位於台北市萬華區，由武昌街二段、漢中街、峨嵋街的行人徒步區所形成的區域為西門町的黃金地帶。大台北地區來往公車的密集交界，流行元素走在前端，是許多年輕學子及各國觀光客朝聖之處。",
                rank: 4,
                types: ["文化", "樂活"],
                date: "2015/3/7",
                author: "奧革士",
                isCollect: false,
                location: "台北、桃園市、新竹市",
                collectCount: 234,
                recommend: false,
                comment: 233,
                view: 100
            }],
            [{
                title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
                descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
                rank: 4,
                types: ["文化", "樂活"],
                date: "2015/3/7",
                author: "奧革士",
                isCollect: false,
                location: "台北、桃園市、新竹市",
                collectCount: 234,
                recommend: true,
                comment: 233,
                view: 100
            }, {
                title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
                descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，園",
                rank: 4,
                types: ["文化", "樂活"],
                date: "2015/3/7",
                author: "奧革士",
                isCollect: true,
                location: "台北、桃園市、新竹市",
                collectCount: 234,
                recommend: false,
                comment: 233,
                view: 100
            }, {
                title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
                descript: "西門町位於台北市萬華區，由武昌街二段、漢中街、峨嵋街的行人徒步區所形成的區域為西門町的黃金地帶。大台北地區來往公車的密集交界，流行元素走在前端，是許多年輕學子及各國觀光客朝聖之處。",
                rank: 4,
                types: ["文化", "樂活"],
                date: "2015/3/7",
                author: "奧革士",
                isCollect: false,
                location: "台北、桃園市、新竹市",
                collectCount: 234,
                recommend: false,
                comment: 233,
                view: 100
            }],
            [{
                title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
                descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
                rank: 4,
                types: ["文化", "樂活"],
                date: "2015/3/7",
                author: "奧革士",
                isCollect: false,
                location: "台北、桃園市、新竹市",
                collectCount: 234,
                recommend: true,
                comment: 233,
                view: 100
            }, {
                title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
                descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，園",
                rank: 4,
                types: ["文化", "樂活"],
                date: "2015/3/7",
                author: "奧革士",
                isCollect: true,
                location: "台北、桃園市、新竹市",
                collectCount: 234,
                recommend: false,
                comment: 233,
                view: 100
            }, {
                title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
                descript: "西門町位於台北市萬華區，由武昌街二段、漢中街、峨嵋街的行人徒步區所形成的區域為西門町的黃金地帶。大台北地區來往公車的密集交界，流行元素走在前端，是許多年輕學子及各國觀光客朝聖之處。",
                rank: 4,
                types: ["文化", "樂活"],
                date: "2015/3/7",
                author: "奧革士",
                isCollect: false,
                location: "台北、桃園市、新竹市",
                collectCount: 234,
                recommend: false,
                comment: 233,
                view: 100
            }]
        ];
        $scope.travelSlide = {
            currentIdx: 0,
            prev: function() {
                $scope.travelSlide.currentIdx = $scope.travelSlide.currentIdx > 0 ? $scope.travelSlide.currentIdx - 1 : $scope.travelNotes.length - 1;
            },
            next: function() {
                $scope.travelSlide.currentIdx = $scope.travelSlide.currentIdx < ($scope.travelNotes.length - 1) ? $scope.travelSlide.currentIdx + 1 : 0;
            },
            set: function(idx) {
                $scope.travelSlide.currentIdx = idx;
            }
        };


        $scope.trips = [
            [{
                title: "台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊",
                descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
                rank: 4,
                types: ["文化", "樂活"],
                price: 4299,
                duration: 7,
                date: "2014年12月17日",
                author: "奧革士",
                isCollect: false,
                location: "台北市、台中市、高雄市",
                collectCount: 234,
                recommend: true,
                comment: 233,
                view: 100,
                days: 7
            }, {
                title: "台北都會一日遊",
                descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，園",
                rank: 4,
                types: ["文化", "樂活"],
                price: 0,
                duration: 3,
                date: "2014年12月17日",
                author: "奧革士",
                isCollect: true,
                location: "台北市、台中市、高雄市",
                collectCount: 234,
                recommend: false,
                comment: 233,
                view: 100,
                days: 7
            }, {
                title: "台北都會一日遊",
                descript: "西門町位於台北市萬華區，由武昌街二段、漢中街、峨嵋街的行人徒步區所形成的區域為西門町的黃金地帶。大台北地區來往公車的密集交界，流行元素走在前端，是許多年輕學子及各國觀光客朝聖之處。",
                rank: 4,
                types: ["文化", "樂活"],
                price: 0,
                duration: 5,
                date: "2014年12月17日",
                author: "奧革士",
                isCollect: false,
                location: "台北市、台中市、高雄市",
                collectCount: 234,
                recommend: false,
                comment: 233,
                view: 100,
                days: 7
            }],
            [{
                title: "台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊",
                descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
                rank: 4,
                types: ["文化", "樂活"],
                price: 4299,
                duration: 7,
                date: "2014年12月17日",
                author: "奧革士",
                isCollect: false,
                location: "台北市、台中市、高雄市",
                collectCount: 234,
                recommend: true,
                comment: 233,
                view: 100,
                days: 7
            }, {
                title: "台北都會一日遊",
                descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，園",
                rank: 4,
                types: ["文化", "樂活"],
                price: 0,
                duration: 3,
                date: "2014年12月17日",
                author: "奧革士",
                isCollect: true,
                location: "台北市、台中市、高雄市",
                collectCount: 234,
                recommend: false,
                comment: 233,
                view: 100,
                days: 7
            }, {
                title: "台北都會一日遊",
                descript: "西門町位於台北市萬華區，由武昌街二段、漢中街、峨嵋街的行人徒步區所形成的區域為西門町的黃金地帶。大台北地區來往公車的密集交界，流行元素走在前端，是許多年輕學子及各國觀光客朝聖之處。",
                rank: 4,
                types: ["文化", "樂活"],
                price: 0,
                duration: 5,
                date: "2014年12月17日",
                author: "奧革士",
                isCollect: false,
                location: "台北市、台中市、高雄市",
                collectCount: 234,
                recommend: false,
                comment: 233,
                view: 100,
                days: 7
            }],
            [{
                title: "台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊",
                descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
                rank: 4,
                types: ["文化", "樂活"],
                price: 4299,
                duration: 7,
                date: "2014年12月17日",
                author: "奧革士",
                isCollect: false,
                location: "台北市、台中市、高雄市",
                collectCount: 234,
                recommend: true,
                comment: 233,
                view: 100,
                days: 7
            }, {
                title: "台北都會一日遊",
                descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，園",
                rank: 4,
                types: ["文化", "樂活"],
                price: 0,
                duration: 3,
                date: "2014年12月17日",
                author: "奧革士",
                isCollect: true,
                location: "台北市、台中市、高雄市",
                collectCount: 234,
                recommend: false,
                comment: 233,
                view: 100,
                days: 7
            }, {
                title: "台北都會一日遊",
                descript: "西門町位於台北市萬華區，由武昌街二段、漢中街、峨嵋街的行人徒步區所形成的區域為西門町的黃金地帶。大台北地區來往公車的密集交界，流行元素走在前端，是許多年輕學子及各國觀光客朝聖之處。",
                rank: 4,
                types: ["文化", "樂活"],
                price: 0,
                duration: 5,
                date: "2014年12月17日",
                author: "奧革士",
                isCollect: false,
                location: "台北市、台中市、高雄市",
                collectCount: 234,
                recommend: false,
                comment: 233,
                view: 100,
                days: 7
            }]
        ];
        $scope.tripSlide = {
            currentIdx: 0,
            prev: function() {
                $scope.tripSlide.currentIdx = $scope.tripSlide.currentIdx > 0 ? $scope.tripSlide.currentIdx - 1 : $scope.trips.length - 1;
            },
            next: function() {
                $scope.tripSlide.currentIdx = $scope.tripSlide.currentIdx < ($scope.trips.length - 1) ? $scope.tripSlide.currentIdx + 1 : 0;
            },
            set: function(idx) {
                $scope.tripSlide.currentIdx = idx;
            }
        };

        $scope.comment = {
            author: '奧革士',
            authorImg: '../public/images/avatars/1.jpg',
            rank: undefined,
            comment: '',
            images: [
                '../public/images/stays/stay-1.jpg',
                '../public/images/stays/stay-2.jpg',
                '../public/images/stays/stay-3.jpg'
            ],
            commentTime: ''
        };

        $scope.comments = [{
            author: '賀少俠',
            authorImg: '../public/images/avatars/2.jpg',
            rank: 4,
            comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
            images: [
                '../public/images/stays/stay-1.jpg',
                '../public/images/stays/stay-2.jpg',
                '../public/images/stays/stay-3.jpg'
            ],
            commentTime: '5分鐘前',
            replys: [{
                author: '賀少俠',
                authorImg: '../public/images/avatars/2.jpg',
                rank: 4,
                comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
                images: [
                    '../public/images/stays/stay-1.jpg',
                    '../public/images/stays/stay-2.jpg',
                    '../public/images/stays/stay-3.jpg'
                ],
                commentTime: '5分鐘前',
                replys: []
            }, {
                author: '路易四',
                authorImg: '../public/images/avatars/3.jpg',
                rank: 4,
                comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
                images: [],
                commentTime: '5分鐘前',
                replys: []
            }]
        }, {
            author: '路易四',
            authorImg: '../public/images/avatars/3.jpg',
            rank: 4,
            comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
            images: [],
            commentTime: '5分鐘前',
            replys: []
        }, {
            author: '賀少俠',
            authorImg: '../public/images/avatars/4.jpg',
            rank: 4,
            comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
            images: [],
            commentTime: '5分鐘前',
            replys: []
        }, {
            author: '賀少俠',
            authorImg: '../public/images/avatars/1.jpg',
            rank: 4,
            comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
            images: [],
            commentTime: '5分鐘前',
            replys: []
        }];

        $scope.addComment = function() {
            $scope.comments.splice(0, 0, {
                author: $scope.comment.author,
                authorImg: $scope.comment.authorImg,
                rank: 4,
                comment: $scope.comment.comment,
                images: [],
                commentTime: '幾秒鐘前',
                replys: []
            });
            $scope.comment.comment = '';
        };

        $scope.deleteComment = function(index) {
            $scope.comments.splice(index, 1);
        };

        $scope.toReply = function(item) {
            item.replyMode = true;
        };

        $scope.editComment = function(item) {
            item.editMode = true;
            item.commentTemp = item.comment;
        };

        $scope.updateComment = function(item) {
            item.editMode = false;
            item.comment = item.commentTemp;
        };

        $scope.cancelEditComment = function(item) {
            item.editMode = false;
        };

        $scope.addReply = function(comment) {
            comment.replys.splice(0, 0, {
                author: $scope.comment.author,
                authorImg: $scope.comment.authorImg,
                comment: comment.replyTemp,
                commentTime: '幾秒鐘前'
            });
            comment.replyTemp = '';
            comment.replyMode = false;
        };

        $scope.deleteReply = function(comment, index) {
            comment.replys.splice(index, 1);
        };

        // 周邊區塊 開始

        $scope.types = [{
            id: 0,
            name: '景點'
        }, {
            id: 1,
            name: '住宿'
        }, {
            id: 2,
            name: '餐廳'
        }, {
            id: 3,
            name: '商店'
        }];

        $scope.currentType = $scope.types[0];

        $scope.selectType = function(item) {
            $scope.currentType = item;
        };

        $scope.spots = [{
            thumbnail: '../public/images/spots/spot-1.jpg',
            title: '華山文創園區',
            description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
            rank: 4,
            distance: 2
        }, {
            thumbnail: '../public/images/spots/spot-2.jpg',
            title: '市立台北動物園',
            description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
            rank: 4,
            distance: 2
        }, {
            thumbnail: '../public/images/spots/spot-3.jpg',
            title: '松山文創園區',
            description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
            rank: 4,
            distance: 2
        }, {
            thumbnail: '../public/images/spots/spot-4.jpg',
            title: '傳統藝術博物館',
            description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
            rank: 4,
            distance: 2
        }, {
            thumbnail: '../public/images/spots/spot-5.jpg',
            title: '台北美術館',
            description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
            rank: 4,
            distance: 2
        }, {
            thumbnail: '../public/images/spots/spot-1.jpg',
            title: '華山文創園區2',
            description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
            rank: 4,
            distance: 2
        }, {
            thumbnail: '../public/images/spots/spot-2.jpg',
            title: '市立台北動物園2',
            description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
            rank: 4,
            distance: 2
        }, {
            thumbnail: '../public/images/spots/spot-3.jpg',
            title: '松山文創園區2',
            description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
            rank: 4,
            distance: 2
        }, {
            thumbnail: '../public/images/spots/spot-4.jpg',
            title: '傳統藝術博物館',
            description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
            rank: 4,
            distance: 2
        }, {
            thumbnail: '../public/images/spots/spot-5.jpg',
            title: '台北美術館',
            description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
            rank: 4,
            distance: 2
        }];

        // 周邊區塊 結束


        // 伴手禮區塊 左右選單 開始

        $scope.productsSlide = {
            currentIdx: 0,
            prev: function() {
                $scope.productsSlide.currentIdx = $scope.productsSlide.currentIdx > 0 ? $scope.productsSlide.currentIdx - 1 : $scope.products.length - 1;
            },
            next: function() {
                $scope.productsSlide.currentIdx = $scope.productsSlide.currentIdx < ($scope.products.length - 1) ? $scope.productsSlide.currentIdx + 1 : 0;
            },
            set: function(idx) {
                $scope.productsSlide.currentIdx = idx;
            }
        };

        // 伴手禮區塊 左右選單 結束

        // 買行程區塊 左右選單 開始

        $scope.toursSlide = {
            currentIdx: 0,
            prev: function() {
                $scope.toursSlide.currentIdx = $scope.toursSlide.currentIdx > 0 ? $scope.toursSlide.currentIdx - 1 : $scope.products.length - 1;
            },
            next: function() {
                $scope.toursSlide.currentIdx = $scope.toursSlide.currentIdx < ($scope.products.length - 1) ? $scope.toursSlide.currentIdx + 1 : 0;
            },
            set: function(idx) {
                $scope.toursSlide.currentIdx = idx;
            }
        };

        // 買行程區塊 左右選單 結束

        // 伴手禮+買行程區塊 開始

        $scope.products = [
            [{
                thumbnail: '../public/images/stores/1.png',
                title: '柑仔手工肥皂名字真的超長 超長的 無敵長 看會不會變兩行',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/2.png',
                title: '馬櫻丹肥皂',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/3.png',
                title: '阿原竹月刀',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/4.png',
                title: '精油禮盒包 X 6',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/5.png',
                title: '天然草本組合包',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }],
            [{
                thumbnail: '../public/images/stores/1.png',
                title: '柑仔手工肥皂',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/2.png',
                title: '馬櫻丹肥皂',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/3.png',
                title: '阿原竹月刀',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/4.png',
                title: '精油禮盒包 X 6',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/5.png',
                title: '天然草本組合包',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }],
            [{
                thumbnail: '../public/images/stores/1.png',
                title: '柑仔手工肥皂',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/2.png',
                title: '馬櫻丹肥皂',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/3.png',
                title: '阿原竹月刀',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/4.png',
                title: '精油禮盒包 X 6',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/5.png',
                title: '天然草本組合包',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }]
        ];

        // 伴手禮+買行程區塊 結束

        $scope.guessYouLike = [{
            thumbnail: 'https://placeimg.com/100/80/nature?a=11',
            title: '黃家牛肉麵',
            description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
            rank: 4,
            distance: 2
        }, {
            thumbnail: 'https://placeimg.com/100/80/nature?a=12',
            title: '煙燻德國豬腳',
            description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
            rank: 4,
            distance: 2
        }, {
            thumbnail: 'https://placeimg.com/100/80/nature?a=13',
            title: '義式碳火披薩',
            description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
            rank: 4,
            distance: 2
        }, {
            thumbnail: 'https://placeimg.com/100/80/nature?a=14',
            title: '傳統黑糖冰',
            description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
            rank: 4,
            distance: 2
        }, {
            thumbnail: 'https://placeimg.com/100/80/nature?a=15',
            title: '冰淇淋鬆餅',
            description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
            rank: 4,
            distance: 2
        }];

        $scope.blogs = [{
            thumbnail: 'https://placeimg.com/100/80/nature?a=1',
            title: '【台北。信義區】《101觀景台》最繁榮的街景，全在我腳下',
            publishDate: '2014/03/19',
            blogName: '巴台日誌'
        }, {
            thumbnail: 'https://placeimg.com/100/80/nature?a=2',
            title: '台北自由行台北101旁TAIPEI GREEN HOUSE HOSTEL綠舍輕旅',
            publishDate: '2014/03/19',
            blogName: '巴台日誌'
        }, {
            thumbnail: 'https://placeimg.com/100/80/nature?a=3',
            title: '【睡遍全球之airbnb】在台北101樓下發霉，在全台唯一的玉田墅睡得美美，AIRBNB好壞報告',
            publishDate: '2014/03/19',
            blogName: '巴台日誌'
        }, {
            thumbnail: 'https://placeimg.com/100/80/nature?a=4',
            title: '台灣7日跨年背包遊，花東縱谷、太魯閣、台東海岸線、台北101樂遊記！',
            publishDate: '2014/03/19',
            blogName: '巴台日誌'
        }];
        $scope.stay = {
            id: 1,
            title: "鼎泰豐(信義本店)",
            descript: "創立於 1958 年的鼎泰豐，在 1972 年由原來的食用油行轉型經營小籠包與麵點生意。開賣小籠包後，便用心專注於品質與服務的提升，來自四面八方的佳評如潮，並在各家美食報章雜誌的介紹下，漸入佳績，至今不僅是一般市民的最愛，更是許多政商名流、國際級影星讚不絕口指定必嚐的頂級美食。創立於 1958 年的鼎泰豐，在 1972 年由原來的食用油行轉型經營小籠包與麵點生意。開賣小籠包後，便用心專注於品質與服務的提升，來自四面八方的佳評如潮，並在各家美食報章雜誌的介紹下，漸入佳績，至今不僅是一般市民的最愛....",
            istripadv: true,
            rank: 4,
            view: 999,
            comment: 999,
            price: 120,
            types: ["商圈"],
            author: "奧革士",
            isCollect: false,
            address: "台北市大安區信義路二段194號 ( 捷運東門站5號出口 )",
            location: "台北",
            collectCount: 234,
        };
        $scope.stay.rank = 0.5 * Math.round($scope.stay.rank / 0.5);
        $scope.stay.rank = -180 + $scope.stay.rank * 36;
        $scope.tripadv = {
            totalcomment: 2510,
            score: 4.7,
            rank: 4,
            bar: [20, 4, 70, 120, 41],
            barpercent: [8, 1, 27, 47, 16],
            comments: [{
                id: 1,
                title: "很不錯的服務體驗",
                rank: 3.2,
                author: "王*明",
                publishDate: "2016-3-27",
                description: "4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。"
            }, {
                id: 2,
                title: "很不錯的服務體驗",
                rank: 1,
                author: "王*明",
                publishDate: "2016-3-27",
                description: "4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。"
            }, {
                id: 3,
                title: "很不錯的服務體驗",
                rank: 0,
                author: "王*明",
                publishDate: "2016-3-27",
                description: "4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。"
            }, {
                id: 4,
                title: "很不錯的服務體驗",
                rank: 2,
                author: "王*明",
                publishDate: "2016-3-27",
                description: "4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。"
            }, {
                id: 5,
                title: "很不錯的服務體驗",
                rank: 3,
                author: "王*明",
                publishDate: "2016-3-27",
                description: "4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。"
            }]
        };




        angular.forEach($scope.tripadv.comments, function(value, key) {
            value.rank = 0.5 * Math.round(value.rank / 0.5);
            value.rank = -160 + value.rank * 32;
        });

    }
);

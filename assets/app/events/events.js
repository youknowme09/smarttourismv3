"use strict";

angular.module('ng.controller').controller('EventsController' ,
  function( userService, $mdSidenav, $mdBottomSheet, $log, $q, $scope) {

  	$scope.people = 1;
  	// $scope.layout = "List";
  	$scope.layout = "Grid";
  	$scope.minPrice;
  	$scope.maxPrice;
    $scope.isOpenFilter = false;

    $scope.closeFilter = function () {
      $scope.isOpenFilter = false;      
    };

    $scope.openFilter = function () {
      $scope.isOpenFilter = true;      
    };

    $scope.showAllOption = {};

    $scope.toggleMore = function (option) {
      $scope.showAllOption[option] = !$scope.showAllOption[option];
    };

    $scope.orderBy = 'topRated';

  	$scope.events = [
  		{
        thumbnail: '../public/images/events/1.jpg',
  			title: "三義國際木雕藝術節",
  			descript: "三義鄉位於苗栗縣南端，隔大安溪與臺中市后里區相鄰，東北隔三角山（567公尺）和銅鑼鄉新隆、興隆兩村毗連，東面以關刀山脈為天然界線，與大湖、卓蘭比鄰而立，西側則以火炎山脈與苑裡、通霄相接，北則與銅鑼鄉接壤。三義鄉因位在臺灣南北氣候之分界上，因此天氣複雜多變，尤以冬、春二季更為顯著，常有「四時皆夏、一雨成秋」之感；每年11月至翌年3月，濛濛白霧繚繞，整鄉彷彿墜入縹緲雲海，宛如人間仙境，故有「臺",
  			rank: 3,
        price: 0,
        type: "節慶活動",
        address: "台北市大同區長安西路33號",
        startDate: '2015/04/01',
        endDate: '2015/06/30',
        expired: true,
  			author: "奧革士",
  			isCollect : false,
  			tags: ['嚴選活動', '苗栗'],
  			collectCount: 237,
        tags: ['必去', '台北'],
        collectCount: 237,
        comment:30,
        views:100
  		},
  		{
        thumbnail: '../public/images/events/2.jpg',
        title: "臺中爵士音樂節",
        descript: "臺中爵士音樂節（Taichung Jazz Festival）是臺中市每年10月舉辦的爵士樂戶外活動，從2002年開始成為台中閃亮文化季下的活動之一[1]；隨著逐漸培養出參與爵士樂人口，2009年開始從文化季中獨立出來舉辦。音樂節起初只為期三天，2009年已擴大到舉辦十天，2010年年後至今都已固定舉辦九天。",
        rank: 3,
        price: 120,
        type: "文藝活動",
        address: "台北市大同區長安西路33號",
        startDate: '2015/04/01',
        endDate: '2015/06/30',
        expired: false,
        author: "奧革士",
        isCollect : false,
        tags: ['台中'],
        collectCount: 237,
        comment:30,
        views:100
  		},
      {
        thumbnail: '../public/images/events/3.jpg',
        title: "太魯閣峽谷音樂節活動",
        descript: "大家引頸期盼的「太魯閣峽谷音樂節」將於9月20日在清水斷崖及9月27日於長春祠舉行。碧藍的太平洋、廣闊的沙灘和壯麗的清水斷崖，古典雅致的長春祠搭配飛瀑流洩而下，坐在溪畔聆聽音樂家現場演奏，令許多曾參加過的觀眾印象深刻、回味無窮。今年的太魯閣峽谷音樂節，由國內最知名的創意大提琴家張正傑教授擔任藝術總監，將以「擊鼓弦歌」為主題，帶來有別以往的震憾。 ",
        rank: 3,
        price: 120,
        type: "年度活動",
        address: "台北市大同區長安西路33號",
        startDate: '2015/04/01',
        endDate: '2015/06/30',
        expired: false,
        author: "奧革士",
        isCollect : false,
        tags: ['花蓮'],
        collectCount: 237,
        comment:30,
        views:100
      },
      {
        thumbnail: '../public/images/events/4.jpg',
        title: "花蓮金針花季",
        descript: "能看到一大片金針花海， 是一種幸福喔！ 今年夏季期間花蓮赤科山、六十石山， 持續透過留花政策推動，來創造較大最大面積金針花海。",
        rank: 3,
        price: 0,
        type: "四季活動",
        address: "台北市大同區長安西路33號",
        startDate: '2015/04/01',
        endDate: '2015/06/30',
        expired: false,
        author: "奧革士",
        isCollect : false,
        tags: ['花蓮'],
        collectCount: 237,
        comment:30,
        views:100
      },
      {
        thumbnail: '../public/images/events/5.jpg',
        title: "雲林國際偶戲節",
        descript: "臺灣有所謂的偶戲三寶(皮影戲、布袋戲、傀儡戲)，自傳入臺灣以來，結合了本土風俗及信仰，早已發展成為獨特的臺灣偶戲藝術。隨著時間洪流，不論是在師傅手中活靈活現的表演戲偶、一旁樂師現場臨急應變的配樂演奏、或是一代替換過一代的臺下觀眾，乃至偶戲表演的目的從酬神到娛人，偶戲藝術所代表的意義也逐漸地在轉變當中。 「雲林國際偶戲藝術節」成立於1999年，雲林縣是臺灣布袋戲的故鄉，擁有許多派別，「雲林國際偶戲節」的成立，旨在落實文化立縣，發揚傳統精緻藝術，促進國際文化交流。活動除了邀請國際偶戲團隊演出、舉行金掌獎競技、經典與創新布袋戲團競演；同時結合文物藝術展、文創展及農特產「偶」裝置藝術，積極將「布袋戲」文化透過推廣與刺激、傳習與體驗等展演活動，使布袋戲偶戲文化得以永續推廣及傳承。",
        rank: 3,
        price: 120,
        type: "文藝活動",
        address: "台北市大同區長安西路33號",
        startDate: '2015/04/01',
        endDate: '2015/06/30',
        expired: false,
        author: "奧革士",
        isCollect : false,
        tags: ['嚴選活動', '新北'],
        collectCount: 237,
        comment:30,
        views:100
      },
      {
        thumbnail: '../public/images/events/1.jpg',
        title: "花蓮國際石雕藝術季",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        rank: 3,
        price: 120,
        type: "文藝活動",
        address: "台北市大同區長安西路33號",
        startDate: '2015/04/01',
        endDate: '2015/06/30',
        expired: false,
        author: "奧革士",
        isCollect : false,
        tags: ['嚴選活動', '高雄'],
        collectCount: 237,
        comment:30,
        views:100
      },
      {
        thumbnail: '../public/images/events/2.jpg',
        title: "西門町後街文化祭",
        descript: "自2010年後街再起、2011年後勁十足、2012年Hold街一起玩，2013年Fun你的爽，樂在其中! 今年即將邁向五週年的後街文化祭，激盪出最純真的街頭「色」彩，一路從暑假瘋到年底!",
        rank: 3,
        price: 120,
        type: "文藝活動",
        address: "台北市大同區長安西路33號",
        startDate: '2015/03/18',
        endDate: '2015/05/17',
        expired: true,
        author: "奧革士",
        isCollect : false,
        tags: ['嚴選活動', '高雄'],
        collectCount: 237,
        comment:30,
        views:100
      },
      {
        thumbnail: '../public/images/events/3.jpg',
        title: "華山藝術生活節",
        descript: "華山藝術生活節從10月10日開始到11月4日止，邀請200個各種表演團體進行演出，成為一個大小朋友都適合的熱鬧周末休閒，也讓民眾體驗舞蹈與戲劇表演的美好。4日最後一場表演，為金枝演社在焦點劇場演出的「幸福大丈夫」。",
        rank: 3,
        price: 0,
        type: "文化藝術",
        address: "台北市大同區長安西路33號",
        startDate: '2015/04/01',
        endDate: '2015/06/30',
        expired: true,
        author: "奧革士",
        isCollect : false,
        tags: ['嚴選活動', '台北'],
        collectCount: 237,
        comment:30,
        views:100
      }
  	];

  	$scope.toggleCollect = function (trip) {
  		trip.isCollect = !trip.isCollect;
  	};

  	$scope.switchLayout = function (layout) {
  		$scope.layout = layout;
  	};


  }
);

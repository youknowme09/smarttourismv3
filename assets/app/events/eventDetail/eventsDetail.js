"use strict";

angular.module('ng.controller').controller('EventsDetailController' ,
  function( userService, $mdSidenav, $mdBottomSheet, $log, $q, $scope, $mdDialog) {

    $scope.violationsReport = function (ev) {
      $mdDialog.show({
        controller: function ($scope, $mdDialog, $timeout) {
          $scope.report = {
            reasons: {
              harassment: false,
              infringement: false,
              other: false
            }
          };
          $scope.haveReportReason = function () {
            return $scope.report.reasons.harassment ||
              $scope.report.reasons.infringement ||
              $scope.report.reasons.other;
          };
          $scope.cancel = function() {
            $mdDialog.cancel();
          };
          $scope.sendReport = function(answer) {
            $scope.reporting = true;
            $timeout(function () {
              $scope.reporting = false;
              $scope.reportSuccess = true;
            }, 1000);
          };
        },
        templateUrl: '../assets/common/partials/violationsReportMadal.html',
        targetEvent: ev,
      })
      .then(function(answer) {
        $scope.alert = 'You said the information was "' + answer + '".';
      }, function() {
        $scope.alert = 'You cancelled the dialog.';
      });
    };

    $scope.bugReport = function (ev) {
      $mdDialog.show({
        controller: function ($scope, $mdDialog, $timeout) {
          $scope.report = {
            reasons: {
              stopSales: false,
              other: false
            }
          };
          $scope.haveReportReason = function () {
            return $scope.report.reasons.stopSales || 
              $scope.report.reasons.other;
          };
          $scope.cancel = function() {
            $mdDialog.cancel();
          };
          $scope.sendReport = function(answer) {
            $scope.reporting = true;
            $timeout(function () {
              $scope.reporting = false;
              $scope.reportSuccess = true;
            }, 1000);
          };
        },
        templateUrl: '../assets/common/partials/bugReportMadal.html',
        targetEvent: ev,
      })
      .then(function(answer) {
        $scope.alert = 'You said the information was "' + answer + '".';
      }, function() {
        $scope.alert = 'You cancelled the dialog.';
      });
    };

    $scope.editSpotsInfo = function (ev) {
      $mdDialog.show({
        controller: function ($scope, $mdDialog, $timeout) {
          $scope.spotData = {
            stay: 'minute'
          };
          $scope.currentTab = 1;
          $scope.switchTab = function (tab) {
            $scope.currentTab = tab;
          };
          $scope.cancel = function() {
            $mdDialog.cancel();
          };
          $scope.save = function(answer) {
            $scope.saving = true;
            $timeout(function () {
              $scope.saving = false;
              $scope.editSuccess = true;
            }, 1000);
          };
          $scope.slideList = [
            {
              "imgUrl" : "../public/images/events/1.jpg"
            },
            {
              "imgUrl" : "../public/images/events/2.jpg"
            },
            {
              "imgUrl" : "../public/images/events/3.jpg"
            },
            {
              "imgUrl" : "../public/images/events/4.jpg"
            },
            {
              "imgUrl" : "../public/images/events/5.jpg"
            },
            {
              "imgUrl" : "../public/images/events/1.jpg"
            },
            {
              "imgUrl" : "../public/images/events/2.jpg"
            },
            {
              "imgUrl" : "../public/images/events/3.jpg"
            },
            {
              "imgUrl" : "../public/images/events/4.jpg"
            },
            {
              "imgUrl" : "../public/images/events/5.jpg"
            },
            {
              "imgUrl" : "../public/images/events/1.jpg"
            },
            {
              "imgUrl" : "../public/images/events/2.jpg"
            }
          ];

          $scope.currentSlideIdx = 0;

          $scope.prevSlide = function () {
            $scope.currentSlideIdx = $scope.currentSlideIdx > 0 ? $scope.currentSlideIdx - 1 : $scope.slideList.length - 1;
          };

          $scope.nextSlide = function () {
            $scope.currentSlideIdx = $scope.currentSlideIdx < ($scope.slideList.length - 1) ? $scope.currentSlideIdx + 1 : 0;
          };

          $scope.setSlide = function (idx) {
            $scope.currentSlideIdx = idx;
          };

          $scope.uploadPhoto = function () {};

          $scope.citys = [
            {
              id: 1,
              name: '基隆市'
            },
            {
              id: 2,
              name: '台北市'
            },
            {
              id: 3,
              name: '新北市'
            },
            {
              id: 4,
              name: '桃園市'
            },
            {
              id: 5,
              name: '新竹市'
            },
            {
              id: 6,
              name: '新竹縣'
            },
            {
              id: 7,
              name: '宜蘭縣'
            },
            {
              id: 8,
              name: '苗栗縣'
            },
            {
              id: 9,
              name: '台中市'
            },
            {
              id: 10,
              name: '彰化縣'
            },
            {
              id: 11,
              name: '南投縣'
            },
            {
              id: 12,
              name: '雲林縣'
            },
            {
              id: 13,
              name: '嘉義市'
            },
            {
              id: 14,
              name: '嘉義縣'
            },
            {
              id: 15,
              name: '台南市'
            },
            {
              id: 16,
              name: '高雄市'
            },
            {
              id: 17,
              name: '屏東縣'
            },
            {
              id: 18,
              name: '澎湖縣'
            },
            {
              id: 19,
              name: '花蓮縣'
            },
            {
              id: 20,
              name: '台東縣'
            },
            {
              id: 21,
              name: '金門縣'
            },
            {
              id: 22,
              name: '連江縣'
            }
          ];
          $scope.spotData.city = $scope.citys[1].id;

          $scope.spotData.labels = [
            {
              name: '觀光藝文',
              checked: true
            },
            {
              name: '商圈',
              checked: true
            },
            {
              name: '體育健身',
              checked: false
            },
            {
              name: '遊憩公園',
              checked: false
            },
            {
              name: '文化藝術',
              checked: false
            },
            {
              name: '國家風景區',
              checked: false
            },
            {
              name: '古蹟廟宇',
              checked: false
            },
            {
              name: '溫泉',
              checked: false
            },
            {
              name: '自然生態',
              checked: false
            },
            {
              name: '休閒農業',
              checked: false
            }
          ];

          $scope.spotData.otherLabels = [
            {
              text: '台灣小吃'
            }
          ];

          $scope.tempSpotData = {};

          $scope.editSpotsCategory = function () {
            $scope.showSpotCategory = true;
            $scope.tempSpotData = angular.copy($scope.spotData);
            $scope.tempSpotData.hasOtherLabels = true;
          };

          $scope.cancelEditSpotsCategory = function () {
            $scope.showSpotCategory = false;
          };

          $scope.saveCategory = function () {
            $scope.onSavingCategory = true;
            $timeout(function () {
              $scope.spotData = angular.copy($scope.tempSpotData);
              $scope.onSavingCategory = false;
              $scope.showSpotCategory = false;
              if (!$scope.spotData.hasOtherLabels) {
                $scope.spotData.otherLabels.length = 0;
              }
            }, 1000);
          };

          $scope.addCategory = function (category) {
            $scope.spotData.otherLabels.push(category);
          };

          $scope.removeCategory = function (index) {
            $scope.spotData.otherLabels.splice(index, 1);
          };

        },
        templateUrl: '../assets/common/partials/editSpotsInfoMadal.html',
        targetEvent: ev,
      })
      .then(function(answer) {
        $scope.alert = 'You said the information was "' + answer + '".';
      }, function() {
        $scope.alert = 'You cancelled the dialog.';
      });
    };



    $scope.slideList = [
      {
        "imgUrl" : "../public/images/events/1.jpg"
      },
      {
        "imgUrl" : "../public/images/events/2.jpg"
      },
      {
        "imgUrl" : "../public/images/events/3.jpg"
      },
      {
        "imgUrl" : "../public/images/events/4.jpg"
      },
      {
        "imgUrl" : "../public/images/events/5.jpg"
      },
      {
        "imgUrl" : "../public/images/events/1.jpg"
      },
      {
        "imgUrl" : "../public/images/events/2.jpg"
      }
    ];

    $scope.currentSlideIdx = 0;

    $scope.prevSlide = function () {
      $scope.currentSlideIdx = $scope.currentSlideIdx > 0 ? $scope.currentSlideIdx - 1 : $scope.slideList.length - 1;
    };

    $scope.nextSlide = function () {
      $scope.currentSlideIdx = $scope.currentSlideIdx < ($scope.slideList.length - 1) ? $scope.currentSlideIdx + 1 : 0;
    };

    $scope.setSlide = function (idx) {
      $scope.currentSlideIdx = idx;
    };

    $scope.toggleBool = function (bool) {
      $scope[bool] = !$scope[bool];
    };

    $scope.videos = [
      {
        thumbnail: '../public/images/events/1.jpg',
        title: '台灣Hi起來 雲林國際偶戲節',
        author: '奧革士',
        viewcount: '33210',
        description: '台北101大樓，一轉眼已經啟用10年，或許不少國人跟外國遊客都曾經上過位於91樓­的戶外觀景台，但別以為最高就到此為止，其實台北101真的有這第101層，面積只有­62坪，還得多轉乘兩次電梯才能抵達，以往都是招待國外貴賓或者超級VIP，就連許多­101員工都不一定知道有這第101層，今天首度曝光，不只觀景視野更棒，同時也成為­業者週年慶噱頭，得消費101萬才有資格登上這難得開放的第101層。'
      },
      {
        thumbnail: '../public/images/events/2.jpg',
        title: '2010雲林國際偶戲節系列活動 李壽山',
        author: '奧革士',
        viewcount: '33210',
        description: '台北101大樓，一轉眼已經啟用10年，或許不少國人跟外國遊客都曾經上過位於91樓­的戶外觀景台，但別以為最高就到此為止，其實台北101真的有這第101層，面積只有­62坪，還得多轉乘兩次電梯才能抵達，以往都是招待國外貴賓或者超級VIP，就連許多­101員工都不一定知道有這第101層，今天首度曝光，不只觀景視野更棒，同時也成為­業者週年慶噱頭，得消費101萬才有資格登上這難得開放的第101層。'
      },
      {
        thumbnail: '../public/images/events/3.jpg',
        title: '醒報-雲林國際偶戲節 百場表演登場',
        author: '奧革士',
        viewcount: '33210',
        description: '台北101大樓，一轉眼已經啟用10年，或許不少國人跟外國遊客都曾經上過位於91樓­的戶外觀景台，但別以為最高就到此為止，其實台北101真的有這第101層，面積只有­62坪，還得多轉乘兩次電梯才能抵達，以往都是招待國外貴賓或者超級VIP，就連許多­101員工都不一定知道有這第101層，今天首度曝光，不只觀景視野更棒，同時也成為­業者週年慶噱頭，得消費101萬才有資格登上這難得開放的第101層。'
      },
      {
        thumbnail: '../public/images/events/4.jpg',
        title: '2014雲林國際偶戲節 昇平五洲園(英雄酒之風雲再起)',
        author: '奧革士',
        viewcount: '33210',
        description: '台北101大樓，一轉眼已經啟用10年，或許不少國人跟外國遊客都曾經上過位於91樓­的戶外觀景台，但別以為最高就到此為止，其實台北101真的有這第101層，面積只有­62坪，還得多轉乘兩次電梯才能抵達，以往都是招待國外貴賓或者超級VIP，就連許多­101員工都不一定知道有這第101層，今天首度曝光，不只觀景視野更棒，同時也成為­業者週年慶噱頭，得消費101萬才有資格登上這難得開放的第101層。'
      }
    ];

    $scope.tickets = [
      {
        title: '雲林布袋戲館 - 成人票',
        price: 50,
        type: '成人票'
      },
      {
        title: '雲林布袋戲館 - 優待票',
        price: 35,
        type: '優待票'
      }
    ];

    $scope.activities = [
      {
        title: '食我鑰匙大放送',
        startDate: '2015/03/10',
        endDate: '2015/05/10',
        message: '下載 Smart Tourism Taiwan 台灣智慧觀光 APP 並完成註冊、登入，馬上獲得食我鑰匙10把序號乙組！'
      },
      {
        title: 'Smart Tourism Taiwan 會員募集抽獎活動',
        startDate: '2015/03/10',
        endDate: '2015/05/10',
        message: '凡於活動期間下載 Smart Tourism Taiwan APP 並註冊成為會員，於行動裝置上的抽獎活動頁面登錄相關資訊，即可參加抽獎。'
      }
    ];

    $scope.eventsSlide = {
      currentIdx: 0,
      prev: function () {
        $scope.eventsSlide.currentIdx = $scope.eventsSlide.currentIdx > 0 ? $scope.eventsSlide.currentIdx - 1 : $scope.eventsGroup.length - 1;
      },
      next: function () {
        $scope.eventsSlide.currentIdx = $scope.eventsSlide.currentIdx < ($scope.eventsGroup.length - 1) ? $scope.eventsSlide.currentIdx + 1 : 0;
      },
      set: function (idx) {
        $scope.eventsSlide.currentIdx = idx;
      }
    };

    $scope.eventsGroup = [
      [
        {
          title: '雲林國際偶戲節',
          type: '文藝活動',
          price: 120,
          startDate: '2015/5/3',
          endDate: '2015/5/17',
          expired: true,
          thumbnail: '../public/images/spots/spot-1.jpg'
        },
        {
          title: '雲林國際偶戲節',
          type: '文藝活動',
          price: 120,
          startDate: '2015/6/12',
          endDate: '2015/6/31',
          expired: false,
          thumbnail: '../public/images/spots/spot-2.jpg'
        },
        {
          title: '雲林國際偶戲節',
          type: '文藝活動',
          price: 120,
          startDate: '2015/6/12',
          endDate: '2015/6/31',
          expired: false,
          thumbnail: '../public/images/spots/spot-3.jpg'
        },
        {
          title: '雲林國際偶戲節',
          type: '文藝活動',
          price: 120,
          startDate: '2015/6/12',
          endDate: '2015/6/31',
          expired: false,
          thumbnail: '../public/images/spots/spot-4.jpg'
        }
      ],
      [
        {
          title: '雲林國際偶戲節',
          type: '文藝活動',
          price: 120,
          startDate: '2015/5/3',
          endDate: '2015/5/17',
          expired: false,
          thumbnail: '../public/images/spots/spot-5.jpg'
        },
        {
          title: '雲林國際偶戲節',
          type: '文藝活動',
          price: 120,
          startDate: '2015/6/12',
          endDate: '2015/6/31',
          expired: true,
          thumbnail: '../public/images/spots/spot-6.jpg'
        },
        {
          title: '雲林國際偶戲節',
          type: '文藝活動',
          price: 120,
          startDate: '2015/6/12',
          endDate: '2015/6/31',
          expired: false,
          thumbnail: '../public/images/spots/spot-7.jpg'
        },
        {
          title: '雲林國際偶戲節',
          type: '文藝活動',
          price: 120,
          startDate: '2015/6/12',
          endDate: '2015/6/31',
          expired: false,
          thumbnail: '../public/images/spots/spot-8.jpg'
        }
      ],
      [
        {
          title: '雲林國際偶戲節',
          type: '文藝活動',
          price: 120,
          startDate: '2015/5/3',
          endDate: '2015/5/17',
          expired: false,
          thumbnail: '../public/images/stays/stay-1.jpg'
        },
        {
          title: '雲林國際偶戲節',
          type: '文藝活動',
          price: 120,
          startDate: '2015/6/12',
          endDate: '2015/6/31',
          expired: false,
          thumbnail: '../public/images/stays/stay-2.jpg'
        },
        {
          title: '雲林國際偶戲節',
          type: '文藝活動',
          price: 120,
          startDate: '2015/6/12',
          endDate: '2015/6/31',
          expired: true,
          thumbnail: '../public/images/stays/stay-3.jpg'
        },
        {
          title: '雲林國際偶戲節',
          type: '文藝活動',
          price: 120,
          startDate: '2015/6/12',
          endDate: '2015/6/31',
          expired: true,
          thumbnail: '../public/images/stays/stay-4.jpg'
        }
      ]
    ];

    $scope.comment = {
      author: '奧革士',
      authorImg: '../public/images/avatars/1.jpg',
      rank: undefined,
      comment: '',
      images: [
        '../public/images/events/1.jpg', 
        '../public/images/events/2.jpg', 
        '../public/images/events/3.jpg'
      ],
      commentTime: ''
    };

    $scope.comments = [
      {
        author: '賀少俠',
        authorImg: '../public/images/avatars/2.jpg',
        rank: 4,
        comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
        images: [
          '../public/images/events/1.jpg', 
          '../public/images/events/2.jpg', 
          '../public/images/events/3.jpg'
        ],
        commentTime: '5分鐘前',
        replys: [
          {
            author: '賀少俠',
            authorImg: '../public/images/avatars/2.jpg',
            rank: 4,
            comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
            images: [
              '../public/images/events/1.jpg', 
              '../public/images/events/2.jpg', 
              '../public/images/events/3.jpg'
            ],
            commentTime: '5分鐘前',
            replys: []
          },
          {
            author: '路易四',
            authorImg: '../public/images/avatars/3.jpg',
            rank: 4,
            comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
            images: [],
            commentTime: '5分鐘前',
            replys: []
          }
        ]
      },
      {
        author: '路易四',
        authorImg: '../public/images/avatars/3.jpg',
        rank: 4,
        comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
        images: [],
        commentTime: '5分鐘前',
        replys: []
      },
      {
        author: '賀少俠',
        authorImg: '../public/images/avatars/4.jpg',
        rank: 4,
        comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
        images: [],
        commentTime: '5分鐘前',
        replys: []
      },
      {
        author: '賀少俠',
        authorImg: '../public/images/avatars/1.jpg',
        rank: 4,
        comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
        images: [],
        commentTime: '5分鐘前',
        replys: []
      }
    ];

    $scope.addComment = function () {
      $scope.comments.splice(0, 0, {
        author: $scope.comment.author,
        authorImg: $scope.comment.authorImg,
        rank: 4,
        comment: $scope.comment.comment,
        images: [],
        commentTime: '幾秒鐘前',
        replys: []
      });
      $scope.comment.comment = '';
    };

    $scope.deleteComment = function (index) {
      $scope.comments.splice(index, 1);
    };

    $scope.toReply = function (item) {
      item.replyMode = true;
    };

    $scope.editComment = function (item) {
      item.editMode = true;
      item.commentTemp = item.comment;
    };

    $scope.updateComment = function (item) {
      item.editMode = false;
      item.comment = item.commentTemp;
    };

    $scope.cancelEditComment = function (item) {
      item.editMode = false;
    };

    $scope.addReply = function (comment) {
      comment.replys.splice(0, 0, {
        author: $scope.comment.author,
        authorImg: $scope.comment.authorImg,
        comment: comment.replyTemp,
        commentTime: '幾秒鐘前'
      });
      comment.replyTemp = '';
      comment.replyMode = false;
    };

    $scope.deleteReply = function (comment, index) {
      comment.replys.splice(index, 1);
    };

    $scope.types = [
      {
        id: 0,
        name: '景點'
      },
      {
        id: 1,
        name: '住宿'
      },
      {
        id: 2,
        name: '餐廳'
      },
      {
        id: 3,
        name: '商家'
      }
    ];

    $scope.currentType = $scope.types[0];

    $scope.selectType = function(item) {
      $scope.currentType = item;
    };

    $scope.spots = [
      {
        thumbnail: '../public/images/spots/spot-1.jpg',
        title: '華山文創園區',
        description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
        rank: 4,
        distance: 2
      },
      {
        thumbnail: '../public/images/spots/spot-2.jpg',
        title: '市立台北動物園',
        description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
        rank: 4,
        distance: 2
      },
      {
        thumbnail: '../public/images/spots/spot-3.jpg',
        title: '松山文創園區',
        description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
        rank: 4,
        distance: 2
      },
      {
        thumbnail: '../public/images/spots/spot-4.jpg',
        title: '傳統藝術博物館',
        description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
        rank: 4,
        distance: 2
      },
      {
        thumbnail: '../public/images/spots/spot-5.jpg',
        title: '台北美術館',
        description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
        rank: 4,
        distance: 2
      }
    ];

    $scope.guessYouLike = [
      {
        thumbnail: 'https://placeimg.com/100/80/nature?a=11',
        title: '黃家牛肉麵',
        description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
        rank: 4,
        distance: 2
      },
      {
        thumbnail: 'https://placeimg.com/100/80/nature?a=12',
        title: '煙燻德國豬腳',
        description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
        rank: 4,
        distance: 2
      },
      {
        thumbnail: 'https://placeimg.com/100/80/nature?a=13',
        title: '義式碳火披薩',
        description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
        rank: 4,
        distance: 2
      },
      {
        thumbnail: 'https://placeimg.com/100/80/nature?a=14',
        title: '傳統黑糖冰',
        description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
        rank: 4,
        distance: 2
      },
      {
        thumbnail: 'https://placeimg.com/100/80/nature?a=15',
        title: '冰淇淋鬆餅',
        description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
        rank: 4,
        distance: 2
      }
    ];

    $scope.blogs = [
      {
        thumbnail: 'https://placeimg.com/100/80/nature?a=1',
        title: '【台北。信義區】《101觀景台》最繁榮的街景，全在我腳下',
        publishDate: '2014/03/19',
        blogName: '巴台日誌'
      },
      {
        thumbnail: 'https://placeimg.com/100/80/nature?a=2',
        title: '台北自由行台北101旁TAIPEI GREEN HOUSE HOSTEL綠舍輕旅',
        publishDate: '2014/03/19',
        blogName: '巴台日誌'
      },
      {
        thumbnail: 'https://placeimg.com/100/80/nature?a=3',
        title: '【睡遍全球之airbnb】在台北101樓下發霉，在全台唯一的玉田墅睡得美美，AIRBNB好壞報告',
        publishDate: '2014/03/19',
        blogName: '巴台日誌'
      },
      {
        thumbnail: 'https://placeimg.com/100/80/nature?a=4',
        title: '台灣7日跨年背包遊，花東縱谷、太魯閣、台東海岸線、台北101樂遊記！',
        publishDate: '2014/03/19',
        blogName: '巴台日誌'
      }
    ];

  }
);

"use strict";

angular.module('ng.controller').controller('CreateTripController' ,
  function( userService, $mdSidenav, $mdBottomSheet, $log, $q, $scope, $mdDialog) {

    var createTripVM = $scope;

    $scope.layout = "List";
    // $scope.layout = "Map";

    $scope.switchLayout = function (layout) {
      $scope.layout = layout;
    };

    $scope.tripData = {
        title: '北台灣三日遊',
        desc: '用幾句話描述這趟旅程'
    };

    $scope.toggleEditMode = function(item) {
        item.isEditing = !item.isEditing;
    };

    $scope.trips = [
        {
            places: '桃園',
            date: '5月5日 週一',
            spots: [
            ]
        },
        {
            places: '桃園、台北',
            date: '5月6日 週二',
            spots: [
            ]
        },
        {
            places: '台北',
            date: '5月7日 週三',
            spots: [
                {
                    thumbnail: '../public/images/spots/spot-1.jpg',
                    title: '傳統藝術博物館',
                    descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
                    commentCount: 22,
                    collectCount: 77
                },
                {
                    thumbnail: '../public/images/spots/spot-2.jpg',
                    title: '松山文創園區',
                    descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
                    commentCount: 22,
                    collectCount: 77
                },
                {
                    thumbnail: '../public/images/spots/spot-3.jpg',
                    title: '華山文創園區',
                    descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
                    commentCount: 22,
                    collectCount: 77
                },
                {
                    thumbnail: '../public/images/spots/spot-4.jpg',
                    title: '國立故宮博物院',
                    descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
                    commentCount: 22,
                    collectCount: 77
                }
            ]
        },
        {
            places: '台北、瑞芳',
            date: '5月8日 週四',
            spots: [
            ]
        }
    ];

    $scope.currentTrip = $scope.trips[2];
    $scope.currentDay = 3;

    $scope.switchTrip = function(trip, index) {
        $scope.currentTrip = trip;
        $scope.currentDay = index + 1;
    };

    $scope.createTrip = function() {
        $scope.trips.push({
            places: '地點',
            date: '日期',
            spots: [
            ]
        });
    };

    $scope.removeTrip = function($index) {
        $scope.trips.splice($index, 1);
    };

    $scope.addSpot = function(spot) {
        if (!spot.checked){
            $scope.currentTrip.spots.push(angular.copy(spot));
            spot.checked = true;
        }
    };

    $scope.removeSpot = function($index) {
        // $scope.currentTrip.spots[$index].checked = false;
        $scope.currentTrip.spots.splice($index, 1);
    };

    $scope.srcTabs = [
        {
            id: 1,
            name: '遊玩推薦'
        },
        {
            id: 2,
            name: '住宿推薦'
        },
        {
            id: 3,
            name: '熱門景點'
        },
        {
            id: 4,
            name: '我的收藏'
        }
    ];
    $scope.currentSrcTab = $scope.srcTabs[0];
    $scope.switchSrcTab = function(tab) {
        $scope.currentSrcTab = tab;
    };

    $scope.countyList = [
        {
            id: 1,
            name: '台北市'
        }, 
        {
            id: 2,
            name: '高雄市'
        }, 
        {
            id: 3,
            name: '台中市'
        }, 
        {
            id: 4,
            name: '桃園市'
        }, 
        {
            id: 5,
            name: '新竹市'
        }
    ];
    $scope.currentCounty = $scope.countyList[0].id;

    $scope.types = [
        {
            id: 1,
            name: '不限'
        },
        {
            id: 2,
            name: '景點'
        },
        {
            id: 3,
            name: '美食'
        },
        {
            id: 4,
            name: '活動'
        },
        {
            id: 5,
            name: '商店'
        }
    ];
    $scope.currentType = $scope.types[0];
    $scope.switchType = function(type) {
        $scope.currentType = type;
    };

    $scope.searchTextChange = function(keyword) {
        // TODO
    };

    $scope.selectedItemChange = function(item) {
        // TODO
    };

    $scope.querySearch = function(keyword) {
        // TODO
        return [
            {
                display: '九份'
            },
            {
                display: '九份老街'
            },
            {
                display: '九份阿柑姨芋圓'
            }
        ];
    };

    $scope.soptList = [
        {
            thumbnail: '../public/images/spots/spot-1.jpg',
            title: '台北 101 大樓',
            descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
            commentCount: 22,
            collectCount: 77
        },
        {
            thumbnail: '../public/images/spots/spot-2.jpg',
            title: '傳統藝術博物館',
            descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
            commentCount: 22,
            collectCount: 77
        },
        {
            thumbnail: '../public/images/spots/spot-3.jpg',
            title: '台北 101 大樓',
            descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
            commentCount: 22,
            collectCount: 77
        },
        {
            thumbnail: '../public/images/spots/spot-4.jpg',
            title: '傳統藝術博物館',
            descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
            commentCount: 22,
            collectCount: 77
        },
        {
            thumbnail: '../public/images/spots/spot-5.jpg',
            title: '台北 101 大樓',
            descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
            commentCount: 22,
            collectCount: 77
        },
        {
            thumbnail: '../public/images/spots/spot-6.jpg',
            title: '傳統藝術博物館',
            descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
            commentCount: 22,
            collectCount: 77
        },
        {
            thumbnail: '../public/images/spots/spot-7.jpg',
            title: '台北 101 大樓',
            descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
            commentCount: 22,
            collectCount: 77
        },
        {
            thumbnail: '../public/images/spots/spot-8.jpg',
            title: '傳統藝術博物館',
            descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
            commentCount: 22,
            collectCount: 77
        },
        {
            thumbnail: '../public/images/spots/spot-1.jpg',
            title: '台北 101 大樓',
            descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
            commentCount: 22,
            collectCount: 77
        },
        {
            thumbnail: '../public/images/spots/spot-2.jpg',
            title: '傳統藝術博物館',
            descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
            commentCount: 22,
            collectCount: 77
        },
        {
            thumbnail: '../public/images/spots/spot-3.jpg',
            title: '台北 101 大樓',
            descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
            commentCount: 22,
            collectCount: 77
        },
        {
            thumbnail: '../public/images/spots/spot-4.jpg',
            title: '傳統藝術博物館',
            descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
            commentCount: 22,
            collectCount: 77
        },
        {
            thumbnail: '../public/images/spots/spot-5.jpg',
            title: '台北 101 大樓',
            descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
            commentCount: 22,
            collectCount: 77
        },
        {
            thumbnail: '../public/images/spots/spot-6.jpg',
            title: '傳統藝術博物館',
            descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
            commentCount: 22,
            collectCount: 77
        },
        {
            thumbnail: '../public/images/spots/spot-7.jpg',
            title: '台北 101 大樓',
            descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
            commentCount: 22,
            collectCount: 77
        },
        {
            thumbnail: '../public/images/spots/spot-8.jpg',
            title: '傳統藝術博物館',
            descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
            commentCount: 22,
            collectCount: 77
        }
    ];

    $scope.stayTypes = [
        {
            id: 1,
            name: '不限'
        },
        {
            id: 2,
            name: '飯店'
        },
        {
            id: 3,
            name: '青年旅社'
        },
        {
            id: 4,
            name: '旅館'
        },
        {
            id: 5,
            name: '公寓'
        },
        {
            id: 6,
            name: '家庭旅館'
        },
        {
            id: 7,
            name: '民宿'
        },
        {
            id: 8,
            name: '別墅/度假屋'
        }
    ];
    $scope.currentStayType = $scope.stayTypes[0].id;

    $scope.currentMapTab = 'day';


    $scope.createSpot = function (ev) {
        $mdDialog.show({
            controller: function ($scope, $mdDialog, $timeout) {
                $scope.spotData = {
                    thumbnail: '../public/images/spots/spot-1.jpg',
                    targetName: ''
                };
                $scope.cancel = function() {
                    $mdDialog.cancel();
                };
                $scope.createSpot = function(answer) {
                    $scope.creating = true;
                    $timeout(function () {
                        createTripVM.currentTrip.spots.push({
                            thumbnail: $scope.spotData.thumbnail,
                            title: $scope.spotData.targetName,
                            descript: $scope.spotData.note,
                            commentCount: 22,
                            collectCount: 77
                        });
                        $scope.creating = false;
                        $scope.createSuccess = true;
                        $mdDialog.cancel();
                    }, 1000);
                };

                $scope.uploadPhoto = function () {};

                $scope.types = [
                    {
                        id: 1,
                        name: '景點'
                    },
                    {
                        id: 2,
                        name: '美食'
                    },
                    {
                        id: 3,
                        name: '住宿'
                    },
                    {
                        id: 4,
                        name: '活動'
                    },
                    {
                        id: 5,
                        name: '商店'
                    }
                ];
                $scope.spotData.type = $scope.types[0].id;

            },
            templateUrl: '../assets/common/partials/createSpotMadal.html',
            targetEvent: ev,
        })
        .then(function(answer) {
            $scope.alert = 'You said the information was "' + answer + '".';
        }, function() {
            $scope.alert = 'You cancelled the dialog.';
        });
    };


    $scope.editSpot = function (spot, ev) {
        $mdDialog.show({
            controller: function ($scope, $mdDialog, $timeout) {
                $scope.spotData = {
                    thumbnail: spot.thumbnail,
                    targetName: spot.title,
                    note: spot.descript
                };
                $scope.cancel = function() {
                    $mdDialog.cancel();
                };
                $scope.save = function(answer) {
                    $scope.saving = true;
                    $timeout(function () {
                        spot.thumbnail = $scope.spotData.thumbnail;
                        spot.title = $scope.spotData.targetName;
                        spot.descript = $scope.spotData.note;
                        $scope.saving = false;
                        $scope.saveSuccess = true;
                        $mdDialog.cancel();
                    }, 1000);
                };

                $scope.uploadPhoto = function () {};

                $scope.types = [
                {
                    id: 1,
                    name: '景點'
                },
                {
                    id: 2,
                    name: '美食'
                },
                {
                    id: 3,
                    name: '住宿'
                },
                {
                    id: 4,
                    name: '活動'
                },
                {
                    id: 5,
                    name: '商店'
                }
                ];
                $scope.spotData.type = $scope.types[0].id;

            },
            templateUrl: '../assets/common/partials/editSpotMadal.html',
            targetEvent: ev,
        })
        .then(function(answer) {
            $scope.alert = 'You said the information was "' + answer + '".';
        }, function() {
            $scope.alert = 'You cancelled the dialog.';
        });
    };


    $scope.optimizePath = function (ev) {
        $mdDialog.show({
            controller: function ($scope, $mdDialog, $timeout) {
                $scope.spotsBeforeOptimize = ['松山文創園區', '台北美術館', '傳統黑糖冰', '國立故宮博物院'];
                $scope.spotsAfterOptimize = ['台北美術館', '松山文創園區', '傳統黑糖冰', '國立故宮博物院'];
                $scope.cancel = function() {
                    $mdDialog.cancel();
                };
                $scope.optimize = function(answer) {
                    $scope.optimizing = true;
                    $timeout(function () {
                        $scope.optimizing = false;
                        $scope.optimizeSuccess = true;
                        $mdDialog.cancel();
                    }, 1000);
                };

            },
            templateUrl: '../assets/common/partials/pathOptimizationMadal.html',
            targetEvent: ev,
        })
        .then(function(answer) {
            $scope.alert = 'You said the information was "' + answer + '".';
        }, function() {
            $scope.alert = 'You cancelled the dialog.';
        });
    };


    $scope.tripConfig = {
        group: {
            name: 'trips'
        },
        animation: 150,
        scroll: document.querySelector('.trip-list-wrap'),
        scrollSensitivity: 120,
        scrollSpeed: 20,
        ghostClass: "placeholder"
    };

    $scope.tripSpotConfig = {
        group: {
            name: 'tripSpots',
            pull: false,
            put: ['targetSpots', 'sourceSpots']
        },
        animation: 150,
        sort: false,
        scroll: document.querySelector('.trip-list-wrap'),
        scrollSensitivity: 200,
        scrollSpeed: 20
    };

    $scope.targetSpotConfig = {
        group: {
            name: 'targetSpots',
            put: ['sourceSpots']
        },
        animation: 150,
        scroll: document.querySelector('.trip-detail .spot-list-wrap'),
        scrollSensitivity: 200,
        scrollSpeed: 20,
        onStart: function (evt) {
            $scope.isSpotDragging = true;
        },
        onEnd: function (evt) {
            $scope.isSpotDragging = false;
        }
    };

    $scope.sourceSpotConfig = {
        group: {
            name: 'sourceSpots',
            pull: 'clone',
            put: false
        },
        animation: 150,
        scrollSensitivity: 200,
        sort: false,

        // Element is removed from the list into another list
        onRemove: function (evt) {
            evt.models[evt.oldIndex].checked = true;
        },
        onStart: function (evt) {
            $scope.isSpotDragging = true;
        },
        onEnd: function (evt) {
            $scope.isSpotDragging = false;
        }
    };

  }
);

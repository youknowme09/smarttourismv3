"use strict";

angular.module('ng.controller').controller('helpController' ,
  function( userService, $mdSidenav, $mdBottomSheet, $log, $q, $scope, $state, $mdDialog) {

  	$scope.$state = $state;

    var vm = $scope;


    $scope.problemReport = function (ev) {
      $mdDialog.show({
        controller: function ($scope, $mdDialog, $timeout) {
          $scope.report = {
            email: '',
            content: ''
          };
          $scope.cancel = function() {
            $mdDialog.cancel();
          };
          $scope.sendReport = function(answer) {
            $scope.reporting = true;
            $timeout(function () {
              $scope.reporting = false;
              $scope.reportSuccess = true;
            }, 1000);
          };
        },
        templateUrl: '../assets/common/partials/problemReportMadal.html',
        targetEvent: ev,
      })
      .then(function(answer) {
        $scope.alert = 'You said the information was "' + answer + '".';
      }, function() {
        $scope.alert = 'You cancelled the dialog.';
      });
    };

  }
);

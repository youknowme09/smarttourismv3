"use strict";

angular.module('ng.controller').controller('SouvenirController' ,
  function( userService, $mdSidenav, $mdBottomSheet, $log, $q, $scope) {

    $scope.people = 1;
    // $scope.layout = "List";
    $scope.layout = "Grid";
    $scope.minPrice;
    $scope.maxPrice;
    $scope.isOpenFilter = false;

    $scope.closeFilter = function () {
      $scope.isOpenFilter = false;      
    };

    $scope.openFilter = function () {
      $scope.isOpenFilter = true;      
    };

    $scope.showAllOption = {};

    $scope.toggleMore = function (option) {
      $scope.showAllOption[option] = !$scope.showAllOption[option];
    };

    $scope.orderBy = 'topRated';

    $scope.purchases = [
      {
        thumbnail: '../public/images/stays/stay-1.jpg',
        title: "【大黑松小倆口】元首鳳梨酥禮盒12入(鳳梨酥系列)",
        soldCount: 248,
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        soldCount: 248,
        rank: 3,
        viewes: 37,        commentCount: 37,
        price: 400,
        discount: 9,
        discountPrice: 352,
        label: '台灣名產',
        duration:2,
        popular: true,
        handsfree: true
      },
      {
        thumbnail: '../public/images/stays/stay-2.jpg',
        title: "【大黑松小倆口】元首鳳梨酥禮盒12入(鳳梨酥系列)",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        soldCount: 248,
        rank: 3,
        viewes: 37,        commentCount: 37,
        price: 400,
        discount: 9,
        duration:2,
        discountPrice: 352,
        label: '台灣名產',
        topChoice: true,
        handsfree: true
      },
      {
        thumbnail: '../public/images/stays/stay-3.jpg',
        title: "【春鑫食品】鳳梨酥中秋好禮6盒組(一顆15g/每盒24顆)",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        soldCount: 248,
        rank: 3,
        viewes: 37,        
        commentCount: 37,
        price: 352,
        label: '台灣名產',
        duration:2,
        handsfree: true
      },
      {
        thumbnail: '../public/images/stays/stay-4.jpg',
        title: "【大黑松小倆口】元首鳳梨酥禮盒12入(鳳梨酥系列)",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        soldCount: 248,
        rank: 3,
        viewes: 37,        
        commentCount: 37,
        price: 352,
        label: '台灣名產',
        duration:2,
        handsfree: true
      },
      {
        thumbnail: '../public/images/stays/stay-5.jpg',
        title: "【快車肉乾】特厚蜜汁豬肉乾",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        soldCount: 248,
        rank: 3,
        viewes: 37,        
        commentCount: 37,
        price: 352,
        label: '台灣名產',
        duration:2,
        handsfree: true
      },
      {
        thumbnail: '../public/images/stays/stay-6.jpg',
        title: "娘家嚴選最優質滴雞精 (30包搶購組)(J1)",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        soldCount: 248,
        rank: 3,
        viewes: 37,        
        commentCount: 37,
        price: 352,
        label: '台灣名產',
        duration:2,
        handsfree: true
      },
      {
        thumbnail: '../public/images/stays/stay-7.jpg',
        title: "【食上天】無骨德州烤雞排2包(20片/包)",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        soldCount: 248,
        rank: 3,
         viewes: 37,        
        commentCount: 37,
        price: 352,
        label: '台灣名產',
        duration:2,
        handsfree: true
      },
      {
        thumbnail: '../public/images/stays/stay-8.jpg',
        title: "【食上天】CAS合格認證頂級蒲燒鰻魚4尾(200g/尾)",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        soldCount: 248,
        rank: 3,
        viewes: 37,        
        commentCount: 37,
        price: 352,
        label: '台灣名產',
        duration:2,
        handsfree: true
      },
      {
        thumbnail: '../public/images/stays/stay-1.jpg',
        title: "【大黑松小倆口】元首鳳梨酥禮盒12入(鳳梨酥系列)",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        rank: 3,
        duration:2,
        commentCount: 37,
        price: 352,
        label: '台灣名產'
      },
      {
        thumbnail: '../public/images/stays/stay-2.jpg',
        title: "【大黑松小倆口】元首鳳梨酥禮盒12入(鳳梨酥系列)",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        soldCount: 248,
        rank: 3,
         duration:2,
       viewes: 37,        commentCount: 37,
        price: 352,
        label: '台灣名產'
      },
      {
        thumbnail: '../public/images/stays/stay-3.jpg',
        title: "【大黑松小倆口】元首鳳梨酥禮盒12入(鳳梨酥系列)",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        soldCount: 248,
        rank: 3,
          duration:2,
      viewes: 37,        commentCount: 37,
        price: 352,
        label: '台灣名產'
      },
      {
        thumbnail: '../public/images/stays/stay-4.jpg',
        title: "【大黑松小倆口】元首鳳梨酥禮盒12入(鳳梨酥系列)",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        soldCount: 248,
          duration:2,
      rank: 3,
        viewes: 37,        commentCount: 37,
        price: 352,
        label: '台灣名產'
      },
      {
        thumbnail: '../public/images/stays/stay-5.jpg',
        title: "【大黑松小倆口】元首鳳梨酥禮盒12入(鳳梨酥系列)",
        duration:2,
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        soldCount: 248,
        rank: 3,
        viewes: 37,        commentCount: 37,
        price: 352,
        label: '台灣名產'
      },
      {
        thumbnail: '../public/images/stays/stay-6.jpg',
        title: "【大黑松小倆口】元首鳳梨酥禮盒12入(鳳梨酥系列)",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        soldCount: 248,
        rank: 3,
        viewes: 37,        commentCount: 37,
        duration:2,
        price: 352,
        label: '台灣名產'
      },
      {
        thumbnail: '../public/images/stays/stay-7.jpg',
        title: "【大黑松小倆口】元首鳳梨酥禮盒12入(鳳梨酥系列)",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        soldCount: 248,
        rank: 3,
        duration:2,
        viewes: 37,        commentCount: 37,
        price: 352,
        label: '台灣名產'
      },
      {
        thumbnail: '../public/images/stays/stay-8.jpg',
        title: "【大黑松小倆口】元首鳳梨酥禮盒12入(鳳梨酥系列)",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        soldCount: 248,
        rank: 3,
         duration:2,
       viewes: 37,        commentCount: 37,
        price: 352,
        label: '台灣名產'
      }
    ];

    $scope.switchLayout = function (layout) {
      $scope.layout = layout;
    };


  }
);

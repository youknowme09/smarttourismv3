"use strict";

angular.module('ng.controller').controller('TrafficController' ,
  function( userService, $mdSidenav, $mdBottomSheet, $log, $q, $scope) {

  	$scope.people = 1;
  	// $scope.layout = "List";
  	$scope.layout = "Grid";
    $scope.isOpenFilter = false;

    $scope.closeFilter = function () {
      $scope.isOpenFilter = false;      
    };

    $scope.openFilter = function () {
      $scope.isOpenFilter = true;      
    };

    $scope.showAllOption = {};

    $scope.toggleMore = function (option) {
      $scope.showAllOption[option] = !$scope.showAllOption[option];
    };

    $scope.orderBy = 'topRated';

  	$scope.traffics = [
  		{
        thumbnail: '../public/images/stores/1.png',
  			title: "和運租車 - 台北濱江站",
  			descript: "和運租車股份有限公司簡稱和運租車，是臺灣一家汽車租賃公司，為和泰汽車與日本豐田金融服務株式會社於1999年5月24日合資創立。該公司目",
  			rank: 3,
        type: "汽車租賃",
        address: "台北市中山區松江路557號",
  			author: "奧革士",
  			isCollect : false,
  			commentCount: 237,
        tags: ['必去', '台北'],
        discount:true,
        comment:30,
        views:100
  		},{
        thumbnail: '../public/images/stores/1.png',
        title: "和運租車 - 台北濱江站",
        descript: "和運租車股份有限公司簡稱和運租車，是臺灣一家汽車租賃公司，為和泰汽車與日本豐田金融服務株式會社於1999年5月24日合資創立。該公司目",
        rank: 3,
        type: "汽車租賃",
        address: "台北市中山區松江路557號",
        author: "奧革士",
        isCollect : false,
        commentCount: 237,
        tags: ['必去', '台北'],
        discount:true,
        comment:30,
        views:100
      },
  		{
        thumbnail: '../public/images/stores/2.png',
        title: "格上租車",
        descript: "和運租車股份有限公司簡稱和運租車，是臺灣一家汽車租賃公司，為和泰汽車與日本豐田金融服務株式會社於1999年5月24日合資創立。該公司目",
        rank: 3,
        type: "接送服務",
        address: "台北市中山區松江路557號",
        author: "奧革士",
        isCollect : false,
        commentCount: 237,
        tags: ['必去', '台北'],
        discount:true,
        comment:30,
        views:100
  		},
      {
        thumbnail: '../public/images/stores/3.png',
        title: "格上租車",
        descript: "和運租車股份有限公司簡稱和運租車，是臺灣一家汽車租賃公司，為和泰汽車與日本豐田金融服務株式會社於1999年5月24日合資創立。該公司目",
        rank: 3,
        type: "機車租賃",
        address: "台北市中山區松江路557號",
        author: "奧革士",
        isCollect : false,
        commentCount: 237,
        tags: ['必去', '台北'],
        discount:true,
        comment:30,
        views:100
      },
      {
        thumbnail: '../public/images/stores/4.png',
        title: "格上租車",
        descript: "和運租車股份有限公司簡稱和運租車，是臺灣一家汽車租賃公司，為和泰汽車與日本豐田金融服務株式會社於1999年5月24日合資創立。該公司目",
        rank: 3,
        type: "汽車租賃",
        address: "台北市中山區松江路557號",
        author: "奧革士",
        isCollect : false,
        commentCount: 237,
        tags: ['必去', '台北'],
        discount:true,
        comment:30,
        views:100
      }
  	];

  	$scope.toggleCollect = function (trip) {
  		trip.isCollect = !trip.isCollect;
  	};

  	$scope.switchLayout = function (layout) {
  		$scope.layout = layout;
  	};


  }
);

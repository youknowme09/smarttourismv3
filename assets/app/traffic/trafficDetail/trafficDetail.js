"use strict";

angular.module('ng.controller').controller('TrafficDetailController' ,
  function( userService, $mdSidenav, $mdBottomSheet, $log, $q, $scope, $mdDialog) {

    

    $scope.bugReport = function (ev) {
      $mdDialog.show({
        controller: function ($scope, $mdDialog, $timeout) {
          $scope.report = {
            reasons: {
              stopSales: false,
              other: false
            }
          };
          $scope.haveReportReason = function () {
            return $scope.report.reasons.stopSales || 
              $scope.report.reasons.other;
          };
          $scope.cancel = function() {
            $mdDialog.cancel();
          };
          $scope.sendReport = function(answer) {
            $scope.reporting = true;
            $timeout(function () {
              $scope.reporting = false;
              $scope.reportSuccess = true;
            }, 1000);
          };
        },
        templateUrl: '../assets/common/partials/bugReportMadal.html',
        targetEvent: ev,
      })
      .then(function(answer) {
        $scope.alert = 'You said the information was "' + answer + '".';
      }, function() {
        $scope.alert = 'You cancelled the dialog.';
      });
    };



    $scope.slideList = [
      {
        "imgUrl" : "../public/images/stores/1.png"
      },
      {
        "imgUrl" : "../public/images/stores/2.png"
      },
      {
        "imgUrl" : "../public/images/stores/3.png"
      },
      {
        "imgUrl" : "../public/images/stores/4.png"
      },
      {
        "imgUrl" : "../public/images/stores/5.png"
      },
      {
        "imgUrl" : "../public/images/stores/6.png"
      },
      {
        "imgUrl" : "../public/images/stores/7.png"
      }
    ];

    $scope.currentSlideIdx = 0;

    $scope.prevSlide = function () {
      $scope.currentSlideIdx = $scope.currentSlideIdx > 0 ? $scope.currentSlideIdx - 1 : $scope.slideList.length - 1;
    };

    $scope.nextSlide = function () {
      $scope.currentSlideIdx = $scope.currentSlideIdx < ($scope.slideList.length - 1) ? $scope.currentSlideIdx + 1 : 0;
    };

    $scope.setSlide = function (idx) {
      $scope.currentSlideIdx = idx;
    };

    $scope.toggleBool = function (bool) {
      $scope[bool] = !$scope[bool];
    };



    $scope.trafficSlide = {
      currentIdx: 0,
      prev: function () {
        $scope.trafficSlide.currentIdx = $scope.trafficSlide.currentIdx > 0 ? $scope.trafficSlide.currentIdx - 1 : $scope.trafficGroup.length - 1;
      },
      next: function () {
        $scope.trafficSlide.currentIdx = $scope.trafficSlide.currentIdx < ($scope.trafficGroup.length - 1) ? $scope.trafficSlide.currentIdx + 1 : 0;
      },
      set: function (idx) {
        $scope.trafficSlide.currentIdx = idx;
      }
    };

    $scope.trafficGroup = [
      [
        {
          title: '馬櫻丹肥皂',
          price: 7235,
          specialPric: 4235,
          viewCount: 1336,
          thumbnail: '../public/images/spots/spot-1.jpg'
        },
        {
          title: '柑仔手工肥皂',
          price: 4235,
          viewCount: 1336,
          thumbnail: '../public/images/spots/spot-2.jpg'
        },
        {
          title: '阿原竹月刀',
          price: 4235,
          viewCount: 1336,
          thumbnail: '../public/images/spots/spot-3.jpg'
        },
        {
          title: '精油禮盒包 X 6',
          price: 4235,
          viewCount: 1336,
          thumbnail: '../public/images/spots/spot-4.jpg'
        }
      ],
      [
        {
          title: '馬櫻丹肥皂',
          price: 7235,
          specialPric: 4235,
          viewCount: 1336,
          thumbnail: '../public/images/spots/spot-5.jpg'
        },
        {
          title: '柑仔手工肥皂',
          price: 4235,
          viewCount: 1336,
          thumbnail: '../public/images/spots/spot-6.jpg'
        },
        {
          title: '阿原竹月刀',
          price: 4235,
          viewCount: 1336,
          thumbnail: '../public/images/spots/spot-7.jpg'
        },
        {
          title: '精油禮盒包 X 6',
          price: 4235,
          viewCount: 1336,
          thumbnail: '../public/images/spots/spot-8.jpg'
        }
      ]
    ];

    $scope.videos = [
      {
        thumbnail: '../public/images/stores/1.png',
        title: '和運租車微電影院-畢業前夕的告白',
        author: '奧革士',
        viewcount: '33210',
        description: '台北101大樓，一轉眼已經啟用10年，或許不少國人跟外國遊客都曾經上過位於91樓­的戶外觀景台，但別以為最高就到此為止，其實台北101真的有這第101層，面積只有­62坪，還得多轉乘兩次電梯才能抵達，以往都是招待國外貴賓或者超級VIP，就連許多­101員工都不一定知道有這第101層，今天首度曝光，不只觀景視野更棒，同時也成為­業者週年慶噱頭，得消費101萬才有資格登上這難得開放的第101層。'
      },
      {
        thumbnail: '../public/images/stores/2.png',
        title: '和運租車-春節CD_35秒',
        author: '奧革士',
        viewcount: '33210',
        description: '台北101大樓，一轉眼已經啟用10年，或許不少國人跟外國遊客都曾經上過位於91樓­的戶外觀景台，但別以為最高就到此為止，其實台北101真的有這第101層，面積只有­62坪，還得多轉乘兩次電梯才能抵達，以往都是招待國外貴賓或者超級VIP，就連許多­101員工都不一定知道有這第101層，今天首度曝光，不只觀景視野更棒，同時也成為­業者週年慶噱頭，得消費101萬才有資格登上這難得開放的第101層。'
      },
      {
        thumbnail: '../public/images/stores/2.png',
        title: '和運租車生日篇30s',
        author: '奧革士',
        viewcount: '33210',
        description: '台北101大樓，一轉眼已經啟用10年，或許不少國人跟外國遊客都曾經上過位於91樓­的戶外觀景台，但別以為最高就到此為止，其實台北101真的有這第101層，面積只有­62坪，還得多轉乘兩次電梯才能抵達，以往都是招待國外貴賓或者超級VIP，就連許多­101員工都不一定知道有這第101層，今天首度曝光，不只觀景視野更棒，同時也成為­業者週年慶噱頭，得消費101萬才有資格登上這難得開放的第101層。'
      }
    ];

    $scope.tickets = [
      {
        title: '$100 鼎泰豐飲食現金券',
        price: 60
      },
      {
        title: '鼎泰豐300元限量飲食現金折價券',
        price: 120
      }
    ];

    $scope.activities = [
      {
        title: '食我鑰匙大放送',
        startDate: '2015/03/10',
        endDate: '2015/05/10',
        message: '下載 Smart Tourism Taiwan 台灣智慧觀光 APP 並完成註冊、登入，馬上獲得食我鑰匙10把序號乙組！'
      },
      {
        title: 'Smart Tourism Taiwan 會員募集抽獎活動',
        startDate: '2015/03/10',
        endDate: '2015/05/10',
        message: '凡於活動期間下載 Smart Tourism Taiwan APP 並註冊成為會員，於行動裝置上的抽獎活動頁面登錄相關資訊，即可參加抽獎。'
      }
    ];

    $scope.comment = {
      author: '奧革士',
      authorImg: '../public/images/avatars/1.jpg',
      rank: undefined,
      comment: '',
      images: [
        '../public/images/stores/1.png', 
        '../public/images/stores/2.png', 
        '../public/images/stores/3.png'
      ],
      commentTime: ''
    };

    $scope.comments = [
      {
        author: '賀少俠',
        authorImg: '../public/images/avatars/2.jpg',
        rank: 4,
        comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
        images: [
          '../public/images/stores/1.png', 
          '../public/images/stores/2.png', 
          '../public/images/stores/3.png'
        ],
        commentTime: '5分鐘前',
        replys: [
          {
            author: '賀少俠',
            authorImg: '../public/images/avatars/2.jpg',
            rank: 4,
            comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
            images: [
              '../public/images/stores/1.png', 
              '../public/images/stores/2.png', 
              '../public/images/stores/3.png'
            ],
            commentTime: '5分鐘前',
            replys: []
          },
          {
            author: '路易四',
            authorImg: '../public/images/avatars/3.jpg',
            rank: 4,
            comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
            images: [],
            commentTime: '5分鐘前',
            replys: []
          }
        ]
      },
      {
        author: '路易四',
        authorImg: '../public/images/avatars/3.jpg',
        rank: 4,
        comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
        images: [],
        commentTime: '5分鐘前',
        replys: []
      },
      {
        author: '賀少俠',
        authorImg: '../public/images/avatars/4.jpg',
        rank: 4,
        comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
        images: [],
        commentTime: '5分鐘前',
        replys: []
      },
      {
        author: '賀少俠',
        authorImg: '../public/images/avatars/1.jpg',
        rank: 4,
        comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
        images: [],
        commentTime: '5分鐘前',
        replys: []
      }
    ];
 $scope.travelNotes = [[
        {
            title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
            descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
            rank: 4,
            types: ["文化", "樂活"],
            date: "2015/3/7",
            author: "奧革士",
            isCollect : false,
            location: "台北、桃園市、新竹市",
            collectCount: 234,
            recommend:true,
            comment:233,
            view:100
        },
        {
            title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
            descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，園",
            rank: 4,
            types: ["文化", "樂活"],
            date: "2015/3/7",
            author: "奧革士",
            isCollect : true,
            location: "台北、桃園市、新竹市",
            collectCount: 234,
            recommend:false,
            comment:233,
            view:100
        },
        {
            title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
            descript: "西門町位於台北市萬華區，由武昌街二段、漢中街、峨嵋街的行人徒步區所形成的區域為西門町的黃金地帶。大台北地區來往公車的密集交界，流行元素走在前端，是許多年輕學子及各國觀光客朝聖之處。",
            rank: 4,
            types: ["文化", "樂活"],
            date: "2015/3/7",
            author: "奧革士",
            isCollect : false,
            location: "台北、桃園市、新竹市",
            collectCount: 234,
            recommend:false,
            comment:233,
            view:100
        }],[
        {
            title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
            descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
            rank: 4,
            types: ["文化", "樂活"],
            date: "2015/3/7",
            author: "奧革士",
            isCollect : false,
            location: "台北、桃園市、新竹市",
            collectCount: 234,
            recommend:true,
            comment:233,
            view:100
        },
        {
            title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
            descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，園",
            rank: 4,
            types: ["文化", "樂活"],
            date: "2015/3/7",
            author: "奧革士",
            isCollect : true,
            location: "台北、桃園市、新竹市",
            collectCount: 234,
            recommend:false,
            comment:233,
            view:100
        },
        {
            title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
            descript: "西門町位於台北市萬華區，由武昌街二段、漢中街、峨嵋街的行人徒步區所形成的區域為西門町的黃金地帶。大台北地區來往公車的密集交界，流行元素走在前端，是許多年輕學子及各國觀光客朝聖之處。",
            rank: 4,
            types: ["文化", "樂活"],
            date: "2015/3/7",
            author: "奧革士",
            isCollect : false,
            location: "台北、桃園市、新竹市",
            collectCount: 234,
            recommend:false,
            comment:233,
            view:100
        }],[
        {
            title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
            descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
            rank: 4,
            types: ["文化", "樂活"],
            date: "2015/3/7",
            author: "奧革士",
            isCollect : false,
            location: "台北、桃園市、新竹市",
            collectCount: 234,
            recommend:true,
            comment:233,
            view:100
        },
        {
            title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
            descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，園",
            rank: 4,
            types: ["文化", "樂活"],
            date: "2015/3/7",
            author: "奧革士",
            isCollect : true,
            location: "台北、桃園市、新竹市",
            collectCount: 234,
            recommend:false,
            comment:233,
            view:100
        },
        {
            title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
            descript: "西門町位於台北市萬華區，由武昌街二段、漢中街、峨嵋街的行人徒步區所形成的區域為西門町的黃金地帶。大台北地區來往公車的密集交界，流行元素走在前端，是許多年輕學子及各國觀光客朝聖之處。",
            rank: 4,
            types: ["文化", "樂活"],
            date: "2015/3/7",
            author: "奧革士",
            isCollect : false,
            location: "台北、桃園市、新竹市",
            collectCount: 234,
            recommend:false,
            comment:233,
            view:100
        }]];
         $scope.travelSlide = {
      currentIdx: 0,
      prev: function () {
        $scope.travelSlide.currentIdx = $scope.travelSlide.currentIdx > 0 ? $scope.travelSlide.currentIdx - 1 : $scope.travelNotes.length - 1;
      },
      next: function () {
        $scope.travelSlide.currentIdx = $scope.travelSlide.currentIdx < ($scope.travelNotes.length - 1) ? $scope.travelSlide.currentIdx + 1 : 0;
      },
      set: function (idx) {
        $scope.travelSlide.currentIdx = idx;
      }
    };


    $scope.trips =[[
    {
     title: "台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊",
     descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
     rank: 4,
     types: ["文化", "樂活"],
     price: 4299,
     duration: 7,
     date: "2014年12月17日",
     author: "奧革士",
     isCollect : false,
     location: "台北市、台中市、高雄市",
     collectCount: 234,
     recommend:true,
     comment:233,
     view:100,
     days:7
   },
   {
     title: "台北都會一日遊",
     descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，園",
     rank: 4,
     types: ["文化", "樂活"],
     price: 0,
     duration: 3,
     date: "2014年12月17日",
     author: "奧革士",
     isCollect : true,
     location: "台北市、台中市、高雄市",
     collectCount: 234,
     recommend:false,
     comment:233,
     view:100,
     days:7
   },
   {
     title: "台北都會一日遊",
     descript: "西門町位於台北市萬華區，由武昌街二段、漢中街、峨嵋街的行人徒步區所形成的區域為西門町的黃金地帶。大台北地區來往公車的密集交界，流行元素走在前端，是許多年輕學子及各國觀光客朝聖之處。",
     rank: 4,
     types: ["文化", "樂活"],
     price: 0,
     duration: 5,
     date: "2014年12月17日",
     author: "奧革士",
     isCollect : false,
     location: "台北市、台中市、高雄市",
     collectCount: 234,
     recommend:false,
     comment:233,
     view:100,
     days:7
   }],[
    {
     title: "台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊",
     descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
     rank: 4,
     types: ["文化", "樂活"],
     price: 4299,
     duration: 7,
     date: "2014年12月17日",
     author: "奧革士",
     isCollect : false,
     location: "台北市、台中市、高雄市",
     collectCount: 234,
     recommend:true,
     comment:233,
     view:100,
     days:7
   },
   {
     title: "台北都會一日遊",
     descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，園",
     rank: 4,
     types: ["文化", "樂活"],
     price: 0,
     duration: 3,
     date: "2014年12月17日",
     author: "奧革士",
     isCollect : true,
     location: "台北市、台中市、高雄市",
     collectCount: 234,
     recommend:false,
     comment:233,
     view:100,
     days:7
   },
   {
     title: "台北都會一日遊",
     descript: "西門町位於台北市萬華區，由武昌街二段、漢中街、峨嵋街的行人徒步區所形成的區域為西門町的黃金地帶。大台北地區來往公車的密集交界，流行元素走在前端，是許多年輕學子及各國觀光客朝聖之處。",
     rank: 4,
     types: ["文化", "樂活"],
     price: 0,
     duration: 5,
     date: "2014年12月17日",
     author: "奧革士",
     isCollect : false,
     location: "台北市、台中市、高雄市",
     collectCount: 234,
     recommend:false,
     comment:233,
     view:100,
     days:7
   }],[
    {
     title: "台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊",
     descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
     rank: 4,
     types: ["文化", "樂活"],
     price: 4299,
     duration: 7,
     date: "2014年12月17日",
     author: "奧革士",
     isCollect : false,
     location: "台北市、台中市、高雄市",
     collectCount: 234,
     recommend:true,
     comment:233,
     view:100,
     days:7
   },
   {
     title: "台北都會一日遊",
     descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，園",
     rank: 4,
     types: ["文化", "樂活"],
     price: 0,
     duration: 3,
     date: "2014年12月17日",
     author: "奧革士",
     isCollect : true,
     location: "台北市、台中市、高雄市",
     collectCount: 234,
     recommend:false,
     comment:233,
     view:100,
     days:7
   },
   {
     title: "台北都會一日遊",
     descript: "西門町位於台北市萬華區，由武昌街二段、漢中街、峨嵋街的行人徒步區所形成的區域為西門町的黃金地帶。大台北地區來往公車的密集交界，流行元素走在前端，是許多年輕學子及各國觀光客朝聖之處。",
     rank: 4,
     types: ["文化", "樂活"],
     price: 0,
     duration: 5,
     date: "2014年12月17日",
     author: "奧革士",
     isCollect : false,
     location: "台北市、台中市、高雄市",
     collectCount: 234,
     recommend:false,
     comment:233,
     view:100,
     days:7
   }]];
       $scope.tripSlide = {
      currentIdx: 0,
      prev: function () {
        $scope.tripSlide.currentIdx = $scope.tripSlide.currentIdx > 0 ? $scope.tripSlide.currentIdx - 1 : $scope.trips.length - 1;
      },
      next: function () {
        $scope.tripSlide.currentIdx = $scope.tripSlide.currentIdx < ($scope.trips.length - 1) ? $scope.tripSlide.currentIdx + 1 : 0;
      },
      set: function (idx) {
        $scope.tripSlide.currentIdx = idx;
      }
    };
    $scope.addComment = function () {
      $scope.comments.splice(0, 0, {
        author: $scope.comment.author,
        authorImg: $scope.comment.authorImg,
        rank: 4,
        comment: $scope.comment.comment,
        images: [],
        commentTime: '幾秒鐘前',
        replys: []
      });
      $scope.comment.comment = '';
    };

    $scope.deleteComment = function (index) {
      $scope.comments.splice(index, 1);
    };

    $scope.toReply = function (item) {
      item.replyMode = true;
    };

    $scope.editComment = function (item) {
      item.editMode = true;
      item.commentTemp = item.comment;
    };

    $scope.updateComment = function (item) {
      item.editMode = false;
      item.comment = item.commentTemp;
    };

    $scope.cancelEditComment = function (item) {
      item.editMode = false;
    };

    $scope.addReply = function (comment) {
      comment.replys.splice(0, 0, {
        author: $scope.comment.author,
        authorImg: $scope.comment.authorImg,
        comment: comment.replyTemp,
        commentTime: '幾秒鐘前'
      });
      comment.replyTemp = '';
      comment.replyMode = false;
    };

    $scope.deleteReply = function (comment, index) {
      comment.replys.splice(index, 1);
    };

    $scope.types = [
      {
        id: 0,
        name: '景點'
      },
      {
        id: 1,
        name: '住宿'
      },
      {
        id: 2,
        name: '餐廳'
      },
      {
        id: 3,
        name: '商家'
      }
    ];

    $scope.currentType = $scope.types[0];

    $scope.selectType = function(item) {
      $scope.currentType = item;
    };

    $scope.spots = [
      {
        thumbnail: '../public/images/spots/spot-1.jpg',
        title: '華山文創園區',
        description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
        rank: 4,
        distance: 2
      },
      {
        thumbnail: '../public/images/spots/spot-2.jpg',
        title: '市立台北動物園',
        description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
        rank: 4,
        distance: 2
      },
      {
        thumbnail: '../public/images/spots/spot-3.jpg',
        title: '松山文創園區',
        description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
        rank: 4,
        distance: 2
      },
      {
        thumbnail: '../public/images/spots/spot-4.jpg',
        title: '傳統藝術博物館',
        description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
        rank: 4,
        distance: 2
      },
      {
        thumbnail: '../public/images/spots/spot-5.jpg',
        title: '台北美術館',
        description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
        rank: 4,
        distance: 2
      }
    ];

  }
);

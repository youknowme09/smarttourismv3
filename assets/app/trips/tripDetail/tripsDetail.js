"use strict";

angular.module('ng.controller').controller('TripsDetailController' ,
  function( userService, $mdSidenav, $mdBottomSheet, $log, $q, $scope, $mdDialog) {

    

    $scope.bugReport = function (ev) {
      $mdDialog.show({
        controller: function ($scope, $mdDialog, $timeout) {
          $scope.report = {
            reasons: {
              stopSales: false,
              other: false
            }
          };
          $scope.haveReportReason = function () {
            return $scope.report.reasons.stopSales || 
              $scope.report.reasons.other;
          };
          $scope.cancel = function() {
            $mdDialog.cancel();
          };
          $scope.sendReport = function(answer) {
            $scope.reporting = true;
            $timeout(function () {
              $scope.reporting = false;
              $scope.reportSuccess = true;
            }, 1000);
          };
        },
        templateUrl: '../assets/common/partials/bugReportMadal.html',
        targetEvent: ev,
      })
      .then(function(answer) {
        $scope.alert = 'You said the information was "' + answer + '".';
      }, function() {
        $scope.alert = 'You cancelled the dialog.';
      });
    };



    $scope.slideList = [
      {
        "imgUrl" : "../public/images/trips/trip-1.jpg"
      },
      {
        "imgUrl" : "../public/images/trips/trip-2.jpg"
      },
      {
        "imgUrl" : "../public/images/trips/trip-3.jpg"
      },
      {
        "imgUrl" : "../public/images/trips/trip-4.jpg"
      },
      {
        "imgUrl" : "../public/images/trips/trip-5.jpg"
      },
      {
        "imgUrl" : "../public/images/trips/trip-6.jpg"
      },
      {
        "imgUrl" : "../public/images/trips/trip-7.jpg"
      }
    ];

    $scope.currentSlideIdx = 0;

    $scope.prevSlide = function () {
      $scope.currentSlideIdx = $scope.currentSlideIdx > 0 ? $scope.currentSlideIdx - 1 : $scope.slideList.length - 1;
    };

    $scope.nextSlide = function () {
      $scope.currentSlideIdx = $scope.currentSlideIdx < ($scope.slideList.length - 1) ? $scope.currentSlideIdx + 1 : 0;
    };

    $scope.setSlide = function (idx) {
      $scope.currentSlideIdx = idx;
    };

    $scope.toggleBool = function (bool) {
      $scope[bool] = !$scope[bool];
    };

    $scope.tickets = [
      {
        title: '墾丁沙灘．小鹿斑比．仁武彩繪三日',
        description: '座落於東港渡船頭邊的黃昏市場，提供各式各樣現撈且物美價廉的海鮮魚貨而遠近馳名，攤位數達四百多個，目前已集中規劃為鮮魚區、生、熟食區、漁特產區及美食攤。',
        price: 120,
        specialPrice: 120
      },
      {
        title: '鼎泰豐300元限量飲食現金折價券',
        description: '座落於東港渡船頭邊的黃昏市場，提供各式各樣現撈且物美價廉的海鮮魚貨而遠近馳名，攤位數達四百多個，目前已集中規劃為鮮魚區、生、熟食區、漁特產區及美食攤。',
        price: 120,
        specialPrice: 120
      }
    ];

    $scope.comment = {
      author: '奧革士',
      authorImg: '../public/images/avatars/1.jpg',
      rank: undefined,
      comment: '',
      images: [
        '../public/images/trips/trip-1.jpg', 
        '../public/images/trips/trip-2.jpg', 
        '../public/images/trips/trip-3.jpg'
      ],
      commentTime: ''
    };

    $scope.comments = [
      {
        author: '賀少俠',
        authorImg: '../public/images/avatars/2.jpg',
        rank: 4,
        comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
        images: [
          '../public/images/trips/trip-1.jpg', 
          '../public/images/trips/trip-2.jpg', 
          '../public/images/trips/trip-3.jpg'
        ],
        commentTime: '5分鐘前',
        replys: [
          {
            author: '賀少俠',
            authorImg: '../public/images/avatars/2.jpg',
            rank: 4,
            comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
            images: [
              '../public/images/trips/trip-1.jpg', 
              '../public/images/trips/trip-2.jpg', 
              '../public/images/trips/trip-3.jpg'
            ],
            commentTime: '5分鐘前',
            replys: []
          },
          {
            author: '路易四',
            authorImg: '../public/images/avatars/3.jpg',
            rank: 4,
            comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
            images: [],
            commentTime: '5分鐘前',
            replys: []
          }
        ]
      },
      {
        author: '路易四',
        authorImg: '../public/images/avatars/3.jpg',
        rank: 4,
        comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
        images: [],
        commentTime: '5分鐘前',
        replys: []
      },
      {
        author: '賀少俠',
        authorImg: '../public/images/avatars/4.jpg',
        rank: 4,
        comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
        images: [],
        commentTime: '5分鐘前',
        replys: []
      },
      {
        author: '賀少俠',
        authorImg: '../public/images/avatars/1.jpg',
        rank: 4,
        comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
        images: [],
        commentTime: '5分鐘前',
        replys: []
      }
    ];

    $scope.addComment = function () {
      $scope.comments.splice(0, 0, {
        author: $scope.comment.author,
        authorImg: $scope.comment.authorImg,
        rank: 4,
        comment: $scope.comment.comment,
        images: [],
        commentTime: '幾秒鐘前',
        replys: []
      });
      $scope.comment.comment = '';
    };

    $scope.deleteComment = function (index) {
      $scope.comments.splice(index, 1);
    };

    $scope.toReply = function (item) {
      item.replyMode = true;
    };

    $scope.editComment = function (item) {
      item.editMode = true;
      item.commentTemp = item.comment;
    };

    $scope.updateComment = function (item) {
      item.editMode = false;
      item.comment = item.commentTemp;
    };

    $scope.cancelEditComment = function (item) {
      item.editMode = false;
    };

    $scope.addReply = function (comment) {
      comment.replys.splice(0, 0, {
        author: $scope.comment.author,
        authorImg: $scope.comment.authorImg,
        comment: comment.replyTemp,
        commentTime: '幾秒鐘前'
      });
      comment.replyTemp = '';
      comment.replyMode = false;
    };

    $scope.deleteReply = function (comment, index) {
      comment.replys.splice(index, 1);
    };

    $scope.types = [
      {
        id: 0,
        name: '景點'
      },
      {
        id: 1,
        name: '住宿'
      },
      {
        id: 2,
        name: '餐廳'
      },
      {
        id: 3,
        name: '商家'
      }
    ];

    $scope.currentType = $scope.types[0];

    $scope.selectType = function(item) {
      $scope.currentType = item;
    };

    $scope.spots = [
      {
        thumbnail: '../public/images/spots/spot-1.jpg',
        title: '華山文創園區',
        description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
        rank: 4,
        distance: 2
      },
      {
        thumbnail: '../public/images/spots/spot-2.jpg',
        title: '市立台北動物園',
        description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
        rank: 4,
        distance: 2
      },
      {
        thumbnail: '../public/images/spots/spot-3.jpg',
        title: '松山文創園區',
        description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
        rank: 4,
        distance: 2
      },
      {
        thumbnail: '../public/images/spots/spot-4.jpg',
        title: '傳統藝術博物館',
        description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
        rank: 4,
        distance: 2
      },
      {
        thumbnail: '../public/images/spots/spot-5.jpg',
        title: '台北美術館',
        description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
        rank: 4,
        distance: 2
      }
    ];

    $scope.guessYouLike = [
      {
        thumbnail: 'https://placeimg.com/100/80/nature?a=11',
        title: '黃家牛肉麵',
        description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
        rank: 4,
        distance: 2
      },
      {
        thumbnail: 'https://placeimg.com/100/80/nature?a=12',
        title: '煙燻德國豬腳',
        description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
        rank: 4,
        distance: 2
      },
      {
        thumbnail: 'https://placeimg.com/100/80/nature?a=13',
        title: '義式碳火披薩',
        description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
        rank: 4,
        distance: 2
      },
      {
        thumbnail: 'https://placeimg.com/100/80/nature?a=14',
        title: '傳統黑糖冰',
        description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
        rank: 4,
        distance: 2
      },
      {
        thumbnail: 'https://placeimg.com/100/80/nature?a=15',
        title: '冰淇淋鬆餅',
        description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
        rank: 4,
        distance: 2
      }
    ];

    $scope.blogs = [
      {
        thumbnail: 'https://placeimg.com/100/80/nature?a=1',
        title: '【台北。信義區】《101觀景台》最繁榮的街景，全在我腳下',
        publishDate: '2014/03/19',
        blogName: '巴台日誌'
      },
      {
        thumbnail: 'https://placeimg.com/100/80/nature?a=2',
        title: '台北自由行台北101旁TAIPEI GREEN HOUSE HOSTEL綠舍輕旅',
        publishDate: '2014/03/19',
        blogName: '巴台日誌'
      },
      {
        thumbnail: 'https://placeimg.com/100/80/nature?a=3',
        title: '【睡遍全球之airbnb】在台北101樓下發霉，在全台唯一的玉田墅睡得美美，AIRBNB好壞報告',
        publishDate: '2014/03/19',
        blogName: '巴台日誌'
      },
      {
        thumbnail: 'https://placeimg.com/100/80/nature?a=4',
        title: '台灣7日跨年背包遊，花東縱谷、太魯閣、台東海岸線、台北101樂遊記！',
        publishDate: '2014/03/19',
        blogName: '巴台日誌'
      }
    ];



    $scope.tabs = [
      {
        id: 0,
        name: '列表模式',
        type: 'list'
      },
      {
        id: 1,
        name: '日曆模式',
        type: 'calendar'
      }
    ];

    $scope.currentTab = $scope.tabs[0];

    $scope.switchTab = function(tab) {
      $scope.currentTab = tab;
    };


    $scope.trips = [
      {
        isOpen: true,
        place: '陽明山國家公園·賞花 ＞淡水老街',
        date_year: '2015',
        date_month: '12',
        date_day: '08',
        date_week: '二',
        spots: [
          {
            thumbnail: '../public/images/spots/spot-1.jpg',
            title: '1松山文創園區',
            descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
            commentCount: 22,
            collectCount: 77,
            type: 'spot'
          },
          {
            thumbnail: '../public/images/spots/spot-2.jpg',
            title: '1台北101大樓',
            descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
            commentCount: 22,
            collectCount: 77,
            type: 'spot'
          },
          {
            thumbnail: '../public/images/spots/spot-3.jpg',
            title: '1華山文創園區',
            descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
            commentCount: 22,
            collectCount: 77,
            type: 'spot'
          },
          {
            thumbnail: '../public/images/spots/spot-4.jpg',
            title: '1傳統黑糖冰',
            address: '台北市大同區長安西路33號',
            phone: '02 - 23456218',
            commentCount: 22,
            collectCount: 77,
            type: 'food'
          },
          {
            thumbnail: '../public/images/spots/spot-5.jpg',
            title: '1台北美術館',
            descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
            commentCount: 22,
            collectCount: 77,
            type: 'spot'
          }
        ],
        stays: [
          {
            thumbnail: '../public/images/spots/spot-6.jpg',
            title: '1療癒系公寓 乾淨又舒適的溫暖...',
            address: '台北市大同區長安西路33號',
            phone: '02 - 23456218',
            commentCount: 22,
            collectCount: 77,
            type: 'stay',
            notes: '住的是高級的房間，記得可以免費索取溫泉泡湯券'
          }
        ]
      },
      {
        place: '陽明山國家公園·賞花 ＞淡水老街',
        date_year: '2015',
        date_month: '12',
        date_day: '09',
        date_week: '三',
        spots: [
          {
            thumbnail: '../public/images/spots/spot-1.jpg',
            title: '2松山文創園區',
            descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
            commentCount: 22,
            collectCount: 77,
            type: 'spot'
          },
          {
            thumbnail: '../public/images/spots/spot-2.jpg',
            title: '2台北101大樓',
            descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
            commentCount: 22,
            collectCount: 77,
            type: 'spot'
          },
          {
            thumbnail: '../public/images/spots/spot-3.jpg',
            title: '2華山文創園區',
            descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
            commentCount: 22,
            collectCount: 77,
            type: 'spot'
          },
          {
            thumbnail: '../public/images/spots/spot-4.jpg',
            title: '2傳統黑糖冰',
            address: '台北市大同區長安西路33號',
            phone: '02 - 23456218',
            commentCount: 22,
            collectCount: 77,
            type: 'food'
          },
          {
            thumbnail: '../public/images/spots/spot-5.jpg',
            title: '2台北美術館',
            descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
            commentCount: 22,
            collectCount: 77,
            type: 'spot'
          }
        ],
        stays: [
          {
            thumbnail: '../public/images/spots/spot-6.jpg',
            title: '2療癒系公寓 乾淨又舒適的溫暖...',
            address: '台北市大同區長安西路33號',
            phone: '02 - 23456218',
            commentCount: 22,
            collectCount: 77,
            type: 'stay',
            notes: '住的是高級的房間，記得可以免費索取溫泉泡湯券'
          }
        ]
      },
      {
        place: '陽明山國家公園·賞花 ＞淡水老街',
        date_year: '2015',
        date_month: '12',
        date_day: '10',
        date_week: '四',
        spots: [
          {
            thumbnail: '../public/images/spots/spot-1.jpg',
            title: '3松山文創園區',
            descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
            commentCount: 22,
            collectCount: 77,
            type: 'spot'
          },
          {
            thumbnail: '../public/images/spots/spot-2.jpg',
            title: '3台北101大樓',
            descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
            commentCount: 22,
            collectCount: 77,
            type: 'spot'
          },
          {
            thumbnail: '../public/images/spots/spot-3.jpg',
            title: '3華山文創園區',
            descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
            commentCount: 22,
            collectCount: 77,
            type: 'spot'
          },
          {
            thumbnail: '../public/images/spots/spot-4.jpg',
            title: '3傳統黑糖冰',
            address: '台北市大同區長安西路33號',
            phone: '02 - 23456218',
            commentCount: 22,
            collectCount: 77,
            type: 'food'
          },
          {
            thumbnail: '../public/images/spots/spot-5.jpg',
            title: '3台北美術館',
            descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
            commentCount: 22,
            collectCount: 77,
            type: 'spot'
          }
        ],
        stays: [
          {
            thumbnail: '../public/images/spots/spot-6.jpg',
            title: '3療癒系公寓 乾淨又舒適的溫暖...',
            address: '台北市大同區長安西路33號',
            phone: '02 - 23456218',
            commentCount: 22,
            collectCount: 77,
            type: 'stay',
            notes: '住的是高級的房間，記得可以免費索取溫泉泡湯券'
          }
        ]
      },
      {
        place: '陽明山國家公園·賞花 ＞淡水老街',
        date_year: '2015',
        date_month: '12',
        date_day: '11',
        date_week: '五',
        spots: [
          {
            thumbnail: '../public/images/spots/spot-1.jpg',
            title: '4松山文創園區',
            descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
            commentCount: 22,
            collectCount: 77,
            type: 'spot'
          },
          {
            thumbnail: '../public/images/spots/spot-2.jpg',
            title: '4台北101大樓',
            descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
            commentCount: 22,
            collectCount: 77,
            type: 'spot'
          },
          {
            thumbnail: '../public/images/spots/spot-3.jpg',
            title: '4華山文創園區',
            descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
            commentCount: 22,
            collectCount: 77,
            type: 'spot'
          },
          {
            thumbnail: '../public/images/spots/spot-4.jpg',
            title: '4傳統黑糖冰',
            address: '台北市大同區長安西路33號',
            phone: '02 - 23456218',
            commentCount: 22,
            collectCount: 77,
            type: 'food'
          },
          {
            thumbnail: '../public/images/spots/spot-5.jpg',
            title: '4台北美術館',
            descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
            commentCount: 22,
            collectCount: 77,
            type: 'spot'
          }
        ],
        stays: [
          {
            thumbnail: '../public/images/spots/spot-6.jpg',
            title: '4療癒系公寓 乾淨又舒適的溫暖...',
            address: '台北市大同區長安西路33號',
            phone: '02 - 23456218',
            commentCount: 22,
            collectCount: 77,
            type: 'stay',
            notes: '住的是高級的房間，記得可以免費索取溫泉泡湯券'
          }
        ]
      }
    ];

    

    $scope.traffic = function (ev) {
      $mdDialog.show({
        controller: function ($scope, $mdDialog, $timeout) {
          $scope.showPublicTransportation = true;
          $scope.showDriving = true;
          $scope.showWalk = true;
          $scope.cancel = function() {
            $mdDialog.cancel();
          };
          $scope.done = function(answer) {
            $scope.processing = true;
            $timeout(function () {
              $scope.processing = false;
              $scope.success = true;
            }, 1000);
          };
        },
        templateUrl: '../assets/common/partials/trafficMadal.html',
        targetEvent: ev,
      })
      .then(function(answer) {
        $scope.alert = 'You said the information was "' + answer + '".';
      }, function() {
        $scope.alert = 'You cancelled the dialog.';
      });
    };

    

    $scope.openFullMap = function (ev) {
      $mdDialog.show({
        scope: $scope,
        controller: function ($scope, $mdDialog, $timeout) {
          console.log($scope);
          if ($scope.trips && $scope.trips.length > 0) {
            $scope.currentTrip = $scope.trips[0];
          }
          $scope.selectTrip = function(trip) {
            $scope.currentTrip = trip;
          };
          $scope.cancel = function() {
            $mdDialog.cancel();
          };
          $scope.done = function(answer) {
          };
        },
        templateUrl: '../assets/common/partials/tripFullMapMadal.html',
        targetEvent: ev,
      })
      .then(function(answer) {
        $scope.alert = 'You said the information was "' + answer + '".';
      }, function() {
        $scope.alert = 'You cancelled the dialog.';
      });
    };


  }
);

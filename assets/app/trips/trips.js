"use strict";

angular.module('ng.controller').controller('TripsController' ,
  function( userService, $mdSidenav, $mdBottomSheet, $log, $q, $scope) {

  	$scope.days = 1;
  	// $scope.layout = "List";
  	$scope.layout = "Grid";
  	$scope.minPrice;
  	$scope.maxPrice;
    $scope.isOpenFilter = false;

    $scope.closeFilter = function () {
      $scope.isOpenFilter = false;      
    };

    $scope.openFilter = function () {
      $scope.isOpenFilter = true;      
    };

    $scope.getTypes = function (spot) {
      return spot.types.slice(0, 2).join("、");
    };

    $scope.showAllOption = {};

    $scope.toggleMore = function (option) {
      $scope.showAllOption[option] = !$scope.showAllOption[option];
    };

    $scope.orderBy = 'topRated';

    $scope.trips = [
    {
     title: "台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊",
     descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
     rank: 4,
     types: ["文化", "樂活"],
     price: 4299,
     duration: 7,
     date: "2014年12月17日",
     author: "奧革士",
     isCollect : false,
     location: "台北市、台中市、高雄市",
     collectCount: 234,
     recommend:true,
     comment:233,
     view:100,
     days:7
   },
   {
     title: "台北都會一日遊",
     descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，園",
     rank: 4,
     types: ["文化", "樂活"],
     price: 0,
     duration: 3,
     date: "2014年12月17日",
     author: "奧革士",
     isCollect : true,
     location: "台北市、台中市、高雄市",
     collectCount: 234,
     recommend:false,
     comment:233,
     view:100,
     days:7
   },
   {
     title: "台北都會一日遊",
     descript: "西門町位於台北市萬華區，由武昌街二段、漢中街、峨嵋街的行人徒步區所形成的區域為西門町的黃金地帶。大台北地區來往公車的密集交界，流行元素走在前端，是許多年輕學子及各國觀光客朝聖之處。",
     rank: 4,
     types: ["文化", "樂活"],
     price: 0,
     duration: 5,
     date: "2014年12月17日",
     author: "奧革士",
     isCollect : false,
     location: "台北市、台中市、高雄市",
     collectCount: 234,
     recommend:false,
     comment:233,
     view:100,
     days:7
   },
   {
     title: "台北都會一日遊",
     descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式，大廳中央放置蔣介石坐姿銅像，底座部份設有展覽室和放映室，陳列蔣介石的一些文物，供民眾參觀。",
     rank: 4,
     types: ["文化", "樂活"],
     price: 4299,
     duration: 1,
     date: "2014年12月17日",
     author: "奧革士",
     isCollect : false,
     location: "台北市、台中市、高雄市",
     collectCount: 234,
     recommend:false,
     comment:233,
     view:100,
     days:7
   },
   {
     title: "台北都會一日遊",
     descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
     rank: 4,
     types: ["文化", "樂活"],
     price: 4299,
     duration: 12,
     date: "2014年12月17日",
     author: "奧革士",
     isCollect : false,
     location: "台北市、台中市、高雄市",
     collectCount: 234,
     recommend:false,
     comment:233,
     view:100,
     days:7
   },
   {
     title: "台北都會一日遊",
     descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，",
     rank: 4,
     types: ["文化", "樂活"],
     price: 4299,
     duration: 3,
     date: "2014年12月17日",
     author: "奧革士",
     isCollect : false,
     location: "台北市、台中市、高雄市",
     collectCount: 234,
     recommend:false,
     comment:233,
     view:100,
     days:7
   },
   {
     title: "台北都會一日遊",
     descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
     rank: 4,
     types: ["文化", "樂活"],
     price: 0,
     duration: 7,
     date: "2014年12月17日",
     author: "奧革士",
     isCollect : false,
     location: "台北市、台中市、高雄市",
     collectCount: 234,
     recommend:false,
     comment:233,
     view:100,
     days:7
   },
   {
     title: "台北都會一日遊",
     descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
     rank: 4,
     types: ["文化", "樂活"],
     price: 4299,
     duration: 7,
     date: "2014年12月17日",
     author: "奧革士",
     isCollect : false,
     location: "台北市、台中市、高雄市",
     collectCount: 234,
     recommend:false,
     comment:233,
     view:100,
     days:7
   },
   {
     title: "台北都會一日遊",
     descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
     rank: 4,
     types: ["文化", "樂活"],
     price: 4299,
     duration: 20,
     date: "2014年12月17日",
     author: "奧革士",
     isCollect : false,
     location: "台北市、台中市、高雄市",
     collectCount: 234,
     recommend:false,
     comment:233,
     view:100,
     days:7
   },
   {
     title: "台北都會一日遊",
     descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，園",
     rank: 4,
     types: ["文化", "樂活"],
     price: 4299,
     duration: 12,
     date: "2014年12月17日",
     author: "奧革士",
     isCollect : true,
     location: "台北市、台中市、高雄市",
     collectCount: 234,
     recommend:false,
     comment:233,
     view:100,
     days:7
   },
   {
     title: "台北都會一日遊",
     descript: "西門町位於台北市萬華區，由武昌街二段、漢中街、峨嵋街的行人徒步區所形成的區域為西門町的黃金地帶。大台北地區來往公車的密集交界，流行元素走在前端，是許多年輕學子及各國觀光客朝聖之處。",
     rank: 4,
     types: ["文化", "樂活"],
     price: 0,
     duration: 2,
     date: "2014年12月17日",
     author: "奧革士",
     isCollect : false,
     location: "台北市、台中市、高雄市",
     collectCount: 234,
     recommend:false,
     comment:233,
     view:100,
     days:7
   },
   {
     title: "台北都會一日遊",
     descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式，大廳中央放置蔣介石坐姿銅像，底座部份設有展覽室和放映室，陳列蔣介石的一些文物，供民眾參觀。",
     rank: 4,
     types: ["文化", "樂活"],
     price: 4299,
     duration: 7,
     date: "2014年12月17日",
     author: "奧革士",
     isCollect : false,
     location: "台北市、台中市、高雄市",
     collectCount: 234,
     recommend:false,
     comment:233,
     view:100,
     days:7
   },
   {
     title: "台北都會一日遊",
     descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
     rank: 4,
     types: ["文化", "樂活"],
     price: 4299,
     duration: 2,
     date: "2014年12月17日",
     author: "奧革士",
     isCollect : false,
     location: "台北市、台中市、高雄市",
     collectCount: 234,
     recommend:false,
     comment:233,
     view:100,
     days:7
   },
   {
     title: "台北都會一日遊",
     descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，",
     rank: 4,
     types: ["文化", "樂活"],
     price: 0,
     duration: 7,
     date: "2014年12月17日",
     author: "奧革士",
     isCollect : false,
     location: "台北市、台中市、高雄市",
     collectCount: 234,
     recommend:false,
     comment:233,
     view:100,
     days:7
   },
   {
     title: "台北都會一日遊",
     descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
     rank: 4,
     types: ["文化", "樂活"],
     price: 4299,
     duration: 2,
     date: "2014年12月17日",
     author: "奧革士",
     isCollect : false,
     location: "台北",
     collectCount: 234,
     recommend:false,
     comment:233,
     view:100,
     days:7
   },
   {
     title: "台北都會一日遊",
     descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
     rank: 4,
     types: ["文化", "樂活"],
     price: 4299,
     duration: 7,
     date: "2014年12月17日",
     author: "奧革士",
     isCollect : false,
     location: "台北市、台中市、高雄市",
     collectCount: 234,
     recommend:false,
     comment:233,
     view:100,
     days:7
   },

   ];

   $scope.toggleCollect = function (trip) {
    trip.isCollect = !trip.isCollect;
  };

  $scope.switchLayout = function (layout) {
    $scope.layout = layout;
  };


}
);

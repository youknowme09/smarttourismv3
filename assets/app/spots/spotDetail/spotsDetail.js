"use strict";

angular.module('ng.controller').controller('SpotsDetailController',
    function(userService, $mdSidenav, $mdBottomSheet, $log, $q, $scope, $mdDialog) {

        $scope.violationsReport = function(ev) {
            $mdDialog.show({
                    controller: function($scope, $mdDialog, $timeout) {
                        $scope.report = {
                            reasons: {
                                harassment: false,
                                infringement: false,
                                other: false
                            }
                        };
                        $scope.haveReportReason = function() {
                            return $scope.report.reasons.harassment ||
                                $scope.report.reasons.infringement ||
                                $scope.report.reasons.other;
                        };
                        $scope.cancel = function() {
                            $mdDialog.cancel();
                        };
                        $scope.sendReport = function(answer) {
                            $scope.reporting = true;
                            $timeout(function() {
                                $scope.reporting = false;
                                $scope.reportSuccess = true;
                            }, 1000);
                        };
                    },
                    templateUrl: '../assets/common/partials/violationsReportMadal.html',
                    targetEvent: ev,
                })
                .then(function(answer) {
                    $scope.alert = 'You said the information was "' + answer + '".';
                }, function() {
                    $scope.alert = 'You cancelled the dialog.';
                });
        };

        $scope.bugReport = function(ev) {
            $mdDialog.show({
                    controller: function($scope, $mdDialog, $timeout) {
                        $scope.report = {
                            reasons: {
                                stopSales: false,
                                other: false
                            }
                        };
                        $scope.haveReportReason = function() {
                            return $scope.report.reasons.stopSales ||
                                $scope.report.reasons.other;
                        };
                        $scope.cancel = function() {
                            $mdDialog.cancel();
                        };
                        $scope.sendReport = function(answer) {
                            $scope.reporting = true;
                            $timeout(function() {
                                $scope.reporting = false;
                                $scope.reportSuccess = true;
                            }, 1000);
                        };
                    },
                    templateUrl: '../assets/common/partials/bugReportMadal.html',
                    targetEvent: ev,
                })
                .then(function(answer) {
                    $scope.alert = 'You said the information was "' + answer + '".';
                }, function() {
                    $scope.alert = 'You cancelled the dialog.';
                });
        };

        $scope.editSpotsInfo = function(ev) {
            $mdDialog.show({
                    controller: function($scope, $mdDialog, $timeout) {
                        $scope.spotData = {
                            stay: 'minute'
                        };
                        $scope.currentTab = 1;
                        $scope.switchTab = function(tab) {
                            $scope.currentTab = tab;
                        };
                        $scope.cancel = function() {
                            $mdDialog.cancel();
                        };
                        $scope.save = function(answer) {
                            $scope.saving = true;
                            $timeout(function() {
                                $scope.saving = false;
                                $scope.editSuccess = true;
                            }, 1000);
                        };
                        $scope.slideList = [{
                            "imgUrl": "../public/images/spots/spot-1.jpg"
                        }, {
                            "imgUrl": "../public/images/spots/spot-2.jpg"
                        }, {
                            "imgUrl": "../public/images/spots/spot-3.jpg"
                        }, {
                            "imgUrl": "../public/images/spots/spot-4.jpg"
                        }, {
                            "imgUrl": "../public/images/spots/spot-5.jpg"
                        }, {
                            "imgUrl": "../public/images/spots/spot-6.jpg"
                        }, {
                            "imgUrl": "../public/images/spots/spot-7.jpg"
                        }, {
                            "imgUrl": "../public/images/spots/spot-1.jpg"
                        }, {
                            "imgUrl": "../public/images/spots/spot-2.jpg"
                        }, {
                            "imgUrl": "../public/images/spots/spot-3.jpg"
                        }, {
                            "imgUrl": "../public/images/spots/spot-4.jpg"
                        }, {
                            "imgUrl": "../public/images/spots/spot-5.jpg"
                        }];

                        $scope.currentSlideIdx = 0;

                        $scope.prevSlide = function() {
                            $scope.currentSlideIdx = $scope.currentSlideIdx > 0 ? $scope.currentSlideIdx - 1 : $scope.slideList.length - 1;
                        };

                        $scope.nextSlide = function() {
                            $scope.currentSlideIdx = $scope.currentSlideIdx < ($scope.slideList.length - 1) ? $scope.currentSlideIdx + 1 : 0;
                        };

                        $scope.setSlide = function(idx) {
                            $scope.currentSlideIdx = idx;
                        };

                        $scope.uploadPhoto = function() {};

                        $scope.citys = [{
                            id: 1,
                            name: '基隆市'
                        }, {
                            id: 2,
                            name: '台北市'
                        }, {
                            id: 3,
                            name: '新北市'
                        }, {
                            id: 4,
                            name: '桃園市'
                        }, {
                            id: 5,
                            name: '新竹市'
                        }, {
                            id: 6,
                            name: '新竹縣'
                        }, {
                            id: 7,
                            name: '宜蘭縣'
                        }, {
                            id: 8,
                            name: '苗栗縣'
                        }, {
                            id: 9,
                            name: '台中市'
                        }, {
                            id: 10,
                            name: '彰化縣'
                        }, {
                            id: 11,
                            name: '南投縣'
                        }, {
                            id: 12,
                            name: '雲林縣'
                        }, {
                            id: 13,
                            name: '嘉義市'
                        }, {
                            id: 14,
                            name: '嘉義縣'
                        }, {
                            id: 15,
                            name: '台南市'
                        }, {
                            id: 16,
                            name: '高雄市'
                        }, {
                            id: 17,
                            name: '屏東縣'
                        }, {
                            id: 18,
                            name: '澎湖縣'
                        }, {
                            id: 19,
                            name: '花蓮縣'
                        }, {
                            id: 20,
                            name: '台東縣'
                        }, {
                            id: 21,
                            name: '金門縣'
                        }, {
                            id: 22,
                            name: '連江縣'
                        }];
                        $scope.spotData.city = $scope.citys[1].id;

                        $scope.spotData.labels = [{
                            name: '觀光藝文',
                            checked: true
                        }, {
                            name: '商圈',
                            checked: true
                        }, {
                            name: '體育健身',
                            checked: false
                        }, {
                            name: '遊憩公園',
                            checked: false
                        }, {
                            name: '文化藝術',
                            checked: false
                        }, {
                            name: '國家風景區',
                            checked: false
                        }, {
                            name: '古蹟廟宇',
                            checked: false
                        }, {
                            name: '溫泉',
                            checked: false
                        }, {
                            name: '自然生態',
                            checked: false
                        }, {
                            name: '休閒農業',
                            checked: false
                        }];

                        $scope.spotData.otherLabels = [{
                            text: '台灣小吃'
                        }];

                        $scope.tempSpotData = {};

                        $scope.editSpotsCategory = function() {
                            $scope.showSpotCategory = true;
                            $scope.tempSpotData = angular.copy($scope.spotData);
                            $scope.tempSpotData.hasOtherLabels = true;
                        };

                        $scope.cancelEditSpotsCategory = function() {
                            $scope.showSpotCategory = false;
                        };

                        $scope.saveCategory = function() {
                            $scope.onSavingCategory = true;
                            $timeout(function() {
                                $scope.spotData = angular.copy($scope.tempSpotData);
                                $scope.onSavingCategory = false;
                                $scope.showSpotCategory = false;
                                if (!$scope.spotData.hasOtherLabels) {
                                    $scope.spotData.otherLabels.length = 0;
                                }
                            }, 1000);
                        };

                        $scope.addCategory = function(category) {
                            $scope.spotData.otherLabels.push(category);
                        };

                        $scope.removeCategory = function(index) {
                            $scope.spotData.otherLabels.splice(index, 1);
                        };

                    },
                    templateUrl: '../assets/common/partials/editSpotsInfoMadal.html',
                    targetEvent: ev,
                })
                .then(function(answer) {
                    $scope.alert = 'You said the information was "' + answer + '".';
                }, function() {
                    $scope.alert = 'You cancelled the dialog.';
                });
        };



        $scope.slideList = [{
            "imgUrl": "../public/images/spots/spot-1.jpg"
        }, {
            "imgUrl": "../public/images/spots/spot-2.jpg"
        }, {
            "imgUrl": "../public/images/spots/spot-3.jpg"
        }, {
            "imgUrl": "../public/images/spots/spot-4.jpg"
        }, {
            "imgUrl": "../public/images/spots/spot-5.jpg"
        }, {
            "imgUrl": "../public/images/spots/spot-6.jpg"
        }, {
            "imgUrl": "../public/images/spots/spot-7.jpg"
        }];

        $scope.currentSlideIdx = 0;

        $scope.prevSlide = function() {
            $scope.currentSlideIdx = $scope.currentSlideIdx > 0 ? $scope.currentSlideIdx - 1 : $scope.slideList.length - 1;
        };

        $scope.nextSlide = function() {
            $scope.currentSlideIdx = $scope.currentSlideIdx < ($scope.slideList.length - 1) ? $scope.currentSlideIdx + 1 : 0;
        };

        $scope.setSlide = function(idx) {
            $scope.currentSlideIdx = idx;
        };

        $scope.toggleBool = function(bool) {
            $scope[bool] = !$scope[bool];
        };

        $scope.videos = [{
            thumbnail: '../public/images/spots/spot-1.jpg',
            title: '3分鐘導覽台北101周邊',
            author: '奧革士',
            viewcount: '33210',
            description: '台北101大樓，一轉眼已經啟用10年，或許不少國人跟外國遊客都曾經上過位於91樓­的戶外觀景台，但別以為最高就到此為止，其實台北101真的有這第101層，面積只有­62坪，還得多轉乘兩次電梯才能抵達，以往都是招待國外貴賓或者超級VIP，就連許多­101員工都不一定知道有這第101層，今天首度曝光，不只觀景視野更棒，同時也成為­業者週年慶噱頭，得消費101萬才有資格登上這難得開放的第101層。'
        }, {
            thumbnail: '../public/images/spots/spot-2.jpg',
            title: '建築奇觀：台北101大樓',
            author: '奧革士',
            viewcount: '33210',
            description: '台北101大樓，一轉眼已經啟用10年，或許不少國人跟外國遊客都曾經上過位於91樓­的戶外觀景台，但別以為最高就到此為止，其實台北101真的有這第101層，面積只有­62坪，還得多轉乘兩次電梯才能抵達，以往都是招待國外貴賓或者超級VIP，就連許多­101員工都不一定知道有這第101層，今天首度曝光，不只觀景視野更棒，同時也成為­業者週年慶噱頭，得消費101萬才有資格登上這難得開放的第101層。'
        }, {
            thumbnail: '../public/images/spots/spot-3.jpg',
            title: '攝影者最愛，台北101夜景',
            author: '奧革士',
            viewcount: '33210',
            description: '台北101大樓，一轉眼已經啟用10年，或許不少國人跟外國遊客都曾經上過位於91樓­的戶外觀景台，但別以為最高就到此為止，其實台北101真的有這第101層，面積只有­62坪，還得多轉乘兩次電梯才能抵達，以往都是招待國外貴賓或者超級VIP，就連許多­101員工都不一定知道有這第101層，今天首度曝光，不只觀景視野更棒，同時也成為­業者週年慶噱頭，得消費101萬才有資格登上這難得開放的第101層。'
        }, {
            thumbnail: '../public/images/spots/spot-4.jpg',
            title: '2014高空看煙火跨年夜雙人套票 限量300組 12月1日起全台開賣',
            author: '奧革士',
            viewcount: '33210',
            description: '台北101大樓，一轉眼已經啟用10年，或許不少國人跟外國遊客都曾經上過位於91樓­的戶外觀景台，但別以為最高就到此為止，其實台北101真的有這第101層，面積只有­62坪，還得多轉乘兩次電梯才能抵達，以往都是招待國外貴賓或者超級VIP，就連許多­101員工都不一定知道有這第101層，今天首度曝光，不只觀景視野更棒，同時也成為­業者週年慶噱頭，得消費101萬才有資格登上這難得開放的第101層。'
        }];

        $scope.tickets = [{
            title: '初鹿觀光牧場 - 兒童票',
            preferentialMsg: '合作活動，免費領取',
            price: 0
        }, {
            title: '初鹿觀光牧場 - 成人票',
            price: 120
        }];

        $scope.activities = [{
            title: '食我鑰匙大放送',
            startDate: '2015/03/10',
            endDate: '2015/05/10',
            message: '下載 Smart Tourism Taiwan 台灣智慧觀光 APP 並完成註冊、登入，馬上獲得食我鑰匙10把序號乙組！'
        }, {
            title: 'Smart Tourism Taiwan 會員募集抽獎活動',
            startDate: '2015/03/10',
            endDate: '2015/05/10',
            message: '凡於活動期間下載 Smart Tourism Taiwan APP 並註冊成為會員，於行動裝置上的抽獎活動頁面登錄相關資訊，即可參加抽獎。'
        }];

        $scope.travelNotes = [
            [{
                title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
                descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
                rank: 4,
                types: ["文化", "樂活"],
                date: "2015/3/7",
                author: "奧革士",
                isCollect: false,
                location: "台北、桃園市、新竹市",
                collectCount: 234,
                recommend: true,
                comment: 233,
                view: 100
            }, {
                title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
                descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，園",
                rank: 4,
                types: ["文化", "樂活"],
                date: "2015/3/7",
                author: "奧革士",
                isCollect: true,
                location: "台北、桃園市、新竹市",
                collectCount: 234,
                recommend: false,
                comment: 233,
                view: 100
            }, {
                title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
                descript: "西門町位於台北市萬華區，由武昌街二段、漢中街、峨嵋街的行人徒步區所形成的區域為西門町的黃金地帶。大台北地區來往公車的密集交界，流行元素走在前端，是許多年輕學子及各國觀光客朝聖之處。",
                rank: 4,
                types: ["文化", "樂活"],
                date: "2015/3/7",
                author: "奧革士",
                isCollect: false,
                location: "台北、桃園市、新竹市",
                collectCount: 234,
                recommend: false,
                comment: 233,
                view: 100
            }],
            [{
                title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
                descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
                rank: 4,
                types: ["文化", "樂活"],
                date: "2015/3/7",
                author: "奧革士",
                isCollect: false,
                location: "台北、桃園市、新竹市",
                collectCount: 234,
                recommend: true,
                comment: 233,
                view: 100
            }, {
                title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
                descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，園",
                rank: 4,
                types: ["文化", "樂活"],
                date: "2015/3/7",
                author: "奧革士",
                isCollect: true,
                location: "台北、桃園市、新竹市",
                collectCount: 234,
                recommend: false,
                comment: 233,
                view: 100
            }, {
                title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
                descript: "西門町位於台北市萬華區，由武昌街二段、漢中街、峨嵋街的行人徒步區所形成的區域為西門町的黃金地帶。大台北地區來往公車的密集交界，流行元素走在前端，是許多年輕學子及各國觀光客朝聖之處。",
                rank: 4,
                types: ["文化", "樂活"],
                date: "2015/3/7",
                author: "奧革士",
                isCollect: false,
                location: "台北、桃園市、新竹市",
                collectCount: 234,
                recommend: false,
                comment: 233,
                view: 100
            }],
            [{
                title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
                descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
                rank: 4,
                types: ["文化", "樂活"],
                date: "2015/3/7",
                author: "奧革士",
                isCollect: false,
                location: "台北、桃園市、新竹市",
                collectCount: 234,
                recommend: true,
                comment: 233,
                view: 100
            }, {
                title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
                descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，園",
                rank: 4,
                types: ["文化", "樂活"],
                date: "2015/3/7",
                author: "奧革士",
                isCollect: true,
                location: "台北、桃園市、新竹市",
                collectCount: 234,
                recommend: false,
                comment: 233,
                view: 100
            }, {
                title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
                descript: "西門町位於台北市萬華區，由武昌街二段、漢中街、峨嵋街的行人徒步區所形成的區域為西門町的黃金地帶。大台北地區來往公車的密集交界，流行元素走在前端，是許多年輕學子及各國觀光客朝聖之處。",
                rank: 4,
                types: ["文化", "樂活"],
                date: "2015/3/7",
                author: "奧革士",
                isCollect: false,
                location: "台北、桃園市、新竹市",
                collectCount: 234,
                recommend: false,
                comment: 233,
                view: 100
            }]
        ];
        $scope.travelSlide = {
            currentIdx: 0,
            prev: function() {
                $scope.travelSlide.currentIdx = $scope.travelSlide.currentIdx > 0 ? $scope.travelSlide.currentIdx - 1 : $scope.travelNotes.length - 1;
            },
            next: function() {
                $scope.travelSlide.currentIdx = $scope.travelSlide.currentIdx < ($scope.travelNotes.length - 1) ? $scope.travelSlide.currentIdx + 1 : 0;
            },
            set: function(idx) {
                $scope.travelSlide.currentIdx = idx;
            }
        };


        $scope.trips = [
            [{
                title: "台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊",
                descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
                rank: 4,
                types: ["文化", "樂活"],
                price: 4299,
                duration: 7,
                date: "2014年12月17日",
                author: "奧革士",
                isCollect: false,
                location: "台北市、台中市、高雄市",
                collectCount: 234,
                recommend: true,
                comment: 233,
                view: 100,
                days: 7
            }, {
                title: "台北都會一日遊",
                descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，園",
                rank: 4,
                types: ["文化", "樂活"],
                price: 0,
                duration: 3,
                date: "2014年12月17日",
                author: "奧革士",
                isCollect: true,
                location: "台北市、台中市、高雄市",
                collectCount: 234,
                recommend: false,
                comment: 233,
                view: 100,
                days: 7
            }, {
                title: "台北都會一日遊",
                descript: "西門町位於台北市萬華區，由武昌街二段、漢中街、峨嵋街的行人徒步區所形成的區域為西門町的黃金地帶。大台北地區來往公車的密集交界，流行元素走在前端，是許多年輕學子及各國觀光客朝聖之處。",
                rank: 4,
                types: ["文化", "樂活"],
                price: 0,
                duration: 5,
                date: "2014年12月17日",
                author: "奧革士",
                isCollect: false,
                location: "台北市、台中市、高雄市",
                collectCount: 234,
                recommend: false,
                comment: 233,
                view: 100,
                days: 7
            }],
            [{
                title: "台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊",
                descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
                rank: 4,
                types: ["文化", "樂活"],
                price: 4299,
                duration: 7,
                date: "2014年12月17日",
                author: "奧革士",
                isCollect: false,
                location: "台北市、台中市、高雄市",
                collectCount: 234,
                recommend: true,
                comment: 233,
                view: 100,
                days: 7
            }, {
                title: "台北都會一日遊",
                descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，園",
                rank: 4,
                types: ["文化", "樂活"],
                price: 0,
                duration: 3,
                date: "2014年12月17日",
                author: "奧革士",
                isCollect: true,
                location: "台北市、台中市、高雄市",
                collectCount: 234,
                recommend: false,
                comment: 233,
                view: 100,
                days: 7
            }, {
                title: "台北都會一日遊",
                descript: "西門町位於台北市萬華區，由武昌街二段、漢中街、峨嵋街的行人徒步區所形成的區域為西門町的黃金地帶。大台北地區來往公車的密集交界，流行元素走在前端，是許多年輕學子及各國觀光客朝聖之處。",
                rank: 4,
                types: ["文化", "樂活"],
                price: 0,
                duration: 5,
                date: "2014年12月17日",
                author: "奧革士",
                isCollect: false,
                location: "台北市、台中市、高雄市",
                collectCount: 234,
                recommend: false,
                comment: 233,
                view: 100,
                days: 7
            }],
            [{
                title: "台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊台北都會一日遊",
                descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
                rank: 4,
                types: ["文化", "樂活"],
                price: 4299,
                duration: 7,
                date: "2014年12月17日",
                author: "奧革士",
                isCollect: false,
                location: "台北市、台中市、高雄市",
                collectCount: 234,
                recommend: true,
                comment: 233,
                view: 100,
                days: 7
            }, {
                title: "台北都會一日遊",
                descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，園",
                rank: 4,
                types: ["文化", "樂活"],
                price: 0,
                duration: 3,
                date: "2014年12月17日",
                author: "奧革士",
                isCollect: true,
                location: "台北市、台中市、高雄市",
                collectCount: 234,
                recommend: false,
                comment: 233,
                view: 100,
                days: 7
            }, {
                title: "台北都會一日遊",
                descript: "西門町位於台北市萬華區，由武昌街二段、漢中街、峨嵋街的行人徒步區所形成的區域為西門町的黃金地帶。大台北地區來往公車的密集交界，流行元素走在前端，是許多年輕學子及各國觀光客朝聖之處。",
                rank: 4,
                types: ["文化", "樂活"],
                price: 0,
                duration: 5,
                date: "2014年12月17日",
                author: "奧革士",
                isCollect: false,
                location: "台北市、台中市、高雄市",
                collectCount: 234,
                recommend: false,
                comment: 233,
                view: 100,
                days: 7
            }]
        ];
        $scope.tripSlide = {
            currentIdx: 0,
            prev: function() {
                $scope.tripSlide.currentIdx = $scope.tripSlide.currentIdx > 0 ? $scope.tripSlide.currentIdx - 1 : $scope.trips.length - 1;
            },
            next: function() {
                $scope.tripSlide.currentIdx = $scope.tripSlide.currentIdx < ($scope.trips.length - 1) ? $scope.tripSlide.currentIdx + 1 : 0;
            },
            set: function(idx) {
                $scope.tripSlide.currentIdx = idx;
            }
        };


        $scope.comment = {
            author: '奧革士',
            authorImg: '../public/images/avatars/1.jpg',
            rank: undefined,
            comment: '',
            images: [
                '../public/images/spots/spot-1.jpg',
                '../public/images/spots/spot-2.jpg',
                '../public/images/spots/spot-3.jpg'
            ],
            commentTime: ''
        };

        $scope.comments = [{
            author: '賀少俠',
            authorImg: '../public/images/avatars/2.jpg',
            rank: 4,
            comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
            images: [
                '../public/images/spots/spot-1.jpg',
                '../public/images/spots/spot-2.jpg',
                '../public/images/spots/spot-3.jpg'
            ],
            commentTime: '5分鐘前',
            replys: [{
                author: '賀少俠',
                authorImg: '../public/images/avatars/2.jpg',
                rank: 4,
                comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
                images: [
                    '../public/images/spots/spot-1.jpg',
                    '../public/images/spots/spot-2.jpg',
                    '../public/images/spots/spot-3.jpg'
                ],
                commentTime: '5分鐘前',
                replys: []
            }, {
                author: '路易四',
                authorImg: '../public/images/avatars/3.jpg',
                rank: 4,
                comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
                images: [],
                commentTime: '5分鐘前',
                replys: []
            }]
        }, {
            author: '路易四',
            authorImg: '../public/images/avatars/3.jpg',
            rank: 4,
            comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
            images: [],
            commentTime: '5分鐘前',
            replys: []
        }, {
            author: '賀少俠',
            authorImg: '../public/images/avatars/4.jpg',
            rank: 4,
            comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
            images: [],
            commentTime: '5分鐘前',
            replys: []
        }, {
            author: '賀少俠',
            authorImg: '../public/images/avatars/1.jpg',
            rank: 4,
            comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
            images: [],
            commentTime: '5分鐘前',
            replys: []
        }];

        $scope.addComment = function() {
            $scope.comments.splice(0, 0, {
                author: $scope.comment.author,
                authorImg: $scope.comment.authorImg,
                rank: 4,
                comment: $scope.comment.comment,
                images: [],
                commentTime: '幾秒鐘前',
                replys: []
            });
            $scope.comment.comment = '';
        };

        $scope.deleteComment = function(index) {
            $scope.comments.splice(index, 1);
        };

        $scope.toReply = function(item) {
            item.replyMode = true;
        };

        $scope.editComment = function(item) {
            item.editMode = true;
            item.commentTemp = item.comment;
        };

        $scope.updateComment = function(item) {
            item.editMode = false;
            item.comment = item.commentTemp;
        };

        $scope.cancelEditComment = function(item) {
            item.editMode = false;
        };

        $scope.addReply = function(comment) {
            comment.replys.splice(0, 0, {
                author: $scope.comment.author,
                authorImg: $scope.comment.authorImg,
                comment: comment.replyTemp,
                commentTime: '幾秒鐘前'
            });
            comment.replyTemp = '';
            comment.replyMode = false;
        };

        $scope.deleteReply = function(comment, index) {
            comment.replys.splice(index, 1);
        };

        // 周邊區塊 開始

        $scope.types = [{
            id: 0,
            name: '景點'
        }, {
            id: 1,
            name: '住宿'
        }, {
            id: 2,
            name: '餐廳'
        }, {
            id: 3,
            name: '商店'
        }];

        $scope.currentType = $scope.types[0];

        $scope.selectType = function(item) {
            $scope.currentType = item;
        };

        $scope.spots = [{
            thumbnail: '../public/images/spots/spot-1.jpg',
            title: '華山文創園區',
            description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
            rank: 4,
            distance: 2
        }, {
            thumbnail: '../public/images/spots/spot-2.jpg',
            title: '市立台北動物園',
            description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
            rank: 4,
            distance: 2
        }, {
            thumbnail: '../public/images/spots/spot-3.jpg',
            title: '松山文創園區',
            description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
            rank: 4,
            distance: 2
        }, {
            thumbnail: '../public/images/spots/spot-4.jpg',
            title: '傳統藝術博物館',
            description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
            rank: 4,
            distance: 2
        }, {
            thumbnail: '../public/images/spots/spot-5.jpg',
            title: '台北美術館',
            description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
            rank: 4,
            distance: 2
        }, {
            thumbnail: '../public/images/spots/spot-1.jpg',
            title: '華山文創園區2',
            description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
            rank: 4,
            distance: 2
        }, {
            thumbnail: '../public/images/spots/spot-2.jpg',
            title: '市立台北動物園2',
            description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
            rank: 4,
            distance: 2
        }, {
            thumbnail: '../public/images/spots/spot-3.jpg',
            title: '松山文創園區2',
            description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
            rank: 4,
            distance: 2
        }, {
            thumbnail: '../public/images/spots/spot-4.jpg',
            title: '傳統藝術博物館',
            description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
            rank: 4,
            distance: 2
        }, {
            thumbnail: '../public/images/spots/spot-5.jpg',
            title: '台北美術館',
            description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
            rank: 4,
            distance: 2
        }];

        // 周邊區塊 結束


        // 伴手禮區塊 左右選單 開始

        $scope.productsSlide = {
            currentIdx: 0,
            prev: function() {
                $scope.productsSlide.currentIdx = $scope.productsSlide.currentIdx > 0 ? $scope.productsSlide.currentIdx - 1 : $scope.products.length - 1;
            },
            next: function() {
                $scope.productsSlide.currentIdx = $scope.productsSlide.currentIdx < ($scope.products.length - 1) ? $scope.productsSlide.currentIdx + 1 : 0;
            },
            set: function(idx) {
                $scope.productsSlide.currentIdx = idx;
            }
        };

        // 伴手禮區塊 左右選單 結束

        // 買行程區塊 左右選單 開始

        $scope.toursSlide = {
            currentIdx: 0,
            prev: function() {
                $scope.toursSlide.currentIdx = $scope.toursSlide.currentIdx > 0 ? $scope.toursSlide.currentIdx - 1 : $scope.products.length - 1;
            },
            next: function() {
                $scope.toursSlide.currentIdx = $scope.toursSlide.currentIdx < ($scope.products.length - 1) ? $scope.toursSlide.currentIdx + 1 : 0;
            },
            set: function(idx) {
                $scope.toursSlide.currentIdx = idx;
            }
        };

        // 買行程區塊 左右選單 結束

        // 伴手禮+買行程區塊 開始

        $scope.products = [
            [{
                thumbnail: '../public/images/stores/1.png',
                title: '柑仔手工肥皂名字真的超長 超長的 無敵長 看會不會變兩行',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/2.png',
                title: '馬櫻丹肥皂',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/3.png',
                title: '阿原竹月刀',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/4.png',
                title: '精油禮盒包 X 6',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/5.png',
                title: '天然草本組合包',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }],
            [{
                thumbnail: '../public/images/stores/1.png',
                title: '柑仔手工肥皂',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/2.png',
                title: '馬櫻丹肥皂',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/3.png',
                title: '阿原竹月刀',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/4.png',
                title: '精油禮盒包 X 6',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/5.png',
                title: '天然草本組合包',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }],
            [{
                thumbnail: '../public/images/stores/1.png',
                title: '柑仔手工肥皂',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/2.png',
                title: '馬櫻丹肥皂',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/3.png',
                title: '阿原竹月刀',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/4.png',
                title: '精油禮盒包 X 6',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/5.png',
                title: '天然草本組合包',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }]
        ];

        // 伴手禮+買行程區塊 結束


        // 猜你喜歡區塊 開始

        $scope.guessYouLike = [{
            thumbnail: 'https://placeimg.com/100/80/nature?a=11',
            title: '黃家牛肉麵',
            description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
            rank: 4,
            distance: 2
        }, {
            thumbnail: 'https://placeimg.com/100/80/nature?a=12',
            title: '煙燻德國豬腳',
            description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
            rank: 4,
            distance: 2
        }, {
            thumbnail: 'https://placeimg.com/100/80/nature?a=13',
            title: '義式碳火披薩',
            description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
            rank: 4,
            distance: 2
        }, {
            thumbnail: 'https://placeimg.com/100/80/nature?a=14',
            title: '傳統黑糖冰',
            description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
            rank: 4,
            distance: 2
        }, {
            thumbnail: 'https://placeimg.com/100/80/nature?a=15',
            title: '冰淇淋鬆餅',
            description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
            rank: 4,
            distance: 2
        }];

        // 猜你喜歡區塊 結束


        $scope.blogs = [{
            thumbnail: 'https://placeimg.com/100/80/nature?a=1',
            title: '在台北101樓下發霉，在全台唯一的玉田墅睡得美美，AIRBNB好壞報告',
            publishDate: '2014/03/19',
            blogName: '巴台日誌'
        }, {
            thumbnail: 'https://placeimg.com/100/80/nature?a=2',
            title: '在台北101樓下發霉，在全台唯一的玉田墅睡得美美，AIRBNB好壞報告',
            publishDate: '2014/03/19',
            blogName: '巴台日誌'
        }, {
            thumbnail: 'https://placeimg.com/100/80/nature?a=3',
            title: '在台北101樓下發霉，在全台唯一的玉田墅睡得美美，AIRBNB好壞報告',
            publishDate: '2014/03/19',
            blogName: '巴台日誌'
        }, {
            thumbnail: 'https://placeimg.com/100/80/nature?a=4',
            title: '在台北101樓下發霉，在全台唯一的玉田墅睡得美美，AIRBNB好壞報告',
            publishDate: '2014/03/19',
            blogName: '巴台日誌'
        }];

        $scope.spot = {
            id: 1,
            title: "台北101大樓2",
            descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
            istripadv: true,
            rank: 4,
            view: 999,
            comment: 999,
            price: 120,
            types: ["商圈"],
            author: "奧革士",
            isCollect: false,
            address: "台北市大同區長安西路33號",
            location: "台北",
            collectCount: 234,
        };
        $scope.spot.rank = 0.5 * Math.round($scope.spot.rank / 0.5);
        $scope.spot.rank = -180 + $scope.spot.rank * 36;
        $scope.tripadv = {
            totalcomment: 2510,
            score: 4.7,
            rank: 4,
            bar: [20, 4, 70, 120, 41],
            barpercent: [8, 1, 27, 47, 16],
            comments: [{
                id: 1,
                title: "很不錯的服務體驗",
                rank: 3.2,
                author: "王*明",
                publishDate: "2016-3-27",
                description: "4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。"
            }, {
                id: 2,
                title: "很不錯的服務體驗",
                rank: 1,
                author: "王*明",
                publishDate: "2016-3-27",
                description: "4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。"
            }, {
                id: 3,
                title: "很不錯的服務體驗",
                rank: 0,
                author: "王*明",
                publishDate: "2016-3-27",
                description: "4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。"
            }, {
                id: 4,
                title: "很不錯的服務體驗",
                rank: 2,
                author: "王*明",
                publishDate: "2016-3-27",
                description: "4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。"
            }, {
                id: 5,
                title: "很不錯的服務體驗",
                rank: 3,
                author: "王*明",
                publishDate: "2016-3-27",
                description: "4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。"
            }]
        };

        angular.forEach($scope.tripadv.comments, function(value, key) {
            value.rank = 0.5 * Math.round(value.rank / 0.5);
            value.rank = -160 + value.rank * 32;
        });



    }
);

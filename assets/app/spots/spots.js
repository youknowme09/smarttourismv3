"use strict";

angular.module('ng.controller').controller('SpotsController' ,
  function( userService, $mdSidenav, $mdBottomSheet, $log, $q, $scope, $rootScope, $location) {

    $scope.poiType='spots';
  	$scope.days = 1;
  	// $scope.layout = "List";
  	$scope.layout = "Grid";
  	$scope.minPrice;
  	$scope.maxPrice;
    $scope.isOpenFilter = false;
    $scope.isEmpty = $location.search().empty;

    $scope.markerClick= function (poiId){
      $rootScope.$broadcast('markerClick',poiId);
    };
    $scope.closeFilter = function () {
      $scope.isOpenFilter = false;
    };

    $scope.openFilter = function () {
      $scope.isOpenFilter = true;
    };

    $scope.getTypes = function (spot) {
      return spot.types.slice(0, 2).join("、");
    };

    $scope.showAllOption = {};

    $scope.toggleMore = function (option) {
      $scope.showAllOption[option] = !$scope.showAllOption[option];
    };

    $scope.orderBy = 'topRated';

    $scope.spots = [
      {
        id:1,
        title: "台北101大樓台北101大樓台北101大樓台北101大樓台北101大樓台北101大樓",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        istripadv:true,
        mustgo:true,
        rank: 4,
        view:999,
        comment:999,
        price:200,
        isCollect : false,
        label: '台灣名產',
        address: "台北市大同區長安西路33號",
        locationtag: "TW1",
        location: "台北",
        tags: ["優惠"],
        location2:{
          lat:25.0339031,
          lng:121.5645098
        }
      },
      {
        id:2,
        title: "國立故宮博物院",
        descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，園",
        istripadv:false,
        mustgo:false,
        rank: 4,
        view:999,
        comment:999,
        price:200,
        isCollect : true,
        address: "台北市大同區長安西路33號",
        locationtag: "TW1",
        location: "台北",
        location2:{
          lat:25.1011729,
          lng:121.5487908
        }
      },
      {
        id:3,
        title: "西門町",
        descript: "西門町位於台北市萬華區，由武昌街二段、漢中街、峨嵋街的行人徒步區所形成的區域為西門町的黃金地帶。大台北地區來往公車的密集交界，流行元素走在前端，是許多年輕學子及各國觀光客朝聖之處。",
        istripadv:true,
        mustgo:false,
        rank: 4,
        view:999,
        comment:999,
        price:200,
        isCollect : false,
        address: "台北市大同區長安西路33號",
        locationtag: "TW1",
        location: "台北",
        location2:{
          lat:25.0451181,
          lng:121.5076305
        }
      },
      {
        id:4,
        title: "中正紀念堂",
        descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式，大廳中央放置蔣介石坐姿銅像，底座部份設有展覽室和放映室，陳列蔣介石的一些文物，供民眾參觀。",
        istripadv:true,
        mustgo:false,
        rank: 4,
        view:999,
        comment:999,
        price:200,
        isCollect : false,
        address: "台北市大同區長安西路33號",
        locationtag: "TW1",
        location: "台北",
        location2:{
          lat:25.034731,
          lng:121.521934
        }
      },
      {
        id:5,
        title: "九份老街",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        istripadv:true,
        mustgo:false,
        rank: 4,
        view:999,
        comment:999,
        price:200,
        isCollect : false,
        address: "台北市大同區長安西路33號",
        locationtag: "TW1",
        location: "台北",
        location2:{
          lat:25.108158,
          lng:121.8437955
        }
      },
      {
        id:6,
        title: "鵝鑾鼻公園",
        descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，",
        istripadv:true,
        mustgo:false,
        rank: 4,
        view:999,
        comment:999,
        price:200,
        isCollect : false,
        address: "台北市大同區長安西路33號",
        locationtag: "TW1",
        location: "台北",
        location2:{
          lat:21.9020441,
          lng:120.8529676
        }
      },
      {
        id:7,
        title: "高雄捷運美麗島站",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        istripadv:true,
        mustgo:false,
        rank: 4,
        view:999,
        comment:999,
        price:200,
        isCollect : false,
        address: "台北市大同區長安西路33號",
        locationtag: "TW1",
        location: "台北",
        location2:{
          lat:22.631386,
          lng:120.301951
        }
      },
      {
        id:8,
        title: "野柳地質公園",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        istripadv:true,
        mustgo:false,
        rank: 4,
        view:999,
        comment:999,
        price:200,
        isCollect : false,
        address: "台北市大同區長安西路33號",
        locationtag: "TW1",
        location: "台北",
        location2:{
          lat:25.2072029,
          lng:121.690895
        }
      },
      {
        id:9,
        title: "台北101大樓",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        istripadv:true,
        mustgo:false,
        rank: 4,
        view:999,
        comment:999,
        price:200,
        isCollect : false,
        address: "台北市大同區長安西路33號",
        locationtag: "TW1",
        location: "台北",
        location2:{
          lat:25.0339031,
          lng:121.5645098
        }
      },
      {
        id:10,
        title: "國立故宮博物院",
        descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，園",
        istripadv:true,
        mustgo:false,
        rank: 4,
        view:999,
        comment:999,
        price:200,
        isCollect : false,
        address: "台北市大同區長安西路33號",
        locationtag: "TW1",
        location: "台北",
        location2:{
          lat:25.1011729,
          lng:121.5487908
        }
      },
      {
        id:11,
        title: "西門町",
        descript: "西門町位於台北市萬華區，由武昌街二段、漢中街、峨嵋街的行人徒步區所形成的區域為西門町的黃金地帶。大台北地區來往公車的密集交界，流行元素走在前端，是許多年輕學子及各國觀光客朝聖之處。",
        istripadv:true,
        mustgo:false,
        rank: 4,
        view:999,
        comment:999,
        price:200,
        isCollect : false,
        address: "台北市大同區長安西路33號",
        locationtag: "TW1",
        location: "台北",
        location2:{
          lat:25.0451181,
          lng:121.5076305
        }
      },
      {
        id:12,
        title: "中正紀念堂",
        descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式，大廳中央放置蔣介石坐姿銅像，底座部份設有展覽室和放映室，陳列蔣介石的一些文物，供民眾參觀。",
        istripadv:true,
        mustgo:false,
        rank: 4,
        view:999,
        comment:999,
        price:200,
        isCollect : false,
        address: "台北市大同區長安西路33號",
        locationtag: "TW1",
        location: "台北",
        location2:{
          lat:25.034731,
          lng:121.521934
        }
      },
      {
        id:13,
        title: "九份老街",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        istripadv:true,
        mustgo:false,
        rank: 4,
        view:999,
        comment:999,
        price:200,
        isCollect : false,
        address: "台北市大同區長安西路33號",
        locationtag: "TW1",
        location: "台北",
        location2:{
          lat:25.108158,
          lng:121.8437955
        }
      },
      {
        id:14,
        title: "鵝鑾鼻公園",
        descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，",
        istripadv:true,
        mustgo:false,
        rank: 4,
        view:999,
        comment:999,
        price:200,
        isCollect : false,
        address: "台北市大同區長安西路33號",
        locationtag: "TW1",
        location: "台北",
        location2:{
          lat:21.9020441,
          lng:120.8529676
        }
      },
      {
        id:15,
        title: "高雄捷運美麗島站",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        istripadv:true,
        mustgo:false,
        rank: 4,
        view:999,
        comment:999,
        price:200,
        isCollect : false,
        address: "台北市大同區長安西路33號",
        locationtag: "TW1",
        location: "台北",
        location2:{
          lat:22.631386,
          lng:120.301951
        }
      },
      {
        id:16,
        title: "野柳地質公園",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        istripadv:true,
        mustgo:false,
        rank: 4,
        view:999,
        comment:999,
        price:200,
        isCollect : false,
        address: "台北市大同區長安西路33號",
        locationtag: "TW1",
        location: "台北",
        location2:{
          lat:25.2072029,
          lng:121.690895
        }
      }

    ];
     angular.forEach($scope.spots, function(value, key) {
      value.rankstar = 0.5*Math.round(value.rank/0.5);
      value.rankstar = -160+value.rankstar*32;
    });

  	$scope.toggleCollect = function (trip) {
  		trip.isCollect = !trip.isCollect;
  	};

  	$scope.switchLayout = function (layout) {
  		$scope.layout = layout;
  	};


  }
);

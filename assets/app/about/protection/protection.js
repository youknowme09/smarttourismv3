"use strict";

angular.module('ng.controller').controller('protectionController' ,
  function( userService, $mdSidenav, $mdBottomSheet, $log, $q, $scope, $state, $mdDialog) {

	$scope.$state = $state;

  	var vm = $scope;

  	$scope.articleList = [
  		{
  			title: '服務維護公告 - 系統將於5月29日進行維護',
  			publishDate: '2015年3月2日',
  			content: '各位親愛的使用者，台灣智慧觀光將於5月29日凌晨1點至8點進行網路服務升級維護，屆時相關頁面將無法訪問，給大家使用帶來不便，敬請見諒。'
  		},
  		{
  			title: '服務維護公告 - 系統將於5月29日進行維護',
  			publishDate: '2015年3月2日',
  			content: '各位親愛的使用者，台灣智慧觀光將於5月29日凌晨1點至8點進行網路服務升級維護，屆時相關頁面將無法訪問，給大家使用帶來不便，敬請見諒。'
  		},
  		{
  			title: '服務維護公告 - 系統將於5月29日進行維護',
  			publishDate: '2015年3月2日',
  			content: '各位親愛的使用者，台灣智慧觀光將於5月29日凌晨1點至8點進行網路服務升級維護，屆時相關頁面將無法訪問，給大家使用帶來不便，敬請見諒。'
  		},
  		{
  			title: '服務維護公告 - 系統將於5月29日進行維護',
  			publishDate: '2015年3月2日',
  			content: '各位親愛的使用者，台灣智慧觀光將於5月29日凌晨1點至8點進行網路服務升級維護，屆時相關頁面將無法訪問，給大家使用帶來不便，敬請見諒。'
  		},
  		{
  			title: '服務維護公告 - 系統將於5月29日進行維護',
  			publishDate: '2015年3月2日',
  			content: '各位親愛的使用者，台灣智慧觀光將於5月29日凌晨1點至8點進行網路服務升級維護，屆時相關頁面將無法訪問，給大家使用帶來不便，敬請見諒。'
  		}
  	];

  }
);

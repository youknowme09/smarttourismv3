"use strict";

angular.module('ng.controller').controller('TravelNotesController' ,
  function( userService, $mdSidenav, $mdBottomSheet, $log, $q, $scope, $mdDialog) {

    $scope.days = 1;
    // $scope.layout = "List";
    $scope.layout = "Grid";
    $scope.isOpenFilter = false;

    $scope.closeFilter = function () {
      $scope.isOpenFilter = false;      
    };

    $scope.openFilter = function () {
      $scope.isOpenFilter = true;      
    };

    $scope.getTypes = function (spot) {
      return spot.types.slice(0, 2).join("、");
    };

    $scope.showAllOption = {};

    $scope.toggleMore = function (option) {
      $scope.showAllOption[option] = !$scope.showAllOption[option];
    };

    $scope.orderBy = 'topRated';

    $scope.travelNotes = [
        {
            title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
            descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
            rank: 4,
            types: ["文化", "樂活"],
            date: "2015/3/7",
            author: "奧革士",
            isCollect : false,
            location: "台北、桃園市、新竹市",
            collectCount: 234,
            recommend:true,
            comment:233,
            view:100
        },
        {
            title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
            descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，園",
            rank: 4,
            types: ["文化", "樂活"],
            date: "2015/3/7",
            author: "奧革士",
            isCollect : true,
            location: "台北、桃園市、新竹市",
            collectCount: 234,
            recommend:false,
            comment:233,
            view:100
        },
        {
            title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
            descript: "西門町位於台北市萬華區，由武昌街二段、漢中街、峨嵋街的行人徒步區所形成的區域為西門町的黃金地帶。大台北地區來往公車的密集交界，流行元素走在前端，是許多年輕學子及各國觀光客朝聖之處。",
            rank: 4,
            types: ["文化", "樂活"],
            date: "2015/3/7",
            author: "奧革士",
            isCollect : false,
            location: "台北、桃園市、新竹市",
            collectCount: 234,
            recommend:false,
            comment:233,
            view:100
        },
        {
            title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
            descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式，大廳中央放置蔣介石坐姿銅像，底座部份設有展覽室和放映室，陳列蔣介石的一些文物，供民眾參觀。",
            rank: 4,
            types: ["文化", "樂活"],
            date: "2015/3/7",
            author: "奧革士",
            isCollect : false,
            location: "台北、桃園市、新竹市",
            collectCount: 234,
            recommend:false,
            comment:233,
            view:100
        },
        {
            title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
            descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
            rank: 4,
            types: ["文化", "樂活"],
            date: "2015/3/7",
            author: "奧革士",
            isCollect : false,
            location: "台北、桃園市、新竹市",
            collectCount: 234,
            recommend:false,
            comment:233,
            view:100
        },
        {
            title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
            descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，",
            rank: 4,
            types: ["文化", "樂活"],
            date: "2015/3/7",
            author: "奧革士",
            isCollect : false,
            location: "台北、桃園市、新竹市",
            collectCount: 234,
            recommend:false,
            comment:233,
            view:100
        },
        {
            title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
            descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
            rank: 4,
            types: ["文化", "樂活"],
            date: "2015/3/7",
            author: "奧革士",
            isCollect : false,
            location: "台北、桃園市、新竹市",
            collectCount: 234,
            recommend:false,
            comment:233,
            view:100
        },
        {
            title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
            descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
            rank: 4,
            types: ["文化", "樂活"],
            date: "2015/3/7",
            author: "奧革士",
            isCollect : false,
            location: "台北、桃園市、新竹市",
            collectCount: 234,
            recommend:false,
            comment:233,
            view:100
        },
        {
            title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
            descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
            rank: 4,
            types: ["文化", "樂活"],
            date: "2015/3/7",
            author: "奧革士",
            isCollect : false,
            location: "台北、桃園市、新竹市",
            collectCount: 234,
            recommend:false,
            comment:233,
            view:100
        },
        {
            title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
            descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，園",
            rank: 4,
            types: ["文化", "樂活"],
            date: "2015/3/7",
            author: "奧革士",
            isCollect : true,
            location: "台北、桃園市、新竹市",
            collectCount: 234,
            recommend:false,
            comment:233,
            view:100
        },
        {
            title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
            descript: "西門町位於台北市萬華區，由武昌街二段、漢中街、峨嵋街的行人徒步區所形成的區域為西門町的黃金地帶。大台北地區來往公車的密集交界，流行元素走在前端，是許多年輕學子及各國觀光客朝聖之處。",
            rank: 4,
            types: ["文化", "樂活"],
            date: "2015/3/7",
            author: "奧革士",
            isCollect : false,
            location: "台北、桃園市、新竹市",
            collectCount: 234,
            recommend:false,
            comment:233,
            view:100
        },
        {
            title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
            descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式，大廳中央放置蔣介石坐姿銅像，底座部份設有展覽室和放映室，陳列蔣介石的一些文物，供民眾參觀。",
            rank: 4,
            types: ["文化", "樂活"],
            date: "2015/3/7",
            author: "奧革士",
            isCollect : false,
            location: "台北、桃園市、新竹市",
            collectCount: 234,
            recommend:false,
            comment:233,
            view:100
        },
        {
            title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
            descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
            rank: 4,
            types: ["文化", "樂活"],
            date: "2015/3/7",
            author: "奧革士",
            isCollect : false,
            location: "台北、桃園市、新竹市",
            collectCount: 234,
            recommend:false,
            comment:233,
            view:100
        },
        {
            title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
            descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，",
            rank: 4,
            types: ["文化", "樂活"],
            date: "2015/3/7",
            author: "奧革士",
            isCollect : false,
            location: "台北、桃園市、新竹市",
            collectCount: 234,
            recommend:false,
            comment:233,
            view:100
        },
        {
            title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
            descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
            rank: 4,
            types: ["文化", "樂活"],
            date: "2015/3/7",
            author: "奧革士",
            isCollect : false,
            location: "台北、桃園市、新竹市",
            collectCount: 234,
            recommend:false,
            comment:233,
            view:100
        },
        {
            title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
            descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
            rank: 4,
            types: ["文化", "樂活"],
            date: "2015/3/7",
            author: "奧革士",
            isCollect : false,
            location: "台北、桃園市、新竹市",
            collectCount: 234,
            recommend:false,
            comment:233,
            view:100
        },

    ];

    $scope.toggleCollect = function (travelNote) {
        travelNote.isCollect = !travelNote.isCollect;
    };

    $scope.switchLayout = function (layout) {
        $scope.layout = layout;
    };

  }
);

"use strict";

angular.module('ng.controller').controller('TravelNotesDetailController' ,
  function( userService, $mdSidenav, $mdBottomSheet, $log, $q, $scope, $mdDialog) {

    $scope.violationsReport = function (ev) {
      $mdDialog.show({
        controller: function ($scope, $mdDialog, $timeout) {
          $scope.report = {
            reasons: {
              harassment: false,
              infringement: false,
              other: false
            }
          };
          $scope.haveReportReason = function () {
            return $scope.report.reasons.harassment ||
            $scope.report.reasons.infringement ||
            $scope.report.reasons.other;
          };
          $scope.cancel = function() {
            $mdDialog.cancel();
          };
          $scope.sendReport = function(answer) {
            $scope.reporting = true;
            $timeout(function () {
              $scope.reporting = false;
              $scope.reportSuccess = true;
            }, 1000);
          };
        },
        templateUrl: '../assets/common/partials/violationsReportMadal.html',
        targetEvent: ev,
      })
      .then(function(answer) {
        $scope.alert = 'You said the information was "' + answer + '".';
      }, function() {
        $scope.alert = 'You cancelled the dialog.';
      });
    };

    $scope.bugReport = function (ev) {
      $mdDialog.show({
        controller: function ($scope, $mdDialog, $timeout) {
          $scope.report = {
            reasons: {
              stopSales: false,
              other: false
            }
          };
          $scope.haveReportReason = function () {
            return $scope.report.reasons.stopSales || 
            $scope.report.reasons.other;
          };
          $scope.cancel = function() {
            $mdDialog.cancel();
          };
          $scope.sendReport = function(answer) {
            $scope.reporting = true;
            $timeout(function () {
              $scope.reporting = false;
              $scope.reportSuccess = true;
            }, 1000);
          };
        },
        templateUrl: '../assets/common/partials/bugReportMadal.html',
        targetEvent: ev,
      })
      .then(function(answer) {
        $scope.alert = 'You said the information was "' + answer + '".';
      }, function() {
        $scope.alert = 'You cancelled the dialog.';
      });
    };



    $scope.toggleBool = function (bool) {
      $scope[bool] = !$scope[bool];
    };



    $scope.travelNote = {
      title: '台北101的韓式銅盤美食《雪嶽山韓式料理》',
      content: '<p class="p1"><span class="s1">真正的夏天已經悄悄到來，你是否已經感受到它熱情的威力了呢？當你汗流浹背，此時腦海中出現的影像可不會是熱騰騰的火鍋，而是冰冰涼涼的清新消暑飲品呀！今天小編就來和大家分享，這個夏天可以讓你很「<b><i>歐蝦雷</i></b>」的玻璃罐</span><span class="s2"><b style="color: #31c4ff;"><u>清涼飲品</u></b></span><span class="s1">製作 DIY，一起來看看吧！</span></p><p class="p1"><span class="s1"><br/></span></p><p class="p1"><span class="s1">今天要做的是 Mojito！來看看要準備些什麼材料吧！</span></p><p class="p1"><span class="s1"><br/></span></p><p class="p1"><span class="s1"><img src="http://ext.pimg.tw/kuku0510/1367307219-28740645_m.jpg"/><br/></span></p><h3 class="p1"><b>準備材料</b></h3><p class="p1"><title></title></p><p class="p1"><span class="s1">真正的夏天已經悄悄到來，你是否已經感受到它熱情的威力了呢？當你汗流浹背，此時腦海中出現的影像可不會是熱騰騰的火鍋，而是冰冰涼涼的清新消暑飲品呀！今天小編就來和大家分享，這個夏天可以讓你很「歐蝦雷」的玻璃罐清涼飲品製作 DIY，一起來看看吧！</span></p><p class="p1"><span class="s1"><br/></span></p><p class="p1"><title></title></p><blockquote class="p1">真正的夏天已經悄悄到來，你是否已經感受到它熱情的威力了呢？當你汗流浹背，此時腦海中出現的影像可不會是熱騰騰的火鍋，而是冰冰涼涼的清新消暑飲品呀！今天小編就來和大家分享，這個夏天可以讓你很「歐蝦雷」的玻璃罐清涼飲品製作 DIY，一起來看看吧！</blockquote><p></p><p></p><p></p>'
    };



    $scope.eventsSlide = {
      currentIdx: 0,
      prev: function () {
        $scope.eventsSlide.currentIdx = $scope.eventsSlide.currentIdx > 0 ? $scope.eventsSlide.currentIdx - 1 : $scope.eventsGroup.length - 1;
      },
      next: function () {
        $scope.eventsSlide.currentIdx = $scope.eventsSlide.currentIdx < ($scope.eventsGroup.length - 1) ? $scope.eventsSlide.currentIdx + 1 : 0;
      },
      set: function (idx) {
        $scope.eventsSlide.currentIdx = idx;
      }
    };

    $scope.eventsGroup = [
    [
    {
      title: '信義線走跳散步美食地圖→吃喝玩樂一日遊BMW新玩法',
      descript: '懒人们在家要怎么吃”真是让太多人煞费了苦心想解决的问题。外卖自是最省事的解决方案，但想到商家用的食材和烹调方式可能会不健康，预订食谱化的食材原料到家自己做则会让人更放心一点。国外正',
      author: '奧革士',
      publishDate: '2015/5/3',
      thumbnail: '../public/images/spots/spot-1.jpg'
    },
    {
      title: '信義線走跳散步美食地圖→吃喝玩樂一日遊BMW新玩法',
      descript: '懒人们在家要怎么吃”真是让太多人煞费了苦心想解决的问题。外卖自是最省事的解决方案，但想到商家用的食材和烹调方式可能会不健康，预订食谱化的食材原料到家自己做则会让人更放心一点。国外正',
      author: '奧革士',
      publishDate: '2015/6/12',
      thumbnail: '../public/images/spots/spot-2.jpg'
    },
    {
      title: '信義線走跳散步美食地圖→吃喝玩樂一日遊BMW新玩法',
      descript: '懒人们在家要怎么吃”真是让太多人煞费了苦心想解决的问题。外卖自是最省事的解决方案，但想到商家用的食材和烹调方式可能会不健康，预订食谱化的食材原料到家自己做则会让人更放心一点。国外正',
      author: '奧革士',
      publishDate: '2015/6/12',
      thumbnail: '../public/images/spots/spot-3.jpg'
    }
    ],
    [
    {
      title: '台北101跨年煙火觀賞地點整理@ bobowin旅行攝影',
      descript: '懒人们在家要怎么吃”真是让太多人煞费了苦心想解决的问题。外卖自是最省事的解决方案，但想到商家用的食材和烹调方式可能会不健康，预订食谱化的食材原料到家自己做则会让人更放心一点。国外正',
      author: '奧革士',
      publishDate: '2015/5/3',
      thumbnail: '../public/images/spots/spot-5.jpg'
    },
    {
      title: '台北101跨年煙火觀賞地點整理@ bobowin旅行攝影',
      descript: '懒人们在家要怎么吃”真是让太多人煞费了苦心想解决的问题。外卖自是最省事的解决方案，但想到商家用的食材和烹调方式可能会不健康，预订食谱化的食材原料到家自己做则会让人更放心一点。国外正',
      author: '奧革士',
      publishDate: '2015/6/12',
      thumbnail: '../public/images/spots/spot-6.jpg'
    },
    {
      title: '台北101跨年煙火觀賞地點整理@ bobowin旅行攝影',
      descript: '懒人们在家要怎么吃”真是让太多人煞费了苦心想解决的问题。外卖自是最省事的解决方案，但想到商家用的食材和烹调方式可能会不健康，预订食谱化的食材原料到家自己做则会让人更放心一点。国外正',
      author: '奧革士',
      publishDate: '2015/6/12',
      thumbnail: '../public/images/spots/spot-7.jpg'
    }
    ],
    [
    {
      title: '【台北】象山俯瞰台北101夜景@ 蝕光印像攝影‧旅行',
      descript: '懒人们在家要怎么吃”真是让太多人煞费了苦心想解决的问题。外卖自是最省事的解决方案，但想到商家用的食材和烹调方式可能会不健康，预订食谱化的食材原料到家自己做则会让人更放心一点。国外正',
      author: '奧革士',
      publishDate: '2015/5/3',
      thumbnail: '../public/images/stays/stay-1.jpg'
    },
    {
      title: '【台北】象山俯瞰台北101夜景@ 蝕光印像攝影‧旅行',
      descript: '懒人们在家要怎么吃”真是让太多人煞费了苦心想解决的问题。外卖自是最省事的解决方案，但想到商家用的食材和烹调方式可能会不健康，预订食谱化的食材原料到家自己做则会让人更放心一点。国外正',
      author: '奧革士',
      publishDate: '2015/6/12',
      thumbnail: '../public/images/stays/stay-2.jpg'
    },
    {
      title: '【台北】象山俯瞰台北101夜景@ 蝕光印像攝影‧旅行',
      descript: '懒人们在家要怎么吃”真是让太多人煞费了苦心想解决的问题。外卖自是最省事的解决方案，但想到商家用的食材和烹调方式可能会不健康，预订食谱化的食材原料到家自己做则会让人更放心一点。国外正',
      author: '奧革士',
      publishDate: '2015/6/12',
      thumbnail: '../public/images/stays/stay-3.jpg'
    }
    ]
    ];

    $scope.comment = {
      author: '奧革士',
      authorImg: '../public/images/avatars/1.jpg',
      rank: undefined,
      comment: '',
      images: [
      '../public/images/events/1.jpg', 
      '../public/images/events/2.jpg', 
      '../public/images/events/3.jpg'
      ],
      commentTime: ''
    };

    $scope.comments = [
    {
      author: '賀少俠',
      authorImg: '../public/images/avatars/2.jpg',
      rank: 4,
      comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
      images: [
      '../public/images/events/1.jpg', 
      '../public/images/events/2.jpg', 
      '../public/images/events/3.jpg'
      ],
      commentTime: '5分鐘前',
      replys: [
      {
        author: '賀少俠',
        authorImg: '../public/images/avatars/2.jpg',
        rank: 4,
        comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
        images: [
        '../public/images/events/1.jpg', 
        '../public/images/events/2.jpg', 
        '../public/images/events/3.jpg'
        ],
        commentTime: '5分鐘前',
        replys: []
      },
      {
        author: '路易四',
        authorImg: '../public/images/avatars/3.jpg',
        rank: 4,
        comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
        images: [],
        commentTime: '5分鐘前',
        replys: []
      }
      ]
    },
    {
      author: '路易四',
      authorImg: '../public/images/avatars/3.jpg',
      rank: 4,
      comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
      images: [],
      commentTime: '5分鐘前',
      replys: []
    },
    {
      author: '賀少俠',
      authorImg: '../public/images/avatars/4.jpg',
      rank: 4,
      comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
      images: [],
      commentTime: '5分鐘前',
      replys: []
    },
    {
      author: '賀少俠',
      authorImg: '../public/images/avatars/1.jpg',
      rank: 4,
      comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
      images: [],
      commentTime: '5分鐘前',
      replys: []
    }
    ];

    $scope.addComment = function () {
      $scope.comments.splice(0, 0, {
        author: $scope.comment.author,
        authorImg: $scope.comment.authorImg,
        rank: 4,
        comment: $scope.comment.comment,
        images: [],
        commentTime: '幾秒鐘前',
        replys: []
      });
      $scope.comment.comment = '';
    };

    $scope.deleteComment = function (index) {
      $scope.comments.splice(index, 1);
    };

    $scope.toReply = function (item) {
      item.replyMode = true;
    };

    $scope.editComment = function (item) {
      item.editMode = true;
      item.commentTemp = item.comment;
    };

    $scope.updateComment = function (item) {
      item.editMode = false;
      item.comment = item.commentTemp;
    };

    $scope.cancelEditComment = function (item) {
      item.editMode = false;
    };

    $scope.addReply = function (comment) {
      comment.replys.splice(0, 0, {
        author: $scope.comment.author,
        authorImg: $scope.comment.authorImg,
        comment: comment.replyTemp,
        commentTime: '幾秒鐘前'
      });
      comment.replyTemp = '';
      comment.replyMode = false;
    };

    $scope.deleteReply = function (comment, index) {
      comment.replys.splice(index, 1);
    };

    $scope.spots = [
    {
      thumbnail: '../public/images/spots/spot-1.jpg',
      title: '華山文創園區',
      description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
      rank: 4,
      commentCount: 12,
      collectCound: 72,
      visible : true
    },
    {
      thumbnail: '../public/images/spots/spot-2.jpg',
      title: '市立台北動物園',
      description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
      rank: 4,
      commentCount: 12,
      collectCound: 72,
      visible : true
    },
    {
      thumbnail: '../public/images/spots/spot-3.jpg',
      title: '松山文創園區',
      description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
      rank: 4,
      commentCount: 12,
      collectCound: 72,
      visible : true
    },
    {
      thumbnail: '../public/images/spots/spot-4.jpg',
      title: '傳統藝術博物館',
      description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
      rank: 4,
      commentCount: 12,
      collectCound: 72,
      visible : true
    },
    {
      thumbnail: '../public/images/spots/spot-5.jpg',
      title: '台北美術館',
      description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
      rank: 4,
      commentCount: 12,
      collectCound: 72,
      visible : true
    },
    {
      thumbnail: '../public/images/spots/spot-2.jpg',
      title: '市立台北動物園',
      description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
      rank: 4,
      commentCount: 12,
      collectCound: 72,
      visible : false
    },
    {
      thumbnail: '../public/images/spots/spot-3.jpg',
      title: '松山文創園區',
      description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
      rank: 4,
      commentCount: 12,
      collectCound: 72,
      visible : false
    },
    {
      thumbnail: '../public/images/spots/spot-4.jpg',
      title: '傳統藝術博物館',
      description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
      rank: 4,
      commentCount: 12,
      collectCound: 72,
      visible : false
    },
    {
      thumbnail: '../public/images/spots/spot-5.jpg',
      title: '台北美術館',
      description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
      rank: 4,
      commentCount: 12,
      collectCound: 72,
      visible : false
    },
    {
      thumbnail: '../public/images/spots/spot-2.jpg',
      title: '市立台北動物園',
      description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
      rank: 4,
      commentCount: 12,
      collectCound: 72,
      visible : false
    },
    {
      thumbnail: '../public/images/spots/spot-3.jpg',
      title: '松山文創園區',
      description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
      rank: 4,
      commentCount: 12,
      collectCound: 72,
      visible : false
    },
    {
      thumbnail: '../public/images/spots/spot-4.jpg',
      title: '傳統藝術博物館',
      description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
      rank: 4,
      commentCount: 12,
      collectCound: 72,
      visible : false
    },
    {
      thumbnail: '../public/images/spots/spot-5.jpg',
      title: '台北美術館',
      description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
      rank: 4,
      commentCount: 12,
      collectCound: 72,
      visible : false
    }
    ];

    $scope.setVisible = function (visible) {
     $scope.ind = 0;
    angular.forEach($scope.spots, function (spot) {
       $scope.ind  ++;
       if($scope.ind >5){
      spot.visible = visible;
    }
      
    });
    $scope.ind  =0;
    $scope.ncoll  = visible;

  }

    $scope.relatedTravelNotes =  [
    {
      thumbnail: '../public/images/spots/spot-1.jpg',

      title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
      descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
      rank: 4,
      types: ["文化", "樂活"],
      date: "2015/3/7",
      author: "奧革士",
      isCollect : false,
      location: "台北、桃園市、新竹市",
      collectCount: 234,
      recommend:true,
      comment:233,
      view:100
    },
    {
      thumbnail: '../public/images/spots/spot-2.jpg',

      title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
      descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，園",
      rank: 4,
      types: ["文化", "樂活"],
      date: "2015/3/7",
      author: "奧革士",
      isCollect : true,
      location: "台北、桃園市、新竹市",
      collectCount: 234,
      recommend:false,
      comment:233,
      view:100
    },
    {
      thumbnail: '../public/images/spots/spot-3.jpg',

      title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
      descript: "西門町位於台北市萬華區，由武昌街二段、漢中街、峨嵋街的行人徒步區所形成的區域為西門町的黃金地帶。大台北地區來往公車的密集交界，流行元素走在前端，是許多年輕學子及各國觀光客朝聖之處。",
      rank: 4,
      types: ["文化", "樂活"],
      date: "2015/3/7",
      author: "奧革士",
      isCollect : false,
      location: "台北、桃園市、新竹市",
      collectCount: 234,
      recommend:false,
      comment:233,
      view:100
    }];
    $scope.travelSlide = {
      currentIdx: 0,
      prev: function () {
        $scope.travelSlide.currentIdx = $scope.travelSlide.currentIdx > 0 ? $scope.travelSlide.currentIdx - 1 : $scope.relatedTravelNotes.length - 1;
      },
      next: function () {
        $scope.travelSlide.currentIdx = $scope.travelSlide.currentIdx < ($scope.relatedTravelNotes.length - 1) ? $scope.travelSlide.currentIdx + 1 : 0;
      },
      set: function (idx) {
        $scope.travelSlide.currentIdx = idx;
      }
    };



    $scope.authorTravelNotes =  [  
    [ {
      thumbnail: '../public/images/spots/spot-1.jpg',

      title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
      descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
      rank: 4,
      types: ["文化", "樂活"],
      date: "2015/3/7",
      author: "奧革士",
      isCollect : false,
      location: "台北、桃園市、新竹市",
      collectCount: 234,
      recommend:true,
      comment:233,
      view:100
    },
    {
      thumbnail: '../public/images/spots/spot-2.jpg',

      title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
      descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，園",
      rank: 4,
      types: ["文化", "樂活"],
      date: "2015/3/7",
      author: "奧革士",
      isCollect : true,
      location: "台北、桃園市、新竹市",
      collectCount: 234,
      recommend:false,
      comment:233,
      view:100
    },
    {
      thumbnail: '../public/images/spots/spot-3.jpg',

      title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
      descript: "西門町位於台北市萬華區，由武昌街二段、漢中街、峨嵋街的行人徒步區所形成的區域為西門町的黃金地帶。大台北地區來往公車的密集交界，流行元素走在前端，是許多年輕學子及各國觀光客朝聖之處。",
      rank: 4,
      types: ["文化", "樂活"],
      date: "2015/3/7",
      author: "奧革士",
      isCollect : false,
      location: "台北、桃園市、新竹市",
      collectCount: 234,
      recommend:false,
      comment:233,
      view:100
    }],[ {
      thumbnail: '../public/images/spots/spot-1.jpg',

      title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
      descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
      rank: 4,
      types: ["文化", "樂活"],
      date: "2015/3/7",
      author: "奧革士",
      isCollect : false,
      location: "台北、桃園市、新竹市",
      collectCount: 234,
      recommend:true,
      comment:233,
      view:100
    },
    {
      thumbnail: '../public/images/spots/spot-2.jpg',

      title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
      descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，園",
      rank: 4,
      types: ["文化", "樂活"],
      date: "2015/3/7",
      author: "奧革士",
      isCollect : true,
      location: "台北、桃園市、新竹市",
      collectCount: 234,
      recommend:false,
      comment:233,
      view:100
    },
    {
      thumbnail: '../public/images/spots/spot-3.jpg',

      title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
      descript: "西門町位於台北市萬華區，由武昌街二段、漢中街、峨嵋街的行人徒步區所形成的區域為西門町的黃金地帶。大台北地區來往公車的密集交界，流行元素走在前端，是許多年輕學子及各國觀光客朝聖之處。",
      rank: 4,
      types: ["文化", "樂活"],
      date: "2015/3/7",
      author: "奧革士",
      isCollect : false,
      location: "台北、桃園市、新竹市",
      collectCount: 234,
      recommend:false,
      comment:233,
      view:100
    }],[ {
      thumbnail: '../public/images/spots/spot-1.jpg',

      title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
      descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
      rank: 4,
      types: ["文化", "樂活"],
      date: "2015/3/7",
      author: "奧革士",
      isCollect : false,
      location: "台北、桃園市、新竹市",
      collectCount: 234,
      recommend:true,
      comment:233,
      view:100
    },
    {
      thumbnail: '../public/images/spots/spot-2.jpg',

      title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
      descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，園",
      rank: 4,
      types: ["文化", "樂活"],
      date: "2015/3/7",
      author: "奧革士",
      isCollect : true,
      location: "台北、桃園市、新竹市",
      collectCount: 234,
      recommend:false,
      comment:233,
      view:100
    },
    {
      thumbnail: '../public/images/spots/spot-3.jpg',

      title: "排隊也要吃到的五十年眷村菜老店 孟記復興餐廳",
      descript: "西門町位於台北市萬華區，由武昌街二段、漢中街、峨嵋街的行人徒步區所形成的區域為西門町的黃金地帶。大台北地區來往公車的密集交界，流行元素走在前端，是許多年輕學子及各國觀光客朝聖之處。",
      rank: 4,
      types: ["文化", "樂活"],
      date: "2015/3/7",
      author: "奧革士",
      isCollect : false,
      location: "台北、桃園市、新竹市",
      collectCount: 234,
      recommend:false,
      comment:233,
      view:100
    }]


    ];
     $scope.authortravelSlide = {
      currentIdx: 0,
      prev: function () {
        $scope.authortravelSlide.currentIdx = $scope.authortravelSlide.currentIdx > 0 ? $scope.authortravelSlide.currentIdx - 1 : $scope.authorTravelNotes.length - 1;
      },
      next: function () {
        $scope.authortravelSlide.currentIdx = $scope.authortravelSlide.currentIdx < ($scope.authorTravelNotes.length - 1) ? $scope.authortravelSlide.currentIdx + 1 : 0;
      },
      set: function (idx) {
        $scope.authortravelSlide.currentIdx = idx;
      }
    };

  }
  );

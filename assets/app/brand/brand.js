"use strict";

angular.module('ng.controller').controller('BrandController' ,
  function( userService, $mdSidenav, $mdBottomSheet, $log, $q, $scope) {

  	$scope.people = 1;
  	// $scope.layout = "List";
  	$scope.layout = "Grid";
  	$scope.minPrice;
  	$scope.maxPrice;
    $scope.isOpenFilter = false;

    $scope.closeFilter = function () {
      $scope.isOpenFilter = false;      
    };

    $scope.openFilter = function () {
      $scope.isOpenFilter = true;      
    };

    $scope.showAllOption = {};

    $scope.toggleMore = function (option) {
      $scope.showAllOption[option] = !$scope.showAllOption[option];
    };

    $scope.orderBy = 'topRated';
$scope.currentSlideIdx = 0;

    $scope.prevSlide = function () {
      $scope.currentSlideIdx = $scope.currentSlideIdx > 0 ? $scope.currentSlideIdx - 1 : $scope.slideList.length - 1;
    };

    $scope.nextSlide = function () {
      $scope.currentSlideIdx = $scope.currentSlideIdx < ($scope.slideList.length - 1) ? $scope.currentSlideIdx + 1 : 0;
    };

    $scope.setSlide = function (idx) {
      $scope.currentSlideIdx = idx;
    };

    $scope.toggleBool = function (bool) {
      $scope[bool] = !$scope[bool];
    };

  	$scope.brand = [
  		{
        thumbnail: '../public/images/stores/1.png',
  			title: "台客藍",
  			descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
  			rank: 3,
        price: 399,
        type: "景點、美食",
        address: "台北市大同區長安西路33號",
  			author: "奧革士",
  			isCollect : false,
  			tags: ['必逛商店', '台北'],
  			collectCount: 237
  		},
  		{
        thumbnail: '../public/images/stores/2.png',
        title: "阿原肥皂",
        descript: "阿原的經營想法「台灣四面環海與充滿山林的自然環境，蘊藏許多珍貴的青草藥，是老天爺賜給我們的自然良方。可惜除了老一輩的人知道善加利用之外，現代人已多半不識。」創辦人江榮原由於自身的過敏膚質，而著手自製手工肥皂，於2005年6月成立了阿原肥皂工作室。阿原肥皂秉持著「愛惜人身，將心比心」的初衷，製作肥皂的一切素材都從身邊開始，本持著天然使用。於是位於台灣山坡的魚腥草、田間的左手香以及民間常用的艾草等青草藥材等，都被研究成肥皂的材料。為了肥皂的能量與生命力，阿原肥皂嚴格排除了工業用的純水和過濾水，堅持從萬里山區接引天然湧泉來製作肥皂，唯有活水可以甚深的轉化細胞與洗滌身心。",
        rank: 3,
        price: 399,
        type: "景點、美食",
        address: "台北市大同區長安西路33號",
        author: "奧革士",
        isCollect : false,
        tags: ['新北'],
        collectCount: 237
  		},
      {
        thumbnail: '../public/images/stores/3.png',
        title: "女巫店",
        descript: "位於台大商圈：公館也就是文藝青年的聚集之地，這裡有誠實坦率的獨立音樂唱片行，也有隻泡好咖啡，放好音樂的咖啡館，想讓精神放鬆，釋放自我的壓力，可以到這裡看非主流電影、主題書店、二手書舖當然，美食也是不可少的。",
        rank: 3,
        price: 399,
        type: "景點、美食",
        address: "台北市大同區長安西路33號",
        author: "奧革士",
        isCollect : false,
        tags: ['台北'],
        collectCount: 237
      },
      {
        thumbnail: '../public/images/stores/4.png',
        title: "朵兒咖啡館",
        descript: "一個湛藍色的大門，讓人期待起門後的世界 這個「故事」的由來，其實是一部仍未上檔的國片電影：由桂綸鎂所主演的「第36個故事」中的主要場景 原本只是在台北富錦街中一間荒廢的公寓1樓，如今在經過改裝之下，變成極富風情的咖啡館也是劇中女主角姊妹所經營的朵兒咖啡",
        rank: 3,
        price: 399,
        type: "景點、美食",
        address: "台北市大同區長安西路33號",
        author: "奧革士",
        isCollect : false,
        tags: ['台北'],
        collectCount: 237
      },
      {
        thumbnail: '../public/images/stores/5.png',
        title: "Déjà Vu音樂魔術主題餐廳",
        descript: "Deja Vu源自於法文“似曾相識”一意，由劉謙、周董和劉畊宏共同投資的中世紀風格主題餐廳哦！不要懷疑，雖說是名人餐廳，價格還可以接受的，而且更重要的是，每週六，有免費的表演看呢~強烈推薦，但一定要記得提前訂位。官網： http://www.deja-",
        rank: 3,
        price: 399,
        type: "景點、美食",
        address: "台北市大同區長安西路33號",
        author: "奧革士",
        isCollect : false,
        tags: ['必逛商店', '新北'],
        collectCount: 237
      },
      {
        thumbnail: '../public/images/stores/6.png',
        title: "袖珍博物館",
        descript: "各位縮微模型愛好者，各位miniature控，各位娃娃屋製作者，各位小清新、小文藝，民那~來台北千萬千萬千萬不要錯過一座館，不是故宮博物館啦，千萬不要錯過台北袖珍博物館！好好地表達一次：請一定要去袖珍博物館！",
        rank: 3,
        price: 399,
        type: "景點、美食",
        address: "台北市大同區長安西路33號",
        author: "奧革士",
        isCollect : false,
        tags: ['必逛商店', '高雄'],
        collectCount: 237
      },
      {
        thumbnail: '../public/images/stores/7.png',
        title: "美麗華百樂園",
        descript: "在台北市中山區、基隆河截彎取直的大彎段，美麗華百樂園是一間以全新概念商業設施的百貨公司，這裡最具特色的場景就是全台首創的百米摩天輪，結合娛樂、科技、藝術於一體，媲美日本的東京台場摩天輪，加之音樂旋轉木馬游樂設施更成為情侶約會的首選景點。夜間綻放光彩炫麗的燈光錶演，點綴台北的夜空。",
        rank: 3,
        price: 399,
        type: "景點、美食",
        address: "台北市大同區長安西路33號",
        author: "奧革士",
        isCollect : false,
        tags: ['屏東'],
        collectCount: 237
      },
      {
        thumbnail: '../public/images/stores/8.png',
        title: "京華城購物中心",
        descript: "京華城購物中心一座全新的國際級都心型觀光休閒購物中心，其獨一無二的建築設計，來自於中國傳說中的“雙龍抱珠”意念，首創“L”形主建築體，並結合全球最大球體的結構設計，直徑達58公尺的球體造型設計，基礎深達85公尺，以四根巨柱載重，為國內首創最深建築基礎結構，也成為京華城建築上的最大特色。",
        rank: 3,
        price: 399,
        type: "景點、美食",
        address: "台北市大同區長安西路33號",
        author: "奧革士",
        isCollect : false,
        tags: ['必逛商店', '台北'],
        collectCount: 237
      }
  	];

  	$scope.toggleCollect = function (trip) {
  		trip.isCollect = !trip.isCollect;
  	};

  	$scope.switchLayout = function (layout) {
  		$scope.layout = layout;
  	};


  }
);

"use strict";

angular.module('ng.controller').controller('BrandDetailController',
    function(userService, $mdSidenav, $mdBottomSheet, $log, $q, $scope, $mdDialog) {



        $scope.bugReport = function(ev) {
            $mdDialog.show({
                    controller: function($scope, $mdDialog, $timeout) {
                        $scope.report = {
                            reasons: {
                                stopSales: false,
                                other: false
                            }
                        };
                        $scope.haveReportReason = function() {
                            return $scope.report.reasons.stopSales ||
                                $scope.report.reasons.other;
                        };
                        $scope.cancel = function() {
                            $mdDialog.cancel();
                        };
                        $scope.sendReport = function(answer) {
                            $scope.reporting = true;
                            $timeout(function() {
                                $scope.reporting = false;
                                $scope.reportSuccess = true;
                            }, 1000);
                        };
                    },
                    templateUrl: '../assets/common/partials/bugReportMadal.html',
                    targetEvent: ev,
                })
                .then(function(answer) {
                    $scope.alert = 'You said the information was "' + answer + '".';
                }, function() {
                    $scope.alert = 'You cancelled the dialog.';
                });
        };



        $scope.slideList = [{
            "imgUrl": "../public/images/stores/1.png"
        }, {
            "imgUrl": "../public/images/stores/2.png"
        }, {
            "imgUrl": "../public/images/stores/3.png"
        }, {
            "imgUrl": "../public/images/stores/4.png"
        }, {
            "imgUrl": "../public/images/stores/5.png"
        }, {
            "imgUrl": "../public/images/stores/6.png"
        }, {
            "imgUrl": "../public/images/stores/7.png"
        }];

        $scope.currentSlideIdx = 0;

        $scope.prevSlide = function() {
            $scope.currentSlideIdx = $scope.currentSlideIdx > 0 ? $scope.currentSlideIdx - 1 : $scope.slideList.length - 1;
        };

        $scope.nextSlide = function() {
            $scope.currentSlideIdx = $scope.currentSlideIdx < ($scope.slideList.length - 1) ? $scope.currentSlideIdx + 1 : 0;
        };

        $scope.setSlide = function(idx) {
            $scope.currentSlideIdx = idx;
        };

        $scope.toggleBool = function(bool) {
            $scope[bool] = !$scope[bool];
        };

        $scope.staysSlide = {
            currentIdx: 0,
            prev: function() {
                $scope.staysSlide.currentIdx = $scope.staysSlide.currentIdx > 0 ? $scope.staysSlide.currentIdx - 1 : $scope.staysGroup.length - 1;
            },
            next: function() {
                $scope.staysSlide.currentIdx = $scope.staysSlide.currentIdx < ($scope.staysGroup.length - 1) ? $scope.staysSlide.currentIdx + 1 : 0;
            },
            set: function(idx) {
                $scope.staysSlide.currentIdx = idx;
            }
        };


        $scope.storesSlide = {
            currentIdx: 0,
            prev: function() {
                $scope.storesSlide.currentIdx = $scope.storesSlide.currentIdx > 0 ? $scope.storesSlide.currentIdx - 1 : $scope.storesGroup.length - 1;
            },
            next: function() {
                $scope.storesSlide.currentIdx = $scope.storesSlide.currentIdx < ($scope.storesGroup.length - 1) ? $scope.storesSlide.currentIdx + 1 : 0;
            },
            set: function(idx) {
                $scope.storesSlide.currentIdx = idx;
            }
        };

        $scope.storesGroup = [
            [{
                title: '馬櫻丹肥皂',
                price: 7235,
                specialPric: 4235,
                viewCount: 1336,
                thumbnail: '../public/images/spots/spot-1.jpg'
            }, {
                title: '柑仔手工肥皂',
                price: 4235,
                viewCount: 1336,
                thumbnail: '../public/images/spots/spot-2.jpg'
            }, {
                title: '阿原竹月刀',
                price: 4235,
                viewCount: 1336,
                thumbnail: '../public/images/spots/spot-3.jpg'
            }, {
                title: '精油禮盒包 X 6',
                price: 4235,
                viewCount: 1336,
                thumbnail: '../public/images/spots/spot-4.jpg'
            }],
            [{
                title: '馬櫻丹肥皂',
                price: 7235,
                specialPric: 4235,
                viewCount: 1336,
                thumbnail: '../public/images/spots/spot-5.jpg'
            }, {
                title: '柑仔手工肥皂',
                price: 4235,
                viewCount: 1336,
                thumbnail: '../public/images/spots/spot-6.jpg'
            }, {
                title: '阿原竹月刀',
                price: 4235,
                viewCount: 1336,
                thumbnail: '../public/images/spots/spot-7.jpg'
            }, {
                title: '精油禮盒包 X 6',
                price: 4235,
                viewCount: 1336,
                thumbnail: '../public/images/spots/spot-8.jpg'
            }]
        ];

        $scope.videos = [{
            thumbnail: '../public/images/stores/1.png',
            title: '看見鼎泰豐 完整版',
            author: '奧革士',
            viewcount: '33210',
            description: '阿原工作室草創於2005年，以台灣青草藥手作皂為起點，選用台灣青草藥為主題，融合­東方養生思維、愛惜人身與善待環境的理念，致力實踐失落的'
        }, {
            thumbnail: '../public/images/stores/2.png',
            title: '鼎泰豐 - 我們的驕傲',
            author: '奧革士',
            viewcount: '33210',
            description: '江榮原先生阿原肥皂創辦人阿原工作室與阿原肥皂即是以創辦人江榮原先生為名。 創辦人江榮原先生，由於家庭因素而在高職畢業後就 ...'
        }];

        $scope.tickets = [{
            title: '$100 鼎泰豐飲食現金券',
            price: 60
        }, {
            title: '鼎泰豐300元限量飲食現金折價券',
            price: 120
        }];

        $scope.activities = [{
            title: '食我鑰匙大放送',
            startDate: '2015/03/10',
            endDate: '2015/05/10',
            message: '下載 Smart Tourism Taiwan 台灣智慧觀光 APP 並完成註冊、登入，馬上獲得食我鑰匙10把序號乙組！'
        }, {
            title: 'Smart Tourism Taiwan 會員募集抽獎活動',
            startDate: '2015/03/10',
            endDate: '2015/05/10',
            message: '凡於活動期間下載 Smart Tourism Taiwan APP 並註冊成為會員，於行動裝置上的抽獎活動頁面登錄相關資訊，即可參加抽獎。'
        }];

        $scope.travelNotes = [{
            title: '信義線走跳散步美食地圖→吃喝玩樂一日遊BMW新玩法',
            author: '奧革士',
            publishDate: '2014/12/24',
            thumbnail: '../public/images/spots/spot-1.jpg',
            description: '懒人们在家要怎么吃”真是让太多人煞费了苦心想解决的问题。外卖自是最省事的解决方案，但想到商家用的食材和烹调方式可能会不健康，预订食谱化的食材原料到家自己做则会让人更放心一点。国外正'
        }, {
            title: '台北101跨年煙火觀賞地點整理@ bobowin旅行攝影',
            author: '奧革士',
            publishDate: '2014/12/24',
            thumbnail: '../public/images/spots/spot-2.jpg',
            description: '懒人们在家要怎么吃”真是让太多人煞费了苦心想解决的问题。外卖自是最省事的解决方案，但想到商家用的食材和烹调方式可能会不健康，预订食谱化的食材原料到家自己做则会让人更放心一点。国外正'
        }, {
            title: '【台北】象山俯瞰台北101夜景@ 蝕光印像攝影‧旅行',
            author: '奧革士',
            publishDate: '2014/12/24',
            thumbnail: '../public/images/spots/spot-3.jpg',
            description: '懒人们在家要怎么吃”真是让太多人煞费了苦心想解决的问题。外卖自是最省事的解决方案，但想到商家用的食材和烹调方式可能会不健康，预订食谱化的食材原料到家自己做则会让人更放心一点。国外正'
        }];

        $scope.comment = {
            author: '奧革士',
            authorImg: '../public/images/avatars/1.jpg',
            rank: undefined,
            comment: '',
            images: [
                '../public/images/stores/1.png',
                '../public/images/stores/2.png',
                '../public/images/stores/3.png'
            ],
            commentTime: ''
        };

        $scope.comments = [{
            author: '賀少俠',
            authorImg: '../public/images/avatars/2.jpg',
            rank: 4,
            comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
            images: [
                '../public/images/stores/1.png',
                '../public/images/stores/2.png',
                '../public/images/stores/3.png'
            ],
            commentTime: '5分鐘前',
            replys: [{
                author: '賀少俠',
                authorImg: '../public/images/avatars/2.jpg',
                rank: 4,
                comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
                images: [
                    '../public/images/stores/1.png',
                    '../public/images/stores/2.png',
                    '../public/images/stores/3.png'
                ],
                commentTime: '5分鐘前',
                replys: []
            }, {
                author: '路易四',
                authorImg: '../public/images/avatars/3.jpg',
                rank: 4,
                comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
                images: [],
                commentTime: '5分鐘前',
                replys: []
            }]
        }, {
            author: '路易四',
            authorImg: '../public/images/avatars/3.jpg',
            rank: 4,
            comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
            images: [],
            commentTime: '5分鐘前',
            replys: []
        }, {
            author: '賀少俠',
            authorImg: '../public/images/avatars/4.jpg',
            rank: 4,
            comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
            images: [],
            commentTime: '5分鐘前',
            replys: []
        }, {
            author: '賀少俠',
            authorImg: '../public/images/avatars/1.jpg',
            rank: 4,
            comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
            images: [],
            commentTime: '5分鐘前',
            replys: []
        }];

        $scope.addComment = function() {
            $scope.comments.splice(0, 0, {
                author: $scope.comment.author,
                authorImg: $scope.comment.authorImg,
                rank: 4,
                comment: $scope.comment.comment,
                images: [],
                commentTime: '幾秒鐘前',
                replys: []
            });
            $scope.comment.comment = '';
        };

        $scope.deleteComment = function(index) {
            $scope.comments.splice(index, 1);
        };

        $scope.toReply = function(item) {
            item.replyMode = true;
        };

        $scope.editComment = function(item) {
            item.editMode = true;
            item.commentTemp = item.comment;
        };

        $scope.updateComment = function(item) {
            item.editMode = false;
            item.comment = item.commentTemp;
        };

        $scope.cancelEditComment = function(item) {
            item.editMode = false;
        };

        $scope.addReply = function(comment) {
            comment.replys.splice(0, 0, {
                author: $scope.comment.author,
                authorImg: $scope.comment.authorImg,
                comment: comment.replyTemp,
                commentTime: '幾秒鐘前'
            });
            comment.replyTemp = '';
            comment.replyMode = false;
        };

        $scope.deleteReply = function(comment, index) {
            comment.replys.splice(index, 1);
        };

        // 伴手禮區塊 左右選單 開始

        $scope.productsSlide = {
            currentIdx: 0,
            prev: function() {
                $scope.productsSlide.currentIdx = $scope.productsSlide.currentIdx > 0 ? $scope.productsSlide.currentIdx - 1 : $scope.products.length - 1;
            },
            next: function() {
                $scope.productsSlide.currentIdx = $scope.productsSlide.currentIdx < ($scope.products.length - 1) ? $scope.productsSlide.currentIdx + 1 : 0;
            },
            set: function(idx) {
                $scope.productsSlide.currentIdx = idx;
            }
        };

        // 伴手禮區塊 左右選單 結束

        // 伴手禮+買行程區塊 開始

        $scope.products = [
            [{
                thumbnail: '../public/images/stores/1.png',
                title: '柑仔手工肥皂名字真的超長 超長的 無敵長 看會不會變兩行',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/2.png',
                title: '馬櫻丹肥皂',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/3.png',
                title: '阿原竹月刀',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/4.png',
                title: '精油禮盒包 X 6',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/5.png',
                title: '天然草本組合包',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/1.png',
                title: '柑仔手工肥皂名字真的超長 超長的 無敵長 看會不會變兩行',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/2.png',
                title: '馬櫻丹肥皂',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/3.png',
                title: '阿原竹月刀',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/4.png',
                title: '精油禮盒包 X 6',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/5.png',
                title: '天然草本組合包',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }],
            [{
                thumbnail: '../public/images/stores/1.png',
                title: '柑仔手工肥皂名字真的超長 超長的 無敵長 看會不會變兩行',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/2.png',
                title: '馬櫻丹肥皂',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/3.png',
                title: '阿原竹月刀',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/4.png',
                title: '精油禮盒包 X 6',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/5.png',
                title: '天然草本組合包',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/1.png',
                title: '柑仔手工肥皂名字真的超長 超長的 無敵長 看會不會變兩行',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/2.png',
                title: '馬櫻丹肥皂',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/3.png',
                title: '阿原竹月刀',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/4.png',
                title: '精油禮盒包 X 6',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/5.png',
                title: '天然草本組合包',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }],
            [{
                thumbnail: '../public/images/stores/1.png',
                title: '柑仔手工肥皂名字真的超長 超長的 無敵長 看會不會變兩行',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/2.png',
                title: '馬櫻丹肥皂',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/3.png',
                title: '阿原竹月刀',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/4.png',
                title: '精油禮盒包 X 6',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/5.png',
                title: '天然草本組合包',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/1.png',
                title: '柑仔手工肥皂名字真的超長 超長的 無敵長 看會不會變兩行',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/2.png',
                title: '馬櫻丹肥皂',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/3.png',
                title: '阿原竹月刀',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/4.png',
                title: '精油禮盒包 X 6',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }, {
                thumbnail: '../public/images/stores/5.png',
                title: '天然草本組合包',
                description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
                price: 4235,
            }]
        ];

        // 伴手禮+買行程區塊 結束

        $scope.spots = [
        {
            thumbnail: '../public/images/spots/spot-1.jpg',
            title: '華山文創園區',
            description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
            rank: 4,
            distance: 2
        }, {
            thumbnail: '../public/images/spots/spot-2.jpg',
            title: '市立台北動物園',
            description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
            rank: 4,
            distance: 2
        }, {
            thumbnail: '../public/images/spots/spot-3.jpg',
            title: '松山文創園區',
            description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
            rank: 4,
            distance: 2
        }, {
            thumbnail: '../public/images/spots/spot-4.jpg',
            title: '傳統藝術博物館',
            description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
            rank: 4,
            distance: 2
        }, {
            thumbnail: '../public/images/spots/spot-5.jpg',
            title: '台北美術館',
            description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚。',
            rank: 4,
            distance: 2
        }
        ];

        $scope.staysGroup = [
            [{
                title: '分店一',
                description: '台北市信義區6號2樓',

                thumbnail: '../public/images/spots/spot-1.jpg'
            }, {
                title: '分店二',
                description: '台北市信義區6號2樓',

                thumbnail: '../public/images/stays/stay-2.jpg'
            }, {
                title: '分店三',
                description: '台北市信義區6號2樓',

                thumbnail: '../public/images/stays/stay-3.jpg'
            }, {
                title: '分店四',
                description: '台北市信義區6號2樓',

                thumbnail: '../public/images/stays/stay-4.jpg'
            }],
            [{
                title: '分店五',
                description: '台北市信義區6號2樓',

                thumbnail: '../public/images/stays/stay-5.jpg'
            }, {
                title: '分店二',
                description: '台北市信義區6號2樓',

                thumbnail: '../public/images/stays/stay-2.jpg'
            }, {
                title: '分店三',
                description: '台北市信義區6號2樓',

                thumbnail: '../public/images/stays/stay-3.jpg'
            }, {
                title: '分店四',
                description: '台北市信義區6號2樓',

                thumbnail: '../public/images/stays/stay-4.jpg'
            }]
        ];

    }
);

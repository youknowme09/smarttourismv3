"use strict";

angular.module('ng.controller').controller('ResetPasswordController' ,
  function($rootScope, $scope, $state, $mdDialog, $timeout) {

    $scope.cancel = function () {
      $mdDialog.cancel();
    };
    $scope.reset = function () {
      $scope.reseting = true;
      $timeout(function () {
        $scope.reseting = false;
        $scope.hasReset = true;
      }, 1000);
    };
    $scope.goToLogin = function () {
    	
    };

  }
);

"use strict";

angular.module("ng.controller", []);
angular.module("ng.model",['ngResource']);
angular.module("ng.directive", ['ngMaterial', 'angularSpectrumColorpicker','ng.ddslick']);
angular.module("ng.service", []);
angular.module("ng.filter", []);
angular.module("ng.router",['ui.router', 'ngAnimate']);

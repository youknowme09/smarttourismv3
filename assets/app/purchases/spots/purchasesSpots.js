"use strict";

angular.module('ng.controller').controller('PurchasesSpotsController' ,
  function( userService, $mdSidenav, $mdBottomSheet, $log, $q, $scope) {

    $scope.people = 1;
    // $scope.layout = "List";
    $scope.layout = "Grid";
    $scope.minPrice;
    $scope.maxPrice;
    $scope.isOpenFilter = false;

    $scope.closeFilter = function () {
      $scope.isOpenFilter = false;      
    };

    $scope.openFilter = function () {
      $scope.isOpenFilter = true;      
    };

    $scope.showAllOption = {};

    $scope.toggleMore = function (option) {
      $scope.showAllOption[option] = !$scope.showAllOption[option];
    };

    $scope.orderBy = 'topRated';

    $scope.purchases = [
      {
        thumbnail: '../public/images/spots/spot-1.jpg',
        title: "台北101觀景台",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        soldCount: 248,
        rank: 3,
        viewes: 37,
        price: 2499,
        discount: 9,
        discountPrice: 2499,
        address: '大同區',
        popular: true
      },
      {
        thumbnail: '../public/images/spots/spot-2.jpg',
        title: "平溪天燈節一日票券",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        soldCount: 248,
        rank: 3,
        viewes: 37,
        price: 2499,
        discount: 9,
        discountPrice: 2499,
        address: '大同區',
        topChoice: true
      },
      {
        thumbnail: '../public/images/spots/spot-3.jpg',
        title: "台北101觀景台",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        rank: 3,
        viewes: 37,
        price: 2499,
        address: '大同區'
      },
      {
        thumbnail: '../public/images/spots/spot-4.jpg',
        title: "台北101觀景台",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        rank: 3,
        viewes: 37,
        price: 2499,
        address: '大同區'
      },
      {
        thumbnail: '../public/images/spots/spot-5.jpg',
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        title: "台北101觀景台",
        rank: 3,
        viewes: 37,
        price: 2499,
        address: '大同區'
      },
      {
        thumbnail: '../public/images/spots/spot-6.jpg',
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        title: "台北101觀景台",
        rank: 3,
        viewes: 37,
        price: 2499,
        address: '大同區'
      },
      {
        thumbnail: '../public/images/spots/spot-7.jpg',
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        title: "台北101觀景台",
        rank: 3,
        viewes: 37,
        price: 2499,
        address: '大同區'
      },
      {
        thumbnail: '../public/images/spots/spot-8.jpg',
        title: "台北101觀景台",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        rank: 3,
        viewes: 37,
        price: 2499,
        address: '大同區'
      },
      {
        thumbnail: '../public/images/spots/spot-1.jpg',
        title: "台北101觀景台",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        rank: 3,
        viewes: 37,
        price: 2499,
        address: '大同區'
      },
      {
        thumbnail: '../public/images/spots/spot-2.jpg',
        title: "台北101觀景台",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        rank: 3,
        viewes: 37,
        price: 2499,
        address: '大同區'
      },
      {
        thumbnail: '../public/images/spots/spot-3.jpg',
        title: "台北101觀景台",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        rank: 3,
        viewes: 37,
        price: 2499,
        address: '大同區'
      },
      {
        thumbnail: '../public/images/spots/spot-4.jpg',
        title: "台北101觀景台",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        rank: 3,
        viewes: 37,
        price: 2499,
        address: '大同區'
      },
      {
        thumbnail: '../public/images/spots/spot-5.jpg',
        title: "台北101觀景台",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        rank: 3,
        viewes: 37,
        price: 2499,
        address: '大同區'
      },
      {
        thumbnail: '../public/images/spots/spot-6.jpg',
        title: "台北101觀景台",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        rank: 3,
        viewes: 37,
        price: 2499,
        address: '大同區'
      },
      {
        thumbnail: '../public/images/spots/spot-7.jpg',
        title: "台北101觀景台",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        rank: 3,
        viewes: 37,
        price: 2499,
        address: '大同區'
      },
      {
        thumbnail: '../public/images/spots/spot-8.jpg',
        title: "台北101觀景台",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        rank: 3,
        viewes: 37,
        price: 2499,
        address: '大同區'
      }
    ];

    $scope.switchLayout = function (layout) {
      $scope.layout = layout;
    };


  }
);

"use strict";

angular.module('ng.controller').controller('PurchasesController' ,
  function( userService, $mdSidenav, $mdBottomSheet, $log, $q, $scope) {

    $scope.people = 1;
    $scope.isOpenFilter = false;

    $scope.closeFilter = function () {
      $scope.isOpenFilter = false;      
    };

    $scope.openFilter = function () {
      $scope.isOpenFilter = true;      
    };



    $scope.spotsSlide = {
      currentIdx: 0,
      prev: function () {
        $scope.spotsSlide.currentIdx = $scope.spotsSlide.currentIdx > 0 ? $scope.spotsSlide.currentIdx - 1 : $scope.spotsGroup.length - 1;
      },
      next: function () {
        $scope.spotsSlide.currentIdx = $scope.spotsSlide.currentIdx < ($scope.spotsGroup.length - 1) ? $scope.spotsSlide.currentIdx + 1 : 0;
      },
      set: function (idx) {
        $scope.spotsSlide.currentIdx = idx;
      }
    };

    $scope.spotsGroup = [
      [
        {
          thumbnail: '../public/images/spots/spot-1.jpg',
          title: "台北101觀景台",
          soldCount: 248,
          rank: 3,
          commentCount: 37,
          price: 2499,
          discount: 9,
          discountPrice: 2499,
          address: '大同區',
          popular: true
        },
        {
          thumbnail: '../public/images/spots/spot-2.jpg',
          title: "平溪天燈節一日票券",
          soldCount: 248,
          rank: 3,
          commentCount: 37,
          price: 2499,
          discount: 9,
          discountPrice: 2499,
          address: '大同區',
          topChoice: true
        },
        {
          thumbnail: '../public/images/spots/spot-3.jpg',
          title: "台北101觀景台",
          rank: 3,
          commentCount: 37,
          price: 2499,
          address: '大同區'
        },
        {
          thumbnail: '../public/images/spots/spot-4.jpg',
          title: "台北101觀景台",
          rank: 3,
          commentCount: 37,
          price: 2499,
          address: '大同區'
        }
      ],
      [
        {
          thumbnail: '../public/images/spots/spot-5.jpg',
          title: "台北101觀景台",
          rank: 3,
          commentCount: 37,
          price: 2499,
          address: '大同區'
        },
        {
          thumbnail: '../public/images/spots/spot-6.jpg',
          title: "台北101觀景台",
          rank: 3,
          commentCount: 37,
          price: 2499,
          address: '大同區'
        },
        {
          thumbnail: '../public/images/spots/spot-7.jpg',
          title: "台北101觀景台",
          rank: 3,
          commentCount: 37,
          price: 2499,
          address: '大同區'
        },
        {
          thumbnail: '../public/images/spots/spot-8.jpg',
          title: "台北101觀景台",
          rank: 3,
          commentCount: 37,
          price: 2499,
          address: '大同區'
        }
      ],
      [
        {
          thumbnail: '../public/images/spots/spot-1.jpg',
          title: "台北101觀景台",
          rank: 3,
          commentCount: 37,
          price: 2499,
          address: '大同區'
        },
        {
          thumbnail: '../public/images/spots/spot-2.jpg',
          title: "台北101觀景台",
          rank: 3,
          commentCount: 37,
          price: 2499,
          address: '大同區'
        },
        {
          thumbnail: '../public/images/spots/spot-3.jpg',
          title: "台北101觀景台",
          rank: 3,
          commentCount: 37,
          price: 2499,
          address: '大同區'
        },
        {
          thumbnail: '../public/images/spots/spot-4.jpg',
          title: "台北101觀景台",
          rank: 3,
          commentCount: 37,
          price: 2499,
          address: '大同區'
        }
      ],
      [
        {
          thumbnail: '../public/images/spots/spot-5.jpg',
          title: "台北101觀景台",
          rank: 3,
          commentCount: 37,
          price: 2499,
          address: '大同區'
        },
        {
          thumbnail: '../public/images/spots/spot-6.jpg',
          title: "台北101觀景台",
          rank: 3,
          commentCount: 37,
          price: 2499,
          address: '大同區'
        },
        {
          thumbnail: '../public/images/spots/spot-7.jpg',
          title: "台北101觀景台",
          rank: 3,
          commentCount: 37,
          price: 2499,
          address: '大同區'
        },
        {
          thumbnail: '../public/images/spots/spot-8.jpg',
          title: "台北101觀景台",
          rank: 3,
          commentCount: 37,
          price: 2499,
          address: '大同區'
        }
      ]
    ];

    

    $scope.eventsSlide = {
      currentIdx: 0,
      prev: function () {
        $scope.eventsSlide.currentIdx = $scope.eventsSlide.currentIdx > 0 ? $scope.eventsSlide.currentIdx - 1 : $scope.eventsGroup.length - 1;
      },
      next: function () {
        $scope.eventsSlide.currentIdx = $scope.eventsSlide.currentIdx < ($scope.eventsGroup.length - 1) ? $scope.eventsSlide.currentIdx + 1 : 0;
      },
      set: function (idx) {
        $scope.eventsSlide.currentIdx = idx;
      }
    };

    $scope.eventsGroup = [
      [
        {
          thumbnail: '../public/images/events/1.jpg',
          title: "台中爵士音樂節",
          soldCount: 248,
          rank: 3,
          commentCount: 37,
          price: 2499,
          discount: 9,
          discountPrice: 2499,
          date: '2015年9月15日'
        },
        {
          thumbnail: '../public/images/events/2.jpg',
          title: "台中爵士音樂節",
          rank: 3,
          commentCount: 37,
          price: 2499,
          discount: 9,
          discountPrice: 2499,
          date: '2015年9月15日'
        },
        {
          thumbnail: '../public/images/events/3.jpg',
          title: "台中爵士音樂節",
          rank: 3,
          commentCount: 37,
          price: 2499,
          date: '2015年9月15日'
        },
        {
          thumbnail: '../public/images/events/4.jpg',
          title: "台中爵士音樂節",
          rank: 3,
          commentCount: 37,
          price: 2499,
          date: '2015年9月15日'
        }
      ],
      [
        {
          thumbnail: '../public/images/events/5.jpg',
          title: "台中爵士音樂節",
          rank: 3,
          commentCount: 37,
          price: 2499,
          date: '2015年9月15日'
        },
        {
          thumbnail: '../public/images/events/1.jpg',
          title: "台中爵士音樂節",
          rank: 3,
          commentCount: 37,
          price: 2499,
          date: '2015年9月15日'
        },
        {
          thumbnail: '../public/images/events/2.jpg',
          title: "台中爵士音樂節",
          rank: 3,
          commentCount: 37,
          price: 2499,
          date: '2015年9月15日'
        },
        {
          thumbnail: '../public/images/events/3.jpg',
          title: "台中爵士音樂節",
          rank: 3,
          commentCount: 37,
          price: 2499,
          date: '2015年9月15日'
        }
      ],
      [
        {
          thumbnail: '../public/images/events/4.jpg',
          title: "台中爵士音樂節",
          rank: 3,
          commentCount: 37,
          price: 2499,
          date: '2015年9月15日'
        },
        {
          thumbnail: '../public/images/events/5.jpg',
          title: "台中爵士音樂節",
          rank: 3,
          commentCount: 37,
          price: 2499,
          date: '2015年9月15日'
        },
        {
          thumbnail: '../public/images/events/1.jpg',
          title: "台中爵士音樂節",
          rank: 3,
          commentCount: 37,
          price: 2499,
          date: '2015年9月15日'
        },
        {
          thumbnail: '../public/images/events/2.jpg',
          title: "台中爵士音樂節",
          rank: 3,
          commentCount: 37,
          price: 2499,
          date: '2015年9月15日'
        }
      ],
      [
        {
          thumbnail: '../public/images/events/3.jpg',
          title: "台中爵士音樂節",
          rank: 3,
          commentCount: 37,
          price: 2499,
          date: '2015年9月15日'
        },
        {
          thumbnail: '../public/images/events/4.jpg',
          title: "台中爵士音樂節",
          rank: 3,
          commentCount: 37,
          price: 2499,
          date: '2015年9月15日'
        },
        {
          thumbnail: '../public/images/events/5.jpg',
          title: "台中爵士音樂節",
          rank: 3,
          commentCount: 37,
          price: 2499,
          date: '2015年9月15日'
        },
        {
          thumbnail: '../public/images/events/1.jpg',
          title: "台中爵士音樂節",
          rank: 3,
          commentCount: 37,
          price: 2499,
          date: '2015年9月15日'
        }
      ]
    ];

    

    $scope.foodsSlide = {
      currentIdx: 0,
      prev: function () {
        $scope.foodsSlide.currentIdx = $scope.foodsSlide.currentIdx > 0 ? $scope.foodsSlide.currentIdx - 1 : $scope.foodsGroup.length - 1;
      },
      next: function () {
        $scope.foodsSlide.currentIdx = $scope.foodsSlide.currentIdx < ($scope.foodsGroup.length - 1) ? $scope.foodsSlide.currentIdx + 1 : 0;
      },
      set: function (idx) {
        $scope.foodsSlide.currentIdx = idx;
      }
    };

    $scope.foodsGroup = [
      [
        {
          thumbnail: '../public/images/foods/food-1.jpg',
          title: "洋旗牛排餐廳",
          soldCount: 248,
          rank: 3,
          commentCount: 37,
          price: 2499,
          discount: 9,
          discountPrice: 2499,
          address: '大同區'
        },
        {
          thumbnail: '../public/images/foods/food-2.jpg',
          title: "洋旗牛排餐廳",
          rank: 3,
          commentCount: 37,
          price: 2499,
          address: '大同區'
        },
        {
          thumbnail: '../public/images/foods/food-3.jpg',
          title: "洋旗牛排餐廳",
          rank: 3,
          commentCount: 37,
          price: 2499,
          address: '大同區'
        },
        {
          thumbnail: '../public/images/foods/food-4.jpg',
          title: "洋旗牛排餐廳",
          rank: 3,
          commentCount: 37,
          price: 2499,
          address: '大同區'
        }
      ],
      [
        {
          thumbnail: '../public/images/foods/food-5.jpg',
          title: "洋旗牛排餐廳",
          rank: 3,
          commentCount: 37,
          price: 2499,
          address: '大同區'
        },
        {
          thumbnail: '../public/images/foods/food-6.jpg',
          title: "洋旗牛排餐廳",
          rank: 3,
          commentCount: 37,
          price: 2499,
          address: '大同區'
        },
        {
          thumbnail: '../public/images/foods/food-7.jpg',
          title: "洋旗牛排餐廳",
          rank: 3,
          commentCount: 37,
          price: 2499,
          address: '大同區'
        },
        {
          thumbnail: '../public/images/foods/food-8.jpg',
          title: "洋旗牛排餐廳",
          rank: 3,
          commentCount: 37,
          price: 2499,
          address: '大同區'
        }
      ],
      [
        {
          thumbnail: '../public/images/foods/food-1.jpg',
          title: "洋旗牛排餐廳",
          rank: 3,
          commentCount: 37,
          price: 2499,
          address: '大同區'
        },
        {
          thumbnail: '../public/images/foods/food-2.jpg',
          title: "洋旗牛排餐廳",
          rank: 3,
          commentCount: 37,
          price: 2499,
          address: '大同區'
        },
        {
          thumbnail: '../public/images/foods/food-3.jpg',
          title: "洋旗牛排餐廳",
          rank: 3,
          commentCount: 37,
          price: 2499,
          address: '大同區'
        },
        {
          thumbnail: '../public/images/foods/food-4.jpg',
          title: "洋旗牛排餐廳",
          rank: 3,
          commentCount: 37,
          price: 2499,
          address: '大同區'
        }
      ],
      [
        {
          thumbnail: '../public/images/foods/food-5.jpg',
          title: "洋旗牛排餐廳",
          rank: 3,
          commentCount: 37,
          price: 2499,
          address: '大同區'
        },
        {
          thumbnail: '../public/images/foods/food-6.jpg',
          title: "洋旗牛排餐廳",
          rank: 3,
          commentCount: 37,
          price: 2499,
          address: '大同區'
        },
        {
          thumbnail: '../public/images/foods/food-7.jpg',
          title: "洋旗牛排餐廳",
          rank: 3,
          commentCount: 37,
          price: 2499,
          address: '大同區'
        },
        {
          thumbnail: '../public/images/foods/food-8.jpg',
          title: "洋旗牛排餐廳",
          rank: 3,
          commentCount: 37,
          price: 2499,
          address: '大同區'
        }
      ]
    ];

    

    $scope.staysSlide = {
      currentIdx: 0,
      prev: function () {
        $scope.staysSlide.currentIdx = $scope.staysSlide.currentIdx > 0 ? $scope.staysSlide.currentIdx - 1 : $scope.staysGroup.length - 1;
      },
      next: function () {
        $scope.staysSlide.currentIdx = $scope.staysSlide.currentIdx < ($scope.staysGroup.length - 1) ? $scope.staysSlide.currentIdx + 1 : 0;
      },
      set: function (idx) {
        $scope.staysSlide.currentIdx = idx;
      }
    };

    $scope.staysGroup = [
      [
        {
          thumbnail: '../public/images/stays/stay-1.jpg',
          title: "紅米國際青年旅館",
          soldCount: 248,
          rank: 3,
          commentCount: 37,
          price: 2499,
          discount: 9,
          discountPrice: 2499,
          label: '民宿'
        },
        {
          thumbnail: '../public/images/stays/stay-2.jpg',
          title: "紅米國際青年旅館",
          rank: 3,
          commentCount: 37,
          price: 2499,
          discount: 9,
          discountPrice: 2499,
          label: '民宿'
        },
        {
          thumbnail: '../public/images/stays/stay-3.jpg',
          title: "紅米國際青年旅館",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '民宿'
        },
        {
          thumbnail: '../public/images/stays/stay-4.jpg',
          title: "紅米國際青年旅館",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '民宿'
        }
      ],
      [
        {
          thumbnail: '../public/images/stays/stay-5.jpg',
          title: "紅米國際青年旅館",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '民宿'
        },
        {
          thumbnail: '../public/images/stays/stay-6.jpg',
          title: "紅米國際青年旅館",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '民宿'
        },
        {
          thumbnail: '../public/images/stays/stay-7.jpg',
          title: "紅米國際青年旅館",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '民宿'
        },
        {
          thumbnail: '../public/images/stays/stay-8.jpg',
          title: "紅米國際青年旅館",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '民宿'
        }
      ],
      [
        {
          thumbnail: '../public/images/stays/stay-1.jpg',
          title: "紅米國際青年旅館",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '民宿'
        },
        {
          thumbnail: '../public/images/stays/stay-2.jpg',
          title: "紅米國際青年旅館",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '民宿'
        },
        {
          thumbnail: '../public/images/stays/stay-3.jpg',
          title: "紅米國際青年旅館",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '民宿'
        },
        {
          thumbnail: '../public/images/stays/stay-4.jpg',
          title: "紅米國際青年旅館",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '民宿'
        }
      ],
      [
        {
          thumbnail: '../public/images/stays/stay-5.jpg',
          title: "紅米國際青年旅館",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '民宿'
        },
        {
          thumbnail: '../public/images/stays/stay-6.jpg',
          title: "紅米國際青年旅館",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '民宿'
        },
        {
          thumbnail: '../public/images/stays/stay-7.jpg',
          title: "紅米國際青年旅館",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '民宿'
        },
        {
          thumbnail: '../public/images/stays/stay-8.jpg',
          title: "紅米國際青年旅館",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '民宿'
        }
      ]
    ];

    

    $scope.tripsSlide = {
      currentIdx: 0,
      prev: function () {
        $scope.tripsSlide.currentIdx = $scope.tripsSlide.currentIdx > 0 ? $scope.tripsSlide.currentIdx - 1 : $scope.tripsGroup.length - 1;
      },
      next: function () {
        $scope.tripsSlide.currentIdx = $scope.tripsSlide.currentIdx < ($scope.tripsGroup.length - 1) ? $scope.tripsSlide.currentIdx + 1 : 0;
      },
      set: function (idx) {
        $scope.tripsSlide.currentIdx = idx;
      }
    };

    $scope.tripsGroup = [
      [
        {
          thumbnail: '../public/images/trips/trip-1.jpg',
          title: "東北角的藍天與大海",
          soldCount: 248,
          rank: 3,
          commentCount: 37,
          price: 2499,
          discount: 9,
          discountPrice: 2499,
          label: '7日行程 / 遊歷12景點',
          duration: 7
        },
        {
          thumbnail: '../public/images/trips/trip-2.jpg',
          title: "東北角的藍天與大海",
          soldCount: 248,
          rank: 3,
          commentCount: 37,
          price: 2499,
          discount: 9,
          discountPrice: 2499,
          label: '7日行程 / 遊歷12景點',
          duration: 7
        },
        {
          thumbnail: '../public/images/trips/trip-3.jpg',
          title: "東北角的藍天與大海",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '7日行程 / 遊歷12景點',
          duration: 7
        },
        {
          thumbnail: '../public/images/trips/trip-4.jpg',
          title: "東北角的藍天與大海",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '7日行程 / 遊歷12景點',
          duration: 7
        }
      ],
      [
        {
          thumbnail: '../public/images/trips/trip-5.jpg',
          title: "東北角的藍天與大海",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '7日行程 / 遊歷12景點',
          duration: 7
        },
        {
          thumbnail: '../public/images/trips/trip-6.jpg',
          title: "東北角的藍天與大海",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '7日行程 / 遊歷12景點',
          duration: 7
        },
        {
          thumbnail: '../public/images/trips/trip-7.jpg',
          title: "東北角的藍天與大海",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '7日行程 / 遊歷12景點',
          duration: 7
        },
        {
          thumbnail: '../public/images/trips/trip-8.jpg',
          title: "東北角的藍天與大海",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '7日行程 / 遊歷12景點',
          duration: 7
        }
      ],
      [
        {
          thumbnail: '../public/images/trips/trip-1.jpg',
          title: "東北角的藍天與大海",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '7日行程 / 遊歷12景點',
          duration: 7
        },
        {
          thumbnail: '../public/images/trips/trip-2.jpg',
          title: "東北角的藍天與大海",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '7日行程 / 遊歷12景點',
          duration: 7
        },
        {
          thumbnail: '../public/images/trips/trip-3.jpg',
          title: "東北角的藍天與大海",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '7日行程 / 遊歷12景點',
          duration: 7
        },
        {
          thumbnail: '../public/images/trips/trip-4.jpg',
          title: "東北角的藍天與大海",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '7日行程 / 遊歷12景點',
          duration: 7
        }
      ],
      [
        {
          thumbnail: '../public/images/trips/trip-5.jpg',
          title: "東北角的藍天與大海",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '7日行程 / 遊歷12景點',
          duration: 7
        },
        {
          thumbnail: '../public/images/trips/trip-6.jpg',
          title: "東北角的藍天與大海",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '7日行程 / 遊歷12景點',
          duration: 7
        },
        {
          thumbnail: '../public/images/trips/trip-7.jpg',
          title: "東北角的藍天與大海",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '7日行程 / 遊歷12景點',
          duration: 7
        },
        {
          thumbnail: '../public/images/trips/trip-8.jpg',
          title: "東北角的藍天與大海",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '7日行程 / 遊歷12景點',
          duration: 7
        }
      ]
    ];

    

    $scope.carsSlide = {
      currentIdx: 0,
      prev: function () {
        $scope.carsSlide.currentIdx = $scope.carsSlide.currentIdx > 0 ? $scope.carsSlide.currentIdx - 1 : $scope.carsGroup.length - 1;
      },
      next: function () {
        $scope.carsSlide.currentIdx = $scope.carsSlide.currentIdx < ($scope.carsGroup.length - 1) ? $scope.carsSlide.currentIdx + 1 : 0;
      },
      set: function (idx) {
        $scope.carsSlide.currentIdx = idx;
      }
    };

    $scope.carsGroup = [
      [
        {
          thumbnail: 'http://www.autoviva.com/img/photos/608/ford_focus_1_6l_ecoboost_1st_edition_large_58608.jpg',
          title: "FOCUS 1.6",
          soldCount: 248,
          rank: 3,
          commentCount: 37,
          price: 2499,
          discount: 9,
          discountPrice: 2499,
          label: '和運租車'
        },
        {
          thumbnail: 'http://srv2.betterparts.org/images/ford-focus-1.6-tdci-09.jpg',
          title: "FOCUS 1.6",
          soldCount: 248,
          rank: 3,
          commentCount: 37,
          price: 2499,
          discount: 9,
          discountPrice: 2499,
          label: '和運租車'
        },
        {
          thumbnail: 'http://www.motorstown.com/images/ford-focus-1.6-ti-vct-trend-03.jpg',
          title: "FOCUS 1.6",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '和運租車'
        },
        {
          thumbnail: 'http://www.autoclassicpoa.com.br/fotoscarros/1518/FOCUS%20GLX%201.6%20001.jpg',
          title: "FOCUS 1.6",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '和運租車'
        }
      ],
      [
        {
          thumbnail: 'http://ext.pimg.tw/theoleaves/1396970728-2581328208_n.jpg',
          title: "FOCUS 1.6",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '和運租車'
        },
        {
          thumbnail: 'http://ext.pimg.tw/celsior/1433592347-2595382366_n.jpg',
          title: "FOCUS 1.6",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '和運租車'
        },
        {
          thumbnail: 'http://img.epochtimes.com/i6/806290918311641.jpg',
          title: "FOCUS 1.6",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '和運租車'
        },
        {
          thumbnail: 'https://farm6.staticflickr.com/5611/15685991896_7114e6df1c_z.jpg',
          title: "FOCUS 1.6",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '和運租車'
        }
      ],
      [
        {
          thumbnail: 'http://www.autoviva.com/img/photos/608/ford_focus_1_6l_ecoboost_1st_edition_large_58608.jpg',
          title: "FOCUS 1.6",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '和運租車'
        },
        {
          thumbnail: 'http://srv2.betterparts.org/images/ford-focus-1.6-tdci-09.jpg',
          title: "FOCUS 1.6",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '和運租車'
        },
        {
          thumbnail: 'http://www.motorstown.com/images/ford-focus-1.6-ti-vct-trend-03.jpg',
          title: "FOCUS 1.6",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '和運租車'
        },
        {
          thumbnail: 'http://www.autoclassicpoa.com.br/fotoscarros/1518/FOCUS%20GLX%201.6%20001.jpg',
          title: "FOCUS 1.6",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '和運租車'
        }
      ],
      [
        {
          thumbnail: 'http://ext.pimg.tw/theoleaves/1396970728-2581328208_n.jpg',
          title: "FOCUS 1.6",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '和運租車'
        },
        {
          thumbnail: 'http://ext.pimg.tw/celsior/1433592347-2595382366_n.jpg',
          title: "FOCUS 1.6",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '和運租車'
        },
        {
          thumbnail: 'http://img.epochtimes.com/i6/806290918311641.jpg',
          title: "FOCUS 1.6",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '和運租車'
        },
        {
          thumbnail: 'https://farm6.staticflickr.com/5611/15685991896_7114e6df1c_z.jpg',
          title: "FOCUS 1.6",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '和運租車'
        }
      ]
    ];

    

    $scope.storesSlide = {
      currentIdx: 0,
      prev: function () {
        $scope.storesSlide.currentIdx = $scope.storesSlide.currentIdx > 0 ? $scope.storesSlide.currentIdx - 1 : $scope.storesGroup.length - 1;
      },
      next: function () {
        $scope.storesSlide.currentIdx = $scope.storesSlide.currentIdx < ($scope.storesGroup.length - 1) ? $scope.storesSlide.currentIdx + 1 : 0;
      },
      set: function (idx) {
        $scope.storesSlide.currentIdx = idx;
      }
    };

    $scope.storesGroup = [
      [
        {
          thumbnail: '../public/images/stores/1.png',
          title: "阿原肥皂小肥皂優惠券",
          soldCount: 248,
          rank: 3,
          commentCount: 37,
          price: 2499,
          discount: 9,
          discountPrice: 2499,
          label: '優惠券'
        },
        {
          thumbnail: '../public/images/stores/2.png',
          title: "阿原肥皂小肥皂優惠券",
          soldCount: 248,
          rank: 3,
          commentCount: 37,
          price: 2499,
          discount: 9,
          discountPrice: 2499,
          label: '優惠券'
        },
        {
          thumbnail: '../public/images/stores/3.png',
          title: "阿原肥皂小肥皂優惠券",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '優惠券'
        },
        {
          thumbnail: '../public/images/stores/4.png',
          title: "阿原肥皂小肥皂優惠券",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '優惠券'
        }
      ],
      [
        {
          thumbnail: '../public/images/stores/5.png',
          title: "阿原肥皂小肥皂優惠券",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '優惠券'
        },
        {
          thumbnail: '../public/images/stores/6.png',
          title: "阿原肥皂小肥皂優惠券",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '優惠券'
        },
        {
          thumbnail: '../public/images/stores/7.png',
          title: "阿原肥皂小肥皂優惠券",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '優惠券'
        },
        {
          thumbnail: '../public/images/stores/8.png',
          title: "阿原肥皂小肥皂優惠券",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '優惠券'
        }
      ],
      [
        {
          thumbnail: '../public/images/stores/1.png',
          title: "阿原肥皂小肥皂優惠券",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '優惠券'
        },
        {
          thumbnail: '../public/images/stores/2.png',
          title: "阿原肥皂小肥皂優惠券",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '優惠券'
        },
        {
          thumbnail: '../public/images/stores/3.png',
          title: "阿原肥皂小肥皂優惠券",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '優惠券'
        },
        {
          thumbnail: '../public/images/stores/4.png',
          title: "阿原肥皂小肥皂優惠券",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '優惠券'
        }
      ],
      [
        {
          thumbnail: '../public/images/stores/5.png',
          title: "阿原肥皂小肥皂優惠券",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '優惠券'
        },
        {
          thumbnail: '../public/images/stores/6.png',
          title: "阿原肥皂小肥皂優惠券",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '優惠券'
        },
        {
          thumbnail: '../public/images/stores/7.png',
          title: "阿原肥皂小肥皂優惠券",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '優惠券'
        },
        {
          thumbnail: '../public/images/stores/8.png',
          title: "阿原肥皂小肥皂優惠券",
          rank: 3,
          commentCount: 37,
          price: 2499,
          label: '優惠券'
        }
      ]
    ];


  }
);

"use strict";

angular.module('ng.controller').controller('PurchasesCarsDetailController' ,
  function( userService, $mdSidenav, $mdBottomSheet, $log, $q, $scope, $mdDialog) {

    
    $scope.ticketInfo = {
      thumbnail: '../public/images/spots/spot-1.jpg',
      title: "台北101觀景台",
      soldCount: 248,
      rank: 3,
      commentCount: 37,
      price: 320,
      discount: 9,
      discountPrice: 180,
      address: '大同區',
      popular: true
    };

    $scope.toggleBool = function (bool) {
      $scope[bool] = !$scope[bool];
    };


    $scope.activities = [
      {
        title: '食我鑰匙大放送',
        startDate: '2015/03/10',
        endDate: '2015/05/10',
        message: '下載 Smart Tourism Taiwan 台灣智慧觀光 APP 並完成註冊、登入，馬上獲得食我鑰匙10把序號乙組！'
      },
      {
        title: 'Smart Tourism Taiwan 會員募集抽獎活動',
        startDate: '2015/03/10',
        endDate: '2015/05/10',
        message: '凡於活動期間下載 Smart Tourism Taiwan APP 並註冊成為會員，於行動裝置上的抽獎活動頁面登錄相關資訊，即可參加抽獎。'
      }
    ];

    $scope.comment = {
      author: '奧革士',
      authorImg: '../public/images/avatars/1.jpg',
      rank: undefined,
      comment: '',
      images: [
        '../public/images/stores/1.png', 
        '../public/images/stores/2.png', 
        '../public/images/stores/3.png'
      ],
      commentTime: ''
    };

    $scope.comments = [
      {
        author: '賀少俠',
        authorImg: '../public/images/avatars/2.jpg',
        rank: 4,
        comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
        images: [
          '../public/images/stores/1.png', 
          '../public/images/stores/2.png', 
          '../public/images/stores/3.png'
        ],
        commentTime: '5分鐘前',
        replys: [
          {
            author: '賀少俠',
            authorImg: '../public/images/avatars/2.jpg',
            rank: 4,
            comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
            images: [
              '../public/images/stores/1.png', 
              '../public/images/stores/2.png', 
              '../public/images/stores/3.png'
            ],
            commentTime: '5分鐘前',
            replys: []
          },
          {
            author: '路易四',
            authorImg: '../public/images/avatars/3.jpg',
            rank: 4,
            comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
            images: [],
            commentTime: '5分鐘前',
            replys: []
          }
        ]
      },
      {
        author: '路易四',
        authorImg: '../public/images/avatars/3.jpg',
        rank: 4,
        comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
        images: [],
        commentTime: '5分鐘前',
        replys: []
      },
      {
        author: '賀少俠',
        authorImg: '../public/images/avatars/4.jpg',
        rank: 4,
        comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
        images: [],
        commentTime: '5分鐘前',
        replys: []
      },
      {
        author: '賀少俠',
        authorImg: '../public/images/avatars/1.jpg',
        rank: 4,
        comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
        images: [],
        commentTime: '5分鐘前',
        replys: []
      }
    ];

    $scope.addComment = function () {
      $scope.comments.splice(0, 0, {
        author: $scope.comment.author,
        authorImg: $scope.comment.authorImg,
        rank: 4,
        comment: $scope.comment.comment,
        images: [],
        commentTime: '幾秒鐘前',
        replys: []
      });
      $scope.comment.comment = '';
    };

    $scope.deleteComment = function (index) {
      $scope.comments.splice(index, 1);
    };

    $scope.toReply = function (item) {
      item.replyMode = true;
    };

    $scope.editComment = function (item) {
      item.editMode = true;
      item.commentTemp = item.comment;
    };

    $scope.updateComment = function (item) {
      item.editMode = false;
      item.comment = item.commentTemp;
    };

    $scope.cancelEditComment = function (item) {
      item.editMode = false;
    };

    $scope.addReply = function (comment) {
      comment.replys.splice(0, 0, {
        author: $scope.comment.author,
        authorImg: $scope.comment.authorImg,
        comment: comment.replyTemp,
        commentTime: '幾秒鐘前'
      });
      comment.replyTemp = '';
      comment.replyMode = false;
    };

    $scope.deleteReply = function (comment, index) {
      comment.replys.splice(index, 1);
    };

    $scope.spots = [
      {
        thumbnail: '../public/images/spots/spot-1.jpg',
        title: '華山文創園區',
        description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
        rank: 4
      },
      {
        thumbnail: '../public/images/spots/spot-2.jpg',
        title: '市立台北動物園',
        description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
        rank: 4
      },
      {
        thumbnail: '../public/images/spots/spot-3.jpg',
        title: '松山文創園區',
        description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
        rank: 4
      },
      {
        thumbnail: '../public/images/spots/spot-4.jpg',
        title: '傳統藝術博物館',
        description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
        rank: 4
      },
      {
        thumbnail: '../public/images/spots/spot-5.jpg',
        title: '台北美術館',
        description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
        rank: 4
      }
    ];

$scope.products = [
      {
        thumbnail: '../public/images/stores/1.png',
        title: '柑仔手工肥皂',
        description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
        price: 4235,
      },
      {
        thumbnail: '../public/images/stores/2.png',
        title: '馬櫻丹肥皂',
        description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
        price: 4235,
      },
      {
        thumbnail: '../public/images/stores/3.png',
        title: '阿原竹月刀',
        description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
        price: 4235,
      },
      {
        thumbnail: '../public/images/stores/4.png',
        title: '精油禮盒包 X 6',
        description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
        price: 4235,
      },
      {
        thumbnail: '../public/images/stores/5.png',
        title: '天然草本組合包',
        description: '馬櫻丹喜好陽光，植性強健，耐寒也耐熱，用來入皂，可以增加皮膚對環境的防護力，減少氣候轉變引起的皮膚不適。',
        price: 4235,
      }
    ];

    $scope.otherProducts = [
      {
        thumbnail: '../public/images/spots/spot-1.jpg',
        title: '黃家牛肉麵',
        description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
        rank: 4
      },
      {
        thumbnail: '../public/images/spots/spot-2.jpg',
        title: '市立台北動物園',
        description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
        rank: 4
      },
      {
        thumbnail: '../public/images/spots/spot-3.jpg',
        title: '松山文創園區',
        description: '傍晚時分夕陽餘暉，入夜後滿天星空、繽紛花火點綴其中、享用燒烤美食佳餚，用完餐後漫步長堤，聆聽浪花與大自然的對話，盡情享受愉悅舒適的假期。觀景長堤位於民族路與陽明路交叉口，後方緊鄰觀景長堤因而得名。南可至馬公港，沿途至觀音亭腳踏車步道、彩虹橋、天后宮…等，東可達縣立文化局及二呆藝文館，是絕佳的地理位置。地點特色:夜觀星空、漫步長堤、夕陽餘暉、繽紛花火、 海景風光、寬敞空間、不同享受。消費特色:自助式吃到飽－盡情享用精美食材、澎湖當地季節海鮮－烤鮮蚵、烤小管。',
        rank: 4
      }
    ];


    $scope.relatedTravelNotes = [
      {
        thumbnail: 'https://placeimg.com/100/80/nature?a=11',
        title: '[食記]台中市‧台中新地標‧宮原眼科掛號吃冰',
        publishDate: '2015年3月7日',
        author: '賀少俠',
        authorImg: '../public/images/avatars/1.jpg',
        commentCount: 22,
        collectCount: 77
      },
      {
        thumbnail: 'https://placeimg.com/100/80/nature?a=12',
        title: '[食記]台中市‧台中新地標‧宮原眼科掛號吃冰',
        publishDate: '2015年3月7日',
        author: '林小美',
        authorImg: '../public/images/avatars/2.jpg',
        commentCount: 22,
        collectCount: 77
      },
      {
        thumbnail: 'https://placeimg.com/100/80/nature?a=13',
        title: '[食記]台中市‧台中新地標‧宮原眼科掛號吃冰',
        publishDate: '2015年3月7日',
        author: '奧革士',
        authorImg: '../public/images/avatars/3.jpg',
        commentCount: 22,
        collectCount: 77
      }
    ];

  }
);

"use strict";

angular.module('ng.controller').controller('PurchasesTripsController' ,
  function( userService, $mdSidenav, $mdBottomSheet, $log, $q, $scope) {

    $scope.people = 1;
    // $scope.layout = "List";
    $scope.layout = "Grid";
    $scope.minPrice;
    $scope.maxPrice;
    $scope.isOpenFilter = false;

    $scope.closeFilter = function () {
      $scope.isOpenFilter = false;      
    };

    $scope.openFilter = function () {
      $scope.isOpenFilter = true;      
    };

    $scope.showAllOption = {};

    $scope.toggleMore = function (option) {
      $scope.showAllOption[option] = !$scope.showAllOption[option];
    };

    $scope.orderBy = 'topRated';

    $scope.purchases = [
      {
        thumbnail: '../public/images/trips/trip-1.jpg',
        title: "東北角的藍天與大海",
        soldCount: 248,
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        rank: 3,
        viewes: 37,        
        commentCount: 37,
        price: 2499,
        discount: 9,
        discountPrice: 2499,
        label: '7日行程 / 遊歷12景點',
        duration: 7,
        popular: true
      },
      {
        thumbnail: '../public/images/trips/trip-2.jpg',
        title: "東北角的藍天與大海",
        soldCount: 248,
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        rank: 3,
        viewes: 37,        
        commentCount: 37,
        price: 2499,
        discount: 9,
        discountPrice: 2499,
        label: '7日行程 / 遊歷12景點',
        duration: 7
      },
      {
        thumbnail: '../public/images/trips/trip-3.jpg',
        title: "東北角的藍天與大海",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        rank: 3,
        viewes: 37,        
        commentCount: 37,
        price: 2499,
        label: '7日行程 / 遊歷12景點',
        duration: 7
      },
      {
        thumbnail: '../public/images/trips/trip-4.jpg',
        title: "東北角的藍天與大海",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        rank: 3,
        viewes: 37,        
        commentCount: 37,
        price: 2499,
        label: '7日行程 / 遊歷12景點',
        duration: 7
      },
      {
        thumbnail: '../public/images/trips/trip-5.jpg',
        title: "東北角的藍天與大海",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        rank: 3,
        viewes: 37,        
        commentCount: 37,
        price: 2499,
        label: '7日行程 / 遊歷12景點',
        duration: 7
      },
      {
        thumbnail: '../public/images/trips/trip-6.jpg',
        title: "東北角的藍天與大海",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        rank: 3,
        viewes: 37,        
        commentCount: 37,
        price: 2499,
        label: '7日行程 / 遊歷12景點',
        duration: 7
      },
      {
        thumbnail: '../public/images/trips/trip-7.jpg',
        title: "東北角的藍天與大海",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        rank: 3,
        viewes: 37,        
        commentCount: 37,
        price: 2499,
        label: '7日行程 / 遊歷12景點',
        duration: 7
      },
      {
        thumbnail: '../public/images/trips/trip-8.jpg',
        title: "東北角的藍天與大海",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        rank: 3,
        viewes: 37,        
        commentCount: 37,
        price: 2499,
        label: '7日行程 / 遊歷12景點',
        duration: 7
      },
      {
        thumbnail: '../public/images/trips/trip-1.jpg',
        title: "東北角的藍天與大海",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        rank: 3,
        viewes: 37,        
        commentCount: 37,
        price: 2499,
        label: '7日行程 / 遊歷12景點',
        duration: 7
      },
      {
        thumbnail: '../public/images/trips/trip-2.jpg',
        title: "東北角的藍天與大海",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        rank: 3,
        viewes: 37,        
        commentCount: 37,
        price: 2499,
        label: '7日行程 / 遊歷12景點',
        duration: 7
      },
      {
        thumbnail: '../public/images/trips/trip-3.jpg',
        title: "東北角的藍天與大海",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        rank: 3,
        viewes: 37,        
        commentCount: 37,
        price: 2499,
        label: '7日行程 / 遊歷12景點',
        duration: 7
      },
      {
        thumbnail: '../public/images/trips/trip-4.jpg',
        title: "東北角的藍天與大海",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        rank: 3,
        viewes: 37,        
        commentCount: 37,
        price: 2499,
        label: '7日行程 / 遊歷12景點',
        duration: 7
      },
      {
        thumbnail: '../public/images/trips/trip-5.jpg',
        title: "東北角的藍天與大海",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        rank: 3,
        viewes: 37,        
        commentCount: 37,
        price: 2499,
        label: '7日行程 / 遊歷12景點',
        duration: 7
      },
      {
        thumbnail: '../public/images/trips/trip-6.jpg',
        title: "東北角的藍天與大海",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        rank: 3,
        viewes: 37,        
        commentCount: 37,
        price: 2499,
        label: '7日行程 / 遊歷12景點',
        duration: 7
      },
      {
        thumbnail: '../public/images/trips/trip-7.jpg',
        title: "東北角的藍天與大海",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        rank: 3,
        viewes: 37,        
        commentCount: 37,
        price: 2499,
        label: '7日行程 / 遊歷12景點',
        duration: 7
      },
      {
        thumbnail: '../public/images/trips/trip-8.jpg',
        title: "東北角的藍天與大海",
        descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
        rank: 3,
        viewes: 37,        
        commentCount: 37,
        price: 2499,
        label: '7日行程 / 遊歷12景點',
        duration: 7
      }
    ];

    $scope.switchLayout = function (layout) {
      $scope.layout = layout;
    };


  }
);

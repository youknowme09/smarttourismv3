"use strict";

angular.module('ng.controller').controller('PurchasesTripsDetailController' ,
  function( userService, $mdSidenav, $mdBottomSheet, $log, $q, $scope, $mdDialog) {

    
    $scope.ticketInfo = {
      thumbnail: '../public/images/spots/spot-1.jpg',
      title: "台北101觀景台",
      soldCount: 248,
      rank: 3,
      commentCount: 37,
      price: 1290,
      discount: 9,
      discountPrice: 40,
      address: '大同區',
      popular: true
    };

    $scope.toggleBool = function (bool) {
      $scope[bool] = !$scope[bool];
    };


    $scope.activities = [
    {
      title: '食我鑰匙大放送',
      startDate: '2015/03/10',
      endDate: '2015/05/10',
      message: '下載 Smart Tourism Taiwan 台灣智慧觀光 APP 並完成註冊、登入，馬上獲得食我鑰匙10把序號乙組！'
    },
    {
      title: 'Smart Tourism Taiwan 會員募集抽獎活動',
      startDate: '2015/03/10',
      endDate: '2015/05/10',
      message: '凡於活動期間下載 Smart Tourism Taiwan APP 並註冊成為會員，於行動裝置上的抽獎活動頁面登錄相關資訊，即可參加抽獎。'
    }
    ];

    $scope.comment = {
      author: '奧革士',
      authorImg: '../public/images/avatars/1.jpg',
      rank: undefined,
      comment: '',
      images: [
      '../public/images/stores/1.png', 
      '../public/images/stores/2.png', 
      '../public/images/stores/3.png'
      ],
      commentTime: ''
    };

    $scope.comments = [
    {
      author: '賀少俠',
      authorImg: '../public/images/avatars/2.jpg',
      rank: 4,
      comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
      images: [
      '../public/images/stores/1.png', 
      '../public/images/stores/2.png', 
      '../public/images/stores/3.png'
      ],
      commentTime: '5分鐘前',
      replys: [
      {
        author: '賀少俠',
        authorImg: '../public/images/avatars/2.jpg',
        rank: 4,
        comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
        images: [
        '../public/images/stores/1.png', 
        '../public/images/stores/2.png', 
        '../public/images/stores/3.png'
        ],
        commentTime: '5分鐘前',
        replys: []
      },
      {
        author: '路易四',
        authorImg: '../public/images/avatars/3.jpg',
        rank: 4,
        comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
        images: [],
        commentTime: '5分鐘前',
        replys: []
      }
      ]
    },
    {
      author: '路易四',
      authorImg: '../public/images/avatars/3.jpg',
      rank: 4,
      comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
      images: [],
      commentTime: '5分鐘前',
      replys: []
    },
    {
      author: '賀少俠',
      authorImg: '../public/images/avatars/4.jpg',
      rank: 4,
      comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
      images: [],
      commentTime: '5分鐘前',
      replys: []
    },
    {
      author: '賀少俠',
      authorImg: '../public/images/avatars/1.jpg',
      rank: 4,
      comment: '4樓都會廣場擁有完美的空間設計規劃，佔地500餘坪，挑高40米。採光自然，與四周的露天座椅融合為一體，散發著明亮開闊的現代氛圍。與世貿中心和ATT 4 FUN購物中心有空橋連接，近君悅飯店，交通十分方便。',
      images: [],
      commentTime: '5分鐘前',
      replys: []
    }
    ];

    $scope.addComment = function () {
      $scope.comments.splice(0, 0, {
        author: $scope.comment.author,
        authorImg: $scope.comment.authorImg,
        rank: 4,
        comment: $scope.comment.comment,
        images: [],
        commentTime: '幾秒鐘前',
        replys: []
      });
      $scope.comment.comment = '';
    };

    $scope.deleteComment = function (index) {
      $scope.comments.splice(index, 1);
    };

    $scope.toReply = function (item) {
      item.replyMode = true;
    };

    $scope.editComment = function (item) {
      item.editMode = true;
      item.commentTemp = item.comment;
    };

    $scope.updateComment = function (item) {
      item.editMode = false;
      item.comment = item.commentTemp;
    };

    $scope.cancelEditComment = function (item) {
      item.editMode = false;
    };

    $scope.addReply = function (comment) {
      comment.replys.splice(0, 0, {
        author: $scope.comment.author,
        authorImg: $scope.comment.authorImg,
        comment: comment.replyTemp,
        commentTime: '幾秒鐘前'
      });
      comment.replyTemp = '';
      comment.replyMode = false;
    };

    $scope.deleteReply = function (comment, index) {
      comment.replys.splice(index, 1);
    };
    $scope.videos = [
    {
      thumbnail: '../public/images/events/1.jpg',
      title: '看見鼎泰豐 完整版',
      author: '奧革士',
      viewcount: '33210',
      description: '阿原工作室草創於2005年，以台灣青草藥手作皂為起點，選用台灣青草藥為主題，融合­東方養生思維、愛惜人身與善待環境的理念，致力實踐失落的'
    },
    {
      thumbnail: '../public/images/events/2.jpg',
      title: '鼎泰豐 - 我們的驕傲',
      author: '奧革士',
      viewcount: '33210',
      description: '江榮原先生阿原肥皂創辦人阿原工作室與阿原肥皂即是以創辦人江榮原先生為名。 創辦人江榮原先生，由於家庭因素而在高職畢業後就 ...'
    }
    ];
    $scope.setTrips = [
    {
      thumbnail: '../public/images/spots/spot-1.jpg',
      title: '墨爾本神仙藍企鵝·夢幻世界無尾熊·浪漫藍山·紅櫻桃·雪梨港灣遊艇全覽絢麗9日',
      rank: 4
    },
    {
      thumbnail: '../public/images/spots/spot-2.jpg',
      title: '墨爾本神仙藍企鵝·夢幻世界無尾熊·浪漫藍山·紅櫻桃·雪梨港灣遊艇全覽絢麗9日',
      rank: 4
    },
    {
      thumbnail: '../public/images/spots/spot-3.jpg',
      title: '墨爾本神仙藍企鵝·夢幻世界無尾熊·浪漫藍山·紅櫻桃·雪梨港灣遊艇全覽絢麗9日',
      rank: 4
    }
    ];


    $scope.relatedTravelNotes = [
    {
      thumbnail: 'https://placeimg.com/100/80/nature?a=11',
      title: '[食記]台中市‧台中新地標‧宮原眼科掛號吃冰',
      publishDate: '2015年3月7日',
      author: '賀少俠',
      authorImg: '../public/images/avatars/1.jpg',
      commentCount: 22,
      collectCount: 77
    },
    {
      thumbnail: 'https://placeimg.com/100/80/nature?a=12',
      title: '[食記]台中市‧台中新地標‧宮原眼科掛號吃冰',
      publishDate: '2015年3月7日',
      author: '林小美',
      authorImg: '../public/images/avatars/2.jpg',
      commentCount: 22,
      collectCount: 77
    },
    {
      thumbnail: 'https://placeimg.com/100/80/nature?a=13',
      title: '[食記]台中市‧台中新地標‧宮原眼科掛號吃冰',
      publishDate: '2015年3月7日',
      author: '奧革士',
      authorImg: '../public/images/avatars/3.jpg',
      commentCount: 22,
      collectCount: 77
    }
    ];



    $scope.tabs = [
    {
      id: 0,
      name: '列表模式',
      type: 'list'
    },
    {
      id: 1,
      name: '日曆模式',
      type: 'calendar'
    }
    ];

    $scope.currentTab = $scope.tabs[0];

    $scope.switchTab = function(tab) {
      $scope.currentTab = tab;
    };


    $scope.trips = [
    {
      isOpen: true,
      place: '陽明山國家公園·賞花 ＞淡水老街',
      date_year: '2015',
      date_month: '12',
      date_day: '08',
      date_week: '二',
      spots: [
      {
        thumbnail: '../public/images/spots/spot-1.jpg',
        title: '1松山文創園區',
        descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
        commentCount: 22,
        collectCount: 77,
        type: 'spot'
      },
      {
        thumbnail: '../public/images/spots/spot-2.jpg',
        title: '1台北101大樓',
        descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
        commentCount: 22,
        collectCount: 77,
        type: 'spot'
      },
      {
        thumbnail: '../public/images/spots/spot-3.jpg',
        title: '1華山文創園區',
        descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
        commentCount: 22,
        collectCount: 77,
        type: 'spot'
      },
      {
        thumbnail: '../public/images/spots/spot-4.jpg',
        title: '1傳統黑糖冰',
        address: '台北市大同區長安西路33號',
        phone: '02 - 23456218',
        commentCount: 22,
        collectCount: 77,
        type: 'food'
      },
      {
        thumbnail: '../public/images/spots/spot-5.jpg',
        title: '1台北美術館',
        descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
        commentCount: 22,
        collectCount: 77,
        type: 'spot'
      }
      ],
      stays: [
      {
        thumbnail: '../public/images/spots/spot-6.jpg',
        title: '1療癒系公寓 乾淨又舒適的溫暖...',
        address: '台北市大同區長安西路33號',
        phone: '02 - 23456218',
        commentCount: 22,
        collectCount: 77,
        type: 'stay',
        notes: '住的是高級的房間，記得可以免費索取溫泉泡湯券'
      }
      ]
    },
    {
      place: '陽明山國家公園·賞花 ＞淡水老街',
      date_year: '2015',
      date_month: '12',
      date_day: '09',
      date_week: '三',
      spots: [
      {
        thumbnail: '../public/images/spots/spot-1.jpg',
        title: '2松山文創園區',
        descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
        commentCount: 22,
        collectCount: 77,
        type: 'spot'
      },
      {
        thumbnail: '../public/images/spots/spot-2.jpg',
        title: '2台北101大樓',
        descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
        commentCount: 22,
        collectCount: 77,
        type: 'spot'
      },
      {
        thumbnail: '../public/images/spots/spot-3.jpg',
        title: '2華山文創園區',
        descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
        commentCount: 22,
        collectCount: 77,
        type: 'spot'
      },
      {
        thumbnail: '../public/images/spots/spot-4.jpg',
        title: '2傳統黑糖冰',
        address: '台北市大同區長安西路33號',
        phone: '02 - 23456218',
        commentCount: 22,
        collectCount: 77,
        type: 'food'
      },
      {
        thumbnail: '../public/images/spots/spot-5.jpg',
        title: '2台北美術館',
        descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
        commentCount: 22,
        collectCount: 77,
        type: 'spot'
      }
      ],
      stays: [
      {
        thumbnail: '../public/images/spots/spot-6.jpg',
        title: '2療癒系公寓 乾淨又舒適的溫暖...',
        address: '台北市大同區長安西路33號',
        phone: '02 - 23456218',
        commentCount: 22,
        collectCount: 77,
        type: 'stay',
        notes: '住的是高級的房間，記得可以免費索取溫泉泡湯券'
      }
      ]
    },
    {
      place: '陽明山國家公園·賞花 ＞淡水老街',
      date_year: '2015',
      date_month: '12',
      date_day: '10',
      date_week: '四',
      spots: [
      {
        thumbnail: '../public/images/spots/spot-1.jpg',
        title: '3松山文創園區',
        descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
        commentCount: 22,
        collectCount: 77,
        type: 'spot'
      },
      {
        thumbnail: '../public/images/spots/spot-2.jpg',
        title: '3台北101大樓',
        descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
        commentCount: 22,
        collectCount: 77,
        type: 'spot'
      },
      {
        thumbnail: '../public/images/spots/spot-3.jpg',
        title: '3華山文創園區',
        descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
        commentCount: 22,
        collectCount: 77,
        type: 'spot'
      },
      {
        thumbnail: '../public/images/spots/spot-4.jpg',
        title: '3傳統黑糖冰',
        address: '台北市大同區長安西路33號',
        phone: '02 - 23456218',
        commentCount: 22,
        collectCount: 77,
        type: 'food'
      },
      {
        thumbnail: '../public/images/spots/spot-5.jpg',
        title: '3台北美術館',
        descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
        commentCount: 22,
        collectCount: 77,
        type: 'spot'
      }
      ],
      stays: [
      {
        thumbnail: '../public/images/spots/spot-6.jpg',
        title: '3療癒系公寓 乾淨又舒適的溫暖...',
        address: '台北市大同區長安西路33號',
        phone: '02 - 23456218',
        commentCount: 22,
        collectCount: 77,
        type: 'stay',
        notes: '住的是高級的房間，記得可以免費索取溫泉泡湯券'
      }
      ]
    },
    {
      place: '陽明山國家公園·賞花 ＞淡水老街',
      date_year: '2015',
      date_month: '12',
      date_day: '11',
      date_week: '五',
      spots: [
      {
        thumbnail: '../public/images/spots/spot-1.jpg',
        title: '4松山文創園區',
        descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
        commentCount: 22,
        collectCount: 77,
        type: 'spot'
      },
      {
        thumbnail: '../public/images/spots/spot-2.jpg',
        title: '4台北101大樓',
        descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
        commentCount: 22,
        collectCount: 77,
        type: 'spot'
      },
      {
        thumbnail: '../public/images/spots/spot-3.jpg',
        title: '4華山文創園區',
        descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
        commentCount: 22,
        collectCount: 77,
        type: 'spot'
      },
      {
        thumbnail: '../public/images/spots/spot-4.jpg',
        title: '4傳統黑糖冰',
        address: '台北市大同區長安西路33號',
        phone: '02 - 23456218',
        commentCount: 22,
        collectCount: 77,
        type: 'food'
      },
      {
        thumbnail: '../public/images/spots/spot-5.jpg',
        title: '4台北美術館',
        descript: '東北角海岸依山傍海，灣岬羅列，有嶙峋的奇岩聳崖、巨觀的海蝕地形、細柔的金色沙灘，以及繽紛的海洋生態，並且孕育出淳樸的人文史蹟，是一處兼具大自然教室知性功能與濱海遊樂場感性風采的全方位觀光旅遊勝地。',
        commentCount: 22,
        collectCount: 77,
        type: 'spot'
      }
      ],
      stays: [
      {
        thumbnail: '../public/images/spots/spot-6.jpg',
        title: '4療癒系公寓 乾淨又舒適的溫暖...',
        address: '台北市大同區長安西路33號',
        phone: '02 - 23456218',
        commentCount: 22,
        collectCount: 77,
        type: 'stay',
        notes: '住的是高級的房間，記得可以免費索取溫泉泡湯券'
      }
      ]
    }
    ];

    

    $scope.traffic = function (ev) {
      $mdDialog.show({
        controller: function ($scope, $mdDialog, $timeout) {
          $scope.showPublicTransportation = true;
          $scope.showDriving = true;
          $scope.showWalk = true;
          $scope.cancel = function() {
            $mdDialog.cancel();
          };
          $scope.done = function(answer) {
            $scope.processing = true;
            $timeout(function () {
              $scope.processing = false;
              $scope.success = true;
            }, 1000);
          };
        },
        templateUrl: '../assets/common/partials/trafficMadal.html',
        targetEvent: ev,
      })
      .then(function(answer) {
        $scope.alert = 'You said the information was "' + answer + '".';
      }, function() {
        $scope.alert = 'You cancelled the dialog.';
      });
    };

    

    $scope.openFullMap = function (ev) {
      $mdDialog.show({
        scope: $scope,
        controller: function ($scope, $mdDialog, $timeout) {
          console.log($scope);
          if ($scope.trips && $scope.trips.length > 0) {
            $scope.currentTrip = $scope.trips[0];
          }
          $scope.selectTrip = function(trip) {
            $scope.currentTrip = trip;
          };
          $scope.cancel = function() {
            $mdDialog.cancel();
          };
          $scope.done = function(answer) {
          };
        },
        templateUrl: '../assets/common/partials/tripFullMapMadal.html',
        targetEvent: ev,
      })
      .then(function(answer) {
        $scope.alert = 'You said the information was "' + answer + '".';
      }, function() {
        $scope.alert = 'You cancelled the dialog.';
      });
    };

  }
  );

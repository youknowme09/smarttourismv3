"use strict";

angular.module('ng.controller').controller('PurchasesStoresController' ,
  function( userService, $mdSidenav, $mdBottomSheet, $log, $q, $scope) {

    $scope.people = 1;
    // $scope.layout = "List";
    $scope.layout = "Grid";
    $scope.minPrice;
    $scope.maxPrice;
    $scope.isOpenFilter = false;

    $scope.closeFilter = function () {
      $scope.isOpenFilter = false;      
    };

    $scope.openFilter = function () {
      $scope.isOpenFilter = true;      
    };

    $scope.showAllOption = {};

    $scope.toggleMore = function (option) {
      $scope.showAllOption[option] = !$scope.showAllOption[option];
    };

    $scope.orderBy = 'topRated';

    $scope.purchases = [
      {
        thumbnail: '../public/images/stores/1.png',
        title: "阿原肥皂小肥皂優惠券",
        soldCount: 248,
        rank: 3,
        commentCount: 37,
        price: 2499,
        discount: 9,
        discountPrice: 2499,
        label: '優惠券'
      },
      {
        thumbnail: '../public/images/stores/2.png',
        title: "阿原肥皂小肥皂優惠券",
        soldCount: 248,
        rank: 3,
        commentCount: 37,
        price: 2499,
        discount: 9,
        discountPrice: 2499,
        label: '優惠券'
      },
      {
        thumbnail: '../public/images/stores/3.png',
        title: "阿原肥皂小肥皂優惠券",
        rank: 3,
        commentCount: 37,
        price: 2499,
        label: '優惠券'
      },
      {
        thumbnail: '../public/images/stores/4.png',
        title: "阿原肥皂小肥皂優惠券",
        rank: 3,
        commentCount: 37,
        price: 2499,
        label: '優惠券'
      },
      {
        thumbnail: '../public/images/stores/5.png',
        title: "阿原肥皂小肥皂優惠券",
        rank: 3,
        commentCount: 37,
        price: 2499,
        label: '優惠券'
      },
      {
        thumbnail: '../public/images/stores/6.png',
        title: "阿原肥皂小肥皂優惠券",
        rank: 3,
        commentCount: 37,
        price: 2499,
        label: '優惠券'
      },
      {
        thumbnail: '../public/images/stores/7.png',
        title: "阿原肥皂小肥皂優惠券",
        rank: 3,
        commentCount: 37,
        price: 2499,
        label: '優惠券'
      },
      {
        thumbnail: '../public/images/stores/8.png',
        title: "阿原肥皂小肥皂優惠券",
        rank: 3,
        commentCount: 37,
        price: 2499,
        label: '優惠券'
      },
      {
        thumbnail: '../public/images/stores/1.png',
        title: "阿原肥皂小肥皂優惠券",
        rank: 3,
        commentCount: 37,
        price: 2499,
        label: '優惠券'
      },
      {
        thumbnail: '../public/images/stores/2.png',
        title: "阿原肥皂小肥皂優惠券",
        rank: 3,
        commentCount: 37,
        price: 2499,
        label: '優惠券'
      },
      {
        thumbnail: '../public/images/stores/3.png',
        title: "阿原肥皂小肥皂優惠券",
        rank: 3,
        commentCount: 37,
        price: 2499,
        label: '優惠券'
      },
      {
        thumbnail: '../public/images/stores/4.png',
        title: "阿原肥皂小肥皂優惠券",
        rank: 3,
        commentCount: 37,
        price: 2499,
        label: '優惠券'
      },
      {
        thumbnail: '../public/images/stores/5.png',
        title: "阿原肥皂小肥皂優惠券",
        rank: 3,
        commentCount: 37,
        price: 2499,
        label: '優惠券'
      },
      {
        thumbnail: '../public/images/stores/6.png',
        title: "阿原肥皂小肥皂優惠券",
        rank: 3,
        commentCount: 37,
        price: 2499,
        label: '優惠券'
      },
      {
        thumbnail: '../public/images/stores/7.png',
        title: "阿原肥皂小肥皂優惠券",
        rank: 3,
        commentCount: 37,
        price: 2499,
        label: '優惠券'
      },
      {
        thumbnail: '../public/images/stores/8.png',
        title: "阿原肥皂小肥皂優惠券",
        rank: 3,
        commentCount: 37,
        price: 2499,
        label: '優惠券'
      }
    ];

    $scope.switchLayout = function (layout) {
      $scope.layout = layout;
    };


  }
);

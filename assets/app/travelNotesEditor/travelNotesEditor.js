"use strict";

angular.module('ng.controller').controller('TravelNotesEditorController' ,
  function( userService, $mdSidenav, $mdBottomSheet, $log, $q, $scope, $mdDialog) {

    $scope.travelNote = {
        title: '台北101的韓式銅盤美食《雪嶽山韓式料理》',
        content: '<p class="p1"><span class="s1">真正的夏天已經悄悄到來，你是否已經感受到它熱情的威力了呢？當你汗流浹背，此時腦海中出現的影像可不會是熱騰騰的火鍋，而是冰冰涼涼的清新消暑飲品呀！今天小編就來和大家分享，這個夏天可以讓你很「<b><i>歐蝦雷</i></b>」的玻璃罐</span><span class="s2"><b style="color: #31c4ff;"><u>清涼飲品</u></b></span><span class="s1">製作 DIY，一起來看看吧！</span></p><p class="p1"><span class="s1"><br/></span></p><p class="p1"><span class="s1">今天要做的是 Mojito！來看看要準備些什麼材料吧！</span></p><p class="p1"><span class="s1"><br/></span></p><p class="p1"><span class="s1"><img src="http://ext.pimg.tw/kuku0510/1367307219-28740645_m.jpg"/><br/></span></p><h3 class="p1"><b>準備材料</b></h3><p class="p1"><title></title></p><p class="p1"><span class="s1">真正的夏天已經悄悄到來，你是否已經感受到它熱情的威力了呢？當你汗流浹背，此時腦海中出現的影像可不會是熱騰騰的火鍋，而是冰冰涼涼的清新消暑飲品呀！今天小編就來和大家分享，這個夏天可以讓你很「歐蝦雷」的玻璃罐清涼飲品製作 DIY，一起來看看吧！</span></p><p class="p1"><span class="s1"><br/></span></p><p class="p1"><title></title></p><blockquote class="p1">真正的夏天已經悄悄到來，你是否已經感受到它熱情的威力了呢？當你汗流浹背，此時腦海中出現的影像可不會是熱騰騰的火鍋，而是冰冰涼涼的清新消暑飲品呀！今天小編就來和大家分享，這個夏天可以讓你很「歐蝦雷」的玻璃罐清涼飲品製作 DIY，一起來看看吧！</blockquote><p></p><p></p><p></p>'
    };

    $scope.loadAreas = function (query) {
        // console.log(query);
        return [
            '基隆市',
            '新北市',
            '台北市',
            '桃園市',
            '新竹市',
            '苗栗縣'
        ];
    };

    $scope.loadSpots = function (query) {
        // console.log(query);
        return [
            '九份',
            '九份老街',
            '九族文化村',
            '九份阿柑姨芋圓',
            '九份肉圓'
        ];
    };

    $scope.topics = [
        {
            name: '自然生態'
        },
        {
            name: '宗教信仰'
        },
        {
            name: '歷史古蹟'
        },
        {
            name: '美食饗宴'
        },
        {
            name: '農村體驗'
        },
        {
            name: '購物萬象'
        },
        {
            name: '養生溫泉'
        },
        {
            name: '親子同樂'
        }
    ];

    $scope.textTypes = [
        {
            id: 1,
            name: '標題'
        },
        {
            id: 2,
            name: '內文'
        },
        {
            id: 3,
            name: '引用'
        }
    ];
    $scope.currentTextType = $scope.textTypes[0].id;


    $scope.isSelectPicture = false;
  }
);

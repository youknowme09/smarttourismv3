"use strict";

angular.module('ng.controller').controller('themeEventsController',
  function(userService, $mdSidenav, $mdBottomSheet, $log, $q, $scope, $timeout) {
    $scope.slideList = [{
        "imgUrl": "../public/images/banner4.jpg",
        "title": "旅行特價，隨時發現",
        "descript": "整合各式多元的旅遊資訊，透過個人化推薦與適時適地適性的推播功能，協助您簡單快速取得心中理想的旅遊資訊。",
        "buttonText": "瀏覽商品速購"
      },
      {
        "imgUrl": "../public/images/banner2.jpg",
        "title": "旅行特價，隨時發現",
        "descript": "整合各式多元的旅遊資訊，透過個人化推薦與適時適地適性的推播功能，協助您簡單快速取得心中理想的旅遊資訊。",
        "buttonText": "瀏覽商品速購"
      },
      {
        "imgUrl": "../public/images/banner3.jpg",
        "title": "下載APP，玩轉台灣",
        "descript": "整合各式多元的旅遊資訊，透過個人化推薦與適時適地適性的推播功能，協助您簡單快速取得心中理想的旅遊資訊。",
        "buttonText": "馬上建立新的旅程"
      }
    ]
    $scope.people = 1;
    $scope.isOpenFilter = false;

    $scope.closeFilter = function() {
      $scope.isOpenFilter = false;
    };

    $scope.openFilter = function() {
      $scope.isOpenFilter = true;
    };

    $scope.currentSlideIdx = 0;

    $scope.prevSlide = function() {
      $scope.currentSlideIdx = $scope.currentSlideIdx > 0 ? $scope.currentSlideIdx - 1 : $scope.slideList.length - 1;
    };

    $scope.nextSlide = function() {
      $scope.currentSlideIdx = $scope.currentSlideIdx < ($scope.slideList.length - 1) ? $scope.currentSlideIdx + 1 : 0;
    };

    $scope.setSlide = function(idx) {
      $scope.currentSlideIdx = idx;
    };

    var timer;
    var sliderFunc = function() {
      timer = $timeout(function() {
        $scope.currentSlideIdx = $scope.currentSlideIdx < ($scope.slideList.length - 1) ? $scope.currentSlideIdx + 1 : 0;
        timer = $timeout(sliderFunc, 10000);
      }, 10000);
    };

    sliderFunc();





    $scope.spotsSlide = {
      currentIdx: 0,
      prev: function() {
        $scope.spotsSlide.currentIdx = $scope.spotsSlide.currentIdx > 0 ? $scope.spotsSlide.currentIdx - 1 : $scope.spotsGroup.length - 1;
      },
      next: function() {
        $scope.spotsSlide.currentIdx = $scope.spotsSlide.currentIdx < ($scope.spotsGroup.length - 1) ? $scope.spotsSlide.currentIdx + 1 : 0;
      },
      set: function(idx) {
        $scope.spotsSlide.currentIdx = idx;
      }
    };

    $scope.spotsGroup = [
      [{
          id: 1,
          thumbnail: '../public/images/spots/spot-1.jpg',
          title: "台北101大樓",
          descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
          rank: 4.3,
          istripadv: false,
          price: 120,
          types: ["商圈"],
          author: "奧革士",
          isCollect: false,
          location: "台北",
          collectCount: 234,
          location2: {
            lat: 25.0339031,
            lng: 121.5645098
          }
        },
        {
          id: 2,
          title: "國立故宮博物院",
          thumbnail: '../public/images/spots/spot-2.jpg',
          descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，園",
          rank: 4.7,
          istripadv: false,
          price: 0,
          types: ["藝術文化"],
          author: "奧革士",
          isCollect: true,
          location: "台北",
          collectCount: 234,
          location2: {
            lat: 25.1011729,
            lng: 121.5487908
          }
        },
        {
          id: 3,
          title: "西門町",
          thumbnail: '../public/images/spots/spot-3.jpg',
          descript: "西門町位於台北市萬華區，由武昌街二段、漢中街、峨嵋街的行人徒步區所形成的區域為西門町的黃金地帶。大台北地區來往公車的密集交界，流行元素走在前端，是許多年輕學子及各國觀光客朝聖之處。",
          rank: 4.7,
          istripadv: false,
          price: 0,
          types: ["藝術文化", "自然生態"],
          author: "奧革士",
          isCollect: false,
          location: "台北",
          collectCount: 234,
          location2: {
            lat: 25.0451181,
            lng: 121.5076305
          }
        },
        {
          id: 4,
          title: "中正紀念堂",
          thumbnail: '../public/images/spots/spot-4.jpg',
          descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式，大廳中央放置蔣介石坐姿銅像，底座部份設有展覽室和放映室，陳列蔣介石的一些文物，供民眾參觀。",
          rank: 3.7,
          istripadv: true,
          price: 120,
          types: ["藝術文化", "自然生態"],
          author: "奧革士",
          isCollect: false,
          location: "台北",
          collectCount: 234,
          location2: {
            lat: 25.034731,
            lng: 121.521934
          }
        },
        {
          id: 5,
          title: "九份老街",
          thumbnail: '../public/images/spots/spot-5.jpg',
          descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
          rank: 3.7,
          istripadv: true,
          price: 0,
          types: ["藝術文化", "自然生態"],
          author: "奧革士",
          isCollect: false,
          location: "台北",
          collectCount: 234,
          location2: {
            lat: 25.108158,
            lng: 121.8437955
          }
        },
        {
          id: 6,
          title: "鵝鑾鼻公園",
          thumbnail: '../public/images/spots/spot-6.jpg',
          descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，",
          rank: 3.7,
          istripadv: true,
          price: 120,
          types: ["藝術文化", "自然生態"],
          author: "奧革士",
          isCollect: false,
          location: "台北",
          collectCount: 234,
          location2: {
            lat: 21.9020441,
            lng: 120.8529676
          }
        },
        {
          id: 7,
          title: "高雄捷運美麗島站",
          thumbnail: '../public/images/spots/spot-7.jpg',
          descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
          rank: 4.7,
          istripadv: false,
          price: 0,
          types: ["藝術文化", "自然生態"],
          author: "奧革士",
          isCollect: false,
          location: "台北",
          collectCount: 234,
          location2: {
            lat: 22.631386,
            lng: 120.301951
          }
        },
        {
          id: 8,
          title: "野柳地質公園",
          thumbnail: '../public/images/spots/spot-8.jpg',
          descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
          rank: 4.7,
          istripadv: false,
          price: 120,
          types: ["藝術文化", "自然生態"],
          author: "奧革士",
          isCollect: false,
          location: "台北",
          collectCount: 234,
          location2: {
            lat: 25.2072029,
            lng: 121.690895
          }
        }
      ],
      [{
          id: 9,
          title: "台北101大樓",
          thumbnail: '../public/images/spots/spot-1.jpg',
          descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
          rank: 4,
          price: 120,
          types: ["藝術文化", "自然生態"],
          author: "奧革士",
          isCollect: false,
          location: "台北",
          collectCount: 234,
          location2: {
            lat: 25.0339031,
            lng: 121.5645098
          }
        },
        {
          id: 10,
          title: "國立故宮博物院",
          thumbnail: '../public/images/spots/spot-2.jpg',
          descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，園",
          rank: 4,
          price: 120,
          types: ["藝術文化", "自然生態"],
          author: "奧革士",
          isCollect: true,
          location: "台北",
          collectCount: 234,
          location2: {
            lat: 25.1011729,
            lng: 121.5487908
          }
        },
        {
          id: 11,
          title: "西門町",
          thumbnail: '../public/images/spots/spot-3.jpg',
          descript: "西門町位於台北市萬華區，由武昌街二段、漢中街、峨嵋街的行人徒步區所形成的區域為西門町的黃金地帶。大台北地區來往公車的密集交界，流行元素走在前端，是許多年輕學子及各國觀光客朝聖之處。",
          rank: 4,
          price: 120,
          types: ["藝術文化", "自然生態"],
          author: "奧革士",
          isCollect: false,
          location: "台北",
          collectCount: 234,
          location2: {
            lat: 25.0451181,
            lng: 121.5076305
          }
        },
        {
          id: 12,
          title: "中正紀念堂",
          thumbnail: '../public/images/spots/spot-4.jpg',
          descript: "中正紀念堂和自由廣場也是旅行者必到的地方。這是一整個園區，包括中正紀念堂、音樂廳和戲劇院，還有外圍的公園。中正紀念堂是為紀念蔣介石而建造的紀念堂，造型仿北京天壇頂部造型和埃及金字塔的主體建築樣式，大廳中央放置蔣介石坐姿銅像，底座部份設有展覽室和放映室，陳列蔣介石的一些文物，供民眾參觀。",
          rank: 4,
          price: 120,
          types: ["藝術文化", "自然生態"],
          author: "奧革士",
          isCollect: false,
          location: "台北",
          collectCount: 234,
          location2: {
            lat: 25.034731,
            lng: 121.521934
          }
        },
        {
          id: 13,
          title: "九份老街",
          thumbnail: '../public/images/spots/spot-5.jpg',
          descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
          rank: 4,
          price: 120,
          types: ["藝術文化", "自然生態"],
          author: "奧革士",
          isCollect: false,
          location: "台北",
          collectCount: 234,
          location2: {
            lat: 25.108158,
            lng: 121.8437955
          }
        },
        {
          id: 14,
          title: "鵝鑾鼻公園",
          thumbnail: '../public/images/spots/spot-6.jpg',
          descript: "台北故宮博物院完全能讓你在裡面慢慢看上一整天，藏品豐富可與北京故宮博物院媲美，主要以宋、元、明、清四朝為主，幾乎涵蓋五千年的中國歷史，數量達65萬5千多件，擁有“中華文化寶庫”的美名。除了館內的展品可參觀以外，",
          rank: 4,
          price: 120,
          types: ["藝術文化", "自然生態"],
          author: "奧革士",
          isCollect: false,
          location: "台北",
          collectCount: 234,
          location2: {
            lat: 21.9020441,
            lng: 120.8529676
          }
        },
        {
          id: 15,
          title: "高雄捷運美麗島站",
          thumbnail: '../public/images/spots/spot-7.jpg',
          descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
          rank: 4,
          price: 0,
          types: ["藝術文化", "自然生態"],
          author: "奧革士",
          isCollect: false,
          location: "台北",
          collectCount: 234,
          location2: {
            lat: 22.631386,
            lng: 120.301951
          }
        },
        {
          id: 15,
          title: "高雄捷運美麗島站",
          thumbnail: '../public/images/spots/spot-7.jpg',
          descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
          rank: 4,
          price: 0,
          types: ["藝術文化", "自然生態"],
          author: "奧革士",
          isCollect: false,
          location: "台北",
          collectCount: 234,
          location2: {
            lat: 22.631386,
            lng: 120.301951
          }
        }
      ]
    ];

    $scope.foodsSlide = {
      currentIdx: 0,
      prev: function() {
        $scope.foodsSlide.currentIdx = $scope.foodsSlide.currentIdx > 0 ? $scope.foodsSlide.currentIdx - 1 : $scope.foodsGroup.length - 1;
      },
      next: function() {
        $scope.foodsSlide.currentIdx = $scope.foodsSlide.currentIdx < ($scope.foodsGroup.length - 1) ? $scope.foodsSlide.currentIdx + 1 : 0;
      },
      set: function(idx) {
        $scope.foodsSlide.currentIdx = idx;
      }
    };

    $scope.foodsGroup = [
      [{
          title: "洋旗牛排餐廳1",
          descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
          rank: 4,
          istripadv: false,
          price: 200,
          types: ["中式美食", "夜市小吃"],
          address: "台北市大同區長安西路33號",
          date: "2014年12月17日",
          author: "奧革士",
          isCollect: false,
          location: "台北",
          thumbnail: '../public/images/foods/food-1.jpg',
          collectCount: 234
        },
        {
          title: "洋旗牛排餐廳2",
          descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
          rank: 5,
          istripadv: true,
          price: 70,
          types: ["中式美食", "夜市小吃"],
          address: "台北市大同區長安西路33號",
          date: "2014年12月17日",
          author: "奧革士",
          isCollect: false,
          location: "台北",
          thumbnail: '../public/images/foods/food-2.jpg',
          collectCount: 234
        },
        {
          title: "洋旗牛排餐廳",
          descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
          rank: 2,
          istripadv: true,
          price: 250,
          types: ["中式美食", "夜市小吃"],
          address: "台北市大同區長安西路33號",
          date: "2014年12月17日",
          author: "奧革士",
          isCollect: false,
          location: "台北",
          thumbnail: '../public/images/foods/food-3.jpg',
          collectCount: 234
        },
        {
          title: "洋旗牛排餐廳",
          descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
          rank: 4,
          istripadv: false,
          price: 120,
          types: ["中式美食", "夜市小吃"],
          address: "台北市大同區長安西路33號",
          date: "2014年12月17日",
          author: "奧革士",
          isCollect: false,
          location: "台北",
          thumbnail: '../public/images/foods/food-4.jpg',
          collectCount: 234
        },
        {
          title: "洋旗牛排餐廳",
          descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
          rank: 4,
          istripadv: false,
          price: 750,
          types: ["中式美食", "夜市小吃"],
          address: "台北市大同區長安西路33號",
          date: "2014年12月17日",
          author: "奧革士",
          isCollect: false,
          location: "台北",
          thumbnail: '../public/images/foods/food-5.jpg',
          collectCount: 234
        },
        {
          title: "洋旗牛排餐廳",
          descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
          rank: 4,
          istripadv: false,
          price: 300,
          types: ["中式美食", "夜市小吃"],
          address: "台北市大同區長安西路33號",
          date: "2014年12月17日",
          author: "奧革士",
          isCollect: false,
          location: "台北",
          thumbnail: '../public/images/foods/food-6.jpg',
          collectCount: 234
        },
        {
          title: "洋旗牛排餐廳",
          descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
          rank: 4,
          istripadv: false,
          price: 250,
          types: ["中式美食", "夜市小吃"],
          address: "台北市大同區長安西路33號",
          date: "2014年12月17日",
          author: "奧革士",
          isCollect: false,
          location: "台北",
          thumbnail: '../public/images/foods/food-7.jpg',
          collectCount: 234
        },
        {
          title: "洋旗牛排餐廳",
          descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
          rank: 4,
          istripadv: false,
          price: 100,
          types: ["中式美食", "夜市小吃"],
          address: "台北市大同區長安西路33號",
          date: "2014年12月17日",
          author: "奧革士",
          isCollect: false,
          location: "台北",
          thumbnail: '../public/images/foods/food-8.jpg',
          collectCount: 234
        }
      ],
      [{
          title: "洋旗牛排餐廳",
          descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
          rank: 4,
          istripadv: false,
          price: 200,
          types: ["中式美食", "夜市小吃"],
          address: "台北市大同區長安西路33號",
          date: "2014年12月17日",
          author: "奧革士",
          isCollect: false,
          location: "台北",
          thumbnail: '../public/images/foods/food-1.jpg',
          collectCount: 234
        },
        {
          title: "洋旗牛排餐廳",
          descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
          rank: 4,
          istripadv: false,
          price: 70,
          types: ["中式美食", "夜市小吃"],
          address: "台北市大同區長安西路33號",
          date: "2014年12月17日",
          author: "奧革士",
          isCollect: false,
          location: "台北",
          thumbnail: '../public/images/foods/food-2.jpg',
          collectCount: 234
        },
        {
          title: "洋旗牛排餐廳",
          descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
          rank: 4,
          istripadv: false,
          price: 250,
          types: ["中式美食", "夜市小吃"],
          address: "台北市大同區長安西路33號",
          date: "2014年12月17日",
          author: "奧革士",
          isCollect: false,
          location: "台北",
          thumbnail: '../public/images/foods/food-3.jpg',
          collectCount: 234
        },
        {
          title: "洋旗牛排餐廳",
          descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
          rank: 4,
          istripadv: false,
          price: 120,
          types: ["中式美食", "夜市小吃"],
          address: "台北市大同區長安西路33號",
          date: "2014年12月17日",
          author: "奧革士",
          isCollect: false,
          location: "台北",
          thumbnail: '../public/images/foods/food-4.jpg',
          collectCount: 234
        },
        {
          title: "洋旗牛排餐廳",
          descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
          rank: 4,
          istripadv: false,
          price: 750,
          types: ["中式美食", "夜市小吃"],
          address: "台北市大同區長安西路33號",
          date: "2014年12月17日",
          author: "奧革士",
          isCollect: false,
          location: "台北",
          thumbnail: '../public/images/foods/food-5.jpg',
          collectCount: 234
        },
        {
          title: "洋旗牛排餐廳",
          descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
          rank: 4,
          istripadv: false,
          price: 300,
          types: ["中式美食", "夜市小吃"],
          address: "台北市大同區長安西路33號",
          date: "2014年12月17日",
          author: "奧革士",
          isCollect: false,
          location: "台北",
          thumbnail: '../public/images/foods/food-6.jpg',
          collectCount: 234
        },
        {
          title: "洋旗牛排餐廳",
          descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
          rank: 4,
          istripadv: false,
          price: 250,
          types: ["中式美食", "夜市小吃"],
          address: "台北市大同區長安西路33號",
          date: "2014年12月17日",
          author: "奧革士",
          isCollect: false,
          location: "台北",
          thumbnail: '../public/images/foods/food-7.jpg',
          collectCount: 234
        },
        {
          title: "洋旗牛排餐廳",
          descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
          rank: 4,
          istripadv: false,
          price: 100,
          types: ["中式美食", "夜市小吃"],
          address: "台北市大同區長安西路33號",
          date: "2014年12月17日",
          author: "奧革士",
          isCollect: false,
          location: "台北",
          thumbnail: '../public/images/foods/food-8.jpg',
          collectCount: 234
        }
      ]
    ];

    $scope.staysSlide = {
      currentIdx: 0,
      prev: function() {
        $scope.staysSlide.currentIdx = $scope.staysSlide.currentIdx > 0 ? $scope.staysSlide.currentIdx - 1 : $scope.staysGroup.length - 1;
      },
      next: function() {
        $scope.staysSlide.currentIdx = $scope.staysSlide.currentIdx < ($scope.staysGroup.length - 1) ? $scope.staysSlide.currentIdx + 1 : 0;
      },
      set: function(idx) {
        $scope.staysSlide.currentIdx = idx;
      }
    };

    $scope.staysGroup = [
      [{
          title: "金瓜石 月河民宿 鄉村雙人雅房",
          descript: "紅米青年旅館座落於台北火車站附近，位於高鐵、火車、巴士和捷運交會點。旅館的設計年輕有活力，大片的景觀窗戶搭配著繽紛色彩的交誼廳，讓您在高樓眺望台北的",
          rank: 3.7,
          istripadv: true,
          price: 875,
          type: "民宿",
          address: "台北市大同區長安西路33號",
          author: "奧革士",
          isCollect: false,
          location: "台北",
          thumbnail: '../public/images/stays/stay-1.jpg',
          collectCount: 234
        },
        {
          title: "九份老石屋_藍海房",
          descript: "紅米青年旅館座落於台北火車站附近，位於高鐵、火車、巴士和捷運交會點。旅館的設計年輕有活力，大片的景觀窗戶搭配著繽紛色彩的交誼廳，讓您在高樓眺望台北的",
          rank: 3.7,
          istripadv: true,
          price: 875,
          type: "民宿",
          address: "台北市大同區長安西路33號",
          author: "奧革士",
          isCollect: true,
          location: "台北",
          thumbnail: '../public/images/stays/stay-2.jpg',
          collectCount: 234
        },
        {
          title: "療癒系公寓 乾淨又舒適的溫暖空間",
          descript: "紅米青年旅館座落於台北火車站附近，位於高鐵、火車、巴士和捷運交會點。旅館的設計年輕有活力，大片的景觀窗戶搭配著繽紛色彩的交誼廳，讓您在高樓眺望台北的",
          rank: 3.7,
          istripadv: true,
          price: 875,
          type: "民宿",
          address: "台北市大同區長安西路33號",
          author: "奧革士",
          isCollect: false,
          location: "台北",
          thumbnail: '../public/images/stays/stay-3.jpg',
          collectCount: 234
        },
        {
          title: "日月潭旅人小屋",
          descript: "紅米青年旅館座落於台北火車站附近，位於高鐵、火車、巴士和捷運交會點。旅館的設計年輕有活力，大片的景觀窗戶搭配著繽紛色彩的交誼廳，讓您在高樓眺望台北的",
          rank: 4,
          price: 875,
          type: "民宿",
          address: "台北市大同區長安西路33號",
          author: "奧革士",
          isCollect: false,
          location: "台北",
          thumbnail: '../public/images/stays/stay-4.jpg',
          collectCount: 234
        },
        {
          title: "Woo House 典雅雙人房",
          descript: "紅米青年旅館座落於台北火車站附近，位於高鐵、火車、巴士和捷運交會點。旅館的設計年輕有活力，大片的景觀窗戶搭配著繽紛色彩的交誼廳，讓您在高樓眺望台北的",
          rank: 4.7,
          istripadv: false,
          price: 875,
          type: "民宿",
          address: "台北市大同區長安西路33號",
          author: "奧革士",
          isCollect: false,
          location: "台北",
          thumbnail: '../public/images/stays/stay-5.jpg',
          collectCount: 234
        },
        {
          title: "日月潭旅人小屋",
          descript: "紅米青年旅館座落於台北火車站附近，位於高鐵、火車、巴士和捷運交會點。旅館的設計年輕有活力，大片的景觀窗戶搭配著繽紛色彩的交誼廳，讓您在高樓眺望台北的",
          rank: 4.7,
          istripadv: false,
          price: 875,
          type: "民宿",
          address: "台北市大同區長安西路33號",
          author: "奧革士",
          isCollect: false,
          location: "台北",
          thumbnail: '../public/images/stays/stay-6.jpg',
          collectCount: 234
        },
        {
          title: "紅米國際青年旅館",
          descript: "紅米青年旅館座落於台北火車站附近，位於高鐵、火車、巴士和捷運交會點。旅館的設計年輕有活力，大片的景觀窗戶搭配著繽紛色彩的交誼廳，讓您在高樓眺望台北的",
          rank: 4.7,
          istripadv: false,
          price: 875,
          type: "民宿",
          address: "台北市大同區長安西路33號",
          author: "奧革士",
          isCollect: false,
          location: "台北",
          thumbnail: '../public/images/stays/stay-7.jpg',
          collectCount: 234
        },
        {
          title: "群悅青年旅館",
          descript: "紅米青年旅館座落於台北火車站附近，位於高鐵、火車、巴士和捷運交會點。旅館的設計年輕有活力，大片的景觀窗戶搭配著繽紛色彩的交誼廳，讓您在高樓眺望台北的",
          rank: 4.7,
          istripadv: false,
          price: 875,
          type: "民宿",
          address: "台北市大同區長安西路33號",
          author: "奧革士",
          isCollect: false,
          location: "台北",
          thumbnail: '../public/images/stays/stay-8.jpg',
          collectCount: 234
        }
      ],
      [{
          title: "金瓜石 月河民宿 鄉村雙人雅房",
          descript: "紅米青年旅館座落於台北火車站附近，位於高鐵、火車、巴士和捷運交會點。旅館的設計年輕有活力，大片的景觀窗戶搭配著繽紛色彩的交誼廳，讓您在高樓眺望台北的",
          rank: 4.7,
          istripadv: false,
          price: 875,
          type: "民宿",
          address: "台北市大同區長安西路33號",
          author: "奧革士",
          isCollect: false,
          location: "台北",
          thumbnail: '../public/images/stays/stay-1.jpg',
          collectCount: 234
        },
        {
          title: "九份老石屋_藍海房",
          descript: "紅米青年旅館座落於台北火車站附近，位於高鐵、火車、巴士和捷運交會點。旅館的設計年輕有活力，大片的景觀窗戶搭配著繽紛色彩的交誼廳，讓您在高樓眺望台北的",
          rank: 4.7,
          istripadv: false,
          price: 875,
          type: "民宿",
          address: "台北市大同區長安西路33號",
          author: "奧革士",
          isCollect: true,
          location: "台北",
          thumbnail: '../public/images/stays/stay-2.jpg',
          collectCount: 234
        },
        {
          title: "療癒系公寓 乾淨又舒適的溫暖空間",
          descript: "紅米青年旅館座落於台北火車站附近，位於高鐵、火車、巴士和捷運交會點。旅館的設計年輕有活力，大片的景觀窗戶搭配著繽紛色彩的交誼廳，讓您在高樓眺望台北的",
          rank: 4.7,
          istripadv: false,
          price: 875,
          type: "民宿",
          address: "台北市大同區長安西路33號",
          author: "奧革士",
          isCollect: false,
          location: "台北",
          thumbnail: '../public/images/stays/stay-3.jpg',
          collectCount: 234
        },
        {
          title: "日月潭旅人小屋",
          descript: "紅米青年旅館座落於台北火車站附近，位於高鐵、火車、巴士和捷運交會點。旅館的設計年輕有活力，大片的景觀窗戶搭配著繽紛色彩的交誼廳，讓您在高樓眺望台北的",
          rank: 4.7,
          istripadv: false,
          price: 875,
          type: "民宿",
          address: "台北市大同區長安西路33號",
          author: "奧革士",
          isCollect: false,
          location: "台北",
          thumbnail: '../public/images/stays/stay-4.jpg',
          collectCount: 234
        },
        {
          title: "Woo House 典雅雙人房",
          descript: "紅米青年旅館座落於台北火車站附近，位於高鐵、火車、巴士和捷運交會點。旅館的設計年輕有活力，大片的景觀窗戶搭配著繽紛色彩的交誼廳，讓您在高樓眺望台北的",
          rank: 4,
          price: 875,
          type: "民宿",
          address: "台北市大同區長安西路33號",
          author: "奧革士",
          isCollect: false,
          location: "台北",
          thumbnail: '../public/images/stays/stay-5.jpg',
          collectCount: 234
        },
        {
          title: "日月潭旅人小屋",
          descript: "紅米青年旅館座落於台北火車站附近，位於高鐵、火車、巴士和捷運交會點。旅館的設計年輕有活力，大片的景觀窗戶搭配著繽紛色彩的交誼廳，讓您在高樓眺望台北的",
          rank: 4,
          price: 875,
          type: "民宿",
          address: "台北市大同區長安西路33號",
          author: "奧革士",
          isCollect: false,
          location: "台北",
          thumbnail: '../public/images/stays/stay-6.jpg',
          collectCount: 234
        },
        {
          title: "紅米國際青年旅館",
          descript: "紅米青年旅館座落於台北火車站附近，位於高鐵、火車、巴士和捷運交會點。旅館的設計年輕有活力，大片的景觀窗戶搭配著繽紛色彩的交誼廳，讓您在高樓眺望台北的",
          rank: 4,
          price: 875,
          type: "民宿",
          address: "台北市大同區長安西路33號",
          author: "奧革士",
          isCollect: false,
          location: "台北",
          thumbnail: '../public/images/stays/stay-7.jpg',
          collectCount: 234
        },
        {
          title: "群悅青年旅館",
          descript: "紅米青年旅館座落於台北火車站附近，位於高鐵、火車、巴士和捷運交會點。旅館的設計年輕有活力，大片的景觀窗戶搭配著繽紛色彩的交誼廳，讓您在高樓眺望台北的",
          rank: 4,
          price: 875,
          type: "民宿",
          address: "台北市大同區長安西路33號",
          author: "奧革士",
          isCollect: false,
          location: "台北",
          thumbnail: '../public/images/stays/stay-8.jpg',
          collectCount: 234
        }
      ]
    ];


    $scope.eventsSlide = {
      currentIdx: 0,
      prev: function() {
        $scope.eventsSlide.currentIdx = $scope.eventsSlide.currentIdx > 0 ? $scope.eventsSlide.currentIdx - 1 : $scope.eventsGroup.length - 1;
      },
      next: function() {
        $scope.eventsSlide.currentIdx = $scope.eventsSlide.currentIdx < ($scope.eventsGroup.length - 1) ? $scope.eventsSlide.currentIdx + 1 : 0;
      },
      set: function(idx) {
        $scope.eventsSlide.currentIdx = idx;
      }
    };

    $scope.eventsGroup = [
      [{
          thumbnail: '../public/images/events/1.jpg',
          title: "三義國際木雕藝術節",
          descript: "三義鄉位於苗栗縣南端，隔大安溪與臺中市后里區相鄰，東北隔三角山（567公尺）和銅鑼鄉新隆、興隆兩村毗連，東面以關刀山脈為天然界線，與大湖、卓蘭比鄰而立，西側則以火炎山脈與苑裡、通霄相接，北則與銅鑼鄉接壤。三義鄉因位在臺灣南北氣候之分界上，因此天氣複雜多變，尤以冬、春二季更為顯著，常有「四時皆夏、一雨成秋」之感；每年11月至翌年3月，濛濛白霧繚繞，整鄉彷彿墜入縹緲雲海，宛如人間仙境，故有「臺",
          rank: 3,
          price: 0,
          type: "節慶活動",
          address: "台北市大同區長安西路33號",
          startDate: '2015/04/01',
          endDate: '2015/06/30',
          expired: false,
          author: "奧革士",
          isCollect: false,
          tags: ['嚴選活動', '苗栗'],
          collectCount: 237
        },
        {
          thumbnail: '../public/images/events/2.jpg',
          title: "臺中爵士音樂節",
          descript: "臺中爵士音樂節（Taichung Jazz Festival）是臺中市每年10月舉辦的爵士樂戶外活動，從2002年開始成為台中閃亮文化季下的活動之一[1]；隨著逐漸培養出參與爵士樂人口，2009年開始從文化季中獨立出來舉辦。音樂節起初只為期三天，2009年已擴大到舉辦十天，2010年年後至今都已固定舉辦九天。",
          rank: 3,
          price: 120,
          type: "文藝活動",
          address: "台北市大同區長安西路33號",
          startDate: '2015/04/01',
          endDate: '2015/06/30',
          expired: false,
          author: "奧革士",
          isCollect: false,
          tags: ['台中'],
          collectCount: 237
        },
        {
          thumbnail: '../public/images/events/3.jpg',
          title: "太魯閣峽谷音樂節活動",
          descript: "大家引頸期盼的「太魯閣峽谷音樂節」將於9月20日在清水斷崖及9月27日於長春祠舉行。碧藍的太平洋、廣闊的沙灘和壯麗的清水斷崖，古典雅致的長春祠搭配飛瀑流洩而下，坐在溪畔聆聽音樂家現場演奏，令許多曾參加過的觀眾印象深刻、回味無窮。今年的太魯閣峽谷音樂節，由國內最知名的創意大提琴家張正傑教授擔任藝術總監，將以「擊鼓弦歌」為主題，帶來有別以往的震憾。 ",
          rank: 3,
          price: 120,
          type: "年度活動",
          address: "台北市大同區長安西路33號",
          startDate: '2015/04/01',
          endDate: '2015/06/30',
          expired: false,
          author: "奧革士",
          isCollect: false,
          tags: ['花蓮'],
          collectCount: 237
        },
        {
          thumbnail: '../public/images/events/4.jpg',
          title: "花蓮金針花季",
          descript: "能看到一大片金針花海， 是一種幸福喔！ 今年夏季期間花蓮赤科山、六十石山， 持續透過留花政策推動，來創造較大最大面積金針花海。",
          rank: 3,
          price: 0,
          type: "四季活動",
          address: "台北市大同區長安西路33號",
          startDate: '2015/04/01',
          endDate: '2015/06/30',
          expired: false,
          author: "奧革士",
          isCollect: false,
          tags: ['花蓮'],
          collectCount: 237
        },
        {
          thumbnail: '../public/images/events/5.jpg',
          title: "雲林國際偶戲節",
          descript: "臺灣有所謂的偶戲三寶(皮影戲、布袋戲、傀儡戲)，自傳入臺灣以來，結合了本土風俗及信仰，早已發展成為獨特的臺灣偶戲藝術。隨著時間洪流，不論是在師傅手中活靈活現的表演戲偶、一旁樂師現場臨急應變的配樂演奏、或是一代替換過一代的臺下觀眾，乃至偶戲表演的目的從酬神到娛人，偶戲藝術所代表的意義也逐漸地在轉變當中。 「雲林國際偶戲藝術節」成立於1999年，雲林縣是臺灣布袋戲的故鄉，擁有許多派別，「雲林國際偶戲節」的成立，旨在落實文化立縣，發揚傳統精緻藝術，促進國際文化交流。活動除了邀請國際偶戲團隊演出、舉行金掌獎競技、經典與創新布袋戲團競演；同時結合文物藝術展、文創展及農特產「偶」裝置藝術，積極將「布袋戲」文化透過推廣與刺激、傳習與體驗等展演活動，使布袋戲偶戲文化得以永續推廣及傳承。",
          rank: 3,
          price: 120,
          type: "文藝活動",
          address: "台北市大同區長安西路33號",
          startDate: '2015/04/01',
          endDate: '2015/06/30',
          expired: false,
          author: "奧革士",
          isCollect: false,
          tags: ['嚴選活動', '新北'],
          collectCount: 237
        },
        {
          thumbnail: '../public/images/events/1.jpg',
          title: "花蓮國際石雕藝術季",
          descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
          rank: 3,
          price: 120,
          type: "文藝活動",
          address: "台北市大同區長安西路33號",
          startDate: '2015/04/01',
          endDate: '2015/06/30',
          expired: false,
          author: "奧革士",
          isCollect: false,
          tags: ['嚴選活動', '高雄'],
          collectCount: 237
        },
        {
          thumbnail: '../public/images/events/2.jpg',
          title: "西門町後街文化祭",
          descript: "自2010年後街再起、2011年後勁十足、2012年Hold街一起玩，2013年Fun你的爽，樂在其中! 今年即將邁向五週年的後街文化祭，激盪出最純真的街頭「色」彩，一路從暑假瘋到年底!",
          rank: 3,
          price: 120,
          type: "文藝活動",
          address: "台北市大同區長安西路33號",
          startDate: '2015/03/18',
          endDate: '2015/05/17',
          expired: true,
          author: "奧革士",
          isCollect: false,
          tags: ['嚴選活動', '高雄'],
          collectCount: 237
        },
        {
          thumbnail: '../public/images/events/3.jpg',
          title: "華山藝術生活節",
          descript: "華山藝術生活節從10月10日開始到11月4日止，邀請200個各種表演團體進行演出，成為一個大小朋友都適合的熱鬧周末休閒，也讓民眾體驗舞蹈與戲劇表演的美好。4日最後一場表演，為金枝演社在焦點劇場演出的「幸福大丈夫」。",
          rank: 3,
          price: 0,
          type: "文化藝術",
          address: "台北市大同區長安西路33號",
          startDate: '2015/04/01',
          endDate: '2015/06/30',
          expired: true,
          author: "奧革士",
          isCollect: false,
          tags: ['嚴選活動', '台北'],
          collectCount: 237
        }
      ],
      [{
          thumbnail: '../public/images/events/1.jpg',
          title: "三義國際木雕藝術節",
          descript: "三義鄉位於苗栗縣南端，隔大安溪與臺中市后里區相鄰，東北隔三角山（567公尺）和銅鑼鄉新隆、興隆兩村毗連，東面以關刀山脈為天然界線，與大湖、卓蘭比鄰而立，西側則以火炎山脈與苑裡、通霄相接，北則與銅鑼鄉接壤。三義鄉因位在臺灣南北氣候之分界上，因此天氣複雜多變，尤以冬、春二季更為顯著，常有「四時皆夏、一雨成秋」之感；每年11月至翌年3月，濛濛白霧繚繞，整鄉彷彿墜入縹緲雲海，宛如人間仙境，故有「臺",
          rank: 3,
          price: 0,
          type: "節慶活動",
          address: "台北市大同區長安西路33號",
          startDate: '2015/04/01',
          endDate: '2015/06/30',
          expired: false,
          author: "奧革士",
          isCollect: false,
          tags: ['嚴選活動', '苗栗'],
          collectCount: 237
        },
        {
          thumbnail: '../public/images/events/2.jpg',
          title: "臺中爵士音樂節",
          descript: "臺中爵士音樂節（Taichung Jazz Festival）是臺中市每年10月舉辦的爵士樂戶外活動，從2002年開始成為台中閃亮文化季下的活動之一[1]；隨著逐漸培養出參與爵士樂人口，2009年開始從文化季中獨立出來舉辦。音樂節起初只為期三天，2009年已擴大到舉辦十天，2010年年後至今都已固定舉辦九天。",
          rank: 3,
          price: 120,
          type: "文藝活動",
          address: "台北市大同區長安西路33號",
          startDate: '2015/04/01',
          endDate: '2015/06/30',
          expired: false,
          author: "奧革士",
          isCollect: false,
          tags: ['台中'],
          collectCount: 237
        },
        {
          thumbnail: '../public/images/events/3.jpg',
          title: "太魯閣峽谷音樂節活動",
          descript: "大家引頸期盼的「太魯閣峽谷音樂節」將於9月20日在清水斷崖及9月27日於長春祠舉行。碧藍的太平洋、廣闊的沙灘和壯麗的清水斷崖，古典雅致的長春祠搭配飛瀑流洩而下，坐在溪畔聆聽音樂家現場演奏，令許多曾參加過的觀眾印象深刻、回味無窮。今年的太魯閣峽谷音樂節，由國內最知名的創意大提琴家張正傑教授擔任藝術總監，將以「擊鼓弦歌」為主題，帶來有別以往的震憾。 ",
          rank: 3,
          price: 120,
          type: "年度活動",
          address: "台北市大同區長安西路33號",
          startDate: '2015/04/01',
          endDate: '2015/06/30',
          expired: false,
          author: "奧革士",
          isCollect: false,
          tags: ['花蓮'],
          collectCount: 237
        },
        {
          thumbnail: '../public/images/events/4.jpg',
          title: "花蓮金針花季",
          descript: "能看到一大片金針花海， 是一種幸福喔！ 今年夏季期間花蓮赤科山、六十石山， 持續透過留花政策推動，來創造較大最大面積金針花海。",
          rank: 3,
          price: 0,
          type: "四季活動",
          address: "台北市大同區長安西路33號",
          startDate: '2015/04/01',
          endDate: '2015/06/30',
          expired: false,
          author: "奧革士",
          isCollect: false,
          tags: ['花蓮'],
          collectCount: 237
        },
        {
          thumbnail: '../public/images/events/5.jpg',
          title: "雲林國際偶戲節",
          descript: "臺灣有所謂的偶戲三寶(皮影戲、布袋戲、傀儡戲)，自傳入臺灣以來，結合了本土風俗及信仰，早已發展成為獨特的臺灣偶戲藝術。隨著時間洪流，不論是在師傅手中活靈活現的表演戲偶、一旁樂師現場臨急應變的配樂演奏、或是一代替換過一代的臺下觀眾，乃至偶戲表演的目的從酬神到娛人，偶戲藝術所代表的意義也逐漸地在轉變當中。 「雲林國際偶戲藝術節」成立於1999年，雲林縣是臺灣布袋戲的故鄉，擁有許多派別，「雲林國際偶戲節」的成立，旨在落實文化立縣，發揚傳統精緻藝術，促進國際文化交流。活動除了邀請國際偶戲團隊演出、舉行金掌獎競技、經典與創新布袋戲團競演；同時結合文物藝術展、文創展及農特產「偶」裝置藝術，積極將「布袋戲」文化透過推廣與刺激、傳習與體驗等展演活動，使布袋戲偶戲文化得以永續推廣及傳承。",
          rank: 3,
          price: 120,
          type: "文藝活動",
          address: "台北市大同區長安西路33號",
          startDate: '2015/04/01',
          endDate: '2015/06/30',
          expired: false,
          author: "奧革士",
          isCollect: false,
          tags: ['嚴選活動', '新北'],
          collectCount: 237
        },
        {
          thumbnail: '../public/images/events/1.jpg',
          title: "花蓮國際石雕藝術季",
          descript: "這座曾經是世界第一高的大樓，如圖騰般大量出現在有關台灣的一切宣傳物上，台北地標之一，總高度達508米，結合證券、金融、商業活動與娛樂、生活、購物等用途，購物中心內規劃有時尚服飾區、名牌精品區、化妝品專櫃、書店等",
          rank: 3,
          price: 120,
          type: "文藝活動",
          address: "台北市大同區長安西路33號",
          startDate: '2015/04/01',
          endDate: '2015/06/30',
          expired: false,
          author: "奧革士",
          isCollect: false,
          tags: ['嚴選活動', '高雄'],
          collectCount: 237
        },
        {
          thumbnail: '../public/images/events/2.jpg',
          title: "西門町後街文化祭",
          descript: "自2010年後街再起、2011年後勁十足、2012年Hold街一起玩，2013年Fun你的爽，樂在其中! 今年即將邁向五週年的後街文化祭，激盪出最純真的街頭「色」彩，一路從暑假瘋到年底!",
          rank: 3,
          price: 120,
          type: "文藝活動",
          address: "台北市大同區長安西路33號",
          startDate: '2015/03/18',
          endDate: '2015/05/17',
          expired: true,
          author: "奧革士",
          isCollect: false,
          tags: ['嚴選活動', '高雄'],
          collectCount: 237
        },
        {
          thumbnail: '../public/images/events/3.jpg',
          title: "華山藝術生活節",
          descript: "華山藝術生活節從10月10日開始到11月4日止，邀請200個各種表演團體進行演出，成為一個大小朋友都適合的熱鬧周末休閒，也讓民眾體驗舞蹈與戲劇表演的美好。4日最後一場表演，為金枝演社在焦點劇場演出的「幸福大丈夫」。",
          rank: 3,
          price: 0,
          type: "文化藝術",
          address: "台北市大同區長安西路33號",
          startDate: '2015/04/01',
          endDate: '2015/06/30',
          expired: true,
          author: "奧革士",
          isCollect: false,
          tags: ['嚴選活動', '台北'],
          collectCount: 237
        }
      ]
    ];

    $scope.storesSlide = {
      currentIdx: 0,
      prev: function() {
        $scope.storesSlide.currentIdx = $scope.storesSlide.currentIdx > 0 ? $scope.storesSlide.currentIdx - 1 : $scope.storesGroup.length - 1;
      },
      next: function() {
        $scope.storesSlide.currentIdx = $scope.storesSlide.currentIdx < ($scope.storesGroup.length - 1) ? $scope.storesSlide.currentIdx + 1 : 0;
      },
      set: function(idx) {
        $scope.storesSlide.currentIdx = idx;
      }
    };

    $scope.storesGroup = [
      [{
          thumbnail: '../public/images/stores/1.png',
          title: "台客藍 - 直營店",
          descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
          rank: 3.7,
          price: 399,
          type: "異國料理、夜市小吃",
          address: "台北市大同區長安西路33號",
          author: "奧革士",
          isCollect: false,
          tags: ['必逛商店', '台北'],
          collectCount: 237
        },
        {
          thumbnail: '../public/images/stores/2.png',
          title: "阿原肥皂 - 九份店",
          descript: "阿原的經營想法「台灣四面環海與充滿山林的自然環境，蘊藏許多珍貴的青草藥，是老天爺賜給我們的自然良方。可惜除了老一輩的人知道善加利用之外，現代人已多半不識。」創辦人江榮原由於自身的過敏膚質，而著手自製手工肥皂，於2005年6月成立了阿原肥皂工作室。阿原肥皂秉持著「愛惜人身，將心比心」的初衷，製作肥皂的一切素材都從身邊開始，本持著天然使用。於是位於台灣山坡的魚腥草、田間的左手香以及民間常用的艾草等青草藥材等，都被研究成肥皂的材料。為了肥皂的能量與生命力，阿原肥皂嚴格排除了工業用的純水和過濾水，堅持從萬里山區接引天然湧泉來製作肥皂，唯有活水可以甚深的轉化細胞與洗滌身心。",
          rank: 3.7,
          price: 399,
          type: "異國料理、夜市小吃",
          address: "台北市大同區長安西路33號",
          author: "奧革士",
          isCollect: false,
          tags: ['新北'],
          collectCount: 237
        },
        {
          thumbnail: '../public/images/stores/3.png',
          title: "女巫店",
          descript: "位於台大商圈：公館也就是文藝青年的聚集之地，這裡有誠實坦率的獨立音樂唱片行，也有隻泡好咖啡，放好音樂的咖啡館，想讓精神放鬆，釋放自我的壓力，可以到這裡看非主流電影、主題書店、二手書舖當然，美食也是不可少的。",
          rank: 3.7,
          price: 399,
          type: "異國料理、夜市小吃",
          address: "台北市大同區長安西路33號",
          author: "奧革士",
          isCollect: false,
          tags: ['台北'],
          collectCount: 237
        },
        {
          thumbnail: '../public/images/stores/4.png',
          title: "朵兒咖啡館",
          descript: "一個湛藍色的大門，讓人期待起門後的世界 這個「故事」的由來，其實是一部仍未上檔的國片電影：由桂綸鎂所主演的「第36個故事」中的主要場景 原本只是在台北富錦街中一間荒廢的公寓1樓，如今在經過改裝之下，變成極富風情的咖啡館也是劇中女主角姊妹所經營的朵兒咖啡",
          rank: 4.7,
          price: 399,
          type: "異國料理、夜市小吃",
          address: "台北市大同區長安西路33號",
          author: "奧革士",
          isCollect: false,
          tags: ['台北'],
          collectCount: 237
        },
        {
          thumbnail: '../public/images/stores/5.png',
          title: "Déjà Vu音樂魔術主題餐廳",
          descript: "Deja Vu源自於法文“似曾相識”一意，由劉謙、周董和劉畊宏共同投資的中世紀風格主題餐廳哦！不要懷疑，雖說是名人餐廳，價格還可以接受的，而且更重要的是，每週六，有免費的表演看呢~強烈推薦，但一定要記得提前訂位。官網： http://www.deja-",
          rank: 3.7,
          price: 399,
          type: "異國料理、夜市小吃",
          address: "台北市大同區長安西路33號",
          author: "奧革士",
          isCollect: false,
          tags: ['必逛商店', '新北'],
          collectCount: 237
        },
        {
          thumbnail: '../public/images/stores/6.png',
          title: "袖珍博物館",
          descript: "各位縮微模型愛好者，各位miniature控，各位娃娃屋製作者，各位小清新、小文藝，民那~來台北千萬千萬千萬不要錯過一座館，不是故宮博物館啦，千萬不要錯過台北袖珍博物館！好好地表達一次：請一定要去袖珍博物館！",
          rank: 4.7,
          price: 399,
          type: "異國料理、夜市小吃",
          address: "台北市大同區長安西路33號",
          author: "奧革士",
          isCollect: false,
          tags: ['必逛商店', '高雄'],
          collectCount: 237
        },
        {
          thumbnail: '../public/images/stores/7.png',
          title: "美麗華百樂園",
          descript: "在台北市中山區、基隆河截彎取直的大彎段，美麗華百樂園是一間以全新概念商業設施的百貨公司，這裡最具特色的場景就是全台首創的百米摩天輪，結合娛樂、科技、藝術於一體，媲美日本的東京台場摩天輪，加之音樂旋轉木馬游樂設施更成為情侶約會的首選景點。夜間綻放光彩炫麗的燈光錶演，點綴台北的夜空。",
          rank: 3,
          price: 399,
          type: "異國料理、夜市小吃",
          address: "台北市大同區長安西路33號",
          author: "奧革士",
          isCollect: false,
          tags: ['屏東'],
          collectCount: 237
        },
        {
          thumbnail: '../public/images/stores/8.png',
          title: "京華城購物中心",
          descript: "京華城購物中心一座全新的國際級都心型觀光休閒購物中心，其獨一無二的建築設計，來自於中國傳說中的“雙龍抱珠”意念，首創“L”形主建築體，並結合全球最大球體的結構設計，直徑達58公尺的球體造型設計，基礎深達85公尺，以四根巨柱載重，為國內首創最深建築基礎結構，也成為京華城建築上的最大特色。",
          rank: 3,
          price: 399,
          type: "異國料理、夜市小吃",
          address: "台北市大同區長安西路33號",
          author: "奧革士",
          isCollect: false,
          tags: ['必逛商店', '台北'],
          collectCount: 237
        }
      ],
      [{
          thumbnail: '../public/images/stores/1.png',
          title: "台客藍 - 直營店",
          descript: "位於新北市蘆洲長興路上的石窯屋，置身在寧靜的住宅區中，卻突顯 出了濃濃異國味的義式餐廳，擁有挑高明亮的裝橫加上寬敞舒適的坐 位，在裡面用餐，彷彿如在義大利般地悠閒、愜意。 對於義式料理情有獨鐘的我們，希望能把最道地的義式口味帶回台 灣，雖然如此，卻也很注重吃的健康，店門口的各種香料都由自己親 自栽種而得，完全沒有農藥，堅持全程自已把關，包括麵糰、醬料， 甚至是食材採買，希望把最原始的美味呈現給你們~",
          rank: 4.7,
          price: 399,
          type: "異國料理、夜市小吃",
          address: "台北市大同區長安西路33號",
          author: "奧革士",
          isCollect: false,
          tags: ['必逛商店', '台北'],
          collectCount: 237
        },
        {
          thumbnail: '../public/images/stores/2.png',
          title: "阿原肥皂 - 九份店",
          descript: "阿原的經營想法「台灣四面環海與充滿山林的自然環境，蘊藏許多珍貴的青草藥，是老天爺賜給我們的自然良方。可惜除了老一輩的人知道善加利用之外，現代人已多半不識。」創辦人江榮原由於自身的過敏膚質，而著手自製手工肥皂，於2005年6月成立了阿原肥皂工作室。阿原肥皂秉持著「愛惜人身，將心比心」的初衷，製作肥皂的一切素材都從身邊開始，本持著天然使用。於是位於台灣山坡的魚腥草、田間的左手香以及民間常用的艾草等青草藥材等，都被研究成肥皂的材料。為了肥皂的能量與生命力，阿原肥皂嚴格排除了工業用的純水和過濾水，堅持從萬里山區接引天然湧泉來製作肥皂，唯有活水可以甚深的轉化細胞與洗滌身心。",
          rank: 4.7,
          price: 399,
          type: "異國料理、夜市小吃",
          address: "台北市大同區長安西路33號",
          author: "奧革士",
          isCollect: false,
          tags: ['新北'],
          collectCount: 237
        },
        {
          thumbnail: '../public/images/stores/3.png',
          title: "女巫店",
          descript: "位於台大商圈：公館也就是文藝青年的聚集之地，這裡有誠實坦率的獨立音樂唱片行，也有隻泡好咖啡，放好音樂的咖啡館，想讓精神放鬆，釋放自我的壓力，可以到這裡看非主流電影、主題書店、二手書舖當然，美食也是不可少的。",
          rank: 3,
          price: 399,
          type: "異國料理、夜市小吃",
          address: "台北市大同區長安西路33號",
          author: "奧革士",
          isCollect: false,
          tags: ['台北'],
          collectCount: 237
        },
        {
          thumbnail: '../public/images/stores/4.png',
          title: "朵兒咖啡館",
          descript: "一個湛藍色的大門，讓人期待起門後的世界 這個「故事」的由來，其實是一部仍未上檔的國片電影：由桂綸鎂所主演的「第36個故事」中的主要場景 原本只是在台北富錦街中一間荒廢的公寓1樓，如今在經過改裝之下，變成極富風情的咖啡館也是劇中女主角姊妹所經營的朵兒咖啡",
          rank: 3,
          price: 399,
          type: "異國料理、夜市小吃",
          address: "台北市大同區長安西路33號",
          author: "奧革士",
          isCollect: false,
          tags: ['台北'],
          collectCount: 237
        },
        {
          thumbnail: '../public/images/stores/5.png',
          title: "Déjà Vu音樂魔術主題餐廳",
          descript: "Deja Vu源自於法文“似曾相識”一意，由劉謙、周董和劉畊宏共同投資的中世紀風格主題餐廳哦！不要懷疑，雖說是名人餐廳，價格還可以接受的，而且更重要的是，每週六，有免費的表演看呢~強烈推薦，但一定要記得提前訂位。官網： http://www.deja-",
          rank: 3,
          price: 399,
          type: "異國料理、夜市小吃",
          address: "台北市大同區長安西路33號",
          author: "奧革士",
          isCollect: false,
          tags: ['必逛商店', '新北'],
          collectCount: 237
        },
        {
          thumbnail: '../public/images/stores/6.png',
          title: "袖珍博物館",
          descript: "各位縮微模型愛好者，各位miniature控，各位娃娃屋製作者，各位小清新、小文藝，民那~來台北千萬千萬千萬不要錯過一座館，不是故宮博物館啦，千萬不要錯過台北袖珍博物館！好好地表達一次：請一定要去袖珍博物館！",
          rank: 3,
          price: 399,
          type: "異國料理、夜市小吃",
          address: "台北市大同區長安西路33號",
          author: "奧革士",
          isCollect: false,
          tags: ['必逛商店', '高雄'],
          collectCount: 237
        },
        {
          thumbnail: '../public/images/stores/7.png',
          title: "美麗華百樂園",
          descript: "在台北市中山區、基隆河截彎取直的大彎段，美麗華百樂園是一間以全新概念商業設施的百貨公司，這裡最具特色的場景就是全台首創的百米摩天輪，結合娛樂、科技、藝術於一體，媲美日本的東京台場摩天輪，加之音樂旋轉木馬游樂設施更成為情侶約會的首選景點。夜間綻放光彩炫麗的燈光錶演，點綴台北的夜空。",
          rank: 3,
          price: 399,
          type: "異國料理、夜市小吃",
          address: "台北市大同區長安西路33號",
          author: "奧革士",
          isCollect: false,
          tags: ['屏東'],
          collectCount: 237
        },
        {
          thumbnail: '../public/images/stores/8.png',
          title: "京華城購物中心",
          descript: "京華城購物中心一座全新的國際級都心型觀光休閒購物中心，其獨一無二的建築設計，來自於中國傳說中的“雙龍抱珠”意念，首創“L”形主建築體，並結合全球最大球體的結構設計，直徑達58公尺的球體造型設計，基礎深達85公尺，以四根巨柱載重，為國內首創最深建築基礎結構，也成為京華城建築上的最大特色。",
          rank: 3,
          price: 399,
          type: "異國料理、夜市小吃",
          address: "台北市大同區長安西路33號",
          author: "奧革士",
          isCollect: false,
          tags: ['必逛商店', '台北'],
          collectCount: 237
        }
      ]

    ];




    $scope.purchasesSlide = {
      currentIdx: 0,
      prev: function() {
        $scope.purchasesSlide.currentIdx = $scope.purchasesSlide.currentIdx > 0 ? $scope.purchasesSlide.currentIdx - 1 : $scope.purchasesGroup.length - 1;
      },
      next: function() {
        $scope.purchasesSlide.currentIdx = $scope.purchasesSlide.currentIdx < ($scope.purchasesGroup.length - 1) ? $scope.purchasesSlide.currentIdx + 1 : 0;
      },
      set: function(idx) {
        $scope.purchasesSlide.currentIdx = idx;
      }
    };
    $scope.purchasesGroup = [
      [{
          thumbnail: '../public/images/stays/stay-1.jpg',
          title: "【大黑松小倆口】元首鳳梨酥禮盒12入(鳳梨酥系列)",
          soldCount: 248,
          rank: 3,
          commentCount: 37,
          price: 400,
          discount: 9,
          discountPrice: 352,
          label: '台灣名產',
          popular: true,
          handsfree: true
        },
        {
          thumbnail: '../public/images/stays/stay-2.jpg',
          title: "【大黑松小倆口】元首鳳梨酥禮盒12入(鳳梨酥系列)",
          rank: 3,
          commentCount: 37,
          price: 400,
          discount: 9,
          discountPrice: 352,
          label: '台灣名產',
          topChoice: true,
          handsfree: true
        },
        {
          thumbnail: '../public/images/stays/stay-3.jpg',
          title: "【春鑫食品】鳳梨酥中秋好禮6盒組(一顆15g/每盒24顆)",
          rank: 3,
          commentCount: 37,
          price: 352,
          label: '台灣名產',
          handsfree: true
        },
        {
          thumbnail: '../public/images/stays/stay-4.jpg',
          title: "【大黑松小倆口】元首鳳梨酥禮盒12入(鳳梨酥系列)",
          rank: 3,
          commentCount: 37,
          price: 352,
          label: '台灣名產'
        },
        {
          thumbnail: '../public/images/stays/stay-5.jpg',
          title: "【快車肉乾】特厚蜜汁豬肉乾",
          rank: 3,
          commentCount: 37,
          price: 352,
          label: '台灣名產',
          handsfree: true
        },
        {
          thumbnail: '../public/images/stays/stay-6.jpg',
          title: "娘家嚴選最優質滴雞精 (30包搶購組)(J1)",
          rank: 3,
          commentCount: 37,
          price: 352,
          label: '台灣名產',
          handsfree: true
        },
        {
          thumbnail: '../public/images/stays/stay-7.jpg',
          title: "【食上天】無骨德州烤雞排2包(20片/包)",
          rank: 3,
          commentCount: 37,
          price: 352,
          label: '台灣名產'
        },
        {
          thumbnail: '../public/images/stays/stay-8.jpg',
          title: "【食上天】CAS合格認證頂級蒲燒鰻魚4尾(200g/尾)",
          rank: 3,
          commentCount: 37,
          price: 352,
          label: '台灣名產'
        }
      ],
      [{
          thumbnail: '../public/images/stays/stay-1.jpg',
          title: "【大黑松小倆口】元首鳳梨酥禮盒12入(鳳梨酥系列)",
          soldCount: 248,
          rank: 3,
          commentCount: 37,
          price: 400,
          discount: 9,
          discountPrice: 352,
          label: '台灣名產',
          popular: true,
          handsfree: true
        },
        {
          thumbnail: '../public/images/stays/stay-2.jpg',
          title: "【大黑松小倆口】元首鳳梨酥禮盒12入(鳳梨酥系列)",
          rank: 3,
          commentCount: 37,
          price: 400,
          discount: 9,
          discountPrice: 352,
          label: '台灣名產',
          topChoice: true,
          handsfree: true
        },
        {
          thumbnail: '../public/images/stays/stay-3.jpg',
          title: "【春鑫食品】鳳梨酥中秋好禮6盒組(一顆15g/每盒24顆)",
          rank: 3,
          commentCount: 37,
          price: 352,
          label: '台灣名產',
          handsfree: true
        },
        {
          thumbnail: '../public/images/stays/stay-4.jpg',
          title: "【大黑松小倆口】元首鳳梨酥禮盒12入(鳳梨酥系列)",
          rank: 3,
          commentCount: 37,
          price: 352,
          label: '台灣名產'
        },
        {
          thumbnail: '../public/images/stays/stay-5.jpg',
          title: "【快車肉乾】特厚蜜汁豬肉乾",
          rank: 3,
          commentCount: 37,
          price: 352,
          label: '台灣名產',
          handsfree: true
        },
        {
          thumbnail: '../public/images/stays/stay-6.jpg',
          title: "娘家嚴選最優質滴雞精 (30包搶購組)(J1)",
          rank: 3,
          commentCount: 37,
          price: 352,
          label: '台灣名產',
          handsfree: true
        },
        {
          thumbnail: '../public/images/stays/stay-7.jpg',
          title: "【食上天】無骨德州烤雞排2包(20片/包)",
          rank: 3,
          commentCount: 37,
          price: 352,
          label: '台灣名產'
        },
        {
          thumbnail: '../public/images/stays/stay-8.jpg',
          title: "【食上天】CAS合格認證頂級蒲燒鰻魚4尾(200g/尾)",
          rank: 3,
          commentCount: 37,
          price: 352,
          label: '台灣名產'
        }
      ]
    ];

    $scope.themeSlide = {
      currentIdx: 0,
      prev: function() {
        $scope.themeSlide.currentIdx = $scope.themeSlide.currentIdx > 0 ? $scope.themeSlide.currentIdx - 1 : $scope.themeGroup.length - 1;
      },
      next: function() {
        $scope.themeSlide.currentIdx = $scope.themeSlide.currentIdx < ($scope.themeGroup.length - 1) ? $scope.themeSlide.currentIdx + 1 : 0;
      },
      set: function(idx) {
        $scope.themeSlide.currentIdx = idx;
      }
    };

    $scope.themeGroup = [
      [{
          thumbnail: '../public/images/events/1.jpg',
          title: "台南藝事",
          link: "#"

        },
        {
          thumbnail: '../public/images/events/2.jpg',
          title: "台南藝事",
          link: "#"

        },
        {
          thumbnail: '../public/images/events/3.jpg',
          title: "台南藝事",
          link: "#"

        },
        {
          thumbnail: '../public/images/events/4.jpg',
          title: "台南藝事",
          link: "#"

        }, {
          thumbnail: '../public/images/events/5.jpg',
          title: "台南藝事",
          link: "#"

        }
      ],
      [{
          thumbnail: '../public/images/events/1.jpg',
          title: "台南藝事",
          link: "#"

        },
        {
          thumbnail: '../public/images/events/2.jpg',
          title: "台南藝事",
          link: "#"

        },
        {
          thumbnail: '../public/images/events/3.jpg',
          title: "台南藝事",
          link: "#"

        },
        {
          thumbnail: '../public/images/events/4.jpg',
          title: "台南藝事",
          link: "#"

        },
        {
          thumbnail: '../public/images/events/5.jpg',
          title: "台南藝事",
          link: "#"

        }
      ]
    ];






  }
);

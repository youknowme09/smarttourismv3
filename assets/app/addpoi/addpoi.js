 "use strict";

 angular.module('ng.controller').controller('AddPoiController',
    function(userService, $mdSidenav, $mdBottomSheet, $log, $q, $scope, $mdDialog) {


        $scope.zoneitems = [ '自然生態', '宗教信仰', '歷史古蹟', '文創藝術', '離島冒險', '夜市商圈', '休閒農場', '遊憩樂活', '養生溫泉', '體育健康', '主題樂園', '博物館', '其他'];
        $scope.setZone = function($event,menuItem) {
          var n = $scope.zoneitems.indexOf(menuItem);

          if($("#zone .trip-tag").eq( n ).hasClass("select")){
              $("#zone .trip-tag").eq( n ).removeClass( "select" );

          }else{
              $("#zone .trip-tag").eq( n ).addClass( "select" );
          }


      }
      $scope.monthitems = [ '一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'];
      $scope.setMonth = function($event,menuItem) {
          var n = $scope.monthitems.indexOf(menuItem);

          if($("#month .trip-tag").eq( n ).hasClass("select")){
              $("#month .trip-tag").eq( n ).removeClass( "select" );

          }else{
              $("#month .trip-tag").eq( n ).addClass( "select" );
          }
      }


      $( ".delpic" ).each(function(index) {
        $(this).on("click", function(){
           $(this).remove();
       });
    });

      $(".pointer" ).on("click", function(){
       var dpsize = $( ".delpic" ).length;
       if(dpsize>4){
        alert("最多上傳五張照片");
    }else{
        var upelement = $('<div/>');
        upelement.addClass("imgupload delpic");
        upelement.css("background-image", "url(../public/images/spots/spot-1.jpg)");  
        $(upelement).appendTo('#uploadPic');
        $( ".delpic" ).each(function(index) {
            $(this).on("click", function(){
               $(this).remove();
           });
        });

             //TODO: click to upload image
         }
     });

      $scope.countyList = [
      {
        id: 1,
        name: '台北市'
    }, 
    {
        id: 2,
        name: '高雄市'
    }, 
    {
        id: 3,
        name: '台中市'
    }, 
    {
        id: 4,
        name: '桃園市'
    }, 
    {
        id: 5,
        name: '新竹市'
    }
    ];
    $scope.currentCounty = $scope.countyList[0].id;
    $("#addvideo").on("click", function(){
        var yvsize = $("#youtubevideos .txtinput").length
        if(yvsize>4){
            alert("最多添加5個影片");
            return;
        }
        var upelement = $('<input/>');
        upelement.addClass("txtinput");
        upelement.attr("placeholder", "http://");
        $(upelement).prependTo("#youtubevideos");
    });

    $scope.itens = [
    { title: "免費", checked: true },
    { title: "付費", checked: false },
    ];

    $scope.updateSelection = function(position, itens, title) {
        angular.forEach(itens, function(subscription, index) {
            if (position != index){
                subscription.checked = false;
            }
          
            $scope.selected = title;
            //TODO: select ticket price
        }
           );
    }
     $scope.btime= [
    { title: "有固定營業時間，我想編輯", nchecked: true },
    { title: "不對外開放", nchecked: false },
     { title: "無固定營業時間，需自行預約", nchecked: false }
    ];
      $scope.updateBussiness = function(position, btime, title) {
        angular.forEach(btime, function(subscription, index) {
            if (position != index)
                subscription.nchecked = false;
            $scope.businesstime = title;
            //TODO: select ticket price

        }
           );
    }
      $scope.weekdayitems = [ '星期一', '星期二', '星期三', '星期四', '星期五', '星期六', '星期日'];
      $scope.timeinterval = [ '09:00', '09:30', '10:00', '10:30', '11:00', '11:30', '12:00',"12:30","13:00"];
      var weekhtmlmodel = "";
    $scope.addnewday = function(menuItem) {
        if(weekhtmlmodel == ""){
      weekhtmlmodel = $(".pickmodel").eq(0).html();
  }
     $('.pickmodel').eq(menuItem).append(weekhtmlmodel);

    }

    
  
  // $( ".addday" ).each(function(index) {
  //       $(this).on("click", function(){
  //                 var weekhtmlmodel = $(".pickmodel").eq(0).outerHTML;

  //          alert("gg");
  //      });
  //   });

 



    }
    );

"use strict";

angular.module('SmartTourismV3', [
  'ng.router', 'ng.controller', 'ng.model', 'ng.service', 'ng.directive', 'ng.filter', 'perfect_scrollbar', 'ngMaterial', 'ngTagsInput', 'sticky', 'ng-sortable', 'gg.editableText', 'textAngular']);

//url slash
angular.module('SmartTourismV3').config(
  function ($httpProvider, $urlRouterProvider, $animateProvider) {
    // $animateProvider.classNameFilter(/angular-animate/);
    //$httpProvider.defaults.useXDomain = true;
    //$httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
    //delete $httpProvider.defaults.headers.common["X-Requested-With"];
    $urlRouterProvider.rule(function ($injector, $location) {
      var path = $location.url();

      // check to see if the path already has a slash where it should be
      if (path[path.length - 1] === '/' || path.indexOf('/?') > -1) {
        return;
      }

      if (path.indexOf('?') > -1) {
        return path.replace('?', '/?');
      }

      return path + '/';
    });
    $urlRouterProvider.otherwise('/');
  })
  .config(function($mdThemingProvider, $mdIconProvider){
    // This is Angular Material - Starter App
    $mdIconProvider
      .defaultIconSet("./public/images/svg/avatars.svg", 128)
      .icon("menu"       , "./public/images/svg/menu.svg"        , 24)
      .icon("share"      , "./public/images/svg/share.svg"       , 24)
      .icon("google_plus", "./public/images/svg/google_plus.svg" , 512)
      .icon("hangouts"   , "./public/images/svg/hangouts.svg"    , 512)
      .icon("twitter"    , "./public/images/svg/twitter.svg"     , 512)
      .icon("phone"      , "./public/images/svg/phone.svg"       , 512);
    $mdThemingProvider.theme('default')
      .primaryPalette('grey')
      .accentPalette('red');
  })
  .run(function (taRegisterTool, $window, taTranslations, taSelection){
    taRegisterTool('fontName', {
      display: "<span class='bar-btn-dropdown dropdown'>" +
      "<button class='btn btn-blue dropdown-toggle' type='button' ng-disabled='showHtml()' style='padding-top: 4px'><i class='fa fa-font'></i><i class='fa fa-caret-down'></i></button>" +
      "<ul class='dropdown-menu'><li ng-repeat='o in options'><button class='btn btn-blue checked-dropdown' style='font-family: {{o.css}}; width: 100%' type='button' ng-click='action($event, o.css)'><i ng-if='o.active' class='fa fa-check'></i>{{o.name}}</button></li></ul></span>",
      action: function (event, font) {
        //Ask if event is really an event.
        if (!!event.stopPropagation) {
          //With this, you stop the event of textAngular.
          event.stopPropagation();
          //Then click in the body to close the dropdown.
          $("body").trigger("click");
        }
        return this.$editor().wrapSelection('fontName', font);
      },
      options: [
        { name: 'Sans-Serif', css: 'Arial, Helvetica, sans-serif' },
        { name: 'Serif', css: "'times new roman', serif" },
        { name: 'Wide', css: "'arial black', sans-serif" },
        { name: 'Narrow', css: "'arial narrow', sans-serif" },
        { name: 'Comic Sans MS', css: "'comic sans ms', sans-serif" },
        { name: 'Courier New', css: "'courier new', monospace" },
        { name: 'Garamond', css: 'garamond, serif' },
        { name: 'Georgia', css: 'georgia, serif' },
        { name: 'Tahoma', css: 'tahoma, sans-serif' },
        { name: 'Trebuchet MS', css: "'trebuchet ms', sans-serif" },
        { name: "Helvetica", css: "'Helvetica Neue', Helvetica, Arial, sans-serif" },
        { name: 'Verdana', css: 'verdana, sans-serif' },
        { name: 'Proxima Nova', css: 'proxima_nova_rgregular' }
      ]
    });

    taRegisterTool('fontSize', {
      display: "<span class='bar-btn-dropdown dropdown'>" +
      "<button class='btn btn-blue dropdown-toggle' type='button' ng-disabled='showHtml()' style='padding-top: 4px'><i class='fa fa-text-height'></i><i class='fa fa-caret-down'></i></button>" +
      "<ul class='dropdown-menu'><li ng-repeat='o in options'><button class='btn btn-blue checked-dropdown' style='font-size: {{o.css}}; width: 100%' type='button' ng-click='action($event, o.value)'><i ng-if='o.active' class='fa fa-check'></i> {{o.name}}</button></li></ul>" +
      "</span>",
      action: function (event, size) {
        //Ask if event is really an event.
        if (!!event.stopPropagation) {
          //With this, you stop the event of textAngular.
          event.stopPropagation();
          //Then click in the body to close the dropdown.
          $("body").trigger("click");
        }
        return this.$editor().wrapSelection('fontSize', parseInt(size));
      },
      options: [
        { name: 'Muy pequeño', css: 'xx-small', value: 1 },
        { name: 'Pequeño', css: 'x-small', value: 2 },
        { name: 'Mediano', css: 'small', value: 3 },
        { name: 'Grande', css: 'medium', value: 4 },
        { name: 'Muy grande', css: 'large', value: 5 },
        { name: 'Enorme', css: 'x-large', value: 6 }
      ]
    });
    taRegisterTool('backgroundColor', {
      display: "<div spectrum-colorpicker ng-model='color' on-change='!!color && action(color)' format='\"hex\"' options='options'></div>",
      action: function (color) {
        var me = this;
        if (!this.$editor().wrapSelection) {
          setTimeout(function () {
            me.action(color);
          }, 100)
        } else {
          return this.$editor().wrapSelection('backColor', color);
        }
      },
      options: {
        replacerClassName: 'fa fa-paint-brush', showButtons: false
      },
      color: "#fff"
    });
    taRegisterTool('fontColor', {
      display: "<div spectrum-colorpicker ng-model='color' on-change='!!color && action(color)' format='\"hex\"' options='options'></div>",
      action: function (color) {
        var me = this;
        if (!this.$editor().wrapSelection) {
          setTimeout(function () {
            me.action(color);
          }, 100)
        } else {
          return this.$editor().wrapSelection('foreColor', color);
        }
      },
      options: {
        replacerClassName: 'fa fa-font', showButtons: false
      },
      color: "#000"
    });

  });

angular.element(document).ready(function () {
  var isTouchDevice = ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch;
  if( isTouchDevice ) FastClick.attach(document.body) ;
  angular.bootstrap(document, ['SmartTourismV3']);
});
